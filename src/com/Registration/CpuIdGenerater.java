/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Registration;

import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Aniket
 */
public class CpuIdGenerater {
    private final String mbSerialNumber;
    private final String uuID;
    
    public CpuIdGenerater() {
        mbSerialNumber = SerialNumbers.getMotherboardSN();
        uuID = SerialNumbers.getUUID();
    }
    
    public String getCpuId(String productName) {
        HashCodeGenerator hc = new HashCodeGenerator();
        String motherHash = mbSerialNumber.trim() + productName.trim();
        motherHash = motherHash.toLowerCase();
        motherHash = hc.pinHash(motherHash, "MD5") + hc.pinHash(motherHash, "MD2") + hc.pinHash(motherHash,"SHA1");
        
        String uuidHash = uuID.trim() + productName.trim();
        uuidHash = uuidHash.toLowerCase();
        uuidHash = hc.pinHash(uuidHash, "MD5") + hc.pinHash(uuidHash, "MD2") + hc.pinHash(uuidHash,"SHA1");
        
        motherHash = getMbSerialKey(motherHash);
        uuidHash = getUuidKey(uuidHash);
        
        String returnValue = motherHash + uuidHash;
        
        returnValue = swapChars(returnValue);
        returnValue = changeChars(returnValue);
        returnValue = returnValue.toUpperCase();
        returnValue = returnValue.substring(0, 4) + "-"
                    + returnValue.substring(4, 8) + "-"
                    + returnValue.substring(8, 12) + "-"
                    + returnValue.substring(12,
                            16);
        
        return returnValue;
    }
    
    private String getMbSerialKey(String hashValue) {
        String returnVal = ""
                + hashValue.charAt(15)
                + hashValue.charAt(59)
                + hashValue.charAt(29)
                + hashValue.charAt(75)
                + hashValue.charAt(37)
                + hashValue.charAt(8)
                + hashValue.charAt(88)
                + hashValue.charAt(64);
        return returnVal;
    }
    
    private String getUuidKey(String hashValue) {
        String returnVal = ""
                + hashValue.charAt(93)
                + hashValue.charAt(61)
                + hashValue.charAt(13)
                + hashValue.charAt(56)
                + hashValue.charAt(28)
                + hashValue.charAt(79)
                + hashValue.charAt(33)
                + hashValue.charAt(5);
        return returnVal;
    }
    
    private String swapChars(String strValue) {
        String returnString = "";
        returnString += strValue.charAt(3);
        returnString += strValue.charAt(5);
        returnString += strValue.charAt(0);
        returnString += strValue.charAt(4);
        returnString += strValue.charAt(13);
        returnString += strValue.charAt(10);
        returnString += strValue.charAt(12);
        returnString += strValue.charAt(8);
        
        returnString += strValue.charAt(11);
        returnString += strValue.charAt(14);
        returnString += strValue.charAt(9);
        returnString += strValue.charAt(15);
        returnString += strValue.charAt(7);
        returnString += strValue.charAt(1);
        returnString += strValue.charAt(6);
        returnString += strValue.charAt(2);
        
        return returnString;
    }
    
    private String changeChars(String str) {
        ArrayList<Character> chIndexList = new ArrayList<Character>();
        for(int i=0;i<10;i++)
            chIndexList.add(Character.forDigit(i, 10));
        chIndexList.add('a');chIndexList.add('b');chIndexList.add('c');
        chIndexList.add('d');chIndexList.add('e');chIndexList.add('f');
        
        ArrayList<Character> chCharList = new ArrayList<Character>();
        chCharList.add('j');chCharList.add('y'); chCharList.add('2');chCharList.add('g');
        chCharList.add('t');chCharList.add('k');chCharList.add('x');chCharList.add('1');
        chCharList.add('8'); chCharList.add('p');chCharList.add('b');chCharList.add('s');
        chCharList.add('r');chCharList.add('w');chCharList.add('9'); chCharList.add('c');
    
        String returnVal = "";
        for(int i=0;i<str.length();i++) {
            char ch = str.charAt(i);
            if(ch != '-') {
                int index = chIndexList.indexOf(ch);
                returnVal += ""+chCharList.get(index);
            } else {
                returnVal += "-";
            }
        }
        return returnVal;
    }
    
    public static void main(String[] args) {
        CpuIdGenerater cig = new CpuIdGenerater();
//        System.out.println("CpuID:"+new CpuIdGenerater().getCpuId("PrintingJEE-Chemistry"));
        JOptionPane.showMessageDialog(null, "Phy : "+cig.getCpuId("PrintingJEE-Physics")
                +"\nChe : "+cig.getCpuId("PrintingJEE-Chemistry")
                +"\nMat : "+cig.getCpuId("PrintingJEE-Mathematics")
                +"\nJEE : "+cig.getCpuId("PrintingJEE"));
        System.out.println("Phy : "+cig.getCpuId("PrintingJEE-Physics")
                +"\nChe : "+cig.getCpuId("PrintingJEE-Chemistry")
                +"\nMat : "+cig.getCpuId("PrintingJEE-Mathematics")
                +"\nJEE : "+cig.getCpuId("PrintingJEE"));
    }
}
