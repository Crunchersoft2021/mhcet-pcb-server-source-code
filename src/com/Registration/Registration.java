/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Registration;

import com.Model.ValidateCdKey;
import com.Model.ValidatePinKey;
import com.bean.CityBean;
import com.db.operations.RegistrationOperation;
import com.bean.ProductBean;
import com.bean.RegistrationBean;
import com.bean.StateBean;
import com.db.operations.CityOperation;
import com.db.operations.ProductOperation;
import com.db.operations.StateOperation;
import com.pages.HomePage;
import com.ui.support.pages.ProcessingPanel;
import com.Model.TitleInfo;
import com.db.operations.PageSetupOperation;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import org.apache.commons.lang.StringUtils;
import org.jdesktop.swingx.prompt.PromptSupport;

/**
 *
 * @author Aniket
 */
public class Registration extends javax.swing.JFrame {

    /**
     * Creates new form Registrations
     */
    
    private ArrayList<StateBean> stateList;
    private ArrayList<CityBean> cityList;
    private ArrayList<CityBean> sortCityList;
    private CardLayout cardLayout;
    private RegistrationBean registrationBean;
    private ArrayList<RegistrationBean> registrationInfoList;
    private ProductBean selectedProductBean;
    private String cpuId;
    
    public Registration() {
        initComponents();
        this.setLocationRelativeTo(null);
        String titl = new TitleInfo().getTitle();
        setTitle(titl);
        String lgo = new TitleInfo().getLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        PromptSupport.setPrompt("First Name", TxtFname);
        PromptSupport.setPrompt("Last Name", TxtLname);
        PromptSupport.setPrompt("Full Name", TxtSellerName);
        PromptSupport.setPrompt("Mobile", TxtSellerMobile);
        cardLayout = (CardLayout)MainBodyPanel.getLayout();        
        LblNote.setText("<html><FONT color=\\\"#000099\\\">Features</FONT><br />How to get registration details...? <br />Take a clean and neat photo of this window. <br />Whatsapp us the image on +91 8975626060 <br />Or Mail us at Scholars Katta@gmail.com <br />or Contact concern marketing executive. <br />Or call us on +91 8975626060/020 65404041<br />- For more visit on www.cetonlinetest.com<html>");
        ArrayList<ProductBean> allProductList = new ProductOperation().getProductList();
        selectedProductBean = allProductList.get(42); 
        cpuId = new CpuIdGenerater().getCpuId(selectedProductBean.getProductName() + " " + selectedProductBean.getProductTypeBean().getProductType());
        sortCityList = null;
        stateList = null;
        cityList = null;
        
        registrationInfoList = new RegistrationOperation().getRegistrationInfoList();
        if(registrationInfoList == null)
            registrationBean = null;
        else
            registrationBean = registrationInfoList.get(registrationInfoList.size() -1);
        if(registrationBean != null)
            validateHardWareDetails();
        
        if(registrationBean == null) {
            this.setVisible(true);
            stateList = new StateOperation().getStateList();
            cityList = new CityOperation().getCityList();
            CmbState.removeAllItems();
            CmbState.addItem("Select State");
            
            if(stateList != null) {
                for(StateBean stateBean : stateList)
                    CmbState.addItem(stateBean.getStateName().trim());
            }
            cardLayout.show(MainBodyPanel, "UserCard");
        } else if(!registrationBean.isActiveStatus()) { //Expired
            if(registrationBean.getExpiryDate()== null && registrationBean.getCdKey() == null && registrationBean.getPinKey()== null) {
                if(compareDateHalfRegitration(registrationBean.getActivationDate().trim())) { //Check 2 Days
                    this.setVisible(true);
                    cardLayout.show(MainBodyPanel, "KeyCard"); 
                    setShowText();
                } else {
                    new RegistrationOperation().deleteLastRow();
                    JOptionPane.showMessageDialog(null, "Your System Date is Invalid.\nPlease Restart the Application.");
                    System.exit(0);
                }
            } else {
                afterExpired();
            }
        } else if(registrationBean.isActiveStatus()) {
            if(registrationBean.getExpiryDate() != null) {
                int returnValue = compareDateFullRegitration(registrationBean.getActivationDate(),registrationBean.getExpiryDate());
                if(returnValue == 1) {
                    new HomePage().setVisible(true);
                } else if(returnValue == 2) {
                    afterExpired();
                } else {
//                    JOptionPane.showMessageDialog(null, "Your System Date is Invalid.\nPlease Restart the Application.");
//                    System.exit(0);
                    afterSystemDateChanged();
                }
            } else {
                afterSystemDateChanged();
            }
//                System.exit(0);
//            System.out.println("Start : " + registrationInfoBean.getActivationDateTime()+"\nEnd : "+registrationInfoBean.getExpiryDate());
        }
    }
    
    private void afterExpired() {
        Object[] options1 = {"Re-Register the software","Ok"};
        JPanel panel = new JPanel();
        panel.setBackground(new java.awt.Color(0, 20, 72));
        JLabel l1 = new JLabel("This Version of Application has Expired.\nUpdate Application Contact to Scholar's Katta Private Limited.");
        l1.setForeground(Color.WHITE);
        l1.setFont(new Font("Microsoft JhengHei", 0, 16));
        panel.add(l1);
        int result = JOptionPane.showOptionDialog(null, panel, "Warning",
        JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null, options1, null);
        if (result == JOptionPane.YES_OPTION) {
            if(getSystemDateChangedAfterExpiryStatus(registrationBean.getExpiryDate().trim())) {
                insertHalfRegistrationAfterExpiry();
                setShowText();
                this.setVisible(true);
                cardLayout.show(MainBodyPanel, "KeyCard");
            } else {
                JOptionPane.showMessageDialog(null, "Please Set Correct System Date.");
                System.exit(0);
            }
        } else if (result == JOptionPane.NO_OPTION) {
            System.exit(0);
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        MainPanel = new javax.swing.JPanel();
        LblHeader = new javax.swing.JLabel();
        MainBodyPanel = new javax.swing.JPanel();
        UserDetailsPanel = new javax.swing.JPanel();
        LblName = new javax.swing.JLabel();
        TxtFname = new javax.swing.JTextField();
        TxtLname = new javax.swing.JTextField();
        LblMobile = new javax.swing.JLabel();
        TxtMobile = new javax.swing.JTextField();
        LblMail = new javax.swing.JLabel();
        TxtEmail = new javax.swing.JTextField();
        LblAddress = new javax.swing.JLabel();
        AddressScrollPane = new javax.swing.JScrollPane();
        TxtAddress = new javax.swing.JTextArea();
        LblState = new javax.swing.JLabel();
        CmbState = new javax.swing.JComboBox<>();
        LblCity = new javax.swing.JLabel();
        CmbCity = new javax.swing.JComboBox<>();
        LblPinCode = new javax.swing.JLabel();
        TxtPinCode = new javax.swing.JTextField();
        LblSellerDetails = new javax.swing.JLabel();
        TxtSellerName = new javax.swing.JTextField();
        TxtSellerMobile = new javax.swing.JTextField();
        LblInstituteDetails = new javax.swing.JLabel();
        TxtInstituteName = new javax.swing.JTextField();
        BtnUserNext = new javax.swing.JButton();
        KeyPanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        TxtShowKeys = new javax.swing.JTextArea();
        TxtCdKey = new javax.swing.JTextField();
        LblCdKey = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        TxtCpuId = new javax.swing.JTextArea();
        LblPin = new javax.swing.JLabel();
        LblCpuId = new javax.swing.JLabel();
        TxtPinOne = new javax.swing.JTextField();
        TxtPinTwo = new javax.swing.JTextField();
        TxtPinThree = new javax.swing.JTextField();
        TxtPinFour = new javax.swing.JTextField();
        TxtPinFive = new javax.swing.JTextField();
        BtnRegister = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        BtnClear = new javax.swing.JButton();
        LblNote = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setResizable(false);
        setType(java.awt.Window.Type.UTILITY);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        MainPanel.setBackground(new java.awt.Color(57, 57, 57));

        LblHeader.setBackground(new java.awt.Color(0, 64, 80));
        LblHeader.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 36)); // NOI18N
        LblHeader.setForeground(new java.awt.Color(255, 255, 255));
        LblHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblHeader.setText("Registration");

        MainBodyPanel.setLayout(new java.awt.CardLayout());

        UserDetailsPanel.setBackground(new java.awt.Color(0, 64, 80));

        LblName.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        LblName.setForeground(new java.awt.Color(255, 255, 255));
        LblName.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblName.setText("Name");

        TxtFname.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        TxtFname.setMinimumSize(new java.awt.Dimension(181, 24));
        TxtFname.setPreferredSize(new java.awt.Dimension(181, 24));
        TxtFname.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TxtFnameKeyPressed(evt);
            }
        });

        TxtLname.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        TxtLname.setMinimumSize(new java.awt.Dimension(181, 24));
        TxtLname.setPreferredSize(new java.awt.Dimension(181, 24));
        TxtLname.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TxtLnameKeyPressed(evt);
            }
        });

        LblMobile.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        LblMobile.setForeground(new java.awt.Color(255, 255, 255));
        LblMobile.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblMobile.setText("Mobile");

        TxtMobile.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        TxtMobile.setMinimumSize(new java.awt.Dimension(181, 24));
        TxtMobile.setPreferredSize(new java.awt.Dimension(181, 24));
        TxtMobile.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TxtMobileKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                TxtMobileKeyReleased(evt);
            }
        });

        LblMail.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        LblMail.setForeground(new java.awt.Color(255, 255, 255));
        LblMail.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblMail.setText("Email");

        TxtEmail.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        TxtEmail.setPreferredSize(new java.awt.Dimension(181, 24));

        LblAddress.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        LblAddress.setForeground(new java.awt.Color(255, 255, 255));
        LblAddress.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblAddress.setText("Address");

        TxtAddress.setColumns(20);
        TxtAddress.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        TxtAddress.setRows(5);
        AddressScrollPane.setViewportView(TxtAddress);

        LblState.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        LblState.setForeground(new java.awt.Color(255, 255, 255));
        LblState.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblState.setText("State");

        CmbState.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 11)); // NOI18N
        CmbState.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select Product", "JEE Physics", "JEE Chemistry", "JEE Mathematics", "JEE PCM" }));
        CmbState.setPreferredSize(new java.awt.Dimension(181, 24));
        CmbState.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                CmbStateItemStateChanged(evt);
            }
        });

        LblCity.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        LblCity.setForeground(new java.awt.Color(255, 255, 255));
        LblCity.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblCity.setText("City");

        CmbCity.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 11)); // NOI18N
        CmbCity.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select Product", "JEE Physics", "JEE Chemistry", "JEE Mathematics", "JEE PCM" }));
        CmbCity.setPreferredSize(new java.awt.Dimension(181, 24));

        LblPinCode.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        LblPinCode.setForeground(new java.awt.Color(255, 255, 255));
        LblPinCode.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblPinCode.setText("Pin Code");

        TxtPinCode.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        TxtPinCode.setMinimumSize(new java.awt.Dimension(181, 24));
        TxtPinCode.setPreferredSize(new java.awt.Dimension(181, 24));
        TxtPinCode.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TxtPinCodeKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                TxtPinCodeKeyReleased(evt);
            }
        });

        LblSellerDetails.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        LblSellerDetails.setForeground(new java.awt.Color(255, 255, 255));
        LblSellerDetails.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblSellerDetails.setText("Seller Details");

        TxtSellerName.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        TxtSellerName.setPreferredSize(new java.awt.Dimension(181, 24));

        TxtSellerMobile.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        TxtSellerMobile.setPreferredSize(new java.awt.Dimension(181, 24));
        TxtSellerMobile.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TxtSellerMobileKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                TxtSellerMobileKeyReleased(evt);
            }
        });

        LblInstituteDetails.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        LblInstituteDetails.setForeground(new java.awt.Color(255, 255, 255));
        LblInstituteDetails.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblInstituteDetails.setText("Institute Name");

        TxtInstituteName.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        TxtInstituteName.setMinimumSize(new java.awt.Dimension(181, 24));
        TxtInstituteName.setPreferredSize(new java.awt.Dimension(181, 24));

        BtnUserNext.setBackground(new java.awt.Color(208, 87, 96));
        BtnUserNext.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        BtnUserNext.setForeground(new java.awt.Color(255, 255, 255));
        BtnUserNext.setText("Next");
        BtnUserNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnUserNextActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout UserDetailsPanelLayout = new javax.swing.GroupLayout(UserDetailsPanel);
        UserDetailsPanel.setLayout(UserDetailsPanelLayout);
        UserDetailsPanelLayout.setHorizontalGroup(
            UserDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(UserDetailsPanelLayout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(UserDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(BtnUserNext, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(UserDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(UserDetailsPanelLayout.createSequentialGroup()
                            .addComponent(TxtSellerName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(16, 16, 16)
                            .addComponent(TxtSellerMobile, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(TxtPinCode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(LblPinCode)
                        .addGroup(UserDetailsPanelLayout.createSequentialGroup()
                            .addComponent(CmbState, 0, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(CmbCity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(UserDetailsPanelLayout.createSequentialGroup()
                            .addComponent(LblState)
                            .addGap(168, 168, 168)
                            .addComponent(LblCity))
                        .addComponent(LblAddress)
                        .addComponent(LblSellerDetails)
                        .addComponent(LblInstituteDetails)
                        .addGroup(UserDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(AddressScrollPane, javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, UserDetailsPanelLayout.createSequentialGroup()
                                .addGroup(UserDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(TxtMobile, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(LblMobile))
                                .addGap(16, 16, 16)
                                .addGroup(UserDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(TxtEmail, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(LblMail)))
                            .addComponent(LblName, javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, UserDetailsPanelLayout.createSequentialGroup()
                                .addComponent(TxtFname, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(16, 16, 16)
                                .addComponent(TxtLname, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addComponent(TxtInstituteName, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(32, 32, 32))
        );
        UserDetailsPanelLayout.setVerticalGroup(
            UserDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(UserDetailsPanelLayout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(LblName)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(UserDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(TxtFname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(TxtLname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(UserDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LblMobile)
                    .addComponent(LblMail))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(UserDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(TxtMobile, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(TxtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(LblAddress)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(AddressScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(UserDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LblState)
                    .addComponent(LblCity))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(UserDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CmbState, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CmbCity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(LblPinCode)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(TxtPinCode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(LblSellerDetails)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(UserDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(TxtSellerName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(TxtSellerMobile, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(LblInstituteDetails)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(TxtInstituteName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addComponent(BtnUserNext)
                .addGap(26, 26, 26))
        );

        MainBodyPanel.add(UserDetailsPanel, "UserCard");

        KeyPanel.setBackground(new java.awt.Color(0, 64, 80));

        TxtShowKeys.setEditable(false);
        TxtShowKeys.setColumns(20);
        TxtShowKeys.setFont(new java.awt.Font("Segoe UI Semibold", 1, 16)); // NOI18N
        TxtShowKeys.setRows(5);
        jScrollPane1.setViewportView(TxtShowKeys);

        TxtCdKey.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        TxtCdKey.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                TxtCdKeyKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                TxtCdKeyKeyTyped(evt);
            }
        });

        LblCdKey.setBackground(new java.awt.Color(255, 255, 255));
        LblCdKey.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        LblCdKey.setForeground(new java.awt.Color(255, 255, 255));
        LblCdKey.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        LblCdKey.setText("CD Key ");

        jScrollPane2.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane2.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        TxtCpuId.setEditable(false);
        TxtCpuId.setColumns(20);
        TxtCpuId.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        TxtCpuId.setRows(5);
        jScrollPane2.setViewportView(TxtCpuId);

        LblPin.setBackground(new java.awt.Color(255, 255, 255));
        LblPin.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        LblPin.setForeground(new java.awt.Color(255, 255, 255));
        LblPin.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        LblPin.setText("Pin");

        LblCpuId.setBackground(new java.awt.Color(255, 255, 255));
        LblCpuId.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        LblCpuId.setForeground(new java.awt.Color(255, 255, 255));
        LblCpuId.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblCpuId.setText("CPU Id");

        TxtPinOne.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        TxtPinOne.setPreferredSize(new java.awt.Dimension(70, 24));
        TxtPinOne.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                TxtPinOneKeyReleased(evt);
            }
        });

        TxtPinTwo.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        TxtPinTwo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                TxtPinTwoKeyReleased(evt);
            }
        });

        TxtPinThree.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        TxtPinThree.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                TxtPinThreeKeyReleased(evt);
            }
        });

        TxtPinFour.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        TxtPinFour.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                TxtPinFourKeyReleased(evt);
            }
        });

        TxtPinFive.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        TxtPinFive.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                TxtPinFiveKeyReleased(evt);
            }
        });

        BtnRegister.setBackground(new java.awt.Color(208, 87, 96));
        BtnRegister.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        BtnRegister.setForeground(new java.awt.Color(255, 255, 255));
        BtnRegister.setText("Register");
        BtnRegister.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRegisterActionPerformed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(57, 57, 57));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 8, Short.MAX_VALUE)
        );

        BtnClear.setBackground(new java.awt.Color(208, 87, 96));
        BtnClear.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 13)); // NOI18N
        BtnClear.setForeground(new java.awt.Color(255, 255, 255));
        BtnClear.setText("Clear Details");
        BtnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnClearActionPerformed(evt);
            }
        });

        LblNote.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 14)); // NOI18N
        LblNote.setForeground(new java.awt.Color(255, 255, 255));
        LblNote.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/ui/icons/ActMessage.png"))); // NOI18N

        javax.swing.GroupLayout KeyPanelLayout = new javax.swing.GroupLayout(KeyPanel);
        KeyPanel.setLayout(KeyPanelLayout);
        KeyPanelLayout.setHorizontalGroup(
            KeyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(KeyPanelLayout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(KeyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(KeyPanelLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(KeyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(KeyPanelLayout.createSequentialGroup()
                                .addComponent(BtnClear)
                                .addGap(172, 172, 172)
                                .addComponent(BtnRegister, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, KeyPanelLayout.createSequentialGroup()
                                .addComponent(TxtPinOne, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(TxtPinTwo, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(TxtPinThree, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(TxtPinFour, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(TxtPinFive, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(LblCpuId, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(LblCdKey, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(LblPin, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(TxtCdKey, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING)))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 378, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(LblNote, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        KeyPanelLayout.setVerticalGroup(
            KeyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(KeyPanelLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(LblCpuId)
                .addGap(4, 4, 4)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(LblCdKey, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(TxtCdKey, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(LblPin, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(KeyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(TxtPinOne, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(TxtPinTwo)
                    .addComponent(TxtPinThree)
                    .addComponent(TxtPinFour, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(TxtPinFive, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(KeyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BtnClear)
                    .addComponent(BtnRegister))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(LblNote, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        MainBodyPanel.add(KeyPanel, "KeyCard");

        javax.swing.GroupLayout MainPanelLayout = new javax.swing.GroupLayout(MainPanel);
        MainPanel.setLayout(MainPanelLayout);
        MainPanelLayout.setHorizontalGroup(
            MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(LblHeader, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(MainBodyPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        MainPanelLayout.setVerticalGroup(
            MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MainPanelLayout.createSequentialGroup()
                .addComponent(LblHeader, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(MainBodyPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(MainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(MainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void TxtFnameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtFnameKeyPressed
        // TODO add your handling code here:
        keyTypedAlphabets(evt,TxtFname);
    }//GEN-LAST:event_TxtFnameKeyPressed

    private void TxtLnameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtLnameKeyPressed
        // TODO add your handling code here:
        keyTypedAlphabets(evt,TxtLname);
    }//GEN-LAST:event_TxtLnameKeyPressed

    private void TxtMobileKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtMobileKeyPressed
        // TODO add your handling code here:
        keyPressedNumbers(evt, TxtMobile,10);
    }//GEN-LAST:event_TxtMobileKeyPressed

    private void TxtMobileKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtMobileKeyReleased
        // TODO add your handling code here:
        keyReleasedRestrictText(TxtMobile,10);
    }//GEN-LAST:event_TxtMobileKeyReleased

    private void CmbStateItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_CmbStateItemStateChanged
        // TODO add your handling code here:
        if (evt.getStateChange() == java.awt.event.ItemEvent.SELECTED) {
            CmbCity.removeAllItems();
            CmbCity.addItem("Select City");

            if(sortCityList == null)
            sortCityList = new ArrayList<CityBean>();
            else
            sortCityList.clear();

            if(CmbState.getSelectedIndex() != 0 && CmbState.getSelectedIndex() != -1) {
                for(CityBean cityBean : cityList) {
                    if(cityBean.getStateBean().getStateId() == stateList.get(CmbState.getSelectedIndex() - 1).getStateId()) {
                        sortCityList.add(cityBean);
                        CmbCity.addItem(cityBean.getCityName());
                    }
                }
            }
        }
    }//GEN-LAST:event_CmbStateItemStateChanged

    private void TxtPinCodeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtPinCodeKeyPressed
        // TODO add your handling code here:
        keyPressedNumbers(evt, TxtPinCode,6);
    }//GEN-LAST:event_TxtPinCodeKeyPressed

    private void TxtPinCodeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtPinCodeKeyReleased
        // TODO add your handling code here:
        keyReleasedRestrictText(TxtPinCode,6);
    }//GEN-LAST:event_TxtPinCodeKeyReleased

    private void TxtSellerMobileKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtSellerMobileKeyPressed
        // TODO add your handling code here:
        keyPressedNumbers(evt, TxtSellerMobile, 10);
    }//GEN-LAST:event_TxtSellerMobileKeyPressed

    private void TxtSellerMobileKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtSellerMobileKeyReleased
        // TODO add your handling code here:
        keyReleasedRestrictText(TxtSellerMobile,10);
    }//GEN-LAST:event_TxtSellerMobileKeyReleased

    private void BtnUserNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnUserNextActionPerformed
        // TODO add your handling code here:
        if(checkUserDetails()) {
            if(!(TxtMobile.getText().length() != 10 || TxtSellerMobile.getText().length() != 10)) {
                if(TxtPinCode.getText().length() == 6) {
                    String email = TxtEmail.getText().trim();
                    if (!emailValidater(email)) {
                        JOptionPane.showMessageDialog(rootPane, "Please Enter Correct Email Id.");
                        TxtEmail.setText("");
                    } else {
                        registrationBean = new RegistrationBean();
                        registrationBean.setCustName(getCapitlise(TxtFname.getText().trim()) + " " + getCapitlise(TxtLname.getText().trim()));
                        registrationBean.setCustMobile(TxtMobile.getText().trim());
                        registrationBean.setMailId(email);
                        registrationBean.setAddress(TxtAddress.getText().trim());
                        registrationBean.setCityBean(sortCityList.get(CmbCity.getSelectedIndex()-1));
                        registrationBean.setPinCode(TxtPinCode.getText().trim());
                        registrationBean.setSellerName(getCapitlise(TxtSellerName.getText().trim()));
                        registrationBean.setSellerMobile(TxtSellerMobile.getText().trim());
                        registrationBean.setInstituteName(TxtInstituteName.getText().trim());
                        registrationBean.setActivationDate(getSystemDate(0));
                        registrationBean.setProductBean(selectedProductBean);
                        registrationBean.setCpuId(cpuId);
                        
                        new RegistrationOperation().insertRegistrationFirstPart(registrationBean);
                        cardLayout.show(MainBodyPanel, "KeyCard");
                        setShowText();
                    }
                } else {
                    JOptionPane.showMessageDialog(rootPane, "Please Enter Correct Pin Code.");
                    TxtPinCode.setText("");
                }
            } else {
                JOptionPane.showMessageDialog(rootPane, "Please Enter Correct Moblie Number.");
            }
        } else {
            JOptionPane.showMessageDialog(rootPane, "Please Fill All Fields Data.");
        }
    }//GEN-LAST:event_BtnUserNextActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_formWindowClosing

    private void TxtCdKeyKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtCdKeyKeyReleased
        // TODO add your handling code here:
        String str = TxtCdKey.getText().trim().toUpperCase();

        int length = str.length();
//        if(length == 8 || length == 15 || length == 22) {
        if(length == 6 || length == 11 || length == 16) {
            if(str.charAt(length-1) == '-')
            str = str.substring(0, str.length()-1);
        }
        TxtCdKey.setText(str);
        textRestrict(TxtCdKey,19) ;
    }//GEN-LAST:event_TxtCdKeyKeyReleased

    private void TxtCdKeyKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtCdKeyKeyTyped
        // TODO add your handling code here:
        char keyChar = evt.getKeyChar();
        int keyInt = keyChar;
        if(keyInt != KeyEvent.VK_BACK_SPACE) {
            int length = TxtCdKey.getText().length();
            if (length < 19 && length > 0) {
                if (length == 4 || length == 9 || length == 14) {
                    String temp = TxtCdKey.getText();
                    temp = temp + "-";
                    TxtCdKey.setText(temp);
                }
            }
        }
        //            TextRestrict(TxtCdKey,27) ;
    }//GEN-LAST:event_TxtCdKeyKeyTyped

    private void TxtPinOneKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtPinOneKeyReleased
        // TODO add your handling code here:
        if ((evt.isControlDown() && evt.getKeyCode() == 86)) {
            String s = TxtPinOne.getText();
            s.toUpperCase();
            s = s.trim();
            s = s.replace(" ", "");

            if (s.contains("-") && s.length() == 34) {
                String l[] = s.split("-");
                if (l.length == 5) {
                    TxtPinOne.setText(l[0]);
                    TxtPinTwo.setText(l[1]);
                    TxtPinThree.setText(l[2]);
                    TxtPinFour.setText(l[3]);
                    TxtPinFive.setText(l[4]);
                    TxtPinFive.requestFocus();
                } else {
                    JOptionPane.showMessageDialog(this, "Invalid Pin Key.\nPlease Try Again.");
                    TxtPinOne.setText("");
                    TxtPinTwo.setText("");
                    TxtPinThree.setText("");
                    TxtPinFour.setText("");
                    TxtPinFive.setText(""); 
                    TxtPinOne.requestFocus();
                }
            } else if (s.length() == 6) {
                TxtPinTwo.requestFocus();
            } else {
                JOptionPane.showMessageDialog(this, "Invalid Pin Key.\nPlease Try Again.");
                TxtPinOne.setText("");
                TxtPinTwo.setText("");
                TxtPinThree.setText("");
                TxtPinFour.setText("");
                TxtPinFive.setText("");
                TxtPinOne.requestFocus();
            }
        } else {
            textRestrict(TxtPinOne, 6);
            if(TxtPinOne.getText().trim().length() == 6) 
                TxtPinTwo.requestFocus();
        }
    }//GEN-LAST:event_TxtPinOneKeyReleased

    private void setShowText() {
        String showDetails = " Product : " + registrationBean.getProductBean().getProductName() + " "
                            + registrationBean.getProductBean().getProductTypeBean().getProductType() + "\n"
                            + " Name : " + registrationBean.getCustName() + "\n"
                            + " Mobile : " + registrationBean.getCustMobile() + "\n"
                            + " Institute : " + registrationBean.getInstituteName() +"\n"
                            + " City : " + registrationBean.getCityBean().getCityName() + "\n"
                            + " Seller Name : " + registrationBean.getSellerName() + "\n"
                            + " Seller Mobile : " + registrationBean.getSellerMobile();
                        TxtShowKeys.setText(showDetails);
                        TxtCpuId.setText(registrationBean.getCpuId());
    }
    
    private void TxtPinTwoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtPinTwoKeyReleased
        // TODO add your handling code here:
        textRestrict(TxtPinTwo, 6);
        if(TxtPinTwo.getText().trim().length() == 6) 
            TxtPinThree.requestFocus();
    }//GEN-LAST:event_TxtPinTwoKeyReleased

    private void TxtPinThreeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtPinThreeKeyReleased
        // TODO add your handling code here:
        textRestrict(TxtPinThree, 6);
        if(TxtPinThree.getText().trim().length() == 6) 
            TxtPinFour.requestFocus();
    }//GEN-LAST:event_TxtPinThreeKeyReleased

    private void TxtPinFourKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtPinFourKeyReleased
        // TODO add your handling code here:
        textRestrict(TxtPinFour, 6);
        if(TxtPinFour.getText().trim().length() == 6) 
            TxtPinFive.requestFocus();
    }//GEN-LAST:event_TxtPinFourKeyReleased

    private void TxtPinFiveKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtPinFiveKeyReleased
        // TODO add your handling code here:
        textRestrict(TxtPinFive, 6);
        if(TxtPinFive.getText().trim().length() == 6) 
            BtnRegister.requestFocus();
    }//GEN-LAST:event_TxtPinFiveKeyReleased

    private void BtnRegisterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRegisterActionPerformed
        // TODO add your handling code here:
        if(checkActivationDetails()) {
            if(TxtCdKey.getText().trim().length() != 19 && getPinKeyStatus()) {
                JOptionPane.showMessageDialog(this,"Invalid CD & Pin Key");
                TxtCdKey.setText("");
                clearPinKeys();
                TxtCdKey.requestFocus();
            } else if(TxtCdKey.getText().trim().length() != 19) {
                JOptionPane.showMessageDialog(this,"Invalid CD Key");
                TxtCdKey.setText("");
                TxtCdKey.requestFocus();
            } else if(getPinKeyStatus()) {
                JOptionPane.showMessageDialog(this,"Invalid Pin Key");
                clearPinKeys();
                TxtPinOne.requestFocus();
            } else {
                boolean isFirstTime = false;
                if(registrationInfoList == null)
                    isFirstTime = true;
                else if((registrationInfoList.size() == 1 && registrationInfoList.get(0).getExpiryDate() == null && registrationInfoList.get(0).getCdKey() == null && registrationInfoList.get(0).getPinKey() == null)) 
                    isFirstTime = true;
                registrationBean.setCdKey(TxtCdKey.getText());
                String pinKey = TxtPinOne.getText().trim() + "-" + TxtPinTwo.getText().trim() + "-"
                + TxtPinThree.getText().trim() + "-" + TxtPinFour.getText().trim() + "-"
                + TxtPinFive.getText().trim();
                registrationBean.setPinKey(pinKey.toUpperCase());
                boolean cdKeyStatus = new ValidateCdKey().validateKey(registrationBean);
                boolean pinKeyStatus = new ValidatePinKey().checkPinKey(registrationBean);

                if(cdKeyStatus && pinKeyStatus) {
                    JOptionPane.showMessageDialog(this,"Congratulations!!!\nYou have registered successfully.");
                    new RegistrationOperation().insertRegistrationSecondPart(registrationBean);
                    if(registrationInfoList != null) {
                        new RegistrationOperation().deleteUnWantedRow();
                    }
                    if(isFirstTime) {
                        this.setEnabled(false);
                        new ProcessingPanel(this).setVisible(true);
                        writePathInDirectory();
                        new PageSetupOperation().deletePageSetupInfo();
                    }

                    new HomePage().setVisible(true);
                    this.dispose();
                } else if(!cdKeyStatus && !pinKeyStatus) {
                    JOptionPane.showMessageDialog(this,"Invalid CD & Pin Key");
                    TxtCdKey.setText("");
                    clearPinKeys();
                    TxtCdKey.requestFocus();
                } else if(!cdKeyStatus) {
                    JOptionPane.showMessageDialog(this,"Invalid CD Key");
                    TxtCdKey.setText("");
                    TxtCdKey.requestFocus();
                } else if(!pinKeyStatus) {
                    JOptionPane.showMessageDialog(this,"Invalid Pin Key");
                    clearPinKeys();
                    TxtPinOne.requestFocus();
                }
            }
        } else {
            JOptionPane.showMessageDialog(this, "Please Enter All Fields Data.");
        }
    }//GEN-LAST:event_BtnRegisterActionPerformed

    private void BtnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnClearActionPerformed
        // TODO add your handling code here:
        Object[] options = {"YES", "CANCEL"};
        int i = JOptionPane.showOptionDialog(rootPane, "Are You Sure to Clear Deatils?", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
        if (i == 0) {
            new RegistrationOperation().deleteLastRow();
            JOptionPane.showMessageDialog(rootPane, "Restart the Application.");
            System.exit(0);
        }
    }//GEN-LAST:event_BtnClearActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        new Registration();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane AddressScrollPane;
    private javax.swing.JButton BtnClear;
    private javax.swing.JButton BtnRegister;
    private javax.swing.JButton BtnUserNext;
    private javax.swing.JComboBox<String> CmbCity;
    private javax.swing.JComboBox<String> CmbState;
    private javax.swing.JPanel KeyPanel;
    private javax.swing.JLabel LblAddress;
    private javax.swing.JLabel LblCdKey;
    private javax.swing.JLabel LblCity;
    private javax.swing.JLabel LblCpuId;
    private javax.swing.JLabel LblHeader;
    private javax.swing.JLabel LblInstituteDetails;
    private javax.swing.JLabel LblMail;
    private javax.swing.JLabel LblMobile;
    private javax.swing.JLabel LblName;
    private javax.swing.JLabel LblNote;
    private javax.swing.JLabel LblPin;
    private javax.swing.JLabel LblPinCode;
    private javax.swing.JLabel LblSellerDetails;
    private javax.swing.JLabel LblState;
    private javax.swing.JPanel MainBodyPanel;
    private javax.swing.JPanel MainPanel;
    private javax.swing.JTextArea TxtAddress;
    private javax.swing.JTextField TxtCdKey;
    private javax.swing.JTextArea TxtCpuId;
    private javax.swing.JTextField TxtEmail;
    private javax.swing.JTextField TxtFname;
    private javax.swing.JTextField TxtInstituteName;
    private javax.swing.JTextField TxtLname;
    private javax.swing.JTextField TxtMobile;
    private javax.swing.JTextField TxtPinCode;
    private javax.swing.JTextField TxtPinFive;
    private javax.swing.JTextField TxtPinFour;
    private javax.swing.JTextField TxtPinOne;
    private javax.swing.JTextField TxtPinThree;
    private javax.swing.JTextField TxtPinTwo;
    private javax.swing.JTextField TxtSellerMobile;
    private javax.swing.JTextField TxtSellerName;
    private javax.swing.JTextArea TxtShowKeys;
    private javax.swing.JPanel UserDetailsPanel;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    // End of variables declaration//GEN-END:variables
    
    private void keyTypedAlphabets(KeyEvent evt,JTextField text) {
        char c = evt.getKeyChar();
        if(Character.isLetter(c) || Character.isISOControl(c)) {
            text.setEditable(true);
        } else if(evt.getKeyCode() == 8 || evt.getKeyCode() == 9 || evt.getKeyCode() == 13 || evt.getKeyCode() == 16 || evt.getKeyCode() == 17 || evt.getKeyCode() == 20 || evt.getKeyCode() == 27 || evt.getKeyCode() == 37 || evt.getKeyCode() == 38 || evt.getKeyCode() == 39 || evt.getKeyCode() == 40 || evt.getKeyCode() == 32 || evt.getKeyCode() == 45 || evt.getKeyCode() == 46) {
            text.setEditable(true);
        } else {
            text.setEditable(false);
            evt.consume();
            JOptionPane.showMessageDialog(this,"Only letter allowed here");
            text.setEditable(true);
        }
    }
    
    private void keyReleasedRestrictText(JTextField text,int length) {
        String str = text.getText();
        if(str.length() >= length) {
            str = str.substring(0, length);
            text.setText(str);
        }
    }
    
    private void keyPressedNumbers(KeyEvent evt, JTextField text,int length) {
        if((evt.getKeyCode() == KeyEvent.VK_A || evt.getKeyCode() == KeyEvent.VK_C || evt.getKeyCode() == KeyEvent.VK_V || evt.getKeyCode() == KeyEvent.VK_X) && ((evt.getModifiers() & KeyEvent.CTRL_MASK) != 0)) {
        } else {
            String str = text.getText();
            if(str.length() < length) {
                if ((evt.getKeyChar() >= '0' && evt.getKeyChar() <= '9') || evt.getKeyCode() == 8 || evt.getKeyCode() == 9 || evt.getKeyCode() == 13 || evt.getKeyCode() == 16 || evt.getKeyCode() == 17 || evt.getKeyCode() == 20 || evt.getKeyCode() == 27 || evt.getKeyCode() == 37 || evt.getKeyCode() == 38 || evt.getKeyCode() == 39 || evt.getKeyCode() == 40 || evt.getKeyCode() == 46) {
                    text.setEditable(true);
                } else {
                    text.setEditable(false);
                    JOptionPane.showMessageDialog(this, "Please Enter Numbers Only.");
                    text.setEditable(true);
                }
            } else if(!(evt.getKeyCode() == 8 || evt.getKeyCode() == 9 || evt.getKeyCode() == 13 || evt.getKeyCode() == 16 || evt.getKeyCode() == 17 || evt.getKeyCode() == 20 || evt.getKeyCode() == 27 || evt.getKeyCode() == 37 || evt.getKeyCode() == 38 || evt.getKeyCode() == 39 || evt.getKeyCode() == 40 || evt.getKeyCode() == 46)){
                text.setEditable(false);
                JOptionPane.showMessageDialog(this, "Please Enter Only "+length+" Numbers.");
                text.setEditable(true);
            } else {
                text.setEditable(true);
            }
        }
    }
    
    private boolean emailValidater(String email) {
        Pattern pattern;
        Matcher matcher;
        String EMAIL_PATTERN =
		"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
		+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
	matcher = pattern.matcher(email);
        return matcher.matches();
    }
    
    private boolean checkUserDetails(){
        if(TxtFname.getText().trim().isEmpty() || TxtLname.getText().trim().isEmpty() ||
            TxtMobile.getText().trim().isEmpty() || TxtEmail.getText().trim().isEmpty() ||
            CmbCity.getSelectedIndex() < 1 || TxtSellerName.getText().trim().isEmpty() || 
            TxtSellerMobile.getText().trim().isEmpty() || TxtInstituteName.getText().trim().isEmpty() ||
            TxtAddress.getText().trim().isEmpty() || TxtPinCode.getText().trim().isEmpty())
            return false;
        else
            return true;
    }
    
    private String getCapitlise(String string) {
        String returnString = string.trim();
        returnString = returnString.toLowerCase();
        returnString = returnString.replaceAll("  ", " ");
        returnString = returnString.replaceAll("   ", " ");
        String[] strArr = returnString.split(" ");
        returnString = "";
        for(String st : strArr)
            returnString += StringUtils.capitalize(st) +" ";
        returnString = returnString.trim();
        return returnString;
    }
    
    private String getSystemDate(int count) {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, count);
        String returnDate = dateFormat.format(cal.getTimeInMillis());
        return returnDate;
    }
    
    private void validateHardWareDetails() {
        if(!cpuId.equalsIgnoreCase(registrationBean.getCpuId())) {
            new RegistrationOperation().deleteAllRow();
            registrationBean = null;
            registrationInfoList = null;
        }
    }
    
    private boolean compareDateHalfRegitration(String startDate) {
        boolean returnValue = true;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy");
            startDate = startDate.substring(0, 8);
            Date stDate = sdf.parse(startDate);
            Date cdDate = sdf.parse(getSystemDate(0));
            Date nxtDate = sdf.parse(getSystemDate(1));

            if(!cdDate.equals(stDate) && !nxtDate.equals(stDate)) {
                returnValue = false;
            } 
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return returnValue;
    }
    
    private void textRestrict(JTextField txtBox, int range) {
        String input = txtBox.getText();
        input = input.trim();
        input = input.toUpperCase();
        int length = input.length();
        if(length > range)
            input = input.substring(0, range);
        txtBox.setText(input);
    }
    
    private void writePathInDirectory() {
        BufferedWriter bw = null;
        try {
            File fl = new File("CurrentPattern.txt");
            if(!fl.exists())
                fl.createNewFile();
            bw = new BufferedWriter(new FileWriter("CurrentPattern.txt"));
            bw.write(""+0);
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (bw != null)
                    bw.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    private void clearPinKeys() {
        TxtPinOne.setText("");
        TxtPinTwo.setText("");
        TxtPinThree.setText("");
        TxtPinFour.setText("");
        TxtPinFive.setText("");
    }
    
    private boolean getSystemDateChangedAfterExpiryStatus(String expiryDate) {
        boolean returnValue = false;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy");
            Date stDate = sdf.parse(expiryDate);
            Date cdDate = sdf.parse(getSystemDate(0));

            if(cdDate.after(stDate)) {
                returnValue = true;
            } 
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return returnValue;
    }
    
    private void insertHalfRegistrationAfterExpiry() {
        registrationBean.setCpuId(cpuId);
        registrationBean.setActivationDate(getSystemDate(0));
        new RegistrationOperation().insertRegistrationFirstPart(registrationBean);
    }
    
    private int compareDateFullRegitration(String startDate,String endDate) {
        int returnValue = 0;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy");
            startDate = startDate.substring(0, 8);
            Date stDate = sdf.parse(startDate);
            Date edDate = sdf.parse(endDate);
            Date cdDate = sdf.parse(getSystemDate(0));
            Date ydDate = sdf.parse(getSystemDate(-1));

            if(cdDate.equals(stDate) || cdDate.equals(ydDate) || stDate.before(cdDate)) { 
                if(cdDate.before(edDate) || cdDate.equals(edDate)) {
                    System.out.println("Your Product is Active");
                    returnValue = 1;
                } else {
                    System.out.println("Your Product Is Expired");
//                    JOptionPane.showMessageDialog(null,"Your Product Is Expired.\nPlease Contact Evision Private Limited.");
                    new RegistrationOperation().updateExpiryStatus(registrationBean);
                    returnValue = 2;
                }
            } else if(!cdDate.equals(stDate) && !cdDate.equals(ydDate) && !cdDate.after(stDate)) {
                System.out.println("Invalid Date");
                new RegistrationOperation().updateExpiry(registrationBean);
                returnValue = 3;
            }
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return returnValue;
    }
    
    private void afterSystemDateChanged() {
        Object[] options1 = {"Re-Register the software","Ok"};
        JPanel panel = new JPanel();
        panel.setBackground(new java.awt.Color(0, 20, 72));
        JLabel l1 = new JLabel("This Version of Application has Temporary Locked.\nUnlock Application Contact to Scholar's Katta Private Limited.");
        l1.setForeground(Color.WHITE);
        l1.setFont(new Font("Microsoft JhengHei", 0, 16));
        panel.add(l1);
        int result = JOptionPane.showOptionDialog(null, panel, "Warning",
        JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null, options1, null);
        if (result == JOptionPane.YES_OPTION) {
            if(getSystemDateChangedStatus(registrationBean.getActivationDate().trim())) {
                insertHalfRegistrationAfterExpiry();
                this.setVisible(true);
                cardLayout.show(MainBodyPanel, "KeyCard");
            } else {
                JOptionPane.showMessageDialog(null, "Please Set Correct System Date.");
                System.exit(0);
            }
        } else if (result == JOptionPane.NO_OPTION) {
            System.exit(0);
        }
    }
    
    private boolean getSystemDateChangedStatus(String activationDate) {
        boolean returnValue = false;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy");
            activationDate = activationDate.substring(0, 8);
            Date stDate = sdf.parse(activationDate);
            Date cdDate = sdf.parse(getSystemDate(0));

            if(cdDate.equals(stDate) || cdDate.after(stDate)) {
                returnValue = true;
            }
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return returnValue;
    }
    
    private boolean checkActivationDetails() {
        if(TxtCdKey.getText().isEmpty() || TxtPinOne.getText().isEmpty() ||
            TxtPinTwo.getText().isEmpty() || TxtPinThree.getText().isEmpty() ||
            TxtPinFour.getText().isEmpty() || TxtPinFive.getText().isEmpty())
            return false;
        else
            return true;
    }
    
    private boolean getPinKeyStatus() {
        if(TxtPinOne.getText().trim().length() != 6 || TxtPinTwo.getText().trim().length() != 6 ||
            TxtPinThree.getText().trim().length() != 6 || TxtPinFour.getText().trim().length() != 6 ||
            TxtPinFive.getText().trim().length() != 6) {
            return true;
        }
        return false;
    }
}
