/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.LatexProcessing.pdf;

/**
 *
 * @author admin
 */
public class NewQuesLatexProcessing {
    
    public String mboxProcessing(String str,boolean isQuestion){
        String returnString = str;
        //Newly Added
        returnString=  returnString.replace("–", "-");
        returnString=  returnString.replace("…...", "...");
        returnString=  returnString.replace("…", "...");
        returnString = returnString.replace("\\hspace{15pt}", "");
        
        returnString = returnString.replace("\\textit", "tteextitt"); 
        returnString = returnString.replace("\\textbf", "tteextbff");        
        
        returnString = returnString.replace("\\text", "\\mbox");
        
        returnString = returnString.replace("tteextitt", "\\textit"); 
        returnString = returnString.replace("tteextbff", "\\textbf");
        
        returnString = returnString.replace("  ", " ");//1
        returnString = returnString.replace("  ", " ");//2
        returnString = returnString.replace("} \\\\ \\mbox{", "}\\\\\\mbox{");//3
        if(isQuestion)
            returnString = returnString.replace("}\\\\ \\mbox{","}\\\\\\mbox{");//4
        else
            returnString = returnString.replace("\\_", "_");
        returnString = returnString.replace("}\\\\ \\mbox{","}\\\\\\mbox{");//5
        returnString = returnString.replace("} \\\\\\mbox{", "}\\\\\\mbox{");//6
        returnString = returnString.replace("}  \\\\ \\mbox{", "}\\\\\\mbox{");//7
        returnString = returnString.replace("}   \\\\ \\mbox{", "}\\\\\\mbox{");//8
        returnString = returnString.replace("} \\\\  \\mbox{", "}\\\\\\mbox{");//9
        returnString = returnString.replace("\\_", "_");//10
        
        returnString = removeMbox(returnString);
        returnString = removeRm(returnString);
        
        return returnString;
    }
    
    public String removeMbox(String str) {
//        Quest.stringcheck.NewClass ob = new Quest.stringcheck.NewClass();
//        str=ob.Managequotes(str);Managequotes
        str = str.replace("\\mbox {", "\\mbox{");
        str = removeDoubleSlash(str);
        str = "  "+str+"  ";
        char [] carray = str.toCharArray();
        char [] tarray = new char[str.length()+7] ;
        int clen = str.length();
        int k=0,j=0;int flag1=0;
        for(int i=0;i<clen;)
        {
            if(carray[i]=='\\'&&carray[i+1]=='m'&&carray[i+2]=='b'&&carray[i+3]=='o'&&carray[i+4]=='x'&&carray[i+5]=='{')
            {
                if(flag1==1&&carray[i+6]!=' ')
                {
                    tarray[j]=' ';j++;
                }
                i+=6;
                k=newSingleclosingBrackPosition(str, i-6);
                for(;i<k&&i<clen;)
                {
                    tarray[j]=carray[i];i++;j++;
                }
                i++;
                flag1=1;
            }
            else
            {
                tarray[j]=carray[i];i++;j++;
            }
        }
        String s1=new String(tarray);
        s1=s1.trim();
//        System.out.println(s1);
        return s1;
    }
    
    public String removeRm(String str) {
//        Quest.stringcheck.NewClass ob = new Quest.stringcheck.NewClass();
//        str=ob.Managequotes(str);
//        str=removeDoubleSlash(str);
        str="  "+str+"  ";
        char [] carray = str.toCharArray();
        char [] tarray = new char[str.length()+7] ;
        int clen = str.length();
        int k=0,j=0;int flag1=0;
        for(int i=0;i<clen;)
        {
            if(carray[i]=='\\'&&carray[i+1]=='r'&&carray[i+2]=='m'&&carray[i+3]=='{')
            {
                if(flag1==1&&carray[i+3]!=' ')
                {
                    tarray[j]=' ';j++;
                }
                i+=4;
                k=newSingleclosingBrackPosition(str, i-4);
                for(;i<k&&i<clen;)
                {
                    tarray[j]=carray[i];i++;j++;
                }
                i++;
                flag1=1;
            }
            else
            {
                tarray[j]=carray[i];i++;j++;
            }
        }
        String s1=new String(tarray);
        s1=s1.trim();
        return s1;
    }
    
    public String removeDoubleSlash(String str) {
        char [] carray = str.toCharArray();
        char [] tarray = new char[str.length()+7] ;
        int clen = str.length();
        int j=0;
        for(int i=0;i<clen;)
        {
            
            if(carray[i]=='\\'&&carray[i+1]=='\\')
            {
                int beginIndex=str.indexOf("\\begin{", i);
                int endIndex=str.indexOf("\\end{", i);
                
                if(endIndex==-1)
                {
                    i+=2;                                    
                }
                else
                {
                    if(beginIndex==-1||beginIndex>endIndex)
                    {
                        tarray[j]=carray[i];i++;j++;
                        tarray[j]=carray[i];i++;j++;                        
                    }
                    else
                    {
                        i+=2;
                    }
                }
            }
            else
            {
                tarray[j]=carray[i];
                i++;
                j++;
            }
        }
        
        String s1=new String(tarray);
        s1=s1.trim();
        return s1;
    }
    
    public int newSingleclosingBrackPosition(String str,int position) {
       char [] carray = str.toCharArray();
       int clen=carray.length;
       int count=0;
       int i=position;
       for(;carray[i]!='{';)
       {
           i++;
       } 
       for(;i<clen;i++)
       {
           if(carray[i]=='{'&&carray[i-1]!='\\')
           {
               count+=1;
           }
           if(carray[i]=='}'&&carray[i-1]!='\\')
           {
               count-=1;
           }
           if(count==0)
           {
               return i;
           }
       }
       return 0;
    }
}