package com.pages;

import com.LatexProcessing.ErrorManager;
import com.LatexProcessing.MboxSpaceRemoving;
import com.Model.ApplicationManager;
import com.Model.ProcessManager;
import java.awt.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Color;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import org.scilab.forge.jlatexmath.TeXConstants;
import org.scilab.forge.jlatexmath.TeXFormula;
import org.scilab.forge.jlatexmath.TeXIcon;
import javax.imageio.ImageIO;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.Document;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;
import com.bean.ChapterBean;
import com.bean.ImageRatioBean;
import com.bean.QuestionBean;
import com.bean.SubjectBean;
import com.db.operations.ImageRatioOperation;
import com.db.operations.OptionImageDimentionOperation;
import com.db.operations.QuestionOperation;
import com.db.operations.SubjectOperation;
import com.id.operations.NewIdOperation;
import com.ui.support.pages.ChangeClipBoardData;
import com.ui.support.pages.ImagePanel1;
import com.ui.support.pages.PreviewImage;
import com.Model.TitleInfo;
import com.bean.GroupBean;
import com.bean.PatternOptionImageDimensionsBean;
import com.bean.SubMasterChapterBean;
import com.db.operations.MasterGroupOperation;
import com.db.operations.MasterImageRationOperation;
import com.db.operations.MasterOptionImageDimentionOperation;
import com.db.operations.MasterQuestionOperation;
import com.db.operations.SubMasterChapterOperation;
import java.io.BufferedReader;
import java.io.FileReader;

public class FinalQuestionEntry extends javax.swing.JFrame {

    private String questionText,optionAText,optionBText,optionCText, optionDText,hintText;
    private String start,end,first,last,newline;
    private BufferedImage bufferImgQuestion, bufferImgOption, bufferImgHint;
    private TeXFormula formula;
    private float questionRatio,optionRatio,hintRatio;
    private int questionId;
    private UndoManager undoManager;
    private ChapterBean chapterBean;
    private QuestionBean questionsBean;
    private String textFormulaError;
    private String processPath;
    private ArrayList<SubMasterChapterBean> subMasterChapterList;
    private int masterQuestionId;
    ArrayList<GroupBean> groupList;
    

    public FinalQuestionEntry(ChapterBean chapterBean){
        initComponents();
        this.chapterBean = chapterBean;
        questionId = 0;
        masterQuestionId = 0;
        subMasterChapterList = null;
        groupList = null;
        String titl = new TitleInfo().getTitle();
        setTitle(titl);
        String lgo = new TitleInfo().getLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        this.setExtendedState(Frame.MAXIMIZED_BOTH);
        processPath = new ProcessManager().getProcessPath();
        initComp();
        resetValues();
    }
    
    private void initComp() {
        start = "\\begin{array}{l}";
        end = "\\end{array}";
        first = "\\mbox{";
        last = "}";
        newline = "} \\\\\\\\ \\\\mbox{";
        SubjectBean subjectBean = new SubjectOperation().getSubjectBean(chapterBean.getSubjectId());
        lblSubject.setText("Subject: "+subjectBean.getSubjectName());
        lblChap.setText("Chapter: " + chapterBean.getChapterName());
        RdoA.setActionCommand("A");
        RdoB.setActionCommand("B");
        RdoC.setActionCommand("C");
        RdoD.setActionCommand("D");
        buttonGroup1.add(RdoA);
        buttonGroup1.add(RdoB);
        buttonGroup1.add(RdoC);
        buttonGroup1.add(RdoD);
        jScrollPane5.setPreferredSize(jScrollPane5.getSize());
        undoManager = new UndoManager();
        setUndo(TxtQuestion);
        setUndo(TxtOptionA);
        setUndo(TxtOptionB);
        setUndo(TxtOptionC);
        setUndo(TxtOptionD);
        setUndo(TxtHint);
    }

    private void resetValues() {
        TxtQuestion.setText("");
        TxtOptionA.setText("");
        TxtOptionB.setText("");
        TxtOptionC.setText("");
        TxtOptionD.setText("");
        TxtYear.setText("");
        TxtHint.setText("");
        
        ShowLblQuestion2.setIcon(null);
        ShowLblOptionA2.setIcon(null);
        ShowLblOptionB2.setIcon(null);
        ShowLblOptionC2.setIcon(null);
        ShowLblOptionD2.setIcon(null);
        ShowLblHint2.setIcon(null);
        ShowLblYear2.setText("");
        
        CmbLevel.setSelectedIndex(0);
        CmbType.setSelectedIndex(0);
        BtnAdd.setEnabled(false);
        buttonGroup1.clearSelection();
        
        ChkQuestion.setSelected(false);
        LblQuestionPreview.setVisible(false);
        PanelQuetionImage.removeAll();
        bufferImgQuestion = null;
        BtnQuestionPaste.setEnabled(false);
        setQuestionRatio(1.0f);
        
        ChkOption.setSelected(false);
        LblOptionPreview.setVisible(false);
        bufferImgOption = null;
        PanelOptionImage.removeAll();
        BtnOptionPaste.setEnabled(false);
        setOptionRatio(1.0f);
        
        ChkHint.setSelected(false);
        LblHintPreview.setVisible(false);
        PanelHintImage.removeAll();
        bufferImgHint = null;
        BtnHintPaste.setEnabled(false);
        setHintRatio(1.0f);
        
        LblQuestion.setForeground(Color.RED);
        LblYear.setForeground(Color.RED);
        LblOptA.setForeground(Color.RED);
        LblOptB.setForeground(Color.RED);
        LblOptC.setForeground(Color.RED);
        LblOptD.setForeground(Color.RED);
        LblHint.setForeground(Color.RED);
        
        questionText = "";
        optionAText = "";
        optionBText = "";
        optionCText = "";
        optionDText = "";
        hintText = "";
        textFormulaError = "";
        this.repaint();
    }
    
    /**
     * setup the page display and changer panel and return it
     */
    void setUndo(JTextArea textArea) {
        Document doc = textArea.getDocument();
        doc.addUndoableEditListener(new UndoableEditListener() {
            @Override
            public void undoableEditHappened(UndoableEditEvent e) {
                undoManager.addEdit(e.getEdit());
            }
        });

        InputMap im = textArea.getInputMap(JComponent.WHEN_FOCUSED);
        ActionMap am = textArea.getActionMap();
        im.put(KeyStroke.getKeyStroke(KeyEvent.VK_Z, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()), "Undo");
        im.put(KeyStroke.getKeyStroke(KeyEvent.VK_Y, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()), "Redo");
        am.put("Undo", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    if (undoManager.canUndo()) {
                        undoManager.undo();
                    }
                } catch (CannotUndoException exp) {
                    exp.printStackTrace();
                }
            }
        });
        am.put("Redo", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    if (undoManager.canRedo()) {
                        undoManager.redo();
                    }
                } catch (CannotUndoException exp) {
                    exp.printStackTrace();
                }
            }
        });
    }

    
    
    private void stateChange() {
        if(CmbLevel.getSelectedIndex() == 0)
            lblLev.setForeground(Color.RED);
        else
            lblLev.setForeground(Color.BLACK);
        
        if(CmbType.getSelectedIndex() == 0) 
            LblType.setForeground(Color.RED);
        else
            LblType.setForeground(Color.BLACK);
        
        if(RdoA.isSelected() || RdoB.isSelected() || RdoC.isSelected() || RdoD.isSelected())
            LblAns.setForeground(Color.BLACK);
        else
            LblAns.setForeground(Color.RED);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jScrollPane2 = new javax.swing.JScrollPane();
        jPanel5 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        lblChap = new javax.swing.JLabel();
        lblSubject = new javax.swing.JLabel();
        BtnReConfigure = new javax.swing.JButton();
        BtnHome = new javax.swing.JButton();
        jPanel9 = new javax.swing.JPanel();
        LblQuestion = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        TxtQuestion = new javax.swing.JTextArea();
        LblOptA = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        TxtOptionA = new javax.swing.JTextArea();
        LblOptB = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        TxtOptionB = new javax.swing.JTextArea();
        jScrollPane7 = new javax.swing.JScrollPane();
        TxtOptionC = new javax.swing.JTextArea();
        jScrollPane8 = new javax.swing.JScrollPane();
        TxtOptionD = new javax.swing.JTextArea();
        LblOptC = new javax.swing.JLabel();
        LblOptD = new javax.swing.JLabel();
        jScrollPane9 = new javax.swing.JScrollPane();
        TxtHint = new javax.swing.JTextArea();
        LblHint = new javax.swing.JLabel();
        LblAns = new javax.swing.JLabel();
        RdoC = new javax.swing.JRadioButton();
        RdoD = new javax.swing.JRadioButton();
        RdoA = new javax.swing.JRadioButton();
        RdoB = new javax.swing.JRadioButton();
        lblLev = new javax.swing.JLabel();
        BtnAdd = new javax.swing.JButton();
        BtnFormula = new javax.swing.JButton();
        CmbLevel = new javax.swing.JComboBox();
        LblType = new javax.swing.JLabel();
        CmbType = new javax.swing.JComboBox();
        LblYear = new javax.swing.JLabel();
        TxtYear = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        BtnShowQuestion = new javax.swing.JButton();
        BtnSplit = new javax.swing.JButton();
        BtnClear = new javax.swing.JButton();
        BtnShortcuts = new javax.swing.JButton();
        BtnMathType = new javax.swing.JButton();
        jScrollPane5 = new javax.swing.JScrollPane();
        jPanel2 = new javax.swing.JPanel();
        ShowLblQuestion1 = new javax.swing.JLabel();
        ShowLblQuestion2 = new javax.swing.JLabel();
        ShowLblOptionA2 = new javax.swing.JLabel();
        ShowLblHint2 = new javax.swing.JLabel();
        ShowLblHint1 = new javax.swing.JLabel();
        ShowLblOptionA1 = new javax.swing.JLabel();
        ShowLblYear2 = new javax.swing.JLabel();
        ShowLblYear1 = new javax.swing.JLabel();
        ShowLblOptionB2 = new javax.swing.JLabel();
        ShowLblOptionB1 = new javax.swing.JLabel();
        ShowLblOptionC1 = new javax.swing.JLabel();
        ShowLblOptionC2 = new javax.swing.JLabel();
        ShowLblOptionD1 = new javax.swing.JLabel();
        ShowLblOptionD2 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        ChkQuestion = new javax.swing.JCheckBox();
        BtnQuestionPaste = new javax.swing.JButton();
        LblQuestionPreview = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        PanelQuetionImage = new javax.swing.JPanel();
        jScrollPane11 = new javax.swing.JScrollPane();
        PanelOptionImage = new javax.swing.JPanel();
        LblOptionPreview = new javax.swing.JLabel();
        BtnOptionPaste = new javax.swing.JButton();
        ChkOption = new javax.swing.JCheckBox();
        jScrollPane12 = new javax.swing.JScrollPane();
        PanelHintImage = new javax.swing.JPanel();
        LblHintPreview = new javax.swing.JLabel();
        BtnHintPaste = new javax.swing.JButton();
        ChkHint = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                formKeyReleased(evt);
            }
        });

        jPanel5.setBackground(new java.awt.Color(255, 153, 153));

        jPanel8.setBackground(new java.awt.Color(0, 0, 0));

        lblChap.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblChap.setForeground(new java.awt.Color(255, 255, 255));

        lblSubject.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblSubject.setForeground(new java.awt.Color(255, 255, 255));

        BtnReConfigure.setText("Re-Configure"); // NOI18N
        BtnReConfigure.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnReConfigureActionPerformed(evt);
            }
        });

        BtnHome.setText("HOME");
        BtnHome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHomeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblSubject, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(lblChap, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(163, 163, 163)
                .addComponent(BtnReConfigure)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(BtnHome, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblChap, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(BtnHome, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(BtnReConfigure, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(lblSubject, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jPanel9.setBackground(new java.awt.Color(153, 153, 153));

        LblQuestion.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        LblQuestion.setForeground(new java.awt.Color(255, 51, 0));
        LblQuestion.setText("Question :"); // NOI18N

        TxtQuestion.setColumns(20);
        TxtQuestion.setRows(5);
        TxtQuestion.setNextFocusableComponent(TxtOptionA);
        TxtQuestion.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                TxtQuestionFocusGained(evt);
            }
        });
        TxtQuestion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                TxtQuestionKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(TxtQuestion);

        LblOptA.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        LblOptA.setForeground(new java.awt.Color(255, 51, 0));
        LblOptA.setText("Option A :"); // NOI18N

        TxtOptionA.setColumns(20);
        TxtOptionA.setRows(5);
        TxtOptionA.setNextFocusableComponent(TxtOptionB);
        TxtOptionA.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                TxtOptionAFocusGained(evt);
            }
        });
        TxtOptionA.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                TxtOptionAKeyReleased(evt);
            }
        });
        jScrollPane3.setViewportView(TxtOptionA);

        LblOptB.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        LblOptB.setForeground(new java.awt.Color(255, 51, 0));
        LblOptB.setText("Option B :"); // NOI18N

        TxtOptionB.setColumns(20);
        TxtOptionB.setRows(5);
        TxtOptionB.setNextFocusableComponent(TxtOptionC);
        TxtOptionB.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                TxtOptionBFocusGained(evt);
            }
        });
        TxtOptionB.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                TxtOptionBKeyReleased(evt);
            }
        });
        jScrollPane4.setViewportView(TxtOptionB);

        TxtOptionC.setColumns(20);
        TxtOptionC.setRows(5);
        TxtOptionC.setNextFocusableComponent(TxtOptionD);
        TxtOptionC.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                TxtOptionCFocusGained(evt);
            }
        });
        TxtOptionC.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                TxtOptionCKeyReleased(evt);
            }
        });
        jScrollPane7.setViewportView(TxtOptionC);

        TxtOptionD.setColumns(20);
        TxtOptionD.setRows(5);
        TxtOptionD.setNextFocusableComponent(TxtHint);
        TxtOptionD.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                TxtOptionDFocusGained(evt);
            }
        });
        TxtOptionD.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                TxtOptionDKeyReleased(evt);
            }
        });
        jScrollPane8.setViewportView(TxtOptionD);

        LblOptC.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        LblOptC.setForeground(new java.awt.Color(255, 51, 0));
        LblOptC.setText("Option C :"); // NOI18N

        LblOptD.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        LblOptD.setForeground(new java.awt.Color(255, 51, 0));
        LblOptD.setText("Option D:"); // NOI18N

        TxtHint.setColumns(20);
        TxtHint.setRows(5);
        TxtHint.setNextFocusableComponent(RdoA);
        TxtHint.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                TxtHintFocusGained(evt);
            }
        });
        TxtHint.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                TxtHintKeyReleased(evt);
            }
        });
        jScrollPane9.setViewportView(TxtHint);

        LblHint.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        LblHint.setForeground(new java.awt.Color(255, 51, 0));
        LblHint.setText("Hint :"); // NOI18N

        LblAns.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        LblAns.setForeground(new java.awt.Color(255, 51, 0));
        LblAns.setText("Answer :"); // NOI18N

        RdoC.setText("C"); // NOI18N
        RdoC.setNextFocusableComponent(RdoD);
        RdoC.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                RdoCItemStateChanged(evt);
            }
        });

        RdoD.setText("D"); // NOI18N
        RdoD.setNextFocusableComponent(CmbLevel);
        RdoD.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                RdoDItemStateChanged(evt);
            }
        });

        RdoA.setText("A"); // NOI18N
        RdoA.setNextFocusableComponent(RdoB);
        RdoA.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                RdoAItemStateChanged(evt);
            }
        });

        RdoB.setText("B"); // NOI18N
        RdoB.setNextFocusableComponent(RdoC);
        RdoB.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                RdoBItemStateChanged(evt);
            }
        });

        lblLev.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblLev.setForeground(new java.awt.Color(255, 51, 0));
        lblLev.setText("Level :");

        BtnAdd.setText("Add"); // NOI18N
        BtnAdd.setToolTipText("Add Question");
        BtnAdd.setEnabled(false);
        BtnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAddActionPerformed(evt);
            }
        });

        BtnFormula.setText("Formula"); // NOI18N
        BtnFormula.setToolTipText("Alt+F");
        BtnFormula.setMnemonic(KeyEvent.VK_F);
        BtnFormula.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnFormulaActionPerformed(evt);
            }
        });

        CmbLevel.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Select Level", "Easy", "Medium", "Hard" }));
        CmbLevel.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                CmbLevelItemStateChanged(evt);
            }
        });

        LblType.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        LblType.setForeground(new java.awt.Color(255, 51, 0));
        LblType.setText("Type :");

        CmbType.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Select Type", "Theoretical", "Numerical" }));
        CmbType.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                CmbTypeItemStateChanged(evt);
            }
        });

        LblYear.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        LblYear.setForeground(new java.awt.Color(255, 51, 0));
        LblYear.setText("Year :"); // NOI18N

        TxtYear.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                TxtYearFocusGained(evt);
            }
        });
        TxtYear.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                TxtYearKeyReleased(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(153, 153, 153));

        BtnShowQuestion.setText("Show");
        BtnShowQuestion.setToolTipText("Alt+S");
        BtnShowQuestion.setMnemonic(KeyEvent.VK_S);
        BtnShowQuestion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnShowQuestionActionPerformed(evt);
            }
        });

        BtnSplit.setText("Split");
        BtnSplit.setMnemonic(KeyEvent.VK_P);
        BtnSplit.setToolTipText("Alt+P");
        BtnSplit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSplitActionPerformed(evt);
            }
        });

        BtnClear.setText("Clear");
        BtnClear.setToolTipText("Alt+C");
        BtnClear.setMnemonic(KeyEvent.VK_C);
        BtnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnClearActionPerformed(evt);
            }
        });

        BtnShortcuts.setForeground(new java.awt.Color(51, 0, 204));
        BtnShortcuts.setText("Shortcuts");
        BtnShortcuts.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        BtnShortcuts.setContentAreaFilled(false);
        BtnShortcuts.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnShortcutsActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(BtnShortcuts, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(BtnClear, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(BtnShowQuestion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(BtnSplit, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(BtnSplit)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(BtnShowQuestion, javax.swing.GroupLayout.PREFERRED_SIZE, 257, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(BtnClear)
                .addGap(18, 18, 18)
                .addComponent(BtnShortcuts, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        BtnMathType.setText("MathType");
        BtnMathType.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnMathTypeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(LblQuestion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(LblYear, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(LblOptA, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(LblOptC, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(LblOptB, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(LblOptD, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(LblHint, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(LblAns, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addComponent(RdoA)
                        .addGap(2, 2, 2)
                        .addComponent(RdoB)
                        .addGap(4, 4, 4)
                        .addComponent(RdoC)
                        .addGap(4, 4, 4)
                        .addComponent(RdoD)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblLev)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(CmbLevel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(LblType)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(CmbType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BtnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BtnFormula)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BtnMathType)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(TxtYear)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane4)
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane9)
                            .addComponent(jScrollPane8, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane7, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(LblQuestion, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(LblYear)
                            .addComponent(TxtYear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(LblOptA)
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(LblOptB)
                            .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(LblOptC)
                            .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(LblOptD)
                            .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(LblHint)
                            .addComponent(jScrollPane9, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LblAns)
                    .addComponent(RdoA)
                    .addComponent(RdoB)
                    .addComponent(RdoC)
                    .addComponent(RdoD)
                    .addComponent(lblLev)
                    .addComponent(CmbLevel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LblType)
                    .addComponent(CmbType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BtnAdd)
                    .addComponent(BtnFormula)
                    .addComponent(BtnMathType))
                .addContainerGap())
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        ShowLblQuestion1.setText("Question :"); // NOI18N

        ShowLblHint1.setText("Hint :"); // NOI18N

        ShowLblOptionA1.setText("Option A :"); // NOI18N

        ShowLblYear2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        ShowLblYear1.setText("Year :"); // NOI18N

        ShowLblOptionB1.setText("Option B :"); // NOI18N

        ShowLblOptionC1.setText("Option C :"); // NOI18N

        ShowLblOptionD1.setText("Option D :"); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(ShowLblOptionA1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ShowLblOptionA2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(ShowLblHint1)
                        .addGap(29, 29, 29)
                        .addComponent(ShowLblHint2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(ShowLblQuestion1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(ShowLblYear1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(ShowLblYear2, javax.swing.GroupLayout.DEFAULT_SIZE, 46, Short.MAX_VALUE)
                            .addComponent(ShowLblQuestion2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(ShowLblOptionB1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ShowLblOptionB2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(1, 1, 1))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(ShowLblOptionC1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ShowLblOptionC2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(ShowLblOptionD1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ShowLblOptionD2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(631, 631, 631))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ShowLblQuestion1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(ShowLblQuestion2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(15, 15, 15)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ShowLblYear1, javax.swing.GroupLayout.DEFAULT_SIZE, 19, Short.MAX_VALUE)
                    .addComponent(ShowLblYear2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(15, 15, 15)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ShowLblOptionA1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(ShowLblOptionA2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(15, 15, 15)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ShowLblOptionB1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(ShowLblOptionB2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(15, 15, 15)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ShowLblOptionC1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(ShowLblOptionC2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(15, 15, 15)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ShowLblOptionD1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(ShowLblOptionD2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(15, 15, 15)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ShowLblHint1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(ShowLblHint2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(16, 16, 16))
        );

        jScrollPane5.setViewportView(jPanel2);

        ChkQuestion.setText("Question Image");
        ChkQuestion.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ChkQuestionItemStateChanged(evt);
            }
        });

        BtnQuestionPaste.setText("Paste");
        BtnQuestionPaste.setEnabled(false);
        BtnQuestionPaste.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnQuestionPasteActionPerformed(evt);
            }
        });

        LblQuestionPreview.setForeground(new java.awt.Color(51, 0, 255));
        LblQuestionPreview.setText("Preview");
        LblQuestionPreview.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        LblQuestionPreview.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LblQuestionPreviewMouseClicked(evt);
            }
        });

        PanelQuetionImage.setBackground(new java.awt.Color(204, 204, 204));
        PanelQuetionImage.setMaximumSize(new java.awt.Dimension(207, 133));
        PanelQuetionImage.setMinimumSize(new java.awt.Dimension(207, 133));
        PanelQuetionImage.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                PanelQuetionImageMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout PanelQuetionImageLayout = new javax.swing.GroupLayout(PanelQuetionImage);
        PanelQuetionImage.setLayout(PanelQuetionImageLayout);
        PanelQuetionImageLayout.setHorizontalGroup(
            PanelQuetionImageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 207, Short.MAX_VALUE)
        );
        PanelQuetionImageLayout.setVerticalGroup(
            PanelQuetionImageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 223, Short.MAX_VALUE)
        );

        jScrollPane6.setViewportView(PanelQuetionImage);

        PanelOptionImage.setBackground(new java.awt.Color(204, 204, 204));
        PanelOptionImage.setMaximumSize(new java.awt.Dimension(207, 133));
        PanelOptionImage.setMinimumSize(new java.awt.Dimension(207, 133));
        PanelOptionImage.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                PanelOptionImageMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout PanelOptionImageLayout = new javax.swing.GroupLayout(PanelOptionImage);
        PanelOptionImage.setLayout(PanelOptionImageLayout);
        PanelOptionImageLayout.setHorizontalGroup(
            PanelOptionImageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 207, Short.MAX_VALUE)
        );
        PanelOptionImageLayout.setVerticalGroup(
            PanelOptionImageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 223, Short.MAX_VALUE)
        );

        jScrollPane11.setViewportView(PanelOptionImage);

        LblOptionPreview.setForeground(new java.awt.Color(51, 0, 255));
        LblOptionPreview.setText("Preview");
        LblOptionPreview.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        LblOptionPreview.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LblOptionPreviewMouseClicked(evt);
            }
        });

        BtnOptionPaste.setText("Paste");
        BtnOptionPaste.setEnabled(false);
        BtnOptionPaste.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnOptionPasteActionPerformed(evt);
            }
        });

        ChkOption.setText("Option Image");
        ChkOption.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ChkOptionItemStateChanged(evt);
            }
        });

        PanelHintImage.setBackground(new java.awt.Color(204, 204, 204));
        PanelHintImage.setMaximumSize(new java.awt.Dimension(207, 133));
        PanelHintImage.setMinimumSize(new java.awt.Dimension(207, 133));
        PanelHintImage.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                PanelHintImageMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout PanelHintImageLayout = new javax.swing.GroupLayout(PanelHintImage);
        PanelHintImage.setLayout(PanelHintImageLayout);
        PanelHintImageLayout.setHorizontalGroup(
            PanelHintImageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 207, Short.MAX_VALUE)
        );
        PanelHintImageLayout.setVerticalGroup(
            PanelHintImageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 223, Short.MAX_VALUE)
        );

        jScrollPane12.setViewportView(PanelHintImage);

        LblHintPreview.setForeground(new java.awt.Color(51, 0, 255));
        LblHintPreview.setText("Preview");
        LblHintPreview.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        LblHintPreview.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LblHintPreviewMouseClicked(evt);
            }
        });

        BtnHintPaste.setText("Paste");
        BtnHintPaste.setEnabled(false);
        BtnHintPaste.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHintPasteActionPerformed(evt);
            }
        });

        ChkHint.setText("Hint Image");
        ChkHint.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ChkHintItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(ChkQuestion)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(LblQuestionPreview)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BtnQuestionPaste))
                    .addComponent(jScrollPane11, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(ChkOption)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(LblOptionPreview)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BtnOptionPaste))
                    .addComponent(jScrollPane12, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(ChkHint)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(LblHintPreview)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BtnHintPaste)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ChkQuestion)
                    .addComponent(BtnQuestionPaste)
                    .addComponent(LblQuestionPreview))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ChkOption)
                    .addComponent(BtnOptionPaste)
                    .addComponent(LblOptionPreview))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane11, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ChkHint)
                    .addComponent(BtnHintPaste)
                    .addComponent(LblHintPreview))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane12, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel9, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 167, Short.MAX_VALUE)))
                .addGap(0, 0, 0))
        );

        jScrollPane2.setViewportView(jPanel5);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 941, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 643, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void BtnFormulaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnFormulaActionPerformed
    new FormulaFrame().setVisible(true);
}//GEN-LAST:event_BtnFormulaActionPerformed

private void BtnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAddActionPerformed
    String statusValue = checkFillData();
    if(statusValue == null && textFormulaError.equalsIgnoreCase("")) {
        MboxSpaceRemoving spaceRemoving = new MboxSpaceRemoving();
        String answer;
//        boolean optionDimention = false;
        String imageMessage = "";
        if (RdoA.isSelected())
            answer = "A";
        else if (RdoB.isSelected())
            answer = "B";
        else if (RdoC.isSelected())
            answer = "C";
        else
            answer = "D";

//        System.out.println("Que:"+questionText+"\nOpA:"+optionAText+"\nOpB:"+optionBText+"\nOpC:"+optionCText+"\nOpD:"+optionDText+"\nHint:"+hintText);
        questionsBean = new QuestionBean();
        questionText = spaceRemoving.removeBeginEndMboxSpace(questionText);
        optionAText = spaceRemoving.removeBeginEndMboxSpace(optionAText);
        optionBText = spaceRemoving.removeBeginEndMboxSpace(optionBText);
        optionCText = spaceRemoving.removeBeginEndMboxSpace(optionCText);
        optionDText = spaceRemoving.removeBeginEndMboxSpace(optionDText);
        hintText = spaceRemoving.removeBeginEndMboxSpace(hintText);
        if(hintText.equalsIgnoreCase(""))
            hintText = "\\mbox{}";
        if(questionId == 0)
            questionId = new NewIdOperation().getNewMaxId("QUESTION_INFO", "QUESTION_ID");
        
        if(masterQuestionId == 0)
            masterQuestionId = new NewIdOperation().getNewMaxId("MASTER_QUESTION_INFO","QUESTION_ID");
        if(subMasterChapterList == null)
            subMasterChapterList = new SubMasterChapterOperation().getSubMasterChapterList();
        
        questionsBean.setQuestionId(questionId);
        questionsBean.setQuestion(questionText);

        if(TxtYear.getText().trim().isEmpty())
            questionsBean.setYear("");
        else
            questionsBean.setYear(TxtYear.getText().trim());

        questionsBean.setAnswer(answer);

        questionsBean.setHint(hintText);
        questionsBean.setLevel(CmbLevel.getSelectedIndex()-1);
        questionsBean.setSubjectId(chapterBean.getSubjectId());
        questionsBean.setChapterId(chapterBean.getChapterId());
        questionsBean.setTopicId(chapterBean.getChapterId());
        questionsBean.setAttempt(0);
        questionsBean.setType(CmbType.getSelectedIndex()-1);
        questionsBean.setSelected(false);
        
        if(ChkOption.isSelected()) {
            optionAText = "\\mbox{}";
            optionBText = "\\mbox{}";
            optionCText = "\\mbox{}";
            optionDText = "\\mbox{}";
        }
        
        questionsBean.setOptionA(optionAText);
        questionsBean.setOptionB(optionBText);
        questionsBean.setOptionC(optionCText);
        questionsBean.setOptionD(optionDText);
        
        String errorMessage = new ErrorManager().getErrorString(questionsBean,ChkOption.isSelected());
        
        if(errorMessage == null) {
            boolean queImgSaveFlag = true;
            boolean optImgSaveFlag = true;
            boolean hintImgSaveFlag = true;

            if(ChkQuestion.isSelected() && bufferImgQuestion != null) {
                String returnValue = saveImageFile("Question",bufferImgQuestion);
                if(!returnValue.equals("")) {
                    questionsBean.setIsQuestionAsImage(true);
                    questionsBean.setQuestionImagePath(returnValue);
                } else {
                    queImgSaveFlag = false;
                    imageMessage += "Question,";
                } 
            } else if(ChkQuestion.isSelected() && bufferImgQuestion == null) {
                    queImgSaveFlag = false;
                    imageMessage += "Question,";
            } else {
                questionsBean.setIsQuestionAsImage(false);
                questionsBean.setQuestionImagePath("");
            }

            if(ChkOption.isSelected() && bufferImgOption != null) {	
                String returnValue = saveImageFile("Option",bufferImgOption);
                if(!returnValue.equals("")) {
                    questionsBean.setIsOptionAsImage(true);
                    questionsBean.setOptionImagePath(returnValue);
                } else {
                    optImgSaveFlag = false;
                    imageMessage += "Option,";
                }
            } else if(ChkOption.isSelected() && bufferImgOption == null) {
                    optImgSaveFlag = false;
                    imageMessage += "Option,";
            } else {
                questionsBean.setIsOptionAsImage(false);
                questionsBean.setOptionImagePath("");
//                optionDimention = true;
            }

            if(ChkHint.isSelected() && bufferImgHint != null) {	
                String returnValue = saveImageFile("Hint",bufferImgHint);
                if(!returnValue.equals("")) {
                    questionsBean.setIsHintAsImage(true);
                    questionsBean.setHintImagePath(returnValue);
                } else {
                    imageMessage += "Hint,";
                    hintImgSaveFlag = false;
                }
            } else if(ChkHint.isSelected() && bufferImgHint == null) {
                imageMessage += "Hint,";
                hintImgSaveFlag = false;
            } else {
                questionsBean.setIsHintAsImage(false);
                questionsBean.setHintImagePath("");
            }
            
            if(queImgSaveFlag && optImgSaveFlag && hintImgSaveFlag) {

                boolean isPreviousPaperQuestions = false;
                boolean insertStatus = new QuestionOperation().insertQuestion(questionsBean);

                if(insertStatus) {
                    PatternOptionImageDimensionsBean optionImageDimensionsBean = new OptionImageDimentionOperation().insertOptionImageDimentions(questionsBean, isPreviousPaperQuestions);
                    insertImageRatio();
                    int groupId = getSelectedGroupId();
                    ArrayList<ImageRatioBean> masterImageRatioList = new MasterQuestionOperation().insertAddQuestionBean(questionsBean, masterQuestionId, subMasterChapterList,groupId,chapterBean);
                    
                    if(masterImageRatioList != null) {
                        insertMasterImageRatio(masterImageRatioList);
                    }
                    if(optionImageDimensionsBean != null) {
                        optionImageDimensionsBean.setQuestionId(masterQuestionId);
                        new MasterOptionImageDimentionOperation().insertOptionImageDimentionBean(optionImageDimensionsBean);
                    }
                    
                    JOptionPane.showMessageDialog(rootPane, "Insert Sucessfully.");
                    questionId++;
                    masterQuestionId++;
                    resetValues();
                } else {
                    JOptionPane.showMessageDialog(rootPane, "Error In Insertion.");
                }
                
                /*if(optionDimention && insertStatus) {
                    if(new OptionImageDimentionOperation().insertOptionImageDimentions(questionsBean, isPreviousPaperQuestions)) {
                        JOptionPane.showMessageDialog(rootPane, "Insert Sucessfully.");
                        questionId++;
                        resetValues();
                    } else {
                        JOptionPane.showMessageDialog(rootPane, "Error In Insertion.");
                        //Delete Ques Code
                    }
                } else if (insertStatus) {
                    JOptionPane.showMessageDialog(rootPane, "Insert Sucessfully.");
                    questionId++;
                    resetValues();
                } else {
                    JOptionPane.showMessageDialog(rootPane, "Error In Insertion.");
                }*/
            } else {
                imageMessage = imageMessage.substring(0,imageMessage.length()-1);
                deleteFile();
                JOptionPane.showMessageDialog(rootPane, "Error in "+imageMessage+" Image.");
            } 
        } else {
            JOptionPane.showMessageDialog(rootPane, errorMessage);
        }
    } else {
        JOptionPane.showMessageDialog(rootPane, "Please Set "+statusValue+" Values.");
    }
}//GEN-LAST:event_BtnAddActionPerformed
    
    private void insertImageRatio() {
        if(questionsBean.isIsQuestionAsImage() || questionsBean.isIsOptionAsImage() || questionsBean.isIsHintAsImage()) {
            ArrayList<ImageRatioBean> imageRatioList = new ArrayList<ImageRatioBean>();
            ImageRatioBean imageRatioBean = null;
            if(questionsBean.isIsQuestionAsImage()) {
               imageRatioBean = new ImageRatioBean();
               imageRatioBean.setImageName(questionsBean.getQuestionImagePath().trim());
               imageRatioBean.setViewDimention(getQuestionRatio());
               imageRatioList.add(imageRatioBean);
            }
            if(questionsBean.isIsOptionAsImage()) {
                imageRatioBean = new ImageRatioBean();
                imageRatioBean.setImageName(questionsBean.getOptionImagePath().trim());
                imageRatioBean.setViewDimention(getOptionRatio());
                imageRatioList.add(imageRatioBean);
            }
            if(questionsBean.isIsHintAsImage()) {
                imageRatioBean = new ImageRatioBean();
                imageRatioBean.setImageName(questionsBean.getHintImagePath().trim());
                imageRatioBean.setViewDimention(getHintRatio());
                imageRatioList.add(imageRatioBean);
            }
            
            if(imageRatioList.size() != 0)
                new ImageRatioOperation().insertImageRatioList(imageRatioList);
        }
    }

    private void insertMasterImageRatio(ArrayList<ImageRatioBean> masterImageRadioList) {
        for(ImageRatioBean imageRatioBean : masterImageRadioList) {
            if(imageRatioBean.getImageName().trim().contains("Question"))
                imageRatioBean.setViewDimention(getQuestionRatio());
            else if(imageRatioBean.getImageName().trim().contains("Option"))
                imageRatioBean.setViewDimention(getOptionRatio());
            else if(imageRatioBean.getImageName().trim().contains("Hint"))
                imageRatioBean.setViewDimention(getHintRatio());
        }
        
        new MasterImageRationOperation().insertImageRationList(masterImageRadioList);
    }
    
    private String checkFillData() {
        String returnValue = "";
        if(!RdoA.isSelected() && !RdoB.isSelected() && !RdoC.isSelected() && !RdoD.isSelected())
            returnValue += "Answer,";
        if(CmbLevel.getSelectedIndex() == 0)
            returnValue += "Level,";
        if(CmbType.getSelectedIndex() == 0)
            returnValue += "Type,";
        
        if(returnValue.equals(""))
            returnValue = null;
        else
            returnValue = returnValue.substring(0, returnValue.length()-1);
        
        return returnValue;
    }

    private Image getImageFromClipboard() {
        Transferable transferable = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null);
        if (transferable != null && transferable.isDataFlavorSupported(DataFlavor.imageFlavor)) {
            try {
                JOptionPane.showMessageDialog(rootPane, "Got Image");
                return (Image) transferable.getTransferData(DataFlavor.imageFlavor);
            } catch (UnsupportedFlavorException e) {
                // handle this as desired
                e.printStackTrace();
            } catch (IOException e) {
                // handle this as desired
                e.printStackTrace();
            }
        }
        return null;
    }

    private void BtnHomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHomeActionPerformed
        Object[] options = {"YES", "CANCEL"};
        int i = JOptionPane.showOptionDialog(null, "Do you want to close this task?", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
        if (i == 0) {
            new HomePage().setVisible(true);
            this.dispose();
        }
    }//GEN-LAST:event_BtnHomeActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        Object[] options = {"YES", "CANCEL"};
        int i = JOptionPane.showOptionDialog(null, "Do you want to close this task?", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
        if (i == 0) {
            new HomePage().setVisible(true);
            this.dispose();
        }
    }//GEN-LAST:event_formWindowClosing

    private void BtnReConfigureActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnReConfigureActionPerformed
        Object[] options = {"YES", "CANCEL"};
        int i = JOptionPane.showOptionDialog(null, "Do you want to close this task?", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
        if (i == 0) {
            new AddQuestionChapterSelection(chapterBean.getSubjectId()).setVisible(true);
            this.dispose();
        }
    }//GEN-LAST:event_BtnReConfigureActionPerformed

    private void BtnShowQuestionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnShowQuestionActionPerformed
        boolean btnStatus = true;
        BtnAdd.setEnabled(false);
        textFormulaError = "";
        if(!TxtQuestion.getText().isEmpty() && !TxtQuestion.getText().trim().equals("")) {
            questionText = convertText(TxtQuestion.getText().trim(), ShowLblQuestion2,"Question");
        } else {
            ShowLblQuestion2.setIcon(null);
            TxtQuestion.setText("");
            btnStatus = false;
        }
        if(!ChkOption.isSelected()) {
            if(!TxtOptionA.getText().isEmpty() && !TxtOptionA.getText().trim().equals("")) {
                optionAText = convertText(TxtOptionA.getText().trim(), ShowLblOptionA2,"OptionA");
            } else {
                ShowLblOptionA2.setIcon(null);
                optionAText = "";
                btnStatus = false;
            }

            if(!TxtOptionB.getText().isEmpty() && !TxtOptionB.getText().trim().equals("")) {
                optionBText = convertText(TxtOptionB.getText().trim(), ShowLblOptionB2,"OptionB");
            } else {
                ShowLblOptionB2.setIcon(null);
                optionBText = "";
                btnStatus = false;
            }

            if(!TxtOptionC.getText().isEmpty() && !TxtOptionC.getText().trim().equals("")) {
                optionCText = convertText(TxtOptionC.getText().trim(), ShowLblOptionC2,"OptionC");
            } else {
                ShowLblOptionC2.setIcon(null);
                optionCText = "";
                btnStatus = false;
            }

            if(!TxtOptionD.getText().isEmpty() && !TxtOptionD.getText().trim().equals("")) {
                optionDText = convertText(TxtOptionD.getText().trim(), ShowLblOptionD2,"OptionD");
            } else {
                ShowLblOptionD2.setIcon(null);
                optionDText = "";
                btnStatus = false;
            }
        } else {
            ShowLblOptionA2.setIcon(null);
            ShowLblOptionB2.setIcon(null);
            ShowLblOptionC2.setIcon(null);
            ShowLblOptionD2.setIcon(null);
        }
        
        if(!TxtHint.getText().isEmpty() && !TxtHint.getText().trim().equals("")) {
            hintText = convertText(TxtHint.getText().trim(), ShowLblHint2,"Hint");
        } else {
            hintText = "";
            ShowLblHint2.setIcon(null);
        }
        
        String yearText = "";
        if(!TxtYear.getText().isEmpty() || !TxtYear.getText().trim().equals("")) 
            yearText = TxtYear.getText().trim();
        ShowLblYear2.setText(yearText);
        
        if(textFormulaError.equalsIgnoreCase("")) {
            if(btnStatus)
                BtnAdd.setEnabled(true);
        } else {
            textFormulaError = textFormulaError.substring(0, textFormulaError.length()-1);
            JOptionPane.showMessageDialog(rootPane, textFormulaError);
        }
    }//GEN-LAST:event_BtnShowQuestionActionPerformed

    private void BtnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnClearActionPerformed
        Object[] options = {"YES", "CANCEL"};
        int i = JOptionPane.showOptionDialog(null, "Are You Sure to Clear All Data?", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
        if (i == 0) {;
            resetValues();
        }
    }//GEN-LAST:event_BtnClearActionPerformed

    private void CmbLevelItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_CmbLevelItemStateChanged
        stateChange();
    }//GEN-LAST:event_CmbLevelItemStateChanged

    private void RdoAItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_RdoAItemStateChanged
        stateChange();
    }//GEN-LAST:event_RdoAItemStateChanged

    private void RdoBItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_RdoBItemStateChanged
        stateChange();
    }//GEN-LAST:event_RdoBItemStateChanged

    private void RdoCItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_RdoCItemStateChanged
        stateChange();
    }//GEN-LAST:event_RdoCItemStateChanged

    private void RdoDItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_RdoDItemStateChanged
        stateChange();
    }//GEN-LAST:event_RdoDItemStateChanged

    private void BtnSplitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSplitActionPerformed
        String strQuestion = TxtQuestion.getText();
        int loc = strQuestion.indexOf('.');
        strQuestion = strQuestion.substring(loc + 1).trim();
        System.out.println(strQuestion);
        strQuestion = strQuestion.replaceAll("&", "\\&");
        strQuestion = strQuestion.replaceAll("%", "\\%");
        strQuestion = strQuestion.replaceAll("'", "''");
        strQuestion = strQuestion.replaceAll("\\t\\n", "  ");
        strQuestion = strQuestion.replaceAll("\\t\\n\\n", "  ");
        strQuestion = strQuestion.replaceAll("\\n\\n", " ");
        strQuestion = strQuestion.replaceAll("\\n\\n\\n", " ");
        System.out.println(strQuestion);
        try {
            int ab = strQuestion.indexOf("(a)");
            int ae = strQuestion.indexOf("(b)");
            String question = strQuestion.substring(0, ab).trim();
            String optionA = strQuestion.substring(ab + 4, ae).trim();
            int bb = strQuestion.indexOf("(b)");
            int be = strQuestion.indexOf("(c)");
            String optionB = strQuestion.substring(bb + 4, be).trim();
            int cb = strQuestion.indexOf("(c)");
            int ce = strQuestion.indexOf("(d)");
            String optionC = strQuestion.substring(cb + 4, ce).trim();
            int db = strQuestion.indexOf("(d)");
            String optionD = strQuestion.substring(db + 4).trim();
            TxtQuestion.setText(question);
            TxtOptionA.setText(optionA);
            TxtOptionB.setText(optionB);
            TxtOptionC.setText(optionC);
            TxtOptionD.setText(optionD);
        } catch (Exception e) {
        }
        try {
            int ab = strQuestion.indexOf("(A)");
            int ae = strQuestion.indexOf("(B)");
            String question = strQuestion.substring(0, ab).trim();
            String optionA = strQuestion.substring(ab + 4, ae).trim();
            int bb = strQuestion.indexOf("(B)");
            int be = strQuestion.indexOf("(C)");
            String optionB = strQuestion.substring(bb + 4, be).trim();
            int cb = strQuestion.indexOf("(C)");
            int ce = strQuestion.indexOf("(D)");
            String optionC = strQuestion.substring(cb + 4, ce).trim();
            int db = strQuestion.indexOf("(D)");
            String optionD = strQuestion.substring(db + 4).trim();
            TxtQuestion.setText(question);
            TxtOptionA.setText(optionA);
            TxtOptionB.setText(optionB);
            TxtOptionC.setText(optionC);
            TxtOptionD.setText(optionD);
        } catch (Exception e) {
        }
        try {
            int ab = strQuestion.indexOf("a)");
            int ae = strQuestion.indexOf("c)");
            String question = strQuestion.substring(0, ab).trim();
            String optionA = strQuestion.substring(ab + 3, ae).trim();
            int bb = strQuestion.indexOf("c)");
            int be = strQuestion.indexOf("b)");
            String optionB = strQuestion.substring(bb + 3, be).trim();
            int cb = strQuestion.indexOf("b)");
            int ce = strQuestion.indexOf("d)");
            String optionC = strQuestion.substring(cb + 3, ce).trim();
            int db = strQuestion.indexOf("d)");
            String optionD = strQuestion.substring(db + 3).trim();
            TxtQuestion.setText(question);
            TxtOptionA.setText(optionA);
            TxtOptionB.setText(optionC);
            TxtOptionC.setText(optionB);
            TxtOptionD.setText(optionD);
        } catch (Exception e) {
        }
        try {
            int ab = strQuestion.indexOf("a)");
            int ae = strQuestion.indexOf("b)");
            String question = strQuestion.substring(0, ab).trim();
            String optionA = strQuestion.substring(ab + 3, ae).trim();
            int bb = strQuestion.indexOf("b)");
            int be = strQuestion.indexOf("c)");
            String optionB = strQuestion.substring(bb + 3, be).trim();
            int cb = strQuestion.indexOf("c)");
            int ce = strQuestion.indexOf("d)");
            String optionC = strQuestion.substring(cb + 3, ce).trim();
            int db = strQuestion.indexOf("d)");
            String optionD = strQuestion.substring(db + 3).trim();
            TxtQuestion.setText(question);
            TxtOptionA.setText(optionA);
            TxtOptionB.setText(optionB);
            TxtOptionC.setText(optionC);
            TxtOptionD.setText(optionD);
        } catch (Exception e) {
        }
        try {
            int ab = strQuestion.indexOf("A)");
            int ae = strQuestion.indexOf("B)");
            String question = strQuestion.substring(0, ab).trim();
            String optionA = strQuestion.substring(ab + 4, ae).trim();
            int bb = strQuestion.indexOf("B)");
            int be = strQuestion.indexOf("C)");
            String optionB = strQuestion.substring(bb + 4, be).trim();
            int cb = strQuestion.indexOf("C)");
            int ce = strQuestion.indexOf("D)");
            String optionC = strQuestion.substring(cb + 4, ce).trim();
            int db = strQuestion.indexOf("D)");
            String optionD = strQuestion.substring(db + 4).trim();
            TxtQuestion.setText(question);
            TxtOptionA.setText(optionA);
            TxtOptionB.setText(optionB);
            TxtOptionC.setText(optionC);
            TxtOptionD.setText(optionD);
        } catch (Exception e) {
        }
        try {
            int ab = strQuestion.indexOf("A)");
            int ae = strQuestion.indexOf("C)");
            String question = strQuestion.substring(0, ab).trim();
            String optionA = strQuestion.substring(ab + 3, ae).trim();
            int bb = strQuestion.indexOf("C)");
            int be = strQuestion.indexOf("B)");
            String optionB = strQuestion.substring(bb + 3, be).trim();
            int cb = strQuestion.indexOf("B)");
            int ce = strQuestion.indexOf("D)");
            String optionC = strQuestion.substring(cb + 3, ce).trim();
            int db = strQuestion.indexOf("D)");
            String optionD = strQuestion.substring(db + 3).trim();
            TxtQuestion.setText(question);
            TxtOptionA.setText(optionA);
            TxtOptionB.setText(optionC);
            TxtOptionC.setText(optionB);
            TxtOptionD.setText(optionD);
        } catch (Exception e) {
        }
        try {
            int ab = strQuestion.indexOf("A.");
            int ae = strQuestion.indexOf("B.");
            String question = strQuestion.substring(0, ab).trim();
            String optionA = strQuestion.substring(ab + 4, ae).trim();
            int bb = strQuestion.indexOf("B.");
            int be = strQuestion.indexOf("C.");
            String optionB = strQuestion.substring(bb + 4, be).trim();
            int cb = strQuestion.indexOf("C.");
            int ce = strQuestion.indexOf("D.");
            String optionC = strQuestion.substring(cb + 4, ce).trim();
            int db = strQuestion.indexOf("D.");
            String optionD = strQuestion.substring(db + 4).trim();
            TxtQuestion.setText(question);
            TxtOptionA.setText(optionA);
            TxtOptionB.setText(optionB);
            TxtOptionC.setText(optionC);
            TxtOptionD.setText(optionD);
        } catch (Exception e) {
        }
        try {
            int ab = strQuestion.indexOf("a.");
            int ae = strQuestion.indexOf("b.");
            String question = strQuestion.substring(0, ab).trim();
            String optionA = strQuestion.substring(ab + 4, ae).trim();
            int bb = strQuestion.indexOf("b.");
            int be = strQuestion.indexOf("c.");
            String optionB = strQuestion.substring(bb + 4, be).trim();
            int cb = strQuestion.indexOf("c.");
            int ce = strQuestion.indexOf("d.");
            String optionC = strQuestion.substring(cb + 4, ce).trim();
            int db = strQuestion.indexOf("d.");
            String optionD = strQuestion.substring(db + 4).trim();
            TxtQuestion.setText(question);
            TxtOptionA.setText(optionA);
            TxtOptionB.setText(optionB);
            TxtOptionC.setText(optionC);
            TxtOptionD.setText(optionD);
        } catch (Exception e) {
        }
        try {
            int ab = strQuestion.indexOf("A:");
            int ae = strQuestion.indexOf("B:");
            String question = strQuestion.substring(0, ab).trim();
            String optionA = strQuestion.substring(ab + 4, ae).trim();
            int bb = strQuestion.indexOf("B:");
            int be = strQuestion.indexOf("C:");
            String optionB = strQuestion.substring(bb + 4, be).trim();
            int cb = strQuestion.indexOf("C:");
            int ce = strQuestion.indexOf("D:");
            String optionC = strQuestion.substring(cb + 4, ce).trim();
            int db = strQuestion.indexOf("D:");
            String optionD = strQuestion.substring(db + 4).trim();
            TxtQuestion.setText(question);
            TxtOptionA.setText(optionA);
            TxtOptionB.setText(optionB);
            TxtOptionC.setText(optionC);
            TxtOptionD.setText(optionD);
        } catch (Exception e) {
        }
        try {
            int ab = strQuestion.indexOf("a:");
            int ae = strQuestion.indexOf("b:");
            String question = strQuestion.substring(0, ab).trim();
            String optionA = strQuestion.substring(ab + 3, ae).trim();
            int bb = strQuestion.indexOf("b:");
            int be = strQuestion.indexOf("c:");
            String optionB = strQuestion.substring(bb + 3, be).trim();
            int cb = strQuestion.indexOf("c:");
            int ce = strQuestion.indexOf("d:");
            String optionC = strQuestion.substring(cb + 3, ce).trim();
            int db = strQuestion.indexOf("d:");
            String optionD = strQuestion.substring(db + 3).trim();
            TxtQuestion.setText(question);
            TxtOptionA.setText(optionA);
            TxtOptionB.setText(optionB);
            TxtOptionC.setText(optionC);
            TxtOptionD.setText(optionD);
        } catch (Exception e) {
        }
        try {
            int ab = strQuestion.indexOf("A  ");
            int ae = strQuestion.indexOf("B  ");
            String question = strQuestion.substring(0, ab).trim();
            String optionA = strQuestion.substring(ab + 1, ae).trim();
            int bb = strQuestion.indexOf("B  ");
            int be = strQuestion.indexOf("C  ");
            String optionB = strQuestion.substring(bb + 1, be).trim();
            int cb = strQuestion.indexOf("C  ");
            int ce = strQuestion.indexOf("D  ");
            String optionC = strQuestion.substring(cb + 1, ce).trim();
            int db = strQuestion.indexOf("D  ");
            String optionD = strQuestion.substring(db + 1).trim();
            TxtQuestion.setText(question);
            TxtOptionA.setText(optionA);
            TxtOptionB.setText(optionB);
            TxtOptionC.setText(optionC);
            TxtOptionD.setText(optionD);
        } catch (Exception e) {
        }
        try {
            int ab = strQuestion.indexOf("a  ");
            int ae = strQuestion.indexOf("b  ");
            String question = strQuestion.substring(0, ab).trim();
            String optionA = strQuestion.substring(ab + 2, ae).trim();
            int bb = strQuestion.indexOf("b  ");
            int be = strQuestion.indexOf("c  ");
            String optionB = strQuestion.substring(bb + 2, be).trim();
            int cb = strQuestion.indexOf("c  ");
            int ce = strQuestion.indexOf("d  ");
            String optionC = strQuestion.substring(cb + 2, ce).trim();
            int db = strQuestion.indexOf("d  ");
            String optionD = strQuestion.substring(db + 2).trim();
            TxtQuestion.setText(question);
            TxtOptionA.setText(optionA);
            TxtOptionB.setText(optionB);
            TxtOptionC.setText(optionC);
            TxtOptionD.setText(optionD);
        } catch (Exception e) {
        }
        try {
            int ab = strQuestion.indexOf("(1)");
            int ae = strQuestion.indexOf("(2)");
            String question = strQuestion.substring(0, ab).trim();
            String optionA = strQuestion.substring(ab + 4, ae).trim();
            int bb = strQuestion.indexOf("(2)");
            int be = strQuestion.indexOf("(3)");
            String optionB = strQuestion.substring(bb + 4, be).trim();
            int cb = strQuestion.indexOf("(3)");
            int ce = strQuestion.indexOf("(4)");
            String optionC = strQuestion.substring(cb + 4, ce).trim();
            int db = strQuestion.indexOf("(4)");
            String optionD = strQuestion.substring(db + 4).trim();
            TxtQuestion.setText(question);
            TxtOptionA.setText(optionA);
            TxtOptionB.setText(optionB);
            TxtOptionC.setText(optionC);
            TxtOptionD.setText(optionD);
        } catch (Exception e) {
        }
        try {
            int ab = strQuestion.indexOf("1)");
            int ae = strQuestion.indexOf("2)");
            String question = strQuestion.substring(0, ab).trim();
            String optionA = strQuestion.substring(ab + 3, ae).trim();
            int bb = strQuestion.indexOf("2)");
            int be = strQuestion.indexOf("3)");
            String optionB = strQuestion.substring(bb + 3, be).trim();
            int cb = strQuestion.indexOf("3)");
            int ce = strQuestion.indexOf("4)");
            String optionC = strQuestion.substring(cb + 3, ce).trim();
            int db = strQuestion.indexOf("4)");
            String optionD = strQuestion.substring(db + 3).trim();
            TxtQuestion.setText(question);
            TxtOptionA.setText(optionA);
            TxtOptionB.setText(optionB);
            TxtOptionC.setText(optionC);
            TxtOptionD.setText(optionD);
        } catch (Exception e) {
        }
        try {
            int ab = strQuestion.indexOf("1:");
            int ae = strQuestion.indexOf("2:");
            String question = strQuestion.substring(0, ab).trim();
            String optionA = strQuestion.substring(ab + 4, ae).trim();
            int bb = strQuestion.indexOf("2:");
            int be = strQuestion.indexOf("3:");
            String optionB = strQuestion.substring(bb + 4, be).trim();
            int cb = strQuestion.indexOf("3:");
            int ce = strQuestion.indexOf("4:");
            String optionC = strQuestion.substring(cb + 4, ce).trim();
            int db = strQuestion.indexOf("4:");
            String optionD = strQuestion.substring(db + 4).trim();
            TxtQuestion.setText(question);
            TxtOptionA.setText(optionA);
            TxtOptionB.setText(optionB);
            TxtOptionC.setText(optionC);
            TxtOptionD.setText(optionD);
        } catch (Exception e) {
        }
        try {
            int ab = strQuestion.indexOf("1.");
            int ae = strQuestion.indexOf("2.");
            String question = strQuestion.substring(0, ab).trim();
            String optionA = strQuestion.substring(ab + 4, ae).trim();
            int bb = strQuestion.indexOf("2.");
            int be = strQuestion.indexOf("3.");
            String optionB = strQuestion.substring(bb + 4, be).trim();
            int cb = strQuestion.indexOf("3.");
            int ce = strQuestion.indexOf("4.");
            String optionC = strQuestion.substring(cb + 4, ce).trim();
            int db = strQuestion.indexOf("4.");
            String optionD = strQuestion.substring(db + 4).trim();
            TxtQuestion.setText(question);
            TxtOptionA.setText(optionA);
            TxtOptionB.setText(optionB);
            TxtOptionC.setText(optionC);
            TxtOptionD.setText(optionD);
        } catch (Exception e) {
        }
        stateChange();
    }//GEN-LAST:event_BtnSplitActionPerformed

private void BtnShortcutsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnShortcutsActionPerformed
    new Help(this).setVisible(true);
}//GEN-LAST:event_BtnShortcutsActionPerformed

    public void ChangeText() {
        try {
            Transferable str = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null);
            String que = (String) str.getTransferData(DataFlavor.stringFlavor);
            if (str != null && que != null) {
                if (que.equals("")) {
                } else {
                    new ChangeClipBoardData().Change();
                }
            }
        } catch (UnsupportedFlavorException ex) {
        } catch (IOException ex) {
        }
    }

private void TxtQuestionFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_TxtQuestionFocusGained
    ChangeText();
//    btnAddNewState();
}//GEN-LAST:event_TxtQuestionFocusGained

private void TxtOptionAFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_TxtOptionAFocusGained
    ChangeText();
//    btnAddNewState();
}//GEN-LAST:event_TxtOptionAFocusGained

private void TxtOptionBFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_TxtOptionBFocusGained
    ChangeText();
//    btnAddNewState();
}//GEN-LAST:event_TxtOptionBFocusGained

private void TxtOptionCFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_TxtOptionCFocusGained
    ChangeText();
//    btnAddNewState();
}//GEN-LAST:event_TxtOptionCFocusGained

private void TxtOptionDFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_TxtOptionDFocusGained
    ChangeText();
//    btnAddNewState();
}//GEN-LAST:event_TxtOptionDFocusGained

private void TxtHintFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_TxtHintFocusGained
    ChangeText();
//    btnAddNewState();
}//GEN-LAST:event_TxtHintFocusGained

private void CmbTypeItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_CmbTypeItemStateChanged
// TODO add your handling code here:
    stateChange();
}//GEN-LAST:event_CmbTypeItemStateChanged

    private void BtnMathTypeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnMathTypeActionPerformed
        if(!new ApplicationManager().getApplicationRunningStatus("MathType.exe")) {
            boolean is64bit = false;
            if (System.getProperty("os.name").contains("Windows")) {
                is64bit = (System.getenv("ProgramFiles(x86)") != null);
            } else {
                is64bit = (System.getProperty("os.arch").indexOf("64") != -1);
            }

            ProcessBuilder pb = new ProcessBuilder(
                    "C:/Program Files/MathType/MathType.exe",
                    "x",
                    "myjar.jar",
                    "*.*",
                    "new");
            if (is64bit) {
                pb = new ProcessBuilder(
                        "C:/Program Files (x86)/MathType/MathType.exe",
                        "x",
                        "myjar.jar",
                        "*.*",
                        "new");
            }
            pb.directory(new File("C:/"));
            pb.redirectError();
            try {
                Process p = pb.start();


            } catch (IOException ex) {
                Logger.getLogger(FormulaFrame.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            JOptionPane.showMessageDialog(rootPane, "MathType Application is Already Open in Background.");
        }
    }//GEN-LAST:event_BtnMathTypeActionPerformed

    private void formKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_formKeyReleased

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed
    }//GEN-LAST:event_formKeyPressed

    private void TxtYearFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_TxtYearFocusGained
//        btnAddNewState();
    }//GEN-LAST:event_TxtYearFocusGained

    private void BtnQuestionPasteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnQuestionPasteActionPerformed
        bufferImgQuestion = (BufferedImage) getImageFromClipboard();
        PanelQuetionImage.removeAll();
        setQuestionRatio(1.0f);
        if (bufferImgQuestion != null) {
            int h = bufferImgQuestion.getHeight();
            int w = bufferImgQuestion.getWidth();
            h = PanelQuetionImage.getHeight();
            w = PanelQuetionImage.getWidth();
            ImagePanel1 p1 = new ImagePanel1(bufferImgQuestion, w, h);
            PanelQuetionImage.setLayout(new GridLayout());
            PanelQuetionImage.add(p1);
            this.invalidate();
            this.validate();
//            questionImageFlag = true;
            LblQuestionPreview.setVisible(true);
        } else {
//            questionImageFlag = false;
            LblQuestionPreview.setVisible(false);
            JOptionPane.showMessageDialog(rootPane, "Invalid Copied Image");
        }
    }//GEN-LAST:event_BtnQuestionPasteActionPerformed

    private String saveImageFile(String imageType,BufferedImage bufferedImage) {
        String returnPath = "";
        try {
            returnPath = "images/" + chapterBean.getChapterId()+imageType+questionId + ".png";
            File outputFile = new File(returnPath);
            File invoiceFile = new File(processPath+"/" + returnPath);
            ImageIO.write(bufferedImage, "png", outputFile);
            ImageIO.write(bufferedImage, "png", invoiceFile);
        } catch (Exception e) {
            returnPath = "";
            e.printStackTrace();
        }
        return returnPath;
    }
    
    private void ChkQuestionItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ChkQuestionItemStateChanged
        LblQuestionPreview.setVisible(false);
        PanelQuetionImage.removeAll();
        bufferImgQuestion = null;
        BtnQuestionPaste.setEnabled(ChkQuestion.isSelected());
        setQuestionRatio(1.0f);
        if(BtnAdd.isEnabled())
            BtnAdd.setEnabled(false);
        
    }//GEN-LAST:event_ChkQuestionItemStateChanged

    private void PanelQuetionImageMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_PanelQuetionImageMouseClicked
    }//GEN-LAST:event_PanelQuetionImageMouseClicked

    private void LblQuestionPreviewMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblQuestionPreviewMouseClicked
        switch (evt.getModifiers()) {
            case InputEvent.BUTTON1_MASK: {
                new PreviewImage(bufferImgQuestion,"Question",this).setVisible(true);
                this.setEnabled(false);
                break;
            }
        }
    }//GEN-LAST:event_LblQuestionPreviewMouseClicked

    private void PanelOptionImageMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_PanelOptionImageMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_PanelOptionImageMouseClicked

    private void LblOptionPreviewMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblOptionPreviewMouseClicked
        switch (evt.getModifiers()) {
            case InputEvent.BUTTON1_MASK: {
                new PreviewImage(bufferImgOption,"Option",this).setVisible(true);
                this.setEnabled(false);
                break;
            }
        }
    }//GEN-LAST:event_LblOptionPreviewMouseClicked

    private void BtnOptionPasteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnOptionPasteActionPerformed
        bufferImgOption = (BufferedImage) getImageFromClipboard();
        PanelOptionImage.removeAll();
        setOptionRatio(1.0f);
        if (bufferImgOption != null) {
            int h = bufferImgOption.getHeight();
            int w = bufferImgOption.getWidth();
            h = PanelOptionImage.getHeight();
            w = PanelOptionImage.getWidth();

            ImagePanel1 p1 = new ImagePanel1(bufferImgOption, w, h);
//            ImagePanel1 p = new ImagePanel1(bufferImgOption, 400, 400);
            PanelOptionImage.setLayout(new GridLayout());
            PanelOptionImage.add(p1);
            this.invalidate();
            this.validate();
            LblOptionPreview.setVisible(true);
        } else {
            LblOptionPreview.setVisible(false);
            JOptionPane.showMessageDialog(rootPane, "Invalid Copied Image");
        }
    }//GEN-LAST:event_BtnOptionPasteActionPerformed

    private void ChkOptionItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ChkOptionItemStateChanged
        LblOptionPreview.setVisible(false);
        bufferImgOption = null;
        PanelOptionImage.removeAll();
        setOptionRatio(1.0f);
        
        if(BtnAdd.isEnabled())
            BtnAdd.setEnabled(false);
        
        if(ChkOption.isSelected()) {
            BtnOptionPaste.setEnabled(true);
            TxtOptionA.setEditable(false);
            TxtOptionA.setText("\\mbox{}");
            TxtOptionB.setEditable(false);
            TxtOptionB.setText("\\mbox{}");
            TxtOptionC.setEditable(false);
            TxtOptionC.setText("\\mbox{}");
            TxtOptionD.setEditable(false);
            TxtOptionD.setText("\\mbox{}");
        } else {
            BtnOptionPaste.setEnabled(false);
            TxtOptionA.setEditable(true);
            TxtOptionA.setText("");
            TxtOptionB.setEditable(true);
            TxtOptionB.setText("");
            TxtOptionC.setEditable(true);
            TxtOptionC.setText("");
            TxtOptionD.setEditable(true);
            TxtOptionD.setText("");
        }
    }//GEN-LAST:event_ChkOptionItemStateChanged

    private void PanelHintImageMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_PanelHintImageMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_PanelHintImageMouseClicked

    private void LblHintPreviewMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblHintPreviewMouseClicked
        switch (evt.getModifiers()) {
            case InputEvent.BUTTON1_MASK: {
                new PreviewImage(bufferImgHint,"Hint",this).setVisible(true);
                this.setEnabled(false);
                break;
            }
        }
    }//GEN-LAST:event_LblHintPreviewMouseClicked

    private void BtnHintPasteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHintPasteActionPerformed
        bufferImgHint = (BufferedImage) getImageFromClipboard();
        PanelHintImage.removeAll();
        setHintRatio(1.0f);
        if (bufferImgHint != null) {
            int h = bufferImgHint.getHeight();
            int w = bufferImgHint.getWidth();
            h = PanelHintImage.getHeight();
            w = PanelHintImage.getWidth();
            ImagePanel1 p1 = new ImagePanel1(bufferImgHint, w, h);
//            ImagePanel1 p = new ImagePanel1(bufferImgHint, 400, 400);
            PanelHintImage.setLayout(new GridLayout());
            PanelHintImage.add(p1);
            this.invalidate();
            this.validate();
            LblHintPreview.setVisible(true);
        } else {
            LblHintPreview.setVisible(false);
            JOptionPane.showMessageDialog(rootPane, "Invalid Copied Image");
        }
    }//GEN-LAST:event_BtnHintPasteActionPerformed

    private void ChkHintItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ChkHintItemStateChanged
        LblHintPreview.setVisible(false);
        PanelHintImage.removeAll();
        bufferImgHint = null;
        BtnHintPaste.setEnabled(ChkHint.isSelected());
        setHintRatio(1.0f);
        
        if(BtnAdd.isEnabled())
            BtnAdd.setEnabled(false);
    }//GEN-LAST:event_ChkHintItemStateChanged

    private void TxtQuestionKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtQuestionKeyReleased
        // TODO add your handling code here:
        if(TxtQuestion.getText().isEmpty())
            LblQuestion.setForeground(Color.RED);
        else
            LblQuestion.setForeground(Color.BLACK);
        
        if(BtnAdd.isEnabled())
            BtnAdd.setEnabled(false);
    }//GEN-LAST:event_TxtQuestionKeyReleased

    private void TxtYearKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtYearKeyReleased
        // TODO add your handling code here:
        if (TxtYear.getText().isEmpty())
            LblYear.setForeground(Color.RED);
        else
            LblYear.setForeground(Color.BLACK);
        
        if(BtnAdd.isEnabled())
            BtnAdd.setEnabled(false);
    }//GEN-LAST:event_TxtYearKeyReleased

    private void TxtOptionAKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtOptionAKeyReleased
        // TODO add your handling code here:
        if (TxtOptionA.getText().isEmpty())
            LblOptA.setForeground(Color.RED);
        else
            LblOptA.setForeground(Color.BLACK);
        
        if(BtnAdd.isEnabled())
            BtnAdd.setEnabled(false);
    }//GEN-LAST:event_TxtOptionAKeyReleased

    private void TxtOptionBKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtOptionBKeyReleased
        // TODO add your handling code here:
        if (TxtOptionB.getText().isEmpty())
            LblOptB.setForeground(Color.RED);
        else
            LblOptB.setForeground(Color.BLACK);
        
        if(BtnAdd.isEnabled())
            BtnAdd.setEnabled(false);
    }//GEN-LAST:event_TxtOptionBKeyReleased

    private void TxtOptionCKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtOptionCKeyReleased
        // TODO add your handling code here:
        if (TxtOptionC.getText().isEmpty())
            LblOptC.setForeground(Color.RED);
        else
            LblOptC.setForeground(Color.BLACK);
        
        if(BtnAdd.isEnabled())
            BtnAdd.setEnabled(false);
    }//GEN-LAST:event_TxtOptionCKeyReleased

    private void TxtOptionDKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtOptionDKeyReleased
        // TODO add your handling code here:
        if (TxtOptionD.getText().isEmpty())
            LblOptD.setForeground(Color.RED);
        else
            LblOptD.setForeground(Color.BLACK);
        
        if(BtnAdd.isEnabled())
            BtnAdd.setEnabled(false);
    }//GEN-LAST:event_TxtOptionDKeyReleased

    private void TxtHintKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtHintKeyReleased
        // TODO add your handling code here:
        if (TxtHint.getText().isEmpty())
            LblHint.setForeground(Color.RED);
        else
            LblHint.setForeground(Color.BLACK);
        
        if(BtnAdd.isEnabled())
            BtnAdd.setEnabled(false);
    }//GEN-LAST:event_TxtHintKeyReleased

    private void clearAllFields() {
        TxtQuestion.setText("");
        TxtOptionA.setText("");
        TxtOptionB.setText("");
        TxtOptionC.setText("");
        TxtOptionD.setText("");
        TxtYear.setText("");
        TxtHint.setText("");
        ShowLblQuestion2.setIcon(null);
        ShowLblOptionA2.setIcon(null);
        ShowLblOptionB2.setIcon(null);
        ShowLblOptionC2.setIcon(null);
        ShowLblOptionD2.setIcon(null);
        ShowLblHint2.setIcon(null);
        ShowLblYear2.setText("");
        CmbLevel.setSelectedIndex(0);
        CmbType.setSelectedIndex(0);
        BtnAdd.setEnabled(false);
        PanelQuetionImage.removeAll();
        BtnQuestionPaste.setEnabled(false);
        LblQuestionPreview.setVisible(false);
        ChkQuestion.setSelected(false);
        ChkOption.setSelected(false);
        ChkHint.setSelected(false);
        buttonGroup1.clearSelection();
    }
   
    public void deleteFile() {
        try {
            if(!questionsBean.isIsQuestionAsImage()){
                File f1 = new File(questionsBean.getQuestionImagePath());
                File f2 =new File(processPath + "/" + questionsBean.getQuestionImagePath());
                    f1.delete();
                    f2.delete();
            }
        } catch(Exception ex) {
        }
        try {
            if(!questionsBean.isIsOptionAsImage()){
                File f1 = new File(questionsBean.getOptionImagePath());
                File f2 =new File(processPath + "/" + questionsBean.getOptionImagePath());
                    f1.delete();
                    f2.delete();
            }
        } catch(Exception ex) {
        }
        try {
            if(!questionsBean.isIsHintAsImage()){
                File f1 = new File(questionsBean.getHintImagePath());
                File f2 =new File(processPath + "/" + questionsBean.getHintImagePath());
                    f1.delete();
                    f2.delete();
            }
        } catch(Exception ex) {
        }
    }
    
    public String convertText(String txtFieldText,JLabel showLabel,String txtFieldType){
        String txtString,returnString = null;
        try {
            txtString = txtFieldText;
//            System.out.println("finalQuesitonstr :" + txtString);
            String finalStr = "";
            finalStr = first + txtString + last;
//            System.out.println("finalStr :" + finalStr);
            String t = finalStr.replaceAll("\\n", newline);
            String t1 = t.replaceAll("\\[", "[");
            String t2 = t1.replaceAll("\\]", "]");
//            System.out.println("t :" + t2);
            char[] temp = t2.toCharArray();
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] == ' ') {
                    temp[i] = ' ';
                }
            }
            t2 = new String(temp);
            t2 = t2.trim();
            returnString = t2;
            t2 = start + t2 + end;
            formula = new TeXFormula(t2);
            TeXIcon icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, 20);
            icon.setInsets(new Insets(0, 0, 0, 0));
            BufferedImage image = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
            Graphics2D g2 = image.createGraphics();
            g2.setColor(Color.white);
            g2.fillRect(0, 0, icon.getIconWidth(), icon.getIconHeight());
            JLabel jl = new JLabel();
            jl.setForeground(new Color(0, 0, 0));
            icon.paintIcon(jl, g2, 0, 0);
            showLabel.setIcon(icon);
        } catch (Exception ex) {
            ex.printStackTrace();
            showLabel.setIcon(null);
            textFormulaError += "Error in "+txtFieldType+"\n";
        }
        return returnString;
    }
    
    private int getSelectedGroupId() {
        int returnValue = 0;
        int patternIndex = getPatternValue();
        if(groupList == null)
            groupList = new MasterGroupOperation().getGroupList();
        returnValue = groupList.get(patternIndex).getGroupId();
        return returnValue;
    }
    
    
    private int getPatternValue() {
        int returnValue = 0;
        try {
            BufferedReader br = new BufferedReader(new FileReader("CurrentPattern.txt"));
            String currentLineText = null;
            returnValue = 0;
            currentLineText = br.readLine();

            if(currentLineText != null) {
                try {
                    returnValue = Integer.parseInt(currentLineText.trim());
                } catch(Exception ex) {
                    returnValue = 0;
                } 
            }
            try {
                if (br != null)
                    br.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } 
        
        return returnValue;
    }
   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnAdd;
    private javax.swing.JButton BtnClear;
    private javax.swing.JButton BtnFormula;
    private javax.swing.JButton BtnHintPaste;
    private javax.swing.JButton BtnHome;
    private javax.swing.JButton BtnMathType;
    private javax.swing.JButton BtnOptionPaste;
    private javax.swing.JButton BtnQuestionPaste;
    private javax.swing.JButton BtnReConfigure;
    private javax.swing.JButton BtnShortcuts;
    private javax.swing.JButton BtnShowQuestion;
    private javax.swing.JButton BtnSplit;
    private javax.swing.JCheckBox ChkHint;
    private javax.swing.JCheckBox ChkOption;
    private javax.swing.JCheckBox ChkQuestion;
    private javax.swing.JComboBox CmbLevel;
    private javax.swing.JComboBox CmbType;
    private javax.swing.JLabel LblAns;
    private javax.swing.JLabel LblHint;
    private javax.swing.JLabel LblHintPreview;
    private javax.swing.JLabel LblOptA;
    private javax.swing.JLabel LblOptB;
    private javax.swing.JLabel LblOptC;
    private javax.swing.JLabel LblOptD;
    private javax.swing.JLabel LblOptionPreview;
    private javax.swing.JLabel LblQuestion;
    private javax.swing.JLabel LblQuestionPreview;
    private javax.swing.JLabel LblType;
    private javax.swing.JLabel LblYear;
    private javax.swing.JPanel PanelHintImage;
    private javax.swing.JPanel PanelOptionImage;
    private javax.swing.JPanel PanelQuetionImage;
    private javax.swing.JRadioButton RdoA;
    private javax.swing.JRadioButton RdoB;
    private javax.swing.JRadioButton RdoC;
    private javax.swing.JRadioButton RdoD;
    private javax.swing.JLabel ShowLblHint1;
    private javax.swing.JLabel ShowLblHint2;
    private javax.swing.JLabel ShowLblOptionA1;
    private javax.swing.JLabel ShowLblOptionA2;
    private javax.swing.JLabel ShowLblOptionB1;
    private javax.swing.JLabel ShowLblOptionB2;
    private javax.swing.JLabel ShowLblOptionC1;
    private javax.swing.JLabel ShowLblOptionC2;
    private javax.swing.JLabel ShowLblOptionD1;
    private javax.swing.JLabel ShowLblOptionD2;
    private javax.swing.JLabel ShowLblQuestion1;
    private javax.swing.JLabel ShowLblQuestion2;
    private javax.swing.JLabel ShowLblYear1;
    private javax.swing.JLabel ShowLblYear2;
    private javax.swing.JTextArea TxtHint;
    private javax.swing.JTextArea TxtOptionA;
    private javax.swing.JTextArea TxtOptionB;
    private javax.swing.JTextArea TxtOptionC;
    private javax.swing.JTextArea TxtOptionD;
    private javax.swing.JTextArea TxtQuestion;
    private javax.swing.JTextField TxtYear;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane11;
    private javax.swing.JScrollPane jScrollPane12;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JLabel lblChap;
    private javax.swing.JLabel lblLev;
    private javax.swing.JLabel lblSubject;
    // End of variables declaration//GEN-END:variables

    //Setters And Getters
    public float getQuestionRatio() {
        return questionRatio;
    }

    public void setQuestionRatio(float questionRatio) {
        this.questionRatio = questionRatio;
    }

    public float getOptionRatio() {
        return optionRatio;
    }

    public void setOptionRatio(float optionRatio) {
        this.optionRatio = optionRatio;
    }

    public float getHintRatio() {
        return hintRatio;
    }

    public void setHintRatio(float hintRatio) {
        this.hintRatio = hintRatio;
    }
}