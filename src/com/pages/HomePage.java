package com.pages;
import com.LatexProcessing.LatexConversion;
import com.Model.DefaultFolderLocation;
import com.Model.ExportProcess;
import com.Model.FileChooser;
import com.Model.SmsOperation;
import com.Model.TestUtility;
import com.bean.ChapterBean;
import com.bean.CountChapterBean;
import com.bean.QuestionBean;
import com.bean.SubjectBean;
import com.bean.PrintedTestBean;
import com.db.operations.ChapterOperation;
import com.db.operations.QuestionOperation;
import com.db.operations.SubjectOperation;
import com.db.operations.PrintedTestOperation;
import com.db.operations.RegistrationOperation;
import com.db.operations.MasterGroupOperation;
import java.net.URISyntaxException;
import java.util.*;
import javax.swing.*;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import com.ui.support.pages.AboutUs;
import com.ui.support.pages.ProcessingPanel;
import com.Model.TitleInfo;
import com.Server.ClientMonitorSocket;
import com.Server.NetworkDBServer;
import com.bean.AcademicYearBean;
import com.bean.ChapterViewBean;
import com.bean.ClassBean;
import com.bean.ClassTestBean;
import com.bean.ClientInfoBean;
import com.bean.DivisionBean;
import com.bean.GroupBean;
import com.bean.RankBean;
import com.bean.SMSHistoryBean;
import com.bean.SenderIdBean;
import com.bean.StudentBean;
import com.bean.SubMasterChapterBean;
import com.bean.UnitTestBean;
import com.db.DbConnection;
import com.db.operations.AcademicYearOperation;
import com.db.operations.ClassOperations;
import com.db.operations.ClientOperation;
import com.db.operations.LogOutOperation;
import com.db.operations.NetworkPrefixOperation;
import com.db.operations.ClassSaveTestOperation;
import com.db.operations.DivisionOperation;
import com.db.operations.PatternStatusOperation;
import com.db.operations.SMSHistoryOperation;
import com.db.operations.SenderIdOperation;
import com.db.operations.StudentRegistrationOperation;
import com.id.operations.NewIdOperation;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.db.operations.SubMasterChapterOperation;
import com.lowagie.text.pdf.PdfPCell;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import com.ui.support.pages.Weightage;
import com.ui.support.pages.WhatsNew;
import de.nixosoft.jlr.JLROpener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableModel;
import ui.ConfigureServer1;
import ui.RankGeneration;
import ui.RankGenerationGroup;
import ui.SenderIdSMS;
import ui.StartedUnitTestForm;
import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import ui.RestrctDoubleUpto2;

public class HomePage extends javax.swing.JFrame {
    private ArrayList<SubMasterChapterBean>SubMasterchapterList1;
    private ArrayList<SubMasterChapterBean>SubMasterchapterList2;
    private ArrayList<SubMasterChapterBean>SubMasterchapterList3;
    private final CardLayout cardLayout;
    private ArrayList<ChapterBean> chapterList;
    private ArrayList<QuestionBean> questionsList;
    private ArrayList<SubjectBean> subjectList;
    private SubjectBean selectedSubjectBean;
    private ArrayList<SubjectBean> sortingSubjectList;
    private ArrayList<PrintedTestBean> testList;
    private DefaultTableCellRenderer centerRenderer;
    private String printingPaperType;
    private boolean nextPageCallStatus;
    private String instituteName;
    private int patternIndexValue;
    private int currentPatternIndex;
    private String currentFolderDirectory;
    private String tempFileLocation;
    private ArrayList<ChapterViewBean> chapterViewList;
    private ArrayList<SubMasterChapterBean> submasterchapterList;
    ArrayList<String> testInfo = new ArrayList<String>();
    ArrayList<ClassTestBean> classTestList = new ArrayList<ClassTestBean>();
    private int rowRange;
    private Thread t = null;
    NetworkDBServer networkDBServer;
    ClientMonitorSocket clientMonitorSocket;
    int rank;
    int subId;
    private ArrayList<StudentBean> studentBeanList=null;
    private ArrayList<SenderIdBean> senderIdList;
    RankBean rankBean = new RankBean();
    SenderIdBean senderIdBean=null; 
//    String testName;
//    int testId;
    String Standard =null;
    String Division =null;
    String AcademicYear=null;
   
    private ArrayList<ClassBean> classList;
    private ArrayList<AcademicYearBean> academicYearList;
    private ArrayList<DivisionBean> divisionList;
    ArrayList<ClassTestBean> tempclassTestList = new ArrayList<ClassTestBean>();
    int QuestionCount;
    HSSFWorkbook workbook = new HSSFWorkbook();
    HSSFSheet worksheet =null;

    private ArrayList<Object[]> groupList1;
    private ArrayList<Object[]> groupList2;
    private ArrayList<Object[]> groupList3;
    private int subjectFirstId = 0,subjectSecondId = 0,subjectThirdId = 0;
    private ArrayList<GroupBean> groupList;
    
    
    public HomePage() {
        this.setUndecorated(true);
        initComponents();
        initialValues();
        cardLayout = (CardLayout) LeftBodyPanel.getLayout();
        cardLayout.show(LeftBodyPanel, "HomeCard");  
         centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        txtPerQueMark.setText(" ");
        txtWrongQueMark.setText(" ");
        SetComboItem();
//        clearFields();  //for New Student Registration Module (Client Srver)
        lblError.setText(""); //for New Student Registration Module (Client Srver)      
        
        clientMonitorSocket = new ClientMonitorSocket(8088);  //for Start Server Module(ClientServer)     
        networkDBServer = new NetworkDBServer();  //for Start Server Module(ClientServer) 
        String networkPrefix = new NetworkPrefixOperation().getNetworkPrefix();  //for Start Server Module(ClientServer)
        txtNetworkPrefix.setText(networkPrefix);//for Start Server Module(ClientServer)
        javax.swing.table.DefaultTableCellRenderer headerRenderer = new javax.swing.table.DefaultTableCellRenderer();//for Start Server Module(ClientServer)
        headerRenderer.setForeground(new java.awt.Color(29, 9, 44));
        headerRenderer.setFont(headerRenderer.getFont().deriveFont(java.awt.Font.BOLD));
        headerRenderer.setAlignmentX(CENTER_ALIGNMENT);
        headerRenderer.setAlignmentY(CENTER_ALIGNMENT);
        tblConnectedClient.getTableHeader().setDefaultRenderer(headerRenderer);    
        tablesetting1(); //Distribute Test table settting
        resumeTest1();  //Distribute Test table settting
        setAutoRollNo();
        initweightage();
        btn_save.setVisible(false);
        groupList = new MasterGroupOperation().getGroupList();
//        btnDelete.setVisible(false);
    }
                                  
    public HomePage(String printingPaperType) {
        this.setUndecorated(true);
        initComponents();
        initialValues();
        tablesetting1(); //Distribute Test table settting
        resumeTest1(); 
        this.printingPaperType = printingPaperType;
        cardLayout = (CardLayout) LeftBodyPanel.getLayout();
        if(printingPaperType.equals("GroupWise"))
            cardLayout.show(LeftBodyPanel, "QuestionSelectionCard");
        else 
            cardLayout.show(LeftBodyPanel, "SubjectCard");
       
        
    }

    public HomePage(int done) {    
        this.setUndecorated(true);
        initComponents();
        initialValues();
        tablesetting1(); //Distribute Test table settting
        resumeTest1(); 
        cardLayout = (CardLayout) LeftBodyPanel.getLayout();
        tablesetting(); //For Student view Module Table
        resumeTest();  //For Student view Module Table
        cardLayout.show(LeftBodyPanel, "StudentDetail");  
    }
    
    public HomePage(int done,String back) {    
        this.setUndecorated(true);
        initComponents();
        initialValues();
        tablesetting1(); //Distribute Test table settting
        resumeTest1(); 
        cardLayout = (CardLayout) LeftBodyPanel.getLayout();
        tablesetting(); //For Student view Module Table
        resumeTest();  //For Student view Module Table
        cardLayout.show(LeftBodyPanel, "DistributeTest");  
    }
    public HomePage(int done,int back) {    
        this.setUndecorated(true);
        initComponents();
        initialValues();
        tablesetting1(); //Distribute Test table settting
        resumeTest1(); 
        cardLayout = (CardLayout) LeftBodyPanel.getLayout();
        tablesetting(); //For Student view Module Table
        resumeTest();  //For Student view Module Table
        cardLayout.show(LeftBodyPanel, "HomeCard");  
    }
    private void initialValues() {
        String titl = new TitleInfo().getTitle();
        setTitle(titl);
        String lgo = new TitleInfo().getLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        new LatexConversion().setLableText(new JLabel(), "Aniket");
        instituteName = new RegistrationOperation().getLastRegistrationInfoBean().getInstituteName().trim();
        this.addMouseWheelListener(new MouseWheelListener() {
            @Override
            public void mouseWheelMoved(MouseWheelEvent event) {
                if (event.isShiftDown()) {
                    System.err.println("Horizontal " + event.getWheelRotation());
                } else {
                    System.err.println("Vertical " + event.getWheelRotation());
                }
            }
        });
        RdoPatternFirst.setEnabled(true);
        RdoPatternSecond.setEnabled(true);
        RdoPatternThird.setEnabled(false);
        BtnSettingsSave.setEnabled(true);
                
        subjectList = new SubjectOperation().getSubjectsList();
        this.getContentPane().setBackground(new Color(0, 53, 68));
        this.setSize(screenSize.width, (screenSize.height * 95) / 100);
        
        int index = 0;
        for (SubjectBean bean : subjectList) {
            if (index == 0) {
                BtnFirstSubject.setText(bean.getSubjectName());
            } else if (index == 1) {
                BtnSecondSubject.setText(bean.getSubjectName());
            } else if (index == 2) {
                BtnThirdSubject.setText(bean.getSubjectName());
            }
            index++;
        }
        
        studentBeanList=new StudentRegistrationOperation().getStudentInfo();
        Collections.sort(studentBeanList);
//        deleteTests1();
        setAutoRollNo();
        
        RefreshViewStud();
        RefreshRegStudCmb();
        
        testList = new PrintedTestOperation().getPintedTests();
        if(testList != null)
            Collections.sort(testList);
        deleteTests();
        nextPageCallStatus = false;
        chapterViewList = null;
        SettingsBtnGroup.add(RdoPatternFirst);
        SettingsBtnGroup.add(RdoPatternSecond);
        SettingsBtnGroup.add(RdoPatternThird);
        
        StartStopBtnGroup.add(RdoStartServer);
        StartStopBtnGroup.add(RdoStopServer);
        
//        currentPatternIndex = -1;
        setPatternRadio();
        try {
            FileUtils.deleteDirectory(new File("newimages"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        setTableRows();
        setDefalutFileDirectory();
        
        
    }
    
    private void setPatternRadio() {
        try {
            File fl = new File("CurrentPattern.txt");
            if(!fl.exists())
                fl.createNewFile();
            BufferedReader br = new BufferedReader(new FileReader("CurrentPattern.txt"));
                
            String currentLineText = null;
            
//            currentPatternIndex = -1;
            SettingsBtnGroup.clearSelection();
            currentLineText = br.readLine();
            
            System.out.println("Set Pattern ... currentLineText= " +currentLineText);
            
            if(currentLineText != null) {
                try {
                     
                    if(currentLineText.equals("0") || currentLineText.equals("1")|| currentLineText.equals("2"))
                    {
                        currentPatternIndex = Integer.parseInt(currentLineText.trim());
                        if(currentPatternIndex == 0  )
                        {
                            RdoPatternFirst.setSelected(true);
                            new PatternStatusOperation().updateAllPatternStatus();
                            new PatternStatusOperation().updatePatternStatus(1);
                        }
                        else if(currentPatternIndex == 1 )
                        {
                            RdoPatternSecond.setSelected(true);
                            new PatternStatusOperation().updateAllPatternStatus();
                            new PatternStatusOperation().updatePatternStatus(2);
                        }
                        else if(currentPatternIndex == 2 )
                        {
                            RdoPatternThird.setSelected(true);
                            new PatternStatusOperation().updateAllPatternStatus();
                            new PatternStatusOperation().updatePatternStatus(3);
                        }
                    }
                    
                    if(currentLineText.equals("patternIndexValue=0") )
                    {
                        currentPatternIndex=0;
                        RdoPatternFirst.setSelected(true);
                        new PatternStatusOperation().updateAllPatternStatus();
                        new PatternStatusOperation().updatePatternStatus(1);
                    }
                    else if( currentLineText.equals("patternIndexValue=1"))
                    {
                        currentPatternIndex=1;
                        RdoPatternSecond.setSelected(true);
                        new PatternStatusOperation().updateAllPatternStatus();
                        new PatternStatusOperation().updatePatternStatus(2);
                    }
                    else if( currentLineText.equals("patternIndexValue=2"))
                    {
                        currentPatternIndex=2;
                        RdoPatternThird.setSelected(true);
                        new PatternStatusOperation().updateAllPatternStatus();
                        new PatternStatusOperation().updatePatternStatus(3);
                    }
                } catch(Exception ex) {
                    currentPatternIndex = -1;
                } 
            }
            try {
                if (br != null)
                    br.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } 
    }
    
    private void writePathInDirectory() {
        if(patternIndexValue != -1) {
            BufferedWriter bw = null;
            try {
                bw = new BufferedWriter(new FileWriter("CurrentPattern.txt"));
                
                bw.write("patternIndexValue="+patternIndexValue);
            } catch (IOException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (bw != null)
                        bw.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
    
    private void setTableRows1() {
        if(studentBeanList == null) {
            tblStudentDetail.setPreferredSize(new Dimension(523,507));
        } else if(studentBeanList.size() < 21){
            tblStudentDetail.setPreferredSize(new Dimension(523,507));
        }
        DefaultTableModel model = (DefaultTableModel) tblStudentDetail.getModel();
        clearTableRow(model);
        model.addRow(new Object[]{"", "", "", "", "", "", "", ""});
        if(studentBeanList != null && !studentBeanList.isEmpty()) {
            for(int i=0 ;i < studentBeanList.size() ;i++)
                model.addRow(new Object[]{studentBeanList.get(i).getRollno(), studentBeanList.get(i).getFullname(),studentBeanList.get(i).getStudentName(),studentBeanList.get(i).getMobileNo(), studentBeanList.get(i).getStandard(), studentBeanList.get(i).getDivision(), studentBeanList.get(i).getAcademicYear(), studentBeanList.get(i).getPass()});
        }
    }
    
    private void setTableRows() {
        if(testList == null) {
            TableTest.setPreferredSize(new Dimension(523,507));
        } else if(testList.size() < 21){
            TableTest.setPreferredSize(new Dimension(523,507));
        }
        DefaultTableModel model = (DefaultTableModel) TableTest.getModel();
        clearTableRow(model);
        model.addRow(new Object[]{"", "", ""});
        if(testList != null && !testList.isEmpty()) {
            for(int i=0 ;i < testList.size() ;i++)
                model.addRow(new Object[]{(i+1)+" :: "+testList.get(i).getQuesType(),testList.get(i).getTestName(),getStringDateTime(testList.get(i).getTestDateTime())});
        }
    }
    
    private String getStringDateTime(Timestamp timestamp) {
        String returnDate = new SimpleDateFormat("dd-MM-yy hh:mm a").format(timestamp.getTime());
        return returnDate;
    }
    
    private void clearTableRow(DefaultTableModel model) {
        int rowCount = model.getRowCount();
        for(int i=rowCount-1;i>=0;i--)
            model.removeRow(i);        
    }
    void RefreshViewStud()
    {
        //for View Student Detail 
        classList = new ClassOperations().getClassList();
        cmbStandard.removeAllItems();
        cmbStandard.addItem("All");
         if(classList != null) {
            for(ClassBean classBean : classList)             
                cmbStandard.addItem(classBean.getName().trim());
        }
        
        academicYearList = new AcademicYearOperation().getAcademicYearList(); 
        cmbAcademicYear.removeAllItems();
        cmbAcademicYear.addItem("All");
        if(academicYearList != null) {
            for(AcademicYearBean academicYearBean : academicYearList)
                cmbAcademicYear.addItem(academicYearBean.getName().trim());
        } 
        
        divisionList = new DivisionOperation().getDivisionList();
        cmbDivision.removeAllItems();
        cmbDivision.addItem("All");
        if(divisionList != null) {
            for(DivisionBean divisionBean : divisionList)
                cmbDivision.addItem(divisionBean.getName().trim());
        }
    }
    void RefreshRegStudCmb()
    {
        classList = new ClassOperations().getClassList();
        CmbClass.removeAllItems();
        CmbClass.addItem("Select");
        
        if(classList != null) {
            for(ClassBean classBean : classList)
                CmbClass.addItem(classBean.getName().trim());               
        }
        
        academicYearList = new AcademicYearOperation().getAcademicYearList();
        CmbAcademicYear.removeAllItems();
        CmbAcademicYear.addItem("Select");
        
        if(academicYearList != null) {
            for(AcademicYearBean academicYearBean : academicYearList)
                CmbAcademicYear.addItem(academicYearBean.getName().trim());
        }
        
        divisionList = new DivisionOperation().getDivisionList();
        CmbDivision.removeAllItems();
        CmbDivision.addItem("Select");
        
        if(divisionList != null) {
            for(DivisionBean divisionBean : divisionList)
                CmbDivision.addItem(divisionBean.getName().trim());
        }
        
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        SettingsBtnGroup = new javax.swing.ButtonGroup();
        StartStopBtnGroup = new javax.swing.ButtonGroup();
        MainPanelScrollPane = new javax.swing.JScrollPane();
        BodyPanel = new javax.swing.JPanel();
        LeftBodyPanel = new javax.swing.JPanel();
        PanelHome = new javax.swing.JPanel();
        BtnAddQuestion = new javax.swing.JButton();
        BtnCreate = new javax.swing.JButton();
        BtnOMR = new javax.swing.JButton();
        BtnSummary = new javax.swing.JButton();
        BtnOldQuestionPapers = new javax.swing.JButton();
        BtnSyllabus = new javax.swing.JButton();
        BtnAboutUs = new javax.swing.JButton();
        CreatePapers = new javax.swing.JButton();
        Settings = new javax.swing.JButton();
        BtnStartServer = new javax.swing.JButton();
        BtnRegNewStudent = new javax.swing.JButton();
        BtnStudentDetail = new javax.swing.JButton();
        BtnDistributeTest = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        PanelQuestionSelectionFormat = new javax.swing.JPanel();
        BtnUnitWise = new javax.swing.JButton();
        BtnChapterWise = new javax.swing.JButton();
        BtnSubjectWise = new javax.swing.JButton();
        BtnBackSelection = new javax.swing.JButton();
        BtnGroupWise = new javax.swing.JButton();
        PanelOMRSelection = new javax.swing.JPanel();
        BtnBackOmrSelection = new javax.swing.JButton();
        SubPanelOmrSelection = new javax.swing.JPanel();
        LblOmrMessage = new javax.swing.JLabel();
        TxtFldOmrRange = new javax.swing.JTextField();
        BtnCreateOmr = new javax.swing.JButton();
        PanelSubjectSelection = new javax.swing.JPanel();
        BtnBackSubjectSelection = new javax.swing.JButton();
        BtnFirstSubject = new javax.swing.JButton();
        BtnSecondSubject = new javax.swing.JButton();
        BtnThirdSubject = new javax.swing.JButton();
        PanelSettings = new javax.swing.JPanel();
        BtnBackSettingsSelection = new javax.swing.JButton();
        SettingsTab = new javax.swing.JTabbedPane();
        PanelSettingsBody = new javax.swing.JPanel();
        RdoPatternFirst = new javax.swing.JRadioButton();
        RdoPatternSecond = new javax.swing.JRadioButton();
        RdoPatternThird = new javax.swing.JRadioButton();
        BtnSettingsSave = new javax.swing.JButton();
        LblSeetingsHeader = new javax.swing.JLabel();
        SettingsNote = new javax.swing.JLabel();
        LblMorePatternInfo = new javax.swing.JLabel();
        DefaultPathBody = new javax.swing.JPanel();
        BtnSettingsSave1 = new javax.swing.JButton();
        LblPrintSeetingsHeader = new javax.swing.JLabel();
        LblDirectoryPath = new javax.swing.JLabel();
        BtnSettingsSave2 = new javax.swing.JButton();
        SetOnlineSubjectMark = new javax.swing.JPanel();
        LblPrintSeetingsHeader1 = new javax.swing.JLabel();
        txtPerQueMark = new javax.swing.JTextField();
        lblMarkPerQue = new javax.swing.JLabel();
        lblMarkPerWrongQuestion = new javax.swing.JLabel();
        txtWrongQueMark = new javax.swing.JTextField();
        btnSave = new javax.swing.JButton();
        CmboSubject = new javax.swing.JComboBox<String>();
        jLabel1 = new javax.swing.JLabel();
        ResetPanel = new javax.swing.JPanel();
        btnResetUsedQ = new javax.swing.JButton();
        coustom_weight = new javax.swing.JTabbedPane();
        physics = new javax.swing.JScrollPane();
        phy_table = new javax.swing.JTable();
        chemistry = new javax.swing.JScrollPane();
        chem_table = new javax.swing.JTable();
        biology = new javax.swing.JScrollPane();
        bio_table = new javax.swing.JTable();
        btn_save = new javax.swing.JButton();
        PanelRegisterNewStudent = new javax.swing.JPanel();
        BtnBackSelection1 = new javax.swing.JButton();
        MainRegisterPanel = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        txtUserID = new javax.swing.JTextField();
        lblUserId = new javax.swing.JLabel();
        txtPassword = new javax.swing.JPasswordField();
        lblAcademicYear = new javax.swing.JLabel();
        lblRollNo = new javax.swing.JLabel();
        txtConfirmPassword = new javax.swing.JPasswordField();
        txtLoginID = new javax.swing.JTextField();
        lblStandard = new javax.swing.JLabel();
        lblm = new javax.swing.JLabel();
        lblDivision = new javax.swing.JLabel();
        lblCnfrmPassword = new javax.swing.JLabel();
        lblPassword = new javax.swing.JLabel();
        btnClear = new javax.swing.JButton();
        btnRegisterStudent = new javax.swing.JButton();
        lblError = new javax.swing.JLabel();
        lblStudentName = new javax.swing.JLabel();
        txtStudentName = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtMblNo = new javax.swing.JTextField();
        CmbClass = new javax.swing.JComboBox<String>();
        CmbAcademicYear = new javax.swing.JComboBox<String>();
        CmbDivision = new javax.swing.JComboBox<String>();
        jPanel3 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        PanelDistributeTest = new javax.swing.JPanel();
        BtnBackSelection3 = new javax.swing.JButton();
        jPanel10 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        BtnAnalysis = new javax.swing.JButton();
        BtnView = new javax.swing.JButton();
        BtnAutoSendSMS = new javax.swing.JButton();
        ChkSelectAll = new javax.swing.JCheckBox();
        btnSendSMSHistory = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        btnRecheckMarks = new javax.swing.JButton();
        jPanel11 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblSaveTest = new javax.swing.JTable();
        PanelStartServer = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        BtnRefClientList = new javax.swing.JButton();
        lblNetworkPrefix = new javax.swing.JLabel();
        txtNetworkPrefix = new javax.swing.JTextField();
        BtnUpdatePrefix = new javax.swing.JButton();
        BtnLogoutStudent = new javax.swing.JButton();
        RdoStartServer = new javax.swing.JRadioButton();
        RdoStopServer = new javax.swing.JRadioButton();
        lblTitle = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        ScrollPanel = new javax.swing.JScrollPane();
        tblConnectedClient = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        BtnBackSelection4 = new javax.swing.JButton();
        PanelAddYourOwn = new javax.swing.JPanel();
        btnAddQuestions = new javax.swing.JButton();
        btnAddDivision = new javax.swing.JButton();
        btnAddStandard = new javax.swing.JButton();
        BtnBackSubjectSelection1 = new javax.swing.JButton();
        btnAddAcademicYear = new javax.swing.JButton();
        btnAddChapter = new javax.swing.JButton();
        PanelStudentDetail = new javax.swing.JPanel();
        BtnBackSelection2 = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        BtnPrint = new javax.swing.JButton();
        lblStudentDetail = new javax.swing.JLabel();
        btnDelete = new javax.swing.JButton();
        btnModify = new javax.swing.JButton();
        cmbStandard = new javax.swing.JComboBox<String>();
        cmbDivision = new javax.swing.JComboBox<String>();
        cmbAcademicYear = new javax.swing.JComboBox<String>();
        btnImportFile = new javax.swing.JButton();
        btnExport = new javax.swing.JButton();
        btnDeleteAll = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblStudentDetail = new javax.swing.JTable();
        RightBodyPanel = new javax.swing.JPanel();
        TableScrollPane = new javax.swing.JScrollPane();
        TableTest = new javax.swing.JTable();
        LblDelete = new javax.swing.JLabel();
        HeaderPanel = new javax.swing.JPanel();
        LblCompanyName = new javax.swing.JLabel();
        LblProductName = new javax.swing.JLabel();
        BtnMinimizeMainPanel = new javax.swing.JButton();
        BtnCloseMainPanel = new javax.swing.JButton();
        LblWhtasNew = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("MHCET-PCB Server & Question Paper Printing Software 2020-21"); // NOI18N
        setBackground(new java.awt.Color(255, 255, 255));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        MainPanelScrollPane.setBackground(new java.awt.Color(255, 255, 255));
        MainPanelScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        MainPanelScrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        MainPanelScrollPane.setName("MainPanelScrollPane"); // NOI18N
        MainPanelScrollPane.getHorizontalScrollBar().setUnitIncrement(7);

        BodyPanel.setBackground(new java.awt.Color(0, 0, 0));
        BodyPanel.setName("BodyPanel"); // NOI18N

        LeftBodyPanel.setName("LeftBodyPanel"); // NOI18N
        LeftBodyPanel.setLayout(new java.awt.CardLayout());

        PanelHome.setBackground(new java.awt.Color(0, 102, 102));
        PanelHome.setName("PanelHome"); // NOI18N

        BtnAddQuestion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/ui/icons/HomeAddYourOwn.png"))); // NOI18N
        BtnAddQuestion.setBorderPainted(false);
        BtnAddQuestion.setMaximumSize(new java.awt.Dimension(170, 145));
        BtnAddQuestion.setMinimumSize(new java.awt.Dimension(170, 145));
        BtnAddQuestion.setName("BtnAddQuestion"); // NOI18N
        BtnAddQuestion.setPreferredSize(new java.awt.Dimension(170, 145));
        BtnAddQuestion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAddQuestionActionPerformed(evt);
            }
        });

        BtnCreate.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/ui/icons/HomePageCreate.png"))); // NOI18N
        BtnCreate.setBorderPainted(false);
        BtnCreate.setMaximumSize(new java.awt.Dimension(170, 145));
        BtnCreate.setMinimumSize(new java.awt.Dimension(170, 145));
        BtnCreate.setName("BtnCreate"); // NOI18N
        BtnCreate.setPreferredSize(new java.awt.Dimension(170, 145));
        BtnCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCreateActionPerformed(evt);
            }
        });

        BtnOMR.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/ui/icons/HomePageOmr.png"))); // NOI18N
        BtnOMR.setBorderPainted(false);
        BtnOMR.setMaximumSize(new java.awt.Dimension(170, 145));
        BtnOMR.setMinimumSize(new java.awt.Dimension(170, 145));
        BtnOMR.setName("BtnOMR"); // NOI18N
        BtnOMR.setPreferredSize(new java.awt.Dimension(170, 145));
        BtnOMR.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnOMRActionPerformed(evt);
            }
        });

        BtnSummary.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/ui/icons/HomePageSummary.png"))); // NOI18N
        BtnSummary.setBorderPainted(false);
        BtnSummary.setMaximumSize(new java.awt.Dimension(170, 145));
        BtnSummary.setMinimumSize(new java.awt.Dimension(170, 145));
        BtnSummary.setName("BtnSummary"); // NOI18N
        BtnSummary.setPreferredSize(new java.awt.Dimension(170, 145));
        BtnSummary.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSummaryActionPerformed(evt);
            }
        });

        BtnOldQuestionPapers.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/ui/uncommon/icons/HomePageOldPaper.png"))); // NOI18N
        BtnOldQuestionPapers.setBorderPainted(false);
        BtnOldQuestionPapers.setMaximumSize(new java.awt.Dimension(170, 145));
        BtnOldQuestionPapers.setMinimumSize(new java.awt.Dimension(170, 145));
        BtnOldQuestionPapers.setName("BtnOldQuestionPapers"); // NOI18N
        BtnOldQuestionPapers.setPreferredSize(new java.awt.Dimension(170, 145));
        BtnOldQuestionPapers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnOldQuestionPapersActionPerformed(evt);
            }
        });

        BtnSyllabus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/common/images/HomePageSyllabus.png"))); // NOI18N
        BtnSyllabus.setBorderPainted(false);
        BtnSyllabus.setMaximumSize(new java.awt.Dimension(170, 145));
        BtnSyllabus.setMinimumSize(new java.awt.Dimension(170, 145));
        BtnSyllabus.setName("BtnSyllabus"); // NOI18N
        BtnSyllabus.setPreferredSize(new java.awt.Dimension(170, 145));
        BtnSyllabus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSyllabusActionPerformed(evt);
            }
        });

        BtnAboutUs.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/ui/icons/HomePageAboutUs.png"))); // NOI18N
        BtnAboutUs.setBorderPainted(false);
        BtnAboutUs.setMaximumSize(new java.awt.Dimension(170, 145));
        BtnAboutUs.setMinimumSize(new java.awt.Dimension(170, 145));
        BtnAboutUs.setName("BtnAboutUs"); // NOI18N
        BtnAboutUs.setPreferredSize(new java.awt.Dimension(170, 145));
        BtnAboutUs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAboutUsActionPerformed(evt);
            }
        });

        CreatePapers.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/ui/icons/HomePageYearWise1.png"))); // NOI18N
        CreatePapers.setBorderPainted(false);
        CreatePapers.setMaximumSize(new java.awt.Dimension(170, 145));
        CreatePapers.setMinimumSize(new java.awt.Dimension(170, 145));
        CreatePapers.setName("CreatePapers"); // NOI18N
        CreatePapers.setPreferredSize(new java.awt.Dimension(170, 145));
        CreatePapers.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                CreatePapersMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                CreatePapersMouseExited(evt);
            }
        });
        CreatePapers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CreatePapersActionPerformed(evt);
            }
        });

        Settings.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/ui/icons/HomePageSettings.png"))); // NOI18N
        Settings.setBorderPainted(false);
        Settings.setMaximumSize(new java.awt.Dimension(170, 145));
        Settings.setMinimumSize(new java.awt.Dimension(170, 145));
        Settings.setName("Settings"); // NOI18N
        Settings.setPreferredSize(new java.awt.Dimension(170, 145));
        Settings.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                SettingsMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                SettingsMouseExited(evt);
            }
        });
        Settings.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SettingsActionPerformed(evt);
            }
        });

        BtnStartServer.setBackground(new java.awt.Color(0, 0, 0));
        BtnStartServer.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        BtnStartServer.setForeground(new java.awt.Color(255, 255, 255));
        BtnStartServer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/common/images/Start Server.png"))); // NOI18N
        BtnStartServer.setName("BtnStartServer"); // NOI18N
        BtnStartServer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnStartServerActionPerformed(evt);
            }
        });

        BtnRegNewStudent.setBackground(new java.awt.Color(0, 0, 0));
        BtnRegNewStudent.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        BtnRegNewStudent.setForeground(new java.awt.Color(255, 255, 255));
        BtnRegNewStudent.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/common/images/Register Student.png"))); // NOI18N
        BtnRegNewStudent.setName("BtnRegNewStudent"); // NOI18N
        BtnRegNewStudent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRegNewStudentActionPerformed(evt);
            }
        });

        BtnStudentDetail.setBackground(new java.awt.Color(0, 0, 0));
        BtnStudentDetail.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        BtnStudentDetail.setForeground(new java.awt.Color(255, 255, 255));
        BtnStudentDetail.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/common/images/StudentDetail.png"))); // NOI18N
        BtnStudentDetail.setName("BtnStudentDetail"); // NOI18N
        BtnStudentDetail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnStudentDetailActionPerformed(evt);
            }
        });

        BtnDistributeTest.setBackground(new java.awt.Color(0, 0, 0));
        BtnDistributeTest.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        BtnDistributeTest.setForeground(new java.awt.Color(255, 255, 255));
        BtnDistributeTest.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/common/images/Distribute Test.png"))); // NOI18N
        BtnDistributeTest.setName("BtnDistributeTest"); // NOI18N
        BtnDistributeTest.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnDistributeTestActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Microsoft JhengHei", 1, 16)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 0, 0));
        jLabel4.setText("SMS Setting");
        jLabel4.setName("jLabel4"); // NOI18N
        jLabel4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel4MouseClicked(evt);
            }
        });
        jLabel4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jLabel4KeyPressed(evt);
            }
        });

        javax.swing.GroupLayout PanelHomeLayout = new javax.swing.GroupLayout(PanelHome);
        PanelHome.setLayout(PanelHomeLayout);
        PanelHomeLayout.setHorizontalGroup(
            PanelHomeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelHomeLayout.createSequentialGroup()
                .addContainerGap(45, Short.MAX_VALUE)
                .addGroup(PanelHomeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel4)
                    .addGroup(PanelHomeLayout.createSequentialGroup()
                        .addGroup(PanelHomeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(PanelHomeLayout.createSequentialGroup()
                                .addComponent(BtnCreate, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(BtnOMR, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(BtnAddQuestion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(PanelHomeLayout.createSequentialGroup()
                                .addComponent(BtnSummary, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(BtnOldQuestionPapers, javax.swing.GroupLayout.PREFERRED_SIZE, 358, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(CreatePapers, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(PanelHomeLayout.createSequentialGroup()
                                .addComponent(BtnSyllabus, javax.swing.GroupLayout.PREFERRED_SIZE, 358, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(BtnAboutUs, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(Settings, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(PanelHomeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(BtnDistributeTest, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(BtnStartServer, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(BtnRegNewStudent, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(BtnStudentDetail, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(47, Short.MAX_VALUE))
        );
        PanelHomeLayout.setVerticalGroup(
            PanelHomeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelHomeLayout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(PanelHomeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(PanelHomeLayout.createSequentialGroup()
                        .addComponent(BtnStartServer, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(BtnDistributeTest, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BtnRegNewStudent, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(BtnStudentDetail, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                    .addGroup(PanelHomeLayout.createSequentialGroup()
                        .addGroup(PanelHomeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(BtnOMR, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(BtnCreate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(BtnAddQuestion, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(PanelHomeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(BtnOldQuestionPapers, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(BtnSummary, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(CreatePapers, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(PanelHomeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(BtnSyllabus, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(BtnAboutUs, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Settings, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(527, Short.MAX_VALUE))
        );

        LeftBodyPanel.add(PanelHome, "HomeCard");

        PanelQuestionSelectionFormat.setBackground(new java.awt.Color(0, 102, 102));
        PanelQuestionSelectionFormat.setName("PanelQuestionSelectionFormat"); // NOI18N

        BtnUnitWise.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/ui/icons/HomePageUnitWise.png"))); // NOI18N
        BtnUnitWise.setBorderPainted(false);
        BtnUnitWise.setName("BtnUnitWise"); // NOI18N
        BtnUnitWise.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnUnitWiseActionPerformed(evt);
            }
        });

        BtnChapterWise.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/ui/icons/HomePageChapterWise.png"))); // NOI18N
        BtnChapterWise.setBorderPainted(false);
        BtnChapterWise.setName("BtnChapterWise"); // NOI18N
        BtnChapterWise.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnChapterWiseActionPerformed(evt);
            }
        });

        BtnSubjectWise.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/ui/icons/HomePageSubjectWise.png"))); // NOI18N
        BtnSubjectWise.setBorderPainted(false);
        BtnSubjectWise.setName("BtnSubjectWise"); // NOI18N
        BtnSubjectWise.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSubjectWiseActionPerformed(evt);
            }
        });

        BtnBackSelection.setBackground(new java.awt.Color(11, 45, 55));
        BtnBackSelection.setFont(new java.awt.Font("Corbel", 0, 16)); // NOI18N
        BtnBackSelection.setForeground(new java.awt.Color(255, 255, 255));
        BtnBackSelection.setText("Back");
        BtnBackSelection.setBorderPainted(false);
        BtnBackSelection.setName("BtnBackSelection"); // NOI18N
        BtnBackSelection.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnBackSelectionMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnBackSelectionMouseExited(evt);
            }
        });
        BtnBackSelection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBackSelectionActionPerformed(evt);
            }
        });

        BtnGroupWise.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/ui/icons/HomePageGroupWise.png"))); // NOI18N
        BtnGroupWise.setBorderPainted(false);
        BtnGroupWise.setName("BtnGroupWise"); // NOI18N
        BtnGroupWise.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnGroupWiseActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PanelQuestionSelectionFormatLayout = new javax.swing.GroupLayout(PanelQuestionSelectionFormat);
        PanelQuestionSelectionFormat.setLayout(PanelQuestionSelectionFormatLayout);
        PanelQuestionSelectionFormatLayout.setHorizontalGroup(
            PanelQuestionSelectionFormatLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelQuestionSelectionFormatLayout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(BtnBackSelection, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 153, Short.MAX_VALUE)
                .addGroup(PanelQuestionSelectionFormatLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(BtnChapterWise, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BtnSubjectWise, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(45, 45, 45)
                .addGroup(PanelQuestionSelectionFormatLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(BtnGroupWise, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BtnUnitWise, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(287, Short.MAX_VALUE))
        );
        PanelQuestionSelectionFormatLayout.setVerticalGroup(
            PanelQuestionSelectionFormatLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelQuestionSelectionFormatLayout.createSequentialGroup()
                .addGroup(PanelQuestionSelectionFormatLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PanelQuestionSelectionFormatLayout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addComponent(BtnBackSelection, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelQuestionSelectionFormatLayout.createSequentialGroup()
                        .addGap(63, 63, 63)
                        .addGroup(PanelQuestionSelectionFormatLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(BtnChapterWise, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(BtnUnitWise, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(39, 39, 39)
                .addGroup(PanelQuestionSelectionFormatLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(BtnSubjectWise, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BtnGroupWise, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(498, Short.MAX_VALUE))
        );

        LeftBodyPanel.add(PanelQuestionSelectionFormat, "QuestionSelectionCard");

        PanelOMRSelection.setBackground(new java.awt.Color(0, 102, 102));
        PanelOMRSelection.setName("PanelOMRSelection"); // NOI18N

        BtnBackOmrSelection.setBackground(new java.awt.Color(11, 45, 55));
        BtnBackOmrSelection.setFont(new java.awt.Font("Corbel", 0, 16)); // NOI18N
        BtnBackOmrSelection.setForeground(new java.awt.Color(255, 255, 255));
        BtnBackOmrSelection.setText("Back");
        BtnBackOmrSelection.setBorderPainted(false);
        BtnBackOmrSelection.setName("BtnBackOmrSelection"); // NOI18N
        BtnBackOmrSelection.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnBackOmrSelectionMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnBackOmrSelectionMouseExited(evt);
            }
        });
        BtnBackOmrSelection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBackOmrSelectionActionPerformed(evt);
            }
        });

        SubPanelOmrSelection.setBackground(new java.awt.Color(11, 45, 55));
        SubPanelOmrSelection.setName("SubPanelOmrSelection"); // NOI18N

        LblOmrMessage.setBackground(new java.awt.Color(0, 0, 51));
        LblOmrMessage.setFont(new java.awt.Font("Corbel", 0, 24)); // NOI18N
        LblOmrMessage.setForeground(new java.awt.Color(255, 255, 255));
        LblOmrMessage.setText("Enter the number of questions");
        LblOmrMessage.setName("LblOmrMessage"); // NOI18N

        TxtFldOmrRange.setBackground(new java.awt.Color(11, 45, 55));
        TxtFldOmrRange.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 14)); // NOI18N
        TxtFldOmrRange.setForeground(new java.awt.Color(255, 255, 255));
        TxtFldOmrRange.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TxtFldOmrRange.setText("200");
        TxtFldOmrRange.setName("TxtFldOmrRange"); // NOI18N
        TxtFldOmrRange.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TxtFldOmrRangeKeyPressed(evt);
            }
        });

        BtnCreateOmr.setBackground(new java.awt.Color(208, 87, 96));
        BtnCreateOmr.setFont(new java.awt.Font("Corbel", 0, 18)); // NOI18N
        BtnCreateOmr.setForeground(new java.awt.Color(255, 255, 255));
        BtnCreateOmr.setText("Create");
        BtnCreateOmr.setBorderPainted(false);
        BtnCreateOmr.setName("BtnCreateOmr"); // NOI18N
        BtnCreateOmr.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCreateOmrActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout SubPanelOmrSelectionLayout = new javax.swing.GroupLayout(SubPanelOmrSelection);
        SubPanelOmrSelection.setLayout(SubPanelOmrSelectionLayout);
        SubPanelOmrSelectionLayout.setHorizontalGroup(
            SubPanelOmrSelectionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SubPanelOmrSelectionLayout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(SubPanelOmrSelectionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(LblOmrMessage, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 322, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, SubPanelOmrSelectionLayout.createSequentialGroup()
                        .addComponent(TxtFldOmrRange, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(BtnCreateOmr, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(43, Short.MAX_VALUE))
        );
        SubPanelOmrSelectionLayout.setVerticalGroup(
            SubPanelOmrSelectionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SubPanelOmrSelectionLayout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addComponent(LblOmrMessage, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addGroup(SubPanelOmrSelectionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(BtnCreateOmr, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(TxtFldOmrRange, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(34, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout PanelOMRSelectionLayout = new javax.swing.GroupLayout(PanelOMRSelection);
        PanelOMRSelection.setLayout(PanelOMRSelectionLayout);
        PanelOMRSelectionLayout.setHorizontalGroup(
            PanelOMRSelectionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelOMRSelectionLayout.createSequentialGroup()
                .addGap(121, 121, 121)
                .addComponent(BtnBackOmrSelection, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(791, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelOMRSelectionLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(SubPanelOmrSelection, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(121, 121, 121))
        );
        PanelOMRSelectionLayout.setVerticalGroup(
            PanelOMRSelectionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelOMRSelectionLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(BtnBackOmrSelection, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(84, 84, 84)
                .addComponent(SubPanelOmrSelection, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        LeftBodyPanel.add(PanelOMRSelection, "OmrCard");

        PanelSubjectSelection.setBackground(new java.awt.Color(0, 102, 102));
        PanelSubjectSelection.setName("PanelSubjectSelection"); // NOI18N

        BtnBackSubjectSelection.setBackground(new java.awt.Color(11, 45, 55));
        BtnBackSubjectSelection.setFont(new java.awt.Font("Corbel", 0, 16)); // NOI18N
        BtnBackSubjectSelection.setForeground(new java.awt.Color(255, 255, 255));
        BtnBackSubjectSelection.setText("Back");
        BtnBackSubjectSelection.setBorderPainted(false);
        BtnBackSubjectSelection.setName("BtnBackSubjectSelection"); // NOI18N
        BtnBackSubjectSelection.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnBackSubjectSelectionMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnBackSubjectSelectionMouseExited(evt);
            }
        });
        BtnBackSubjectSelection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBackSubjectSelectionActionPerformed(evt);
            }
        });

        BtnFirstSubject.setBackground(new java.awt.Color(131, 164, 83));
        BtnFirstSubject.setFont(new java.awt.Font("Corbel", 0, 24)); // NOI18N
        BtnFirstSubject.setForeground(new java.awt.Color(255, 255, 255));
        BtnFirstSubject.setText("Physics");
        BtnFirstSubject.setBorderPainted(false);
        BtnFirstSubject.setName("BtnFirstSubject"); // NOI18N
        BtnFirstSubject.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnFirstSubjectMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnFirstSubjectMouseExited(evt);
            }
        });
        BtnFirstSubject.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnFirstSubjectActionPerformed(evt);
            }
        });

        BtnSecondSubject.setBackground(new java.awt.Color(225, 183, 109));
        BtnSecondSubject.setFont(new java.awt.Font("Corbel", 0, 24)); // NOI18N
        BtnSecondSubject.setForeground(new java.awt.Color(255, 255, 255));
        BtnSecondSubject.setText("Chemistry");
        BtnSecondSubject.setBorderPainted(false);
        BtnSecondSubject.setName("BtnSecondSubject"); // NOI18N
        BtnSecondSubject.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnSecondSubjectMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnSecondSubjectMouseExited(evt);
            }
        });
        BtnSecondSubject.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSecondSubjectActionPerformed(evt);
            }
        });

        BtnThirdSubject.setBackground(new java.awt.Color(208, 87, 96));
        BtnThirdSubject.setFont(new java.awt.Font("Corbel", 0, 24)); // NOI18N
        BtnThirdSubject.setForeground(new java.awt.Color(255, 255, 255));
        BtnThirdSubject.setText("Mathematics");
        BtnThirdSubject.setBorderPainted(false);
        BtnThirdSubject.setName("BtnThirdSubject"); // NOI18N
        BtnThirdSubject.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnThirdSubjectMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnThirdSubjectMouseExited(evt);
            }
        });
        BtnThirdSubject.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnThirdSubjectActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PanelSubjectSelectionLayout = new javax.swing.GroupLayout(PanelSubjectSelection);
        PanelSubjectSelection.setLayout(PanelSubjectSelectionLayout);
        PanelSubjectSelectionLayout.setHorizontalGroup(
            PanelSubjectSelectionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelSubjectSelectionLayout.createSequentialGroup()
                .addGap(121, 121, 121)
                .addComponent(BtnBackSubjectSelection, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(791, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelSubjectSelectionLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(PanelSubjectSelectionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(BtnThirdSubject, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BtnSecondSubject, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BtnFirstSubject, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(350, 350, 350))
        );
        PanelSubjectSelectionLayout.setVerticalGroup(
            PanelSubjectSelectionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelSubjectSelectionLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(BtnBackSubjectSelection, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(56, 56, 56)
                .addComponent(BtnFirstSubject, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(39, 39, 39)
                .addComponent(BtnSecondSubject, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(44, 44, 44)
                .addComponent(BtnThirdSubject, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(635, Short.MAX_VALUE))
        );

        LeftBodyPanel.add(PanelSubjectSelection, "SubjectCard");

        PanelSettings.setBackground(new java.awt.Color(0, 102, 102));
        PanelSettings.setName("PanelSettings"); // NOI18N

        BtnBackSettingsSelection.setBackground(new java.awt.Color(11, 45, 55));
        BtnBackSettingsSelection.setFont(new java.awt.Font("Corbel", 0, 16)); // NOI18N
        BtnBackSettingsSelection.setForeground(new java.awt.Color(255, 255, 255));
        BtnBackSettingsSelection.setText("Back");
        BtnBackSettingsSelection.setBorderPainted(false);
        BtnBackSettingsSelection.setName("BtnBackSettingsSelection"); // NOI18N
        BtnBackSettingsSelection.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnBackSettingsSelectionMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnBackSettingsSelectionMouseExited(evt);
            }
        });
        BtnBackSettingsSelection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBackSettingsSelectionActionPerformed(evt);
            }
        });

        SettingsTab.setName("SettingsTab"); // NOI18N
        SettingsTab.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                SettingsTabStateChanged(evt);
            }
        });

        PanelSettingsBody.setBackground(new java.awt.Color(11, 45, 55));
        PanelSettingsBody.setName("PanelSettingsBody"); // NOI18N

        RdoPatternFirst.setBackground(new java.awt.Color(153, 153, 255));
        RdoPatternFirst.setFont(new java.awt.Font("Calibri", 0, 18)); // NOI18N
        RdoPatternFirst.setText("Pattern-1");
        RdoPatternFirst.setName("RdoPatternFirst"); // NOI18N
        RdoPatternFirst.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RdoPatternFirstActionPerformed(evt);
            }
        });

        RdoPatternSecond.setBackground(new java.awt.Color(153, 153, 255));
        RdoPatternSecond.setFont(new java.awt.Font("Calibri", 0, 18)); // NOI18N
        RdoPatternSecond.setText("Pattern-2");
        RdoPatternSecond.setName("RdoPatternSecond"); // NOI18N
        RdoPatternSecond.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RdoPatternSecondActionPerformed(evt);
            }
        });

        RdoPatternThird.setBackground(new java.awt.Color(153, 153, 255));
        RdoPatternThird.setFont(new java.awt.Font("Calibri", 0, 18)); // NOI18N
        RdoPatternThird.setText("Pattern-3");
        RdoPatternThird.setName("RdoPatternThird"); // NOI18N

        BtnSettingsSave.setBackground(new java.awt.Color(208, 87, 96));
        BtnSettingsSave.setFont(new java.awt.Font("Calibri", 0, 18)); // NOI18N
        BtnSettingsSave.setForeground(new java.awt.Color(255, 255, 255));
        BtnSettingsSave.setText("Save");
        BtnSettingsSave.setBorder(null);
        BtnSettingsSave.setBorderPainted(false);
        BtnSettingsSave.setName("BtnSettingsSave"); // NOI18N
        BtnSettingsSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSettingsSaveActionPerformed(evt);
            }
        });

        LblSeetingsHeader.setFont(new java.awt.Font("Calibri", 0, 18)); // NOI18N
        LblSeetingsHeader.setForeground(new java.awt.Color(255, 255, 255));
        LblSeetingsHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblSeetingsHeader.setText("Pattern Selection");
        LblSeetingsHeader.setName("LblSeetingsHeader"); // NOI18N

        SettingsNote.setForeground(new java.awt.Color(255, 255, 102));
        SettingsNote.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        SettingsNote.setText("(Note: Change to this option will harmful.)");
        SettingsNote.setName("SettingsNote"); // NOI18N

        LblMorePatternInfo.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        LblMorePatternInfo.setForeground(new java.awt.Color(255, 255, 102));
        LblMorePatternInfo.setText("More About Pattern");
        LblMorePatternInfo.setName("LblMorePatternInfo"); // NOI18N
        LblMorePatternInfo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LblMorePatternInfoMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout PanelSettingsBodyLayout = new javax.swing.GroupLayout(PanelSettingsBody);
        PanelSettingsBody.setLayout(PanelSettingsBodyLayout);
        PanelSettingsBodyLayout.setHorizontalGroup(
            PanelSettingsBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(LblSeetingsHeader, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(PanelSettingsBodyLayout.createSequentialGroup()
                .addGap(58, 58, 58)
                .addGroup(PanelSettingsBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(PanelSettingsBodyLayout.createSequentialGroup()
                        .addComponent(RdoPatternFirst, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(RdoPatternSecond, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(20, 20, 20)
                        .addComponent(RdoPatternThird, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(PanelSettingsBodyLayout.createSequentialGroup()
                        .addComponent(LblMorePatternInfo)
                        .addGap(150, 150, 150)
                        .addComponent(BtnSettingsSave, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(SettingsNote, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        PanelSettingsBodyLayout.setVerticalGroup(
            PanelSettingsBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelSettingsBodyLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(LblSeetingsHeader, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(SettingsNote, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(PanelSettingsBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(RdoPatternFirst)
                    .addComponent(RdoPatternSecond)
                    .addComponent(RdoPatternThird))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 163, Short.MAX_VALUE)
                .addGroup(PanelSettingsBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(BtnSettingsSave, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                    .addComponent(LblMorePatternInfo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(30, 30, 30))
        );

        SettingsTab.addTab("Pattern", PanelSettingsBody);

        DefaultPathBody.setBackground(new java.awt.Color(11, 45, 55));
        DefaultPathBody.setName("DefaultPathBody"); // NOI18N

        BtnSettingsSave1.setBackground(new java.awt.Color(208, 87, 96));
        BtnSettingsSave1.setFont(new java.awt.Font("Calibri", 0, 18)); // NOI18N
        BtnSettingsSave1.setForeground(new java.awt.Color(255, 255, 255));
        BtnSettingsSave1.setText("Change Directory");
        BtnSettingsSave1.setBorder(null);
        BtnSettingsSave1.setBorderPainted(false);
        BtnSettingsSave1.setName("BtnSettingsSave1"); // NOI18N
        BtnSettingsSave1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSettingsSave1ActionPerformed(evt);
            }
        });

        LblPrintSeetingsHeader.setFont(new java.awt.Font("Calibri", 0, 18)); // NOI18N
        LblPrintSeetingsHeader.setForeground(new java.awt.Color(255, 255, 255));
        LblPrintSeetingsHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblPrintSeetingsHeader.setText("Set Default Path for Printing");
        LblPrintSeetingsHeader.setName("LblPrintSeetingsHeader"); // NOI18N

        LblDirectoryPath.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 14)); // NOI18N
        LblDirectoryPath.setForeground(new java.awt.Color(255, 255, 255));
        LblDirectoryPath.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LblDirectoryPath.setText(" Current Directory :");
        LblDirectoryPath.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        LblDirectoryPath.setName("LblDirectoryPath"); // NOI18N

        BtnSettingsSave2.setBackground(new java.awt.Color(208, 87, 96));
        BtnSettingsSave2.setFont(new java.awt.Font("Calibri", 0, 18)); // NOI18N
        BtnSettingsSave2.setForeground(new java.awt.Color(255, 255, 255));
        BtnSettingsSave2.setText("Save");
        BtnSettingsSave2.setBorder(null);
        BtnSettingsSave2.setBorderPainted(false);
        BtnSettingsSave2.setName("BtnSettingsSave2"); // NOI18N
        BtnSettingsSave2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSettingsSave2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout DefaultPathBodyLayout = new javax.swing.GroupLayout(DefaultPathBody);
        DefaultPathBody.setLayout(DefaultPathBodyLayout);
        DefaultPathBodyLayout.setHorizontalGroup(
            DefaultPathBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(LblPrintSeetingsHeader, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, DefaultPathBodyLayout.createSequentialGroup()
                .addContainerGap(45, Short.MAX_VALUE)
                .addGroup(DefaultPathBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(LblDirectoryPath, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 439, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, DefaultPathBodyLayout.createSequentialGroup()
                        .addComponent(BtnSettingsSave1, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(BtnSettingsSave2, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        DefaultPathBodyLayout.setVerticalGroup(
            DefaultPathBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, DefaultPathBodyLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(LblPrintSeetingsHeader, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(46, 46, 46)
                .addComponent(LblDirectoryPath, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 174, Short.MAX_VALUE)
                .addGroup(DefaultPathBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BtnSettingsSave1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BtnSettingsSave2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30))
        );

        SettingsTab.addTab("Print Path", DefaultPathBody);

        SetOnlineSubjectMark.setBackground(new java.awt.Color(11, 45, 55));
        SetOnlineSubjectMark.setName("SetOnlineSubjectMark"); // NOI18N

        LblPrintSeetingsHeader1.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        LblPrintSeetingsHeader1.setForeground(new java.awt.Color(255, 255, 255));
        LblPrintSeetingsHeader1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblPrintSeetingsHeader1.setText("Set Mark");
        LblPrintSeetingsHeader1.setName("LblPrintSeetingsHeader1"); // NOI18N

        txtPerQueMark.setName("txtPerQueMark"); // NOI18N

        lblMarkPerQue.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        lblMarkPerQue.setForeground(new java.awt.Color(255, 255, 255));
        lblMarkPerQue.setText("MARK PER QUESTION :");
        lblMarkPerQue.setName("lblMarkPerQue"); // NOI18N

        lblMarkPerWrongQuestion.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        lblMarkPerWrongQuestion.setForeground(new java.awt.Color(255, 255, 255));
        lblMarkPerWrongQuestion.setText("MARKS PER WRONG QUESTION :");
        lblMarkPerWrongQuestion.setName("lblMarkPerWrongQuestion"); // NOI18N

        txtWrongQueMark.setName("txtWrongQueMark"); // NOI18N

        btnSave.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnSave.setText("Save");
        btnSave.setName("btnSave"); // NOI18N
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        CmboSubject.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        CmboSubject.setName("CmboSubject"); // NOI18N
        CmboSubject.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                CmboSubjectItemStateChanged(evt);
            }
        });
        CmboSubject.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CmboSubjectActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("SUBJECT NAME :");
        jLabel1.setName("jLabel1"); // NOI18N

        javax.swing.GroupLayout SetOnlineSubjectMarkLayout = new javax.swing.GroupLayout(SetOnlineSubjectMark);
        SetOnlineSubjectMark.setLayout(SetOnlineSubjectMarkLayout);
        SetOnlineSubjectMarkLayout.setHorizontalGroup(
            SetOnlineSubjectMarkLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(LblPrintSeetingsHeader1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(SetOnlineSubjectMarkLayout.createSequentialGroup()
                .addGroup(SetOnlineSubjectMarkLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(SetOnlineSubjectMarkLayout.createSequentialGroup()
                        .addGap(353, 353, 353)
                        .addComponent(btnSave))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, SetOnlineSubjectMarkLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(SetOnlineSubjectMarkLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblMarkPerWrongQuestion)
                            .addComponent(lblMarkPerQue)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(36, 36, 36)
                        .addGroup(SetOnlineSubjectMarkLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtPerQueMark)
                            .addComponent(txtWrongQueMark)
                            .addComponent(CmboSubject, 0, 131, Short.MAX_VALUE))))
                .addContainerGap(82, Short.MAX_VALUE))
        );
        SetOnlineSubjectMarkLayout.setVerticalGroup(
            SetOnlineSubjectMarkLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, SetOnlineSubjectMarkLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(LblPrintSeetingsHeader1, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 118, Short.MAX_VALUE)
                .addGroup(SetOnlineSubjectMarkLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(CmboSubject, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addGroup(SetOnlineSubjectMarkLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblMarkPerQue)
                    .addComponent(txtPerQueMark, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(25, 25, 25)
                .addGroup(SetOnlineSubjectMarkLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblMarkPerWrongQuestion)
                    .addComponent(txtWrongQueMark, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addComponent(btnSave)
                .addGap(24, 24, 24))
        );

        SettingsTab.addTab("Set Mark", SetOnlineSubjectMark);

        ResetPanel.setBackground(new java.awt.Color(11, 45, 55));
        ResetPanel.setName("ResetPanel"); // NOI18N

        btnResetUsedQ.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnResetUsedQ.setText("Reset Used Questions");
        btnResetUsedQ.setName("btnResetUsedQ"); // NOI18N
        btnResetUsedQ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResetUsedQActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout ResetPanelLayout = new javax.swing.GroupLayout(ResetPanel);
        ResetPanel.setLayout(ResetPanelLayout);
        ResetPanelLayout.setHorizontalGroup(
            ResetPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ResetPanelLayout.createSequentialGroup()
                .addGap(170, 170, 170)
                .addComponent(btnResetUsedQ)
                .addContainerGap(171, Short.MAX_VALUE))
        );
        ResetPanelLayout.setVerticalGroup(
            ResetPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ResetPanelLayout.createSequentialGroup()
                .addGap(90, 90, 90)
                .addComponent(btnResetUsedQ, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(225, Short.MAX_VALUE))
        );

        SettingsTab.addTab("Reset", ResetPanel);

        coustom_weight.setName("coustom_weight"); // NOI18N
        coustom_weight.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                coustom_weightStateChanged(evt);
            }
        });
        coustom_weight.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                coustom_weightFocusGained(evt);
            }
        });
        coustom_weight.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                coustom_weightMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                coustom_weightMouseEntered(evt);
            }
        });

        physics.setName("physics"); // NOI18N

        phy_table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Chapter_Id", "Chapter_Name", "Weightage"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Object.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                true, true, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        phy_table.setName("phy_table"); // NOI18N
        phy_table.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                phy_tableMouseClicked(evt);
            }
        });
        physics.setViewportView(phy_table);

        coustom_weight.addTab("Physics", physics);

        chemistry.setName("chemistry"); // NOI18N

        chem_table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Chapter_Id", "Chapter_Name", "Weightage"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Object.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                true, true, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        chem_table.setName("chem_table"); // NOI18N
        chem_table.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                chem_tableMouseClicked(evt);
            }
        });
        chemistry.setViewportView(chem_table);

        coustom_weight.addTab("Chemistry", chemistry);

        biology.setName("biology"); // NOI18N

        bio_table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Chapter_Id", "Chapter_Name", "Weightage"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Object.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                true, true, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        bio_table.setName("bio_table"); // NOI18N
        bio_table.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bio_tableMouseClicked(evt);
            }
        });
        biology.setViewportView(bio_table);

        coustom_weight.addTab("Biology", biology);

        SettingsTab.addTab("Customize Weightage", coustom_weight);

        btn_save.setBackground(new java.awt.Color(11, 45, 55));
        btn_save.setFont(new java.awt.Font("Corbel", 0, 16)); // NOI18N
        btn_save.setForeground(new java.awt.Color(255, 255, 255));
        btn_save.setText("Save Settings");
        btn_save.setName("btn_save"); // NOI18N
        btn_save.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                btn_saveFocusGained(evt);
            }
        });
        btn_save.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btn_saveMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btn_saveMouseExited(evt);
            }
        });
        btn_save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_saveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PanelSettingsLayout = new javax.swing.GroupLayout(PanelSettings);
        PanelSettings.setLayout(PanelSettingsLayout);
        PanelSettingsLayout.setHorizontalGroup(
            PanelSettingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelSettingsLayout.createSequentialGroup()
                .addGap(121, 121, 121)
                .addComponent(BtnBackSettingsSelection, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(791, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelSettingsLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(PanelSettingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelSettingsLayout.createSequentialGroup()
                        .addComponent(SettingsTab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(218, 218, 218))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelSettingsLayout.createSequentialGroup()
                        .addComponent(btn_save)
                        .addGap(417, 417, 417))))
        );
        PanelSettingsLayout.setVerticalGroup(
            PanelSettingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelSettingsLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(BtnBackSettingsSelection, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37)
                .addComponent(SettingsTab, javax.swing.GroupLayout.PREFERRED_SIZE, 383, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_save, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(498, Short.MAX_VALUE))
        );

        SettingsTab.getAccessibleContext().setAccessibleName("Tab");

        LeftBodyPanel.add(PanelSettings, "SettingsCard");

        PanelRegisterNewStudent.setBackground(new java.awt.Color(0, 102, 102));
        PanelRegisterNewStudent.setName("PanelRegisterNewStudent"); // NOI18N

        BtnBackSelection1.setBackground(new java.awt.Color(11, 45, 55));
        BtnBackSelection1.setFont(new java.awt.Font("Corbel", 0, 16)); // NOI18N
        BtnBackSelection1.setForeground(new java.awt.Color(255, 255, 255));
        BtnBackSelection1.setText("Back");
        BtnBackSelection1.setBorderPainted(false);
        BtnBackSelection1.setName("BtnBackSelection1"); // NOI18N
        BtnBackSelection1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnBackSelection1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnBackSelection1MouseExited(evt);
            }
        });
        BtnBackSelection1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBackSelection1ActionPerformed(evt);
            }
        });

        MainRegisterPanel.setBackground(new java.awt.Color(0, 0, 0));
        MainRegisterPanel.setName("MainRegisterPanel"); // NOI18N

        jPanel2.setBackground(new java.awt.Color(29, 9, 44));
        jPanel2.setName("jPanel2"); // NOI18N

        txtUserID.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        txtUserID.setName("txtUserID"); // NOI18N
        txtUserID.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtUserIDKeyTyped(evt);
            }
        });

        lblUserId.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        lblUserId.setForeground(new java.awt.Color(255, 255, 255));
        lblUserId.setText("Login Id:");
        lblUserId.setName("lblUserId"); // NOI18N

        txtPassword.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        txtPassword.setToolTipText("");
        txtPassword.setName("txtPassword"); // NOI18N

        lblAcademicYear.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        lblAcademicYear.setForeground(new java.awt.Color(255, 255, 255));
        lblAcademicYear.setText("Academic Year :");
        lblAcademicYear.setName("lblAcademicYear"); // NOI18N

        lblRollNo.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        lblRollNo.setForeground(new java.awt.Color(255, 255, 255));
        lblRollNo.setText("User Id :");
        lblRollNo.setName("lblRollNo"); // NOI18N

        txtConfirmPassword.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        txtConfirmPassword.setToolTipText("");
        txtConfirmPassword.setName("txtConfirmPassword"); // NOI18N

        txtLoginID.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        txtLoginID.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtLoginID.setEnabled(false);
        txtLoginID.setName("txtLoginID"); // NOI18N

        lblStandard.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        lblStandard.setForeground(new java.awt.Color(255, 255, 255));
        lblStandard.setText("Standard :");
        lblStandard.setName("lblStandard"); // NOI18N

        lblm.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        lblm.setForeground(new java.awt.Color(255, 255, 255));
        lblm.setText("Middle Name :");
        lblm.setName("lblm"); // NOI18N

        lblDivision.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        lblDivision.setForeground(new java.awt.Color(255, 255, 255));
        lblDivision.setText("Division :");
        lblDivision.setName("lblDivision"); // NOI18N

        lblCnfrmPassword.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        lblCnfrmPassword.setForeground(new java.awt.Color(255, 255, 255));
        lblCnfrmPassword.setText("Confirm Password :");
        lblCnfrmPassword.setName("lblCnfrmPassword"); // NOI18N

        lblPassword.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        lblPassword.setForeground(new java.awt.Color(255, 255, 255));
        lblPassword.setText("Password :");
        lblPassword.setName("lblPassword"); // NOI18N

        btnClear.setBackground(new java.awt.Color(255, 255, 255));
        btnClear.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        btnClear.setForeground(new java.awt.Color(29, 9, 44));
        btnClear.setText("Clear");
        btnClear.setName("btnClear"); // NOI18N
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        btnRegisterStudent.setBackground(new java.awt.Color(255, 255, 255));
        btnRegisterStudent.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        btnRegisterStudent.setForeground(new java.awt.Color(29, 9, 44));
        btnRegisterStudent.setText("Register Student");
        btnRegisterStudent.setName("btnRegisterStudent"); // NOI18N
        btnRegisterStudent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegisterStudentActionPerformed(evt);
            }
        });

        lblError.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        lblError.setForeground(new java.awt.Color(255, 0, 0));
        lblError.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblError.setText("jLabel12");
        lblError.setName("lblError"); // NOI18N

        lblStudentName.setFont(new java.awt.Font("Segoe UI Semilight", 1, 14)); // NOI18N
        lblStudentName.setForeground(new java.awt.Color(255, 255, 255));
        lblStudentName.setText("Student Name:");
        lblStudentName.setName("lblStudentName"); // NOI18N

        txtStudentName.setName("txtStudentName"); // NOI18N
        txtStudentName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtStudentNameKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtStudentNameKeyTyped(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Mobile No.  :");
        jLabel3.setName("jLabel3"); // NOI18N

        txtMblNo.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        txtMblNo.setName("txtMblNo"); // NOI18N
        txtMblNo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtMblNoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtMblNoKeyReleased(evt);
            }
        });

        CmbClass.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 12)); // NOI18N
        CmbClass.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        CmbClass.setName("CmbClass"); // NOI18N

        CmbAcademicYear.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 12)); // NOI18N
        CmbAcademicYear.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        CmbAcademicYear.setName("CmbAcademicYear"); // NOI18N

        CmbDivision.setFont(new java.awt.Font("Microsoft JhengHei UI", 1, 12)); // NOI18N
        CmbDivision.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        CmbDivision.setName("CmbDivision"); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(lblm, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(btnRegisterStudent)
                        .addGap(122, 122, 122)
                        .addComponent(btnClear))
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(lblError, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                            .addGap(37, 37, 37)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(lblUserId)
                                .addComponent(lblRollNo)
                                .addComponent(lblStandard)
                                .addComponent(lblDivision)
                                .addComponent(lblAcademicYear)
                                .addComponent(lblStudentName)
                                .addComponent(jLabel3)
                                .addComponent(lblPassword)
                                .addComponent(lblCnfrmPassword))
                            .addGap(30, 30, 30)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(txtLoginID)
                                .addComponent(txtUserID)
                                .addComponent(txtPassword)
                                .addComponent(txtConfirmPassword)
                                .addComponent(txtStudentName)
                                .addComponent(txtMblNo, javax.swing.GroupLayout.DEFAULT_SIZE, 199, Short.MAX_VALUE)
                                .addGroup(jPanel2Layout.createSequentialGroup()
                                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(CmbClass, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(CmbAcademicYear, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(CmbDivision, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGap(0, 0, Short.MAX_VALUE))))))
                .addGap(41, 41, 41))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblUserId)
                    .addComponent(txtLoginID, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblm, javax.swing.GroupLayout.PREFERRED_SIZE, 0, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblStudentName, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtStudentName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(CmbClass, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblStandard))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(CmbDivision, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblDivision))
                        .addGap(17, 17, 17)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblAcademicYear)
                            .addComponent(CmbAcademicYear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(txtMblNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblRollNo)
                            .addComponent(txtUserID, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblPassword)
                            .addComponent(txtPassword, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lblCnfrmPassword, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtConfirmPassword, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                        .addGap(12, 12, 12)
                        .addComponent(lblError, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnRegisterStudent)
                            .addComponent(btnClear))))
                .addGap(22, 22, 22))
        );

        jPanel3.setBackground(new java.awt.Color(29, 9, 44));
        jPanel3.setName("jPanel3"); // NOI18N

        jLabel10.setFont(new java.awt.Font("Segoe UI Semibold", 1, 18)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel10.setText("Register Student Form");
        jLabel10.setName("jLabel10"); // NOI18N
        jPanel3.add(jLabel10);

        jPanel1.setName("jPanel1"); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 2, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout MainRegisterPanelLayout = new javax.swing.GroupLayout(MainRegisterPanel);
        MainRegisterPanel.setLayout(MainRegisterPanelLayout);
        MainRegisterPanelLayout.setHorizontalGroup(
            MainRegisterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MainRegisterPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(MainRegisterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, 468, Short.MAX_VALUE))
                .addContainerGap())
        );
        MainRegisterPanelLayout.setVerticalGroup(
            MainRegisterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MainRegisterPanelLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 22, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout PanelRegisterNewStudentLayout = new javax.swing.GroupLayout(PanelRegisterNewStudent);
        PanelRegisterNewStudent.setLayout(PanelRegisterNewStudentLayout);
        PanelRegisterNewStudentLayout.setHorizontalGroup(
            PanelRegisterNewStudentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelRegisterNewStudentLayout.createSequentialGroup()
                .addGap(62, 62, 62)
                .addComponent(BtnBackSelection1, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(111, 111, 111)
                .addComponent(MainRegisterPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(251, Short.MAX_VALUE))
        );
        PanelRegisterNewStudentLayout.setVerticalGroup(
            PanelRegisterNewStudentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelRegisterNewStudentLayout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(PanelRegisterNewStudentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(MainRegisterPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BtnBackSelection1))
                .addContainerGap(494, Short.MAX_VALUE))
        );

        LeftBodyPanel.add(PanelRegisterNewStudent, "RegisterNewStudent");

        PanelDistributeTest.setBackground(new java.awt.Color(0, 102, 102));
        PanelDistributeTest.setName("PanelDistributeTest"); // NOI18N

        BtnBackSelection3.setBackground(new java.awt.Color(11, 45, 55));
        BtnBackSelection3.setFont(new java.awt.Font("Corbel", 0, 16)); // NOI18N
        BtnBackSelection3.setForeground(new java.awt.Color(255, 255, 255));
        BtnBackSelection3.setText("Back");
        BtnBackSelection3.setBorderPainted(false);
        BtnBackSelection3.setName("BtnBackSelection3"); // NOI18N
        BtnBackSelection3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnBackSelection3MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnBackSelection3MouseExited(evt);
            }
        });
        BtnBackSelection3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBackSelection3ActionPerformed(evt);
            }
        });

        jPanel10.setName("jPanel10"); // NOI18N

        jPanel9.setBackground(new java.awt.Color(0, 51, 102));
        jPanel9.setName("jPanel9"); // NOI18N

        BtnAnalysis.setBackground(new java.awt.Color(208, 87, 96));
        BtnAnalysis.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        BtnAnalysis.setForeground(new java.awt.Color(255, 255, 255));
        BtnAnalysis.setText("Analysis");
        BtnAnalysis.setName("BtnAnalysis"); // NOI18N
        BtnAnalysis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAnalysisActionPerformed(evt);
            }
        });

        BtnView.setBackground(new java.awt.Color(208, 87, 96));
        BtnView.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        BtnView.setForeground(new java.awt.Color(255, 255, 255));
        BtnView.setText("View");
        BtnView.setName("BtnView"); // NOI18N
        BtnView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnViewActionPerformed(evt);
            }
        });

        BtnAutoSendSMS.setBackground(new java.awt.Color(208, 87, 96));
        BtnAutoSendSMS.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        BtnAutoSendSMS.setForeground(new java.awt.Color(255, 255, 255));
        BtnAutoSendSMS.setText("Auto Send SMS");
        BtnAutoSendSMS.setName("BtnAutoSendSMS"); // NOI18N
        BtnAutoSendSMS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAutoSendSMSActionPerformed(evt);
            }
        });

        ChkSelectAll.setBackground(new java.awt.Color(0, 51, 102));
        ChkSelectAll.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 14)); // NOI18N
        ChkSelectAll.setForeground(new java.awt.Color(255, 0, 0));
        ChkSelectAll.setText("Select All");
        ChkSelectAll.setName("ChkSelectAll"); // NOI18N
        ChkSelectAll.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ChkSelectAllItemStateChanged(evt);
            }
        });

        btnSendSMSHistory.setBackground(new java.awt.Color(208, 87, 96));
        btnSendSMSHistory.setFont(new java.awt.Font("Calibri Light", 0, 16)); // NOI18N
        btnSendSMSHistory.setForeground(new java.awt.Color(255, 255, 255));
        btnSendSMSHistory.setText("SMS Report");
        btnSendSMSHistory.setName("btnSendSMSHistory"); // NOI18N
        btnSendSMSHistory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSendSMSHistoryActionPerformed(evt);
            }
        });

        jButton2.setBackground(new java.awt.Color(208, 87, 96));
        jButton2.setFont(new java.awt.Font("Calibri Light", 0, 16)); // NOI18N
        jButton2.setForeground(new java.awt.Color(255, 255, 255));
        jButton2.setText("Delete");
        jButton2.setName("jButton2"); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setBackground(new java.awt.Color(208, 87, 96));
        jButton3.setFont(new java.awt.Font("Segoe UI Light", 1, 14)); // NOI18N
        jButton3.setForeground(new java.awt.Color(255, 255, 255));
        jButton3.setText("Reports");
        jButton3.setName("jButton3"); // NOI18N
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        btnRecheckMarks.setBackground(new java.awt.Color(208, 87, 96));
        btnRecheckMarks.setFont(new java.awt.Font("Segoe UI Light", 1, 14)); // NOI18N
        btnRecheckMarks.setForeground(new java.awt.Color(255, 255, 255));
        btnRecheckMarks.setText("Recheck Marks");
        btnRecheckMarks.setName("btnRecheckMarks"); // NOI18N
        btnRecheckMarks.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRecheckMarksActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(ChkSelectAll)
                .addGap(34, 34, 34)
                .addComponent(BtnAnalysis, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(BtnView)
                .addGap(18, 18, 18)
                .addComponent(BtnAutoSendSMS)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnSendSMSHistory)
                .addGap(18, 18, 18)
                .addComponent(jButton2)
                .addGap(18, 18, 18)
                .addComponent(jButton3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnRecheckMarks)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(BtnAnalysis, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(BtnView, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(BtnAutoSendSMS, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(ChkSelectAll, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnSendSMSHistory, javax.swing.GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnRecheckMarks, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel11.setName("jPanel11"); // NOI18N

        jScrollPane2.setBackground(new java.awt.Color(102, 102, 102));
        jScrollPane2.setName("jScrollPane2"); // NOI18N

        tblSaveTest.setBackground(new java.awt.Color(102, 102, 102));
        tblSaveTest.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        tblSaveTest.setForeground(new java.awt.Color(255, 255, 255));
        tblSaveTest.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Test Id ", "Test Name", "Total Quetions", "Date Time", "Status", "Distributed Test", "Select Sender"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblSaveTest.setToolTipText("DoubleClickViewTestDetail");
        tblSaveTest.setName("tblSaveTest"); // NOI18N
        tblSaveTest.setRowHeight(24);
        tblSaveTest.setSelectionBackground(new java.awt.Color(128, 128, 128));
        tblSaveTest.setSelectionForeground(new java.awt.Color(255, 255, 102));
        tblSaveTest.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblSaveTestMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tblSaveTest);

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 950, Short.MAX_VALUE)
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 449, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(29, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout PanelDistributeTestLayout = new javax.swing.GroupLayout(PanelDistributeTest);
        PanelDistributeTest.setLayout(PanelDistributeTestLayout);
        PanelDistributeTestLayout.setHorizontalGroup(
            PanelDistributeTestLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelDistributeTestLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PanelDistributeTestLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(PanelDistributeTestLayout.createSequentialGroup()
                        .addComponent(BtnBackSelection3, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        PanelDistributeTestLayout.setVerticalGroup(
            PanelDistributeTestLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelDistributeTestLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(BtnBackSelection3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(447, Short.MAX_VALUE))
        );

        LeftBodyPanel.add(PanelDistributeTest, "DistributeTest");

        PanelStartServer.setBackground(new java.awt.Color(0, 102, 102));
        PanelStartServer.setName("PanelStartServer"); // NOI18N

        jPanel6.setBackground(new java.awt.Color(58, 18, 88));
        jPanel6.setName("jPanel6"); // NOI18N

        jPanel7.setBackground(new java.awt.Color(58, 18, 88));
        jPanel7.setName("jPanel7"); // NOI18N

        BtnRefClientList.setFont(new java.awt.Font("Segoe UI Semibold", 1, 12)); // NOI18N
        BtnRefClientList.setText("Refresh Client List");
        BtnRefClientList.setName("BtnRefClientList"); // NOI18N
        BtnRefClientList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRefClientListActionPerformed(evt);
            }
        });

        lblNetworkPrefix.setFont(new java.awt.Font("Segoe UI Semibold", 1, 12)); // NOI18N
        lblNetworkPrefix.setForeground(new java.awt.Color(255, 255, 255));
        lblNetworkPrefix.setText("Network Prefix :");
        lblNetworkPrefix.setName("lblNetworkPrefix"); // NOI18N

        txtNetworkPrefix.setFont(new java.awt.Font("Segoe UI Semibold", 1, 12)); // NOI18N
        txtNetworkPrefix.setName("txtNetworkPrefix"); // NOI18N
        txtNetworkPrefix.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNetworkPrefixActionPerformed(evt);
            }
        });

        BtnUpdatePrefix.setFont(new java.awt.Font("Segoe UI Semibold", 1, 12)); // NOI18N
        BtnUpdatePrefix.setText("Update Prefix");
        BtnUpdatePrefix.setName("BtnUpdatePrefix"); // NOI18N
        BtnUpdatePrefix.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnUpdatePrefixActionPerformed(evt);
            }
        });

        BtnLogoutStudent.setFont(new java.awt.Font("Segoe UI Semibold", 1, 12)); // NOI18N
        BtnLogoutStudent.setText("Logout Student");
        BtnLogoutStudent.setName("BtnLogoutStudent"); // NOI18N
        BtnLogoutStudent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnLogoutStudentActionPerformed(evt);
            }
        });

        RdoStartServer.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        RdoStartServer.setText("Start Database Server");
        RdoStartServer.setName("RdoStartServer"); // NOI18N
        RdoStartServer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RdoStartServerActionPerformed(evt);
            }
        });

        RdoStopServer.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        RdoStopServer.setText("Stop Database Server");
        RdoStopServer.setName("RdoStopServer"); // NOI18N
        RdoStopServer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RdoStopServerActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(RdoStartServer)
                .addGap(18, 18, 18)
                .addComponent(RdoStopServer)
                .addGap(18, 18, 18)
                .addComponent(BtnRefClientList)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblNetworkPrefix, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtNetworkPrefix, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(BtnUpdatePrefix)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(BtnLogoutStudent)
                .addGap(0, 106, Short.MAX_VALUE))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(RdoStartServer)
                    .addComponent(RdoStopServer)
                    .addComponent(BtnRefClientList)
                    .addComponent(lblNetworkPrefix, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNetworkPrefix)
                    .addComponent(BtnUpdatePrefix)
                    .addComponent(BtnLogoutStudent))
                .addContainerGap())
        );

        lblTitle.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        lblTitle.setForeground(new java.awt.Color(255, 255, 255));
        lblTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTitle.setText("List Of Connected Client");
        lblTitle.setName("lblTitle"); // NOI18N

        jPanel8.setName("jPanel8"); // NOI18N

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 979, Short.MAX_VALUE)
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 6, Short.MAX_VALUE)
        );

        ScrollPanel.setName("ScrollPanel"); // NOI18N

        tblConnectedClient.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        tblConnectedClient.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Sr.No", "Client Id", "Client PC Name", "Client Ip", "DateTime"
            }
        ));
        tblConnectedClient.setName("tblConnectedClient"); // NOI18N
        ScrollPanel.setViewportView(tblConnectedClient);

        jLabel2.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 0, 51));
        jLabel2.setText("Update Sever IP");
        jLabel2.setName("jLabel2"); // NOI18N
        jLabel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel2MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel6Layout.createSequentialGroup()
                            .addGap(377, 377, 377)
                            .addComponent(lblTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(ScrollPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 913, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTitle)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(ScrollPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 354, Short.MAX_VALUE)
                .addContainerGap())
        );

        BtnBackSelection4.setBackground(new java.awt.Color(11, 45, 55));
        BtnBackSelection4.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        BtnBackSelection4.setForeground(new java.awt.Color(255, 255, 255));
        BtnBackSelection4.setText("Back");
        BtnBackSelection4.setBorderPainted(false);
        BtnBackSelection4.setName("BtnBackSelection4"); // NOI18N
        BtnBackSelection4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnBackSelection4MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnBackSelection4MouseExited(evt);
            }
        });
        BtnBackSelection4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBackSelection4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PanelStartServerLayout = new javax.swing.GroupLayout(PanelStartServer);
        PanelStartServer.setLayout(PanelStartServerLayout);
        PanelStartServerLayout.setHorizontalGroup(
            PanelStartServerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelStartServerLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(PanelStartServerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(BtnBackSelection4, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, 933, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(27, Short.MAX_VALUE))
        );
        PanelStartServerLayout.setVerticalGroup(
            PanelStartServerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelStartServerLayout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addComponent(BtnBackSelection4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(510, Short.MAX_VALUE))
        );

        LeftBodyPanel.add(PanelStartServer, "StartServer");

        PanelAddYourOwn.setBackground(new java.awt.Color(0, 102, 102));
        PanelAddYourOwn.setName("PanelAddYourOwn"); // NOI18N

        btnAddQuestions.setBackground(new java.awt.Color(225, 121, 121));
        btnAddQuestions.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        btnAddQuestions.setForeground(new java.awt.Color(255, 255, 255));
        btnAddQuestions.setText("Add Questions");
        btnAddQuestions.setBorderPainted(false);
        btnAddQuestions.setMaximumSize(new java.awt.Dimension(170, 145));
        btnAddQuestions.setMinimumSize(new java.awt.Dimension(170, 145));
        btnAddQuestions.setName("btnAddQuestions"); // NOI18N
        btnAddQuestions.setPreferredSize(new java.awt.Dimension(170, 145));
        btnAddQuestions.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnAddQuestionsMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnAddQuestionsMouseExited(evt);
            }
        });
        btnAddQuestions.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddQuestionsActionPerformed(evt);
            }
        });

        btnAddDivision.setBackground(new java.awt.Color(208, 87, 96));
        btnAddDivision.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        btnAddDivision.setForeground(new java.awt.Color(255, 255, 255));
        btnAddDivision.setText("Add Division");
        btnAddDivision.setBorderPainted(false);
        btnAddDivision.setMaximumSize(new java.awt.Dimension(170, 145));
        btnAddDivision.setMinimumSize(new java.awt.Dimension(170, 145));
        btnAddDivision.setName("btnAddDivision"); // NOI18N
        btnAddDivision.setPreferredSize(new java.awt.Dimension(170, 145));
        btnAddDivision.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnAddDivisionMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnAddDivisionMouseExited(evt);
            }
        });
        btnAddDivision.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddDivisionActionPerformed(evt);
            }
        });

        btnAddStandard.setBackground(new java.awt.Color(225, 183, 109));
        btnAddStandard.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        btnAddStandard.setForeground(new java.awt.Color(255, 255, 255));
        btnAddStandard.setText("Add Standard");
        btnAddStandard.setBorderPainted(false);
        btnAddStandard.setMaximumSize(new java.awt.Dimension(170, 145));
        btnAddStandard.setMinimumSize(new java.awt.Dimension(170, 145));
        btnAddStandard.setName("btnAddStandard"); // NOI18N
        btnAddStandard.setPreferredSize(new java.awt.Dimension(170, 145));
        btnAddStandard.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnAddStandardMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnAddStandardMouseExited(evt);
            }
        });
        btnAddStandard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddStandardActionPerformed(evt);
            }
        });

        BtnBackSubjectSelection1.setBackground(new java.awt.Color(11, 45, 55));
        BtnBackSubjectSelection1.setFont(new java.awt.Font("Corbel", 0, 16)); // NOI18N
        BtnBackSubjectSelection1.setForeground(new java.awt.Color(255, 255, 255));
        BtnBackSubjectSelection1.setText("Back");
        BtnBackSubjectSelection1.setBorderPainted(false);
        BtnBackSubjectSelection1.setName("BtnBackSubjectSelection1"); // NOI18N
        BtnBackSubjectSelection1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnBackSubjectSelection1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnBackSubjectSelection1MouseExited(evt);
            }
        });
        BtnBackSubjectSelection1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBackSubjectSelection1ActionPerformed(evt);
            }
        });

        btnAddAcademicYear.setBackground(new java.awt.Color(0, 102, 204));
        btnAddAcademicYear.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        btnAddAcademicYear.setForeground(new java.awt.Color(255, 255, 255));
        btnAddAcademicYear.setText("Add Academic Year");
        btnAddAcademicYear.setBorderPainted(false);
        btnAddAcademicYear.setMaximumSize(new java.awt.Dimension(170, 145));
        btnAddAcademicYear.setMinimumSize(new java.awt.Dimension(170, 145));
        btnAddAcademicYear.setName("btnAddAcademicYear"); // NOI18N
        btnAddAcademicYear.setPreferredSize(new java.awt.Dimension(170, 145));
        btnAddAcademicYear.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnAddAcademicYearMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnAddAcademicYearMouseExited(evt);
            }
        });
        btnAddAcademicYear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddAcademicYearActionPerformed(evt);
            }
        });

        btnAddChapter.setBackground(new java.awt.Color(131, 164, 83));
        btnAddChapter.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        btnAddChapter.setForeground(new java.awt.Color(255, 255, 255));
        btnAddChapter.setText("Add Chapter");
        btnAddChapter.setBorderPainted(false);
        btnAddChapter.setMaximumSize(new java.awt.Dimension(170, 145));
        btnAddChapter.setMinimumSize(new java.awt.Dimension(170, 145));
        btnAddChapter.setName("btnAddChapter"); // NOI18N
        btnAddChapter.setPreferredSize(new java.awt.Dimension(170, 145));
        btnAddChapter.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnAddChapterMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnAddChapterMouseExited(evt);
            }
        });
        btnAddChapter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddChapterActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PanelAddYourOwnLayout = new javax.swing.GroupLayout(PanelAddYourOwn);
        PanelAddYourOwn.setLayout(PanelAddYourOwnLayout);
        PanelAddYourOwnLayout.setHorizontalGroup(
            PanelAddYourOwnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelAddYourOwnLayout.createSequentialGroup()
                .addGap(65, 65, 65)
                .addGroup(PanelAddYourOwnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(BtnBackSubjectSelection1, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(PanelAddYourOwnLayout.createSequentialGroup()
                        .addGap(304, 304, 304)
                        .addGroup(PanelAddYourOwnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnAddStandard, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnAddAcademicYear, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnAddDivision, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnAddChapter, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnAddQuestions, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(414, Short.MAX_VALUE))
        );
        PanelAddYourOwnLayout.setVerticalGroup(
            PanelAddYourOwnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelAddYourOwnLayout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addComponent(BtnBackSubjectSelection1, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(btnAddAcademicYear, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28)
                .addComponent(btnAddStandard, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addComponent(btnAddDivision, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnAddChapter, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnAddQuestions, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(575, Short.MAX_VALUE))
        );

        LeftBodyPanel.add(PanelAddYourOwn, "AddYourOwn");

        PanelStudentDetail.setBackground(new java.awt.Color(0, 102, 102));
        PanelStudentDetail.setName("PanelStudentDetail"); // NOI18N

        BtnBackSelection2.setBackground(new java.awt.Color(11, 45, 55));
        BtnBackSelection2.setFont(new java.awt.Font("Corbel", 0, 16)); // NOI18N
        BtnBackSelection2.setForeground(new java.awt.Color(255, 255, 255));
        BtnBackSelection2.setText("Back");
        BtnBackSelection2.setBorderPainted(false);
        BtnBackSelection2.setName("BtnBackSelection2"); // NOI18N
        BtnBackSelection2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnBackSelection2MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnBackSelection2MouseExited(evt);
            }
        });
        BtnBackSelection2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBackSelection2ActionPerformed(evt);
            }
        });

        jPanel4.setName("jPanel4"); // NOI18N

        jPanel5.setBackground(new java.awt.Color(0, 0, 0));
        jPanel5.setName("jPanel5"); // NOI18N

        BtnPrint.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        BtnPrint.setText("Print");
        BtnPrint.setName("BtnPrint"); // NOI18N
        BtnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPrintActionPerformed(evt);
            }
        });

        lblStudentDetail.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblStudentDetail.setForeground(new java.awt.Color(255, 255, 255));
        lblStudentDetail.setText("Student Detail");
        lblStudentDetail.setName("lblStudentDetail"); // NOI18N

        btnDelete.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        btnDelete.setText("Delete");
        btnDelete.setName("btnDelete"); // NOI18N
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        btnModify.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        btnModify.setText("Modify");
        btnModify.setName("btnModify"); // NOI18N
        btnModify.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModifyActionPerformed(evt);
            }
        });

        cmbStandard.setName("cmbStandard"); // NOI18N
        cmbStandard.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbStandardItemStateChanged(evt);
            }
        });

        cmbDivision.setName("cmbDivision"); // NOI18N
        cmbDivision.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbDivisionItemStateChanged(evt);
            }
        });

        cmbAcademicYear.setName("cmbAcademicYear"); // NOI18N
        cmbAcademicYear.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbAcademicYearItemStateChanged(evt);
            }
        });

        btnImportFile.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        btnImportFile.setText("Import");
        btnImportFile.setName("btnImportFile"); // NOI18N
        btnImportFile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImportFileActionPerformed(evt);
            }
        });

        btnExport.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        btnExport.setText("Export");
        btnExport.setName("btnExport"); // NOI18N
        btnExport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExportActionPerformed(evt);
            }
        });

        btnDeleteAll.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        btnDeleteAll.setText("Delete All");
        btnDeleteAll.setName("btnDeleteAll"); // NOI18N
        btnDeleteAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteAllActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cmbStandard, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(cmbDivision, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(cmbAcademicYear, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(BtnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addComponent(lblStudentDetail, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnImportFile)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnExport)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnModify)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnDelete)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDeleteAll)
                .addGap(21, 21, 21))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnModify, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnExport, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnImportFile, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cmbAcademicYear, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(cmbDivision, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(cmbStandard, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(BtnPrint, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnDelete)
                            .addComponent(btnDeleteAll))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(lblStudentDetail)))
                .addContainerGap())
        );

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        tblStudentDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Login Id", "User Id", "Student Name", "Mobile No", "Standard", "Division", "Academic Year", "Password"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        tblStudentDetail.setName("tblStudentDetail"); // NOI18N
        tblStudentDetail.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblStudentDetailMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblStudentDetail);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 413, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16))
        );

        javax.swing.GroupLayout PanelStudentDetailLayout = new javax.swing.GroupLayout(PanelStudentDetail);
        PanelStudentDetail.setLayout(PanelStudentDetailLayout);
        PanelStudentDetailLayout.setHorizontalGroup(
            PanelStudentDetailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelStudentDetailLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(BtnBackSelection2, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(PanelStudentDetailLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        PanelStudentDetailLayout.setVerticalGroup(
            PanelStudentDetailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelStudentDetailLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(BtnBackSelection2)
                .addGap(18, 18, 18)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(485, Short.MAX_VALUE))
        );

        LeftBodyPanel.add(PanelStudentDetail, "StudentDetail");

        RightBodyPanel.setBackground(new java.awt.Color(0, 0, 0));
        RightBodyPanel.setForeground(new java.awt.Color(240, 240, 240));
        RightBodyPanel.setName("RightBodyPanel"); // NOI18N
        RightBodyPanel.setPreferredSize(new java.awt.Dimension(567, 531));

        TableScrollPane.setBackground(new java.awt.Color(102, 102, 102));
        TableScrollPane.setName("TableScrollPane"); // NOI18N

        TableTest.setBackground(new java.awt.Color(102, 102, 102));
        TableTest.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        TableTest.setForeground(new java.awt.Color(255, 255, 255));
        TableTest.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Test Info", "Test Name", "Date"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        TableTest.setName("TableTest"); // NOI18N
        TableTest.setRowHeight(24);
        TableTest.setSelectionBackground(new java.awt.Color(128, 128, 128));
        TableTest.setSelectionForeground(new java.awt.Color(255, 255, 102));
        TableTest.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TableTestMouseClicked(evt);
            }
        });
        TableScrollPane.setViewportView(TableTest);

        javax.swing.GroupLayout RightBodyPanelLayout = new javax.swing.GroupLayout(RightBodyPanel);
        RightBodyPanel.setLayout(RightBodyPanelLayout);
        RightBodyPanelLayout.setHorizontalGroup(
            RightBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(RightBodyPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(TableScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 500, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        RightBodyPanelLayout.setVerticalGroup(
            RightBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(RightBodyPanelLayout.createSequentialGroup()
                .addComponent(TableScrollPane)
                .addGap(1, 1, 1))
        );

        LblDelete.setFont(new java.awt.Font("Microsoft JhengHei", 0, 14)); // NOI18N
        LblDelete.setForeground(new java.awt.Color(255, 0, 0));
        LblDelete.setText("Delete");
        LblDelete.setName("LblDelete"); // NOI18N
        LblDelete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LblDeleteMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout BodyPanelLayout = new javax.swing.GroupLayout(BodyPanel);
        BodyPanel.setLayout(BodyPanelLayout);
        BodyPanelLayout.setHorizontalGroup(
            BodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(BodyPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(LeftBodyPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 990, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(BodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(BodyPanelLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(RightBodyPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 480, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, BodyPanelLayout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addComponent(LblDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(558, 558, 558))))
        );
        BodyPanelLayout.setVerticalGroup(
            BodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, BodyPanelLayout.createSequentialGroup()
                .addGroup(BodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(BodyPanelLayout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(LeftBodyPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(BodyPanelLayout.createSequentialGroup()
                        .addComponent(LblDelete)
                        .addGap(11, 11, 11)
                        .addComponent(RightBodyPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 1009, Short.MAX_VALUE)))
                .addGap(394, 394, 394))
        );

        MainPanelScrollPane.setViewportView(BodyPanel);

        HeaderPanel.setBackground(new java.awt.Color(0, 0, 0));
        HeaderPanel.setName("HeaderPanel"); // NOI18N

        LblCompanyName.setFont(new java.awt.Font("Microsoft JhengHei", 0, 14)); // NOI18N
        LblCompanyName.setForeground(new java.awt.Color(255, 255, 255));
        LblCompanyName.setText("CruncherSoft's");
        LblCompanyName.setName("LblCompanyName"); // NOI18N

        LblProductName.setFont(new java.awt.Font("Microsoft JhengHei", 0, 40)); // NOI18N
        LblProductName.setForeground(new java.awt.Color(255, 255, 255));
        LblProductName.setText("MHCET-PCB Server & Question Paper Printing Software");
        LblProductName.setName("LblProductName"); // NOI18N

        BtnMinimizeMainPanel.setBackground(new java.awt.Color(0, 0, 0));
        BtnMinimizeMainPanel.setFont(new java.awt.Font("Corbel", 0, 16)); // NOI18N
        BtnMinimizeMainPanel.setForeground(new java.awt.Color(255, 255, 255));
        BtnMinimizeMainPanel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/Minimize.png"))); // NOI18N
        BtnMinimizeMainPanel.setAlignmentY(0.0F);
        BtnMinimizeMainPanel.setBorderPainted(false);
        BtnMinimizeMainPanel.setFocusPainted(false);
        BtnMinimizeMainPanel.setName("BtnMinimizeMainPanel"); // NOI18N
        BtnMinimizeMainPanel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnMinimizeMainPanelActionPerformed(evt);
            }
        });

        BtnCloseMainPanel.setBackground(new java.awt.Color(0, 0, 0));
        BtnCloseMainPanel.setFont(new java.awt.Font("Corbel", 0, 16)); // NOI18N
        BtnCloseMainPanel.setForeground(new java.awt.Color(255, 255, 255));
        BtnCloseMainPanel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/Close.png"))); // NOI18N
        BtnCloseMainPanel.setAlignmentY(0.0F);
        BtnCloseMainPanel.setBorderPainted(false);
        BtnCloseMainPanel.setFocusPainted(false);
        BtnCloseMainPanel.setName("BtnCloseMainPanel"); // NOI18N
        BtnCloseMainPanel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCloseMainPanelActionPerformed(evt);
            }
        });

        LblWhtasNew.setFont(new java.awt.Font("Microsoft JhengHei", 1, 12)); // NOI18N
        LblWhtasNew.setForeground(new java.awt.Color(255, 255, 0));
        LblWhtasNew.setText("What's New?");
        LblWhtasNew.setName("LblWhtasNew"); // NOI18N
        LblWhtasNew.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LblWhtasNewMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout HeaderPanelLayout = new javax.swing.GroupLayout(HeaderPanel);
        HeaderPanel.setLayout(HeaderPanelLayout);
        HeaderPanelLayout.setHorizontalGroup(
            HeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(HeaderPanelLayout.createSequentialGroup()
                .addGap(67, 67, 67)
                .addGroup(HeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(HeaderPanelLayout.createSequentialGroup()
                        .addComponent(LblCompanyName)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(BtnMinimizeMainPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(BtnCloseMainPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(HeaderPanelLayout.createSequentialGroup()
                        .addComponent(LblProductName)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(LblWhtasNew, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        HeaderPanelLayout.setVerticalGroup(
            HeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(HeaderPanelLayout.createSequentialGroup()
                .addGroup(HeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(HeaderPanelLayout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addComponent(LblCompanyName))
                    .addGroup(HeaderPanelLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(HeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(BtnCloseMainPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(BtnMinimizeMainPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(HeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LblProductName)
                    .addComponent(LblWhtasNew))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(HeaderPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(MainPanelScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 1350, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(HeaderPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(MainPanelScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 602, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        Object[] options = {"YES", "CANCEL"};
        int i = JOptionPane.showOptionDialog(null, "Are You Sure to Exit Application?", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
        if (i == 0) {
//            this.dispose();
            System.exit(0);
        }
    }//GEN-LAST:event_formWindowClosing

private void BtnCreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCreateActionPerformed
// TODO add your handling code here:
    cardLayout.show(LeftBodyPanel, "QuestionSelectionCard");
}//GEN-LAST:event_BtnCreateActionPerformed

private void BtnSummaryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSummaryActionPerformed
    printingPaperType = "ChapterSummery";
    cardLayout.show(LeftBodyPanel, "SubjectCard");
}//GEN-LAST:event_BtnSummaryActionPerformed

private void BtnSyllabusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSyllabusActionPerformed
// TODO add your handling code here:
    new Weightage().setVisible(true);
    this.dispose();
}//GEN-LAST:event_BtnSyllabusActionPerformed

private void BtnBackSelectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBackSelectionActionPerformed
// TODO add your handling code here:
    tablesetting1();
    resumeTest1();
    cardLayout.show(LeftBodyPanel, "HomeCard");
}//GEN-LAST:event_BtnBackSelectionActionPerformed

private void BtnBackSelectionMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnBackSelectionMouseEntered
// TODO add your handling code here:
    BtnBackSelection.setBackground(new java.awt.Color(255, 255, 255));
    BtnBackSelection.setForeground(new java.awt.Color(11, 45, 55));
}//GEN-LAST:event_BtnBackSelectionMouseEntered

private void BtnBackSelectionMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnBackSelectionMouseExited
// TODO add your handling code here:
    BtnBackSelection.setBackground(new java.awt.Color(11, 45, 55));
    BtnBackSelection.setForeground(new java.awt.Color(255, 255, 255));
}//GEN-LAST:event_BtnBackSelectionMouseExited

private void BtnChapterWiseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnChapterWiseActionPerformed
    printingPaperType = "ChapterWise";
    cardLayout.show(LeftBodyPanel, "SubjectCard");
}//GEN-LAST:event_BtnChapterWiseActionPerformed

private void BtnUnitWiseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnUnitWiseActionPerformed
    printingPaperType = "UnitWise";
    cardLayout.show(LeftBodyPanel, "SubjectCard");
}//GEN-LAST:event_BtnUnitWiseActionPerformed

private void BtnSubjectWiseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSubjectWiseActionPerformed
    printingPaperType = "SubjectWise";
    cardLayout.show(LeftBodyPanel, "SubjectCard");
}//GEN-LAST:event_BtnSubjectWiseActionPerformed

private void BtnAboutUsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAboutUsActionPerformed
    try {
        // TODO add your handling code here:
        new AboutUs().setVisible(true);
    } catch (URISyntaxException ex) {
        Logger.getLogger(HomePage.class.getName()).log(Level.SEVERE, null, ex);
    }
    this.dispose();
}//GEN-LAST:event_BtnAboutUsActionPerformed

private void BtnOldQuestionPapersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnOldQuestionPapersActionPerformed
// TODO add your handling code here:
    new PreviousYearQPapers().setVisible(true);
    this.dispose();
}//GEN-LAST:event_BtnOldQuestionPapersActionPerformed

private void BtnAddQuestionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAddQuestionActionPerformed
// TODO add your handling code here:
//    flagType = 5;
//    printingPaperType = "AddQuestion";
//    cardLayout.show(LeftBodyPanel, "SubjectCard");
      cardLayout.show(LeftBodyPanel, "AddYourOwn");
}//GEN-LAST:event_BtnAddQuestionActionPerformed

    private void BtnBackOmrSelectionMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnBackOmrSelectionMouseEntered
        // TODO add your handling code here:
        BtnBackOmrSelection.setBackground(new java.awt.Color(255, 255, 255));
        BtnBackOmrSelection.setForeground(new java.awt.Color(11, 45, 55));
    }//GEN-LAST:event_BtnBackOmrSelectionMouseEntered

    private void BtnBackOmrSelectionMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnBackOmrSelectionMouseExited
        // TODO add your handling code here:
        BtnBackOmrSelection.setBackground(new java.awt.Color(11, 45, 55));
        BtnBackOmrSelection.setForeground(new java.awt.Color(255, 255, 255));
    }//GEN-LAST:event_BtnBackOmrSelectionMouseExited

    private void BtnBackOmrSelectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBackOmrSelectionActionPerformed
        // TODO add your handling code here:
        cardLayout.show(LeftBodyPanel, "HomeCard");
        TxtFldOmrRange.setText("" + 200);
    }//GEN-LAST:event_BtnBackOmrSelectionActionPerformed

    private void BtnCreateOmrActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCreateOmrActionPerformed
        if(!TxtFldOmrRange.getText().trim().isEmpty()) {
            int totalQuestion = 0;
            try {
                totalQuestion = Integer.parseInt(TxtFldOmrRange.getText().trim());
            } catch(Exception ex) {
                totalQuestion = 0;
            }
            
            if (totalQuestion > 0 && totalQuestion < 1001) {
                String fileLocation = new FileChooser().getPdfFileLocation(this);
                if(fileLocation != null) {
                    new ExportProcess().createOmrSheet(totalQuestion,1,instituteName,fileLocation,null,this);
                    cardLayout.show(LeftBodyPanel, "HomeCard");
                }
            } else {
                JOptionPane.showMessageDialog(null, "Please Enter Valid Range of Questions.\n( Range Should be 1 to 1000 )");
                TxtFldOmrRange.setText("" + 200);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Please Enter Number");
        }
    }//GEN-LAST:event_BtnCreateOmrActionPerformed

    private void BtnOMRActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnOMRActionPerformed
        // TODO add your handling code here:4
        cardLayout.show(LeftBodyPanel, "OmrCard");
    }//GEN-LAST:event_BtnOMRActionPerformed

    private void TxtFldOmrRangeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtFldOmrRangeKeyPressed
        keyPressed(evt, TxtFldOmrRange);
    }//GEN-LAST:event_TxtFldOmrRangeKeyPressed

    private void BtnBackSubjectSelectionMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnBackSubjectSelectionMouseEntered
        // TODO add your handling code here:(255, 255, 255)
        BtnBackSubjectSelection.setBackground(new java.awt.Color(255, 255, 255));
        BtnBackSubjectSelection.setForeground(new java.awt.Color(11, 45, 55));
    }//GEN-LAST:event_BtnBackSubjectSelectionMouseEntered

    private void BtnBackSubjectSelectionMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnBackSubjectSelectionMouseExited
        // TODO add your handling code here:(11, 45, 55)
        BtnBackSubjectSelection.setBackground(new java.awt.Color(11, 45, 55));
        BtnBackSubjectSelection.setForeground(new java.awt.Color(255, 255, 255));
    }//GEN-LAST:event_BtnBackSubjectSelectionMouseExited

    private void BtnBackSubjectSelectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBackSubjectSelectionActionPerformed
        // TODO add your handling code here:
        if(printingPaperType.equalsIgnoreCase("AddQuestion") || printingPaperType.equalsIgnoreCase("ChapterSummery")|| printingPaperType.equalsIgnoreCase("AddChapter"))
            cardLayout.show(LeftBodyPanel, "HomeCard");
        else
            cardLayout.show(LeftBodyPanel, "QuestionSelectionCard");
    }//GEN-LAST:event_BtnBackSubjectSelectionActionPerformed

    private void BtnFirstSubjectMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnFirstSubjectMouseEntered
        // TODO add your handling code here:
        BtnFirstSubject.setBackground(new java.awt.Color(43, 48, 26));
    }//GEN-LAST:event_BtnFirstSubjectMouseEntered

    private void BtnFirstSubjectMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnFirstSubjectMouseExited
        // TODO add your handling code here:
        BtnFirstSubject.setBackground(new java.awt.Color(131, 164, 83));
    }//GEN-LAST:event_BtnFirstSubjectMouseExited

    private void BtnFirstSubjectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnFirstSubjectActionPerformed
        // TODO add your handling code here:
        selectedSubjectBean = subjectList.get(0);
        callSubjectWise();
    }//GEN-LAST:event_BtnFirstSubjectActionPerformed

    
    
    private void BtnSecondSubjectMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnSecondSubjectMouseEntered
        // TODO add your handling code here:
        BtnSecondSubject.setBackground(new java.awt.Color(67, 55, 33));
    }//GEN-LAST:event_BtnSecondSubjectMouseEntered

    private void BtnSecondSubjectMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnSecondSubjectMouseExited
        // TODO add your handling code here:
        BtnSecondSubject.setBackground(new java.awt.Color(225, 183, 109));
    }//GEN-LAST:event_BtnSecondSubjectMouseExited

    private void BtnSecondSubjectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSecondSubjectActionPerformed
        // TODO add your handling code here:
        selectedSubjectBean = subjectList.get(1);
        callSubjectWise();
    }//GEN-LAST:event_BtnSecondSubjectActionPerformed

    private void BtnThirdSubjectMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnThirdSubjectMouseEntered
        // TODO add your handling code here:
        BtnThirdSubject.setBackground(new java.awt.Color(62, 25, 32));
    }//GEN-LAST:event_BtnThirdSubjectMouseEntered

    private void BtnThirdSubjectMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnThirdSubjectMouseExited
        // TODO add your handling code here:
        BtnThirdSubject.setBackground(new java.awt.Color(208, 87, 96));
    }//GEN-LAST:event_BtnThirdSubjectMouseExited

    private void BtnThirdSubjectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnThirdSubjectActionPerformed
        // TODO add your handling code here:
        selectedSubjectBean = subjectList.get(2);
        callSubjectWise();
    }//GEN-LAST:event_BtnThirdSubjectActionPerformed

    private void BtnGroupWiseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnGroupWiseActionPerformed
        printingPaperType = "GroupWise";
        new SelectQuestionsGroupWise(currentPatternIndex,subjectList,printingPaperType,this).setVisible(true);
    }//GEN-LAST:event_BtnGroupWiseActionPerformed

    private void CreatePapersMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CreatePapersMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_CreatePapersMouseEntered

    private void CreatePapersMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CreatePapersMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_CreatePapersMouseExited

    private void CreatePapersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CreatePapersActionPerformed
        // TODO add your handling code here:
        new MultipleYearSelection(currentPatternIndex,"YearWise").setVisible(true);
        this.dispose();
    }//GEN-LAST:event_CreatePapersActionPerformed

    private void SettingsMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_SettingsMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_SettingsMouseEntered

    private void SettingsMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_SettingsMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_SettingsMouseExited

    private void SettingsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SettingsActionPerformed
        // TODO add your handling code here:
        cardLayout.show(LeftBodyPanel, "SettingsCard");
//        System.out.println("currentPatternIndex"+currentPatternIndex);
//        if(currentPatternIndex == 0)
//            RdoPatternFirst.setSelected(true);
//        else if(currentPatternIndex == 1)
//            RdoPatternSecond.setSelected(true);
//        else if(currentPatternIndex == 2)
//            RdoPatternThird.setSelected(true);
    }//GEN-LAST:event_SettingsActionPerformed

    private void LblDeleteMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblDeleteMouseClicked
        // TODO add your handling code here:
        int row = TableTest.getSelectedRow();
        if(row >= 1) {
            Object[] options = {"YES", "CANCEL"};
            int i = JOptionPane.showOptionDialog(rootPane, "Are You Sure to Delete Test?", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
            if (i == 0) {
                row--;
                if(row >= 0) {
                    PrintedTestBean printedTestBean = testList.get(row);
                    new PrintedTestOperation().deletePrintedTest(printedTestBean);
                    int id = 0;
                    for(int j=0;j<row;j++) {
                        id = testList.get(j).getPrintedTestId();
                        testList.get(j).setPrintedTestId(id-1);
                    }
                    testList.remove(printedTestBean);
                    setTableRows();
                }
            }
        } else {
            JOptionPane.showMessageDialog(rootPane, "Select Valid Row For Deletion.");
        }
    }//GEN-LAST:event_LblDeleteMouseClicked

    private void TableTestMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TableTestMouseClicked
        // TODO add your handling code here:
        if (evt.getClickCount() == 2) {
            int row = TableTest.rowAtPoint(evt.getPoint());
            row--;
            if(row >= 0) {;
                    com.bean.PrintedTestBean printedTestBean = testList.get(row);
                new TestUtility().resumeTest(printedTestBean);
                this.dispose();
            }
        }
    }//GEN-LAST:event_TableTestMouseClicked

    private void BtnMinimizeMainPanelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnMinimizeMainPanelActionPerformed
        // TODO add your handling code here:
        setState(this.ICONIFIED);
    }//GEN-LAST:event_BtnMinimizeMainPanelActionPerformed

    private void BtnCloseMainPanelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCloseMainPanelActionPerformed
        // TODO add your handling code here:
        Object[] options = {"YES", "CANCEL"};
        int i = JOptionPane.showOptionDialog(null, "Are You Sure to Exit Application?", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
        if (i == 0)
        System.exit(0);
    }//GEN-LAST:event_BtnCloseMainPanelActionPerformed

     
    
    private void LblWhtasNewMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblWhtasNewMouseClicked
        // TODO add your handling code here:
        new WhatsNew(this).setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_LblWhtasNewMouseClicked

    private void BtnRegNewStudentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRegNewStudentActionPerformed
        // TODO add your handling code here:
        clearFields();         
        setAutoRollNo();
        cardLayout.show(LeftBodyPanel, "RegisterNewStudent");
    }//GEN-LAST:event_BtnRegNewStudentActionPerformed

    private void BtnBackSelection1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnBackSelection1MouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnBackSelection1MouseEntered

    private void BtnBackSelection1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnBackSelection1MouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnBackSelection1MouseExited

    private void BtnBackSelection1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBackSelection1ActionPerformed
        // TODO add your handling code here:
        cardLayout.show(LeftBodyPanel, "HomeCard");
    }//GEN-LAST:event_BtnBackSelection1ActionPerformed

    private void txtUserIDKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUserIDKeyTyped
        String value = txtUserID.getText();
        int l = value.length();
        if (evt.getKeyChar() >= '0' && evt.getKeyChar() <= '9' || evt.getKeyCode() == 8 || evt.getKeyCode() == 127) {
            txtUserID.setEditable(true);
            lblError.setText("");
        } else {
            txtUserID.setEditable(false);
            lblError.setText("* Invalid UserId..");
            txtUserID.setEditable(true);
        }
    }//GEN-LAST:event_txtUserIDKeyTyped

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
       clearFields(); 
    }//GEN-LAST:event_btnClearActionPerformed

    private void btnRegisterStudentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegisterStudentActionPerformed
        
        if(checkUserDetails()) {
        registerStudent();
        setAutoRollNo();
        } else {
            JOptionPane.showMessageDialog(rootPane, "Please Fill All Fields Data.");
        }     
             
    }//GEN-LAST:event_btnRegisterStudentActionPerformed
    
    private void setAutoRollNo() {
        Connection connection = new DbConnection().getConnection();
        try {
            int rollno = 0;
            if (connection != null) {
                Statement s = connection.createStatement();
                ResultSet rs = s.executeQuery("Select MAX(rollno) from Student_Info");
                if (rs.next()) {
                    rollno = rs.getInt(1);
                }
                rollno++;
                txtLoginID.setText(rollno + "");
            } else {
                JOptionPane.showMessageDialog(rootPane, "Unable to register Student. Please Try again");
            }
        } catch (SQLException ex) {
            Logger.getLogger(HomePage.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(HomePage.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    public boolean registerStudent() {
        String academicYear = CmbAcademicYear.getSelectedItem().toString().trim();
        String pwd = txtPassword.getText();
        String cpwd = txtConfirmPassword.getText();
        String div = CmbDivision.getSelectedItem().toString().trim();
        String userId = txtUserID.getText();
        ResultSet rs1;
        String rollno = txtLoginID.getText();
        String std = CmbClass.getSelectedItem().toString().trim();
        String StudentName=txtStudentName.getText();
        String MobileNo=txtMblNo.getText();
        boolean receivedValue = false;
        receivedValue = new StudentRegistrationOperation().checkStudentName(userId);
    
        int count=0;
        boolean val=false;
        for(int i=0;i<userId.length();i++){
            
           if(userId.charAt(i)=='0' || userId.charAt(i)=='1' || userId.charAt(i)=='2' || userId.charAt(i)=='3' || userId.charAt(i)=='4' || userId.charAt(i)=='5' || userId.charAt(i)=='6' || userId.charAt(i)=='0' || userId.charAt(i)=='7' || userId.charAt(i)=='8' || userId.charAt(i)=='9'){
               
              count++; 
              
           }
            
            
        }
        
        if(count==userId.length()){
            
            val=true;
        }else{
            val=false;
        }
        
     
        if (receivedValue == true) {
            if(val==true){ 
            Connection connection = new DbConnection().getConnection();
            try {
                if (connection != null) {
                    Statement s = connection.createStatement();
                    String sql = "Select * from Student_Info where rollno=?";
                    PreparedStatement preparedStatement = connection.prepareStatement(sql);
                    preparedStatement.setInt(1, Integer.parseInt(rollno));
                    ResultSet rs = preparedStatement.executeQuery();
                    if (!rs.next()) {
                        sql = "insert into Student_Info values(?,?,?,?,?,?,?,?)";
                        preparedStatement = connection.prepareStatement(sql);
                        preparedStatement.setInt(1, Integer.parseInt(rollno));
                        preparedStatement.setString(2, userId);
                        preparedStatement.setString(3, pwd);
                        preparedStatement.setString(4, std);
                        preparedStatement.setString(5, div);
                        preparedStatement.setString(6, academicYear);
                        preparedStatement.setString(7, StudentName);
                        preparedStatement.setString(8, MobileNo);
                        int executeUpdate = preparedStatement.executeUpdate();
                        if (executeUpdate > 0) {
                            String lastalpha = "";
                            int maxroll = 0;
                            rs1 = s.executeQuery("select max(rollno) from Set_Rollno_Distribution");
                            //setInitials
                            if (rs1.next()) {
                                maxroll = rs1.getInt(1);
                            }
                            Statement s2 = connection.createStatement();
                            ResultSet rs2 = s2.executeQuery("select setInitials from Set_Rollno_Distribution where rollno=" + maxroll);
                            if (rs2.next()) {
                                lastalpha = rs2.getString(1);
                            }
                            if (lastalpha == null) {
                                lastalpha = "A";
                            }
                            if (lastalpha.equals("A")) {
                                lastalpha = "B";
                            } else if (lastalpha.equals("B")) {
                                lastalpha = "C";
                            } else if (lastalpha.equals("C")) {
                                lastalpha = "D";
                            } else if (lastalpha.equals("D")) {
                                lastalpha = "A";
                            } else {
                                lastalpha = "A";
                            }
                            s.execute("insert into Set_Rollno_Distribution values(" + Integer.parseInt(rollno) + ",'" + lastalpha + "')");
                            clearFields();
                            JOptionPane.showMessageDialog(rootPane, "Student registered Successfully");
                            return true;
                        } else {
                            JOptionPane.showMessageDialog(rootPane, "Unable to register Student. Please Try again");
                            return false;
                        }
                    } else {
                        JOptionPane.showMessageDialog(rootPane, "Student with this rollno is already registered");
                    }
                } else {
                    JOptionPane.showMessageDialog(rootPane, "Unable to register Student. Please Try again");
                }
            } catch (SQLException ex) {
                Logger.getLogger(HomePage.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(HomePage.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }else{
              
                JOptionPane.showMessageDialog(null, "UserId should be a Number"); 
                
            }
     } else {
            JOptionPane.showMessageDialog(null, "This User Id is Already Exist, please insert unique id.");
        }
        return false;
    }  
     public void clearFields() {
        txtLoginID.setText("");        
        txtConfirmPassword.setText("");       
        txtUserID.setText("");
        txtPassword.setText("");      
        txtStudentName.setText("");
        txtMblNo.setText("");
        CmbClass.setSelectedIndex(0);
        CmbAcademicYear.setSelectedIndex(0);
        CmbDivision.setSelectedIndex(0);
        setAutoRollNo();
        
    } 
    private boolean checkUserDetails(){
        if(txtLoginID.getText().trim().isEmpty()  ||
            txtConfirmPassword.getText().trim().isEmpty() ||            
            txtUserID.getText().trim().isEmpty() || 
            txtPassword.getText().trim().isEmpty() ||
            txtStudentName.getText().trim().isEmpty()||
            txtMblNo.getText().trim().isEmpty()||
            CmbClass.getSelectedIndex() < 1 ||
            CmbAcademicYear.getSelectedIndex() < 1 ||
            CmbDivision.getSelectedIndex()  < 1)
            return false;
        else
            return true;
    }
      
    //For Student Detail Module View tablesetting
    public void tablesetting() {
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        tblStudentDetail.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tblStudentDetail.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        tblStudentDetail.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
        tblStudentDetail.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
        tblStudentDetail.getColumnModel().getColumn(4).setCellRenderer(centerRenderer);
        tblStudentDetail.getColumnModel().getColumn(5).setCellRenderer(centerRenderer);
    }  
    private void resumeTest() { 
        //For Student Detail Module View tablesetting
        ArrayList<StudentBean> studentList = new ArrayList<StudentBean>();
        studentList = new StudentRegistrationOperation().getStudentInfo();
        if (studentList.size() > 0) {
            DefaultTableModel model = (DefaultTableModel) tblStudentDetail.getModel();
            clearTableRows(tblStudentDetail);
            model.addRow(new Object[]{"", "", "", "" , "", "", "",""});
            for (int i = 0; i < studentList.size(); i++) {
                StudentBean studentbean1 = studentList.get(i);
                model.addRow(new Object[]{studentbean1.getRollno(), studentbean1.getFullname(),studentbean1.getStudentName(),studentbean1.getMobileNo(), studentbean1.getStandard(), studentbean1.getDivision(), studentbean1.getAcademicYear(), studentbean1.getPass()});
            }
        }

    }
    
    private void BtnStudentDetailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnStudentDetailActionPerformed
        // TODO add your handling code here:
        tablesetting(); //For Student view Module Table
        resumeTest();  //For Student view Module Table
       
        cardLayout.show(LeftBodyPanel, "StudentDetail");
    }//GEN-LAST:event_BtnStudentDetailActionPerformed

    
    
    private void clearTableRows(javax.swing.JTable tblPractice) {
        DefaultTableModel model = (DefaultTableModel) tblPractice.getModel();
        int rowCount = model.getRowCount();
        for (int i = rowCount - 1; i >= 0; i--) {
            model.removeRow(i);
        }
    }
    
    
    private void BtnDistributeTestActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnDistributeTestActionPerformed
        // TODO add your handling code here:
        //Distribute Test Module
                resumeTest1();
                ArrayList<String> testInfo = new ArrayList<String>();                
                testInfo = new ClassSaveTestOperation().getClassTestInfo();
                if (testInfo.size() > 0) {          
                    cardLayout.show(LeftBodyPanel, "DistributeTest");
                    //this.dispose();
                } else {
                    JOptionPane.showMessageDialog(null, "No Test Found.");
                }
       
    }//GEN-LAST:event_BtnDistributeTestActionPerformed

    private void BtnStartServerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnStartServerActionPerformed
        // TODO add your handling code here:
        cardLayout.show(LeftBodyPanel, "StartServer");
    }//GEN-LAST:event_BtnStartServerActionPerformed

    private void BtnBackSelection3MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnBackSelection3MouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnBackSelection3MouseEntered

    private void BtnBackSelection3MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnBackSelection3MouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnBackSelection3MouseExited

    private void BtnBackSelection3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBackSelection3ActionPerformed
        // TODO add your handling code here:
        cardLayout.show(LeftBodyPanel, "HomeCard");
    }//GEN-LAST:event_BtnBackSelection3ActionPerformed

    private void BtnBackSelection4MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnBackSelection4MouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnBackSelection4MouseEntered

    private void BtnBackSelection4MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnBackSelection4MouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnBackSelection4MouseExited

    private void BtnBackSelection4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBackSelection4ActionPerformed
        // TODO add your handling code here:
        cardLayout.show(LeftBodyPanel, "HomeCard");
    }//GEN-LAST:event_BtnBackSelection4ActionPerformed

    private void txtNetworkPrefixActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNetworkPrefixActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNetworkPrefixActionPerformed

    private void BtnRefClientListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRefClientListActionPerformed
        // TODO add your handling code here:
        //Refresh Client List.....StartServer 
        try {          
            clientMonitorSocket.start();    
            loadClients();
        } catch (SQLException ex) {
            Logger.getLogger(HomePage.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(HomePage.class.getName()).log(Level.SEVERE, null, ex);
        }
      
    }//GEN-LAST:event_BtnRefClientListActionPerformed

    private void BtnUpdatePrefixActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnUpdatePrefixActionPerformed
         // TODO add your handling code here:
       String networkPrefix = txtNetworkPrefix.getText();
       new NetworkPrefixOperation().updateNetworkPrefix(networkPrefix);  //Update Prefix Button
    }//GEN-LAST:event_BtnUpdatePrefixActionPerformed

    private void BtnLogoutStudentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnLogoutStudentActionPerformed
        // TODO add your handling code here:
        //Start Server.......LogOut Server
        int rollno = 0;
        TableModel tm = tblConnectedClient.getModel();
        for (int i = 0; i < tm.getRowCount(); i++) {
            Object o = tm.getValueAt(i, 2);
//            rollno = (Integer) o;
            rollno = Integer.valueOf((String) o);
        }
        System.out.println("rollno " + rollno);       
       boolean Status= new LogOutOperation().logout(rollno);
       if (Status) {
            
        } else {
            JOptionPane.showMessageDialog(null, "Cannot Logout, Try Again...!");
        }
    }//GEN-LAST:event_BtnLogoutStudentActionPerformed

    private void tblSaveTestMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblSaveTestMouseClicked
        // TODO add your handling code here:
//        if (evt.getClickCount() == 2) {
//        
//        int row = tblSaveTest.getSelectedRow();
//        row++;
//        if (rank == 0) {
//            if (row > 0) {
//                UnitTestBean unitTestBean = new ClassSaveTestOperation().getClassTestBean(row);
//                unitTestBean.setUnitTestId(row);
//                unitTestBean.setSubjectId(subId);
//                int testTime = new ClassSaveTestOperation().getClassTestTime(row);
//                if (testTime == 0) {
//                    unitTestBean.setTotalTime(60);
//                } else {
//                    unitTestBean.setTotalTime(testTime);
//                }
//                if (unitTestBean != null) {
//                    StartedUnitTestForm t = new StartedUnitTestForm(unitTestBean, true, true, row);
//                    t.setVisible(true);
//                    this.dispose();
//                }
//            }
//        } else {
//            ArrayList<RankBean> rb = new ClassSaveTestOperation().calculateRank(row);
//            if (rb.size() > 0) {
//                new RankGeneration(rb, row).setVisible(true);
//                this.dispose();
//            } else {
//                JOptionPane.showMessageDialog(null, "This is new test still not attempted by any students.");
//            }
//        }
//    }

        //    New Code for Distribute Test

            int row1 = tblSaveTest.getSelectedRow();
            row1++;   
            classTestList = new ClassSaveTestOperation().getClassTestInfobytestid(row1);     
//            Boolean Status = new ClassSaveTestOperation().getClassTestStatus(row1);
            if (evt.getClickCount() == 2)
                    {
                        int row = tblSaveTest.rowAtPoint(evt.getPoint());
                        int column = tblSaveTest.columnAtPoint(evt.getPoint());
                        row++;
                            if(row >= 0 && column == 5) 
                            {
//                                    if(Status)
//                                    {
//                                         JOptionPane.showMessageDialog(null, "This Test Already Attend So Please Create New Test.");
//                                    }
//                                    else
//                                    {
                                        new TestSetupPage(row1,this,classTestList).setVisible(true);
//                                    }   
                            }
                            else if(row >= 0 && column == 0 ||column == 1 ||column == 2 || column == 3 ||column == 4 )
                            {
                               new ViewTests( QuestionCount,tempclassTestList,classTestList).setVisible(true);
                            }
                    }

    }//GEN-LAST:event_tblSaveTestMouseClicked

    private void BtnViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnViewActionPerformed
        // TODO add your handling code here:
        int row = tblSaveTest.getSelectedRow();
        if (row >=0) {
        row++;
              
        UnitTestBean unitTestBean = new ClassSaveTestOperation().getClassTestBean(row);
        unitTestBean.setUnitTestId(row);
        unitTestBean.setSubjectId(subId);
        int testTime = new ClassSaveTestOperation().getClassTestTime(row);
        if (testTime == 0) {
            unitTestBean.setTotalTime(60);
        } else {
            unitTestBean.setTotalTime(testTime);
        }
        if (unitTestBean != null) {
            StartedUnitTestForm t = new StartedUnitTestForm(unitTestBean, true, true, row);
            t.setVisible(true);
            this.dispose();
        }
    }else {
                JOptionPane.showMessageDialog(null, "Please Select Test First");
         } 
    }//GEN-LAST:event_BtnViewActionPerformed

    private void BtnAnalysisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAnalysisActionPerformed
        // TODO add your handling code here:      
//        int row = tblSaveTest.getSelectedRow();
//        row++;
//        ArrayList<RankBean> rankBean = new ClassSaveTestOperation().calculateRank(row);
//        if (rankBean.size() > 0) {
//            new RankGeneration(rankBean, row).setVisible(true);
//            this.dispose();
//        } else {
//            JOptionPane.showMessageDialog(null, "This is new test still not attempted by students.");
//        }

        int row = tblSaveTest.getSelectedRow();
        if (row >=0) {  
        row++;
        
        RankBean rankBean = new RankBean();
        ArrayList<RankBean> rankBeanList = new ClassSaveTestOperation().calculateRank(row);
        
        
        if (rankBeanList.size() > 0) {
              for (int ii = 0; ii < rankBeanList.size(); ii++) {
                    
                    rankBean = rankBeanList.get(ii);
                       
              }
                       if(rankBean.getSubject_Id()==0){
                        new RankGenerationGroup(rankBeanList, row).setVisible(true);               
                        this.dispose();
                       }
                       else
                       {
                          new RankGeneration(rankBeanList, row).setVisible(true);
                          this.dispose(); 
                       }
            
        } else {
            JOptionPane.showMessageDialog(null, "This is new test still not attempted by students.");
        }
        }else {
                        JOptionPane.showMessageDialog(null, "Please Select Test First");
         } 
    }//GEN-LAST:event_BtnAnalysisActionPerformed

    private void SetComboItem()
    {
        
        ArrayList<String> SubjectString = new ArrayList<String>();  
             SubjectString=new SubjectOperation().getSubject();
            if (SubjectString.size() > 0) {
                
                for (int i = 0; i < SubjectString.size(); i++) {               
                    CmboSubject.addItem(SubjectString.get(i));
                }
           }
             
      String SubjectName=CmboSubject.getSelectedItem().toString();
      
      System.out.println("SubjectName=" +SubjectName);     
      SubjectBean subjectBean=new SubjectBean();
      subjectBean=new SubjectOperation().getSubjectBean2(SubjectName);  
      
      System.out.println("subjectBean=" +subjectBean.getMarksPerQuestion()); 
      System.out.println("subjectBean=" +subjectBean.getMarksPerWrong());
      
      txtPerQueMark.setText(Double.toString(subjectBean.getMarksPerQuestion()));
      txtWrongQueMark.setText(Double.toString(subjectBean.getMarksPerWrong()));
      
    }
    private void txtStudentNameKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtStudentNameKeyTyped
        // TODO add your handling code here:
//        char c = evt.getKeyChar();
//        if (Character.isLetter(c) || Character.isISOControl(c) || evt.getKeyCode() == 8 || evt.getKeyCode() == 127) {
//            evt = evt;
//            lblError.setText("");
//        } else {
//            evt.consume();
//            lblError.setText("Please Type Characters Only");
//        }
    }//GEN-LAST:event_txtStudentNameKeyTyped

    private void jLabel2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel2MouseClicked
        // TODO add your handling code here:
        new ConfigureServer1().setVisible(true);
    }//GEN-LAST:event_jLabel2MouseClicked

    
    
    private void txtMblNoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMblNoKeyPressed
        // TODO add your handling code here:
        keyPressedNumbers(evt, txtMblNo,10);
    }//GEN-LAST:event_txtMblNoKeyPressed

    private void txtMblNoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMblNoKeyReleased
        // TODO add your handling code here:
        keyReleasedRestrictText(txtMblNo,10);
    }//GEN-LAST:event_txtMblNoKeyReleased

    private void txtStudentNameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtStudentNameKeyPressed
        // TODO add your handling code here:       
           keyTypedAlphabets(evt,txtStudentName);
    }//GEN-LAST:event_txtStudentNameKeyPressed

    private void jLabel4KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jLabel4KeyPressed
        // TODO add your handling code here:
        
        
    }//GEN-LAST:event_jLabel4KeyPressed

    private void jLabel4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel4MouseClicked
        // TODO add your handling code here:
        new SenderIdSMS().setVisible(true);
        
    }//GEN-LAST:event_jLabel4MouseClicked

    private void ChkSelectAllItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ChkSelectAllItemStateChanged
        // TODO add your handling code here:
        if (testInfo != null) {
            boolean selected = ChkSelectAll.isSelected();
            DefaultTableModel model = (DefaultTableModel) tblSaveTest.getModel();
            for(int i=0;i<testInfo.size();i++)
            model.setValueAt(selected, i, 6);
        }
    }//GEN-LAST:event_ChkSelectAllItemStateChanged

    private void BtnAutoSendSMSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAutoSendSMSActionPerformed
        try {
            // TODO add your handling code here:
            DefaultTableModel model = (DefaultTableModel) tblSaveTest.getModel();
            ArrayList<Integer> row = new ArrayList<Integer>();
            ArrayList<RankBean> rankBeanList=new  ArrayList<RankBean>();
            ArrayList<RankBean> rankBeanList1=new  ArrayList<RankBean>();
            RankBean rankBean = new RankBean();
            for(int i=0;i<testInfo.size();i++) {
                if((boolean)model.getValueAt(i, 6)) {
                    
                    rankBeanList = new ClassSaveTestOperation().calculateRank(i+1);
                    for (RankBean rankBean1 : rankBeanList) {
                        rankBeanList1.add(rankBean1);
                    }
                }
            }
            
            String instituteName = new RegistrationOperation().getLastRegistrationInfoBean().getInstituteName().trim();
            
//         for (int ii = 0; ii < rankBeanList1.size(); ii++) {
//                    
//                    rankBean = rankBeanList1.get(ii);
//
//        }
//         
//                       if(rankBean.getSubject_Id()>0){
//                           SendSMS(rankBeanList1,instituteName);
//                       }
//                       else
//                       {
//                           SendGroupSMS(rankBeanList1,instituteName);
//                            
//                       }

        SendSMS(rankBeanList1,instituteName);
        
        } catch (SQLException ex) {
            Logger.getLogger(HomePage.class.getName()).log(Level.SEVERE, null, ex);
        }
         
    }//GEN-LAST:event_BtnAutoSendSMSActionPerformed

    private void btnSendSMSHistoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSendSMSHistoryActionPerformed
        try {
            // TODO add your handling code here:

            ArrayList<SMSHistoryBean> smsHistoryBeanList =new SMSHistoryOperation().getSMSHistoryList();
           
            new SendAllTestSMSHistory(smsHistoryBeanList,this).setVisible(true);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }//GEN-LAST:event_btnSendSMSHistoryActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        int row = tblSaveTest.getSelectedRow();
        if (row >=0) {
        row++;
        System.out.println("row"+row);

        Object[] options = {"YES", "CANCEL"};
        int i = JOptionPane.showOptionDialog(rootPane, "Are You Sure to Delete Test?", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
        if (i == 0) {

            System.out.println("row"+row);
            new ClassSaveTestOperation().deleteTest(row);
            tablesetting1(); //Distribute Test table settting
            resumeTest1();  //Distribute Test table settting
        }
        }else {
                        JOptionPane.showMessageDialog(null, "Please Select Test First");
               }   
    }//GEN-LAST:event_jButton2ActionPerformed

    private void btnAddQuestionsMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddQuestionsMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btnAddQuestionsMouseEntered

    private void btnAddQuestionsMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddQuestionsMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_btnAddQuestionsMouseExited

    private void btnAddQuestionsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddQuestionsActionPerformed
        // TODO add your handling code here:
        //    flagType = 5;
        printingPaperType = "AddQuestion";
        cardLayout.show(LeftBodyPanel, "SubjectCard");
    }//GEN-LAST:event_btnAddQuestionsActionPerformed

    private void btnAddDivisionMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddDivisionMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btnAddDivisionMouseEntered

    private void btnAddDivisionMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddDivisionMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_btnAddDivisionMouseExited

    private void btnAddDivisionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddDivisionActionPerformed
        // TODO add your handling code here:
        new Division().setVisible(true);
    }//GEN-LAST:event_btnAddDivisionActionPerformed

    private void btnAddStandardMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddStandardMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btnAddStandardMouseEntered

    private void btnAddStandardMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddStandardMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_btnAddStandardMouseExited

    private void btnAddStandardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddStandardActionPerformed
        // TODO add your handling code here:
        new ClassStandard().setVisible(true);
    }//GEN-LAST:event_btnAddStandardActionPerformed

    private void BtnBackSubjectSelection1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnBackSubjectSelection1MouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnBackSubjectSelection1MouseEntered

    private void BtnBackSubjectSelection1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnBackSubjectSelection1MouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnBackSubjectSelection1MouseExited

    private void BtnBackSubjectSelection1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBackSubjectSelection1ActionPerformed
        // TODO add your handling code here:
        this.dispose();
        new HomePage(1,2).setVisible(true);
    }//GEN-LAST:event_BtnBackSubjectSelection1ActionPerformed

    private void btnAddAcademicYearMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddAcademicYearMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btnAddAcademicYearMouseEntered

    private void btnAddAcademicYearMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddAcademicYearMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_btnAddAcademicYearMouseExited

    private void btnAddAcademicYearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddAcademicYearActionPerformed
        // TODO add your handling code here:
        new AcademicYear().setVisible(true);
    }//GEN-LAST:event_btnAddAcademicYearActionPerformed

    private void btnAddChapterMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddChapterMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btnAddChapterMouseEntered

    private void btnAddChapterMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddChapterMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_btnAddChapterMouseExited

    private void btnAddChapterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddChapterActionPerformed
        // TODO add your handling code here:
        printingPaperType = "AddChapter";
        cardLayout.show(LeftBodyPanel, "SubjectCard");
    }//GEN-LAST:event_btnAddChapterActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        
         new ResultReport().setVisible(true);
       
    }//GEN-LAST:event_jButton3ActionPerformed

    private void RdoStartServerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RdoStartServerActionPerformed
        // TODO add your handling code here:
         //Start Server Button

        boolean Status = new ClientOperation().ResetClientList();
        if(Status)
        {
            System.out.println("Successful deleted");
        }
        else
        {
            System.out.println(" Not Successful deleted");
        }

        new Thread(new Runnable() {
            @Override
           
            public void run() {
                System.out.println("*********************************");
                if (networkDBServer.start1()) {
                    System.out.println("**********************networkDBServer.start1()="+networkDBServer.start1());
                    JOptionPane.showMessageDialog(rootPane, "Database Server Started");
               
//                    BtnStartServer1.setEnabled(false);
//                    BtnStopServer.setEnabled(true);
                    BtnRefClientList.setEnabled(true);

                }
            }
        }).start();

        new Thread(new Runnable() {
            @Override   
            public void run() {
                networkDBServer.start2();
            }
        }).start();

        t = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {

                    loadClients();
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(HomePage.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
        t.start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    clientMonitorSocket.start();
                } catch (SQLException ex) {
                    Logger.getLogger(HomePage.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(HomePage.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }).start();

        loadClients();
    }//GEN-LAST:event_RdoStartServerActionPerformed

    private void RdoStopServerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RdoStopServerActionPerformed
        // TODO add your handling code here:
        
         Object[] options = {"YES", "NO"};
        int i = JOptionPane.showOptionDialog(null, "Database Server will be stop, do you want to continue...?", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
        if (i == 0) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    networkDBServer.shutDown();
                }
            }).start();
            JOptionPane.showMessageDialog(rootPane, "Database Server Stopped");
//            BtnStartServer.setEnabled(true);
//            BtnStopServer.setEnabled(false);
            if (t != null) {
                t.interrupt();
            }
        }
    }//GEN-LAST:event_RdoStopServerActionPerformed

    private void btnRecheckMarksActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRecheckMarksActionPerformed
        // TODO add your handling code here:
        int row = tblSaveTest.getSelectedRow();
        if (row >=0) {
            row++;
            System.out.println("row"+row);

            Object[] options = {"YES", "CANCEL"};
            int k = JOptionPane.showOptionDialog(rootPane, "Are You Sure to Recheck Marks?", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
            if (k == 0) {

                ArrayList<QuestionBean> questionBean1= new ArrayList<QuestionBean>();
                ArrayList<Integer> RollNo = new ArrayList<Integer>();
                boolean Status1 = false;
                RollNo = new ClassSaveTestOperation().getRollNo(row);

                for(int roll : RollNo) {

                    String userAnswer,numericalAnswer;
                    int rollno1,subjectId, chapterId, i = 0, corr = 0, incorr = 0,phycorr=0,chemcorr=0,mathcorr=0,phyincorr=0,chemincorr=0,mathincorr=0;
                    rollno1=roll;
                    System.out.println("rollno1="+rollno1);
                    questionBean1 =new ClassSaveTestOperation().getQuestions(row,rollno1);

                    //            if (questionBean != null) {

                        int totalQuestions = 0,totalPhyQues=0,totalChemQues=0,totalMathQues=0;
                        double TotalMarks = 0,totalPhyMarks=0,totalChemMark=0,totalMathMark=0;
                        double correctQuestions = 0.0,PhycorrectQuestions=0.0,ChemcorrectQuestions=0.0,MathcorrectQuestions=0.0;
                        double marksPerQuestion = 0.0, ngtvmarks,PhymarksPerQuestion=0.0,ChemmarksPerQuestion=0.0,MathmarksPerQuestion=0.0;

                        PhymarksPerQuestion = new ClassSaveTestOperation().getPerQuestionMarks(1);
                        ChemmarksPerQuestion = new ClassSaveTestOperation().getPerQuestionMarks(2);
                        MathmarksPerQuestion = new ClassSaveTestOperation().getPerQuestionMarks(4);

                        double Phyngtvmarks =new ClassSaveTestOperation().getPerwrongQuestionMarks(1);
                        double Chemngtvmarks = new ClassSaveTestOperation().getPerwrongQuestionMarks(2);
                        double Mathngtvmarks = new ClassSaveTestOperation().getPerwrongQuestionMarks(4);

                        if (questionBean1.size() > 0) {

                            Iterator<QuestionBean> it = questionBean1.iterator();
                            //TableCellRenderer buttonRenderer = new JTableButtonRenderer();
                            //tblPhysics.getColumn("button1").setCellRenderer(buttonRenderer);
                            int j = 1;
                            while (it.hasNext()) {

                                QuestionBean questionBean = it.next();
                                marksPerQuestion = new ClassSaveTestOperation().getPerQuestionMarks(questionBean.getSubjectId());
                                ngtvmarks = new ClassSaveTestOperation().getPerwrongQuestionMarks(questionBean.getSubjectId());

                                userAnswer = new ClassSaveTestOperation().getResult1(row,rollno1, questionBean.getQuestionId() );
                                numericalAnswer= new ClassSaveTestOperation().getnumAnsResult2(row,rollno1, questionBean.getQuestionId() );
                                System.out.println("Swapna userAnswer" + userAnswer);
                                System.out.println("Swapna numericalAnswer " + numericalAnswer);
                                System.out.println("questionBean.getQuestionId() " + questionBean.getQuestionId());
                                //                    TotalMarks=(totalQuestions * marksPerQuestion);
                                totalPhyQues=new ClassSaveTestOperation().getPhyCount(1,row);
                                totalChemQues=new ClassSaveTestOperation().getChemCount(2,row);
                                totalMathQues=new ClassSaveTestOperation().getMathCount(4,row);

                                totalPhyMarks=(totalPhyQues*PhymarksPerQuestion);
                                totalChemMark=(totalChemQues*ChemmarksPerQuestion);
                                totalMathMark=(totalMathQues*MathmarksPerQuestion);

                                TotalMarks= totalPhyMarks + totalChemMark + totalMathMark;
                                j++;
                               
                                    if (userAnswer != null) {

                                        if (userAnswer.equals("UnAttempted")) {

                                        } else {
                                            if (userAnswer.equals(questionBean.getAnswer())) { //userAnswer.equals(questionBean.getAnswer())

                                                correctQuestions = correctQuestions + marksPerQuestion;
                                                corr++;
                                                if(questionBean.getSubjectId()==1 )
                                                {
                                                    PhycorrectQuestions = PhycorrectQuestions + PhymarksPerQuestion;
                                                    phycorr++;
                                                }else if(questionBean.getSubjectId()==2 )
                                                {
                                                    ChemcorrectQuestions = ChemcorrectQuestions + ChemmarksPerQuestion;
                                                    chemcorr++;
                                                }else if(questionBean.getSubjectId()==3 )
                                                {
                                                    MathcorrectQuestions = MathcorrectQuestions + MathmarksPerQuestion;
                                                    mathcorr++;
                                                }

                                            } else {

                                                if (ngtvmarks <= 0) {
                                                    correctQuestions = correctQuestions + ngtvmarks;
                                                } else {
                                                    correctQuestions = correctQuestions - ngtvmarks;
                                                }
                                                incorr++;

                                                if(questionBean.getSubjectId()==1 )
                                                {
                                                    if (ngtvmarks <= 0) {
                                                        PhycorrectQuestions = PhycorrectQuestions + Phyngtvmarks;
                                                    } else {
                                                        PhycorrectQuestions = PhycorrectQuestions - Phyngtvmarks;
                                                    }
                                                    phyincorr++;
                                                }else if(questionBean.getSubjectId()==2 )
                                                {
                                                    if (ngtvmarks <= 0) {
                                                        ChemcorrectQuestions = ChemcorrectQuestions + Chemngtvmarks;
                                                    } else {
                                                        ChemcorrectQuestions = ChemcorrectQuestions - Chemngtvmarks;
                                                    }
                                                    chemincorr++;
                                                }else if(questionBean.getSubjectId()==3 )
                                                {
                                                    if (ngtvmarks <= 0) {
                                                        MathcorrectQuestions = MathcorrectQuestions + Mathngtvmarks;
                                                    } else {
                                                        MathcorrectQuestions = MathcorrectQuestions - Mathngtvmarks;
                                                    }
                                                    mathincorr++;
                                                }
                                            }
                                        }
                                        totalQuestions++;
                                    }
                                
                            }

                        } else {
                        }
                        //            int mo = 0;
                        //
                        //                mo = con.InsertTestDetails(unitTestBean.getQuestions(),rollNo);

                        //            if(mo>0){
                            correctQuestions=new RestrctDoubleUpto2().round(correctQuestions, 2);
                            PhycorrectQuestions=new RestrctDoubleUpto2().round(PhycorrectQuestions, 2);
                            ChemcorrectQuestions=new RestrctDoubleUpto2().round(ChemcorrectQuestions, 2);
                            MathcorrectQuestions=new RestrctDoubleUpto2().round(MathcorrectQuestions, 2);
                            Status1= new ClassSaveTestOperation().updateModifyResult(row,rollno1, corr, incorr,correctQuestions,TotalMarks,phycorr,chemcorr,mathcorr,phyincorr,chemincorr,mathincorr,totalPhyQues,totalChemQues,totalMathQues,PhycorrectQuestions,ChemcorrectQuestions,MathcorrectQuestions );

                            //            }
                        //        }
                }
                if(Status1)
                {
                    JOptionPane.showMessageDialog(null, "Sucessfully Recheck Mark");
                }
                else
                {
                    JOptionPane.showMessageDialog(null, "Not Sucessfully Recheck Mark");
                }
            }
        }else {
            JOptionPane.showMessageDialog(null, "Please Select Test First");
        }
    }//GEN-LAST:event_btnRecheckMarksActionPerformed

    private void SettingsTabStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_SettingsTabStateChanged
        // TODO add your handling code here:
        if(SettingsTab.getSelectedComponent().getName().equalsIgnoreCase("DefaultPathBody"))
        LblDirectoryPath.setText(" Current Directory : "+currentFolderDirectory);
        //            setDefalutFileDirectory();

    }//GEN-LAST:event_SettingsTabStateChanged

    private void coustom_weightMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_coustom_weightMouseEntered
        // TODO add your handling code here:
        btn_save.setVisible(true);
    }//GEN-LAST:event_coustom_weightMouseEntered

    private void coustom_weightMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_coustom_weightMouseClicked
        // TODO add your handling code here:
        btn_save.setVisible(true);
    }//GEN-LAST:event_coustom_weightMouseClicked

    private void coustom_weightFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_coustom_weightFocusGained
        // TODO add your handling code here:
        btn_save.setVisible(true);
    }//GEN-LAST:event_coustom_weightFocusGained

    private void coustom_weightStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_coustom_weightStateChanged
        // TODO add your handling code here:
        btn_save.setVisible(true);
    }//GEN-LAST:event_coustom_weightStateChanged

    private void bio_tableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bio_tableMouseClicked
        // TODO add your handling code here:
        new WeightageSettings().changeCellValue(evt);
    }//GEN-LAST:event_bio_tableMouseClicked

    private void chem_tableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_chem_tableMouseClicked
        // TODO add your handling code here:
        new WeightageSettings().changeCellValue(evt);
    }//GEN-LAST:event_chem_tableMouseClicked

    private void phy_tableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_phy_tableMouseClicked
        // TODO add your handling code here:
        new WeightageSettings().changeCellValue(evt);
    }//GEN-LAST:event_phy_tableMouseClicked

    private void btnResetUsedQActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnResetUsedQActionPerformed
        // TODO add your handling code here:
        Object[] options = {"YES", "CANCEL"};
        int i = JOptionPane.showOptionDialog(null, "Are You Sure to Reset Used Value Of All Chapter?", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
        if (i == 0)
        {
            if(new QuestionOperation().RefreshUsedValue())
            {
                JOptionPane.showMessageDialog(rootPane, "Sucessfull  Reset Used Value");
            }
            else
            {
                JOptionPane.showMessageDialog(rootPane, "Not Sucessfull  Reset Used Value");
            }
        }
    }//GEN-LAST:event_btnResetUsedQActionPerformed

    private void CmboSubjectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CmboSubjectActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_CmboSubjectActionPerformed

    private void CmboSubjectItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_CmboSubjectItemStateChanged
        // TODO add your handling code here:
        //      int SubjectID=0;
        //      SubjectID=CmboSubject.getSelectedIndex();
        //
        //      System.out.println("SubjectID=" +SubjectID);
        //      SubjectID++;
        //      SubjectBean subjectBean=new SubjectBean();
        //      subjectBean=new SubjectOperation().getSubjectBean1(SubjectID);
        //
        //      System.out.println("subjectBean=" +subjectBean.getMarksPerQuestion());
        //      System.out.println("subjectBean=" +subjectBean.getMarksPerWrong());
        //
        //      txtPerQueMark.setText(Double.toString(subjectBean.getMarksPerQuestion()));
        //      txtWrongQueMark.setText(Double.toString(subjectBean.getMarksPerWrong()));

        String SubjectName=CmboSubject.getSelectedItem().toString();

        System.out.println("SubjectName=" +SubjectName);
        SubjectBean subjectBean=new SubjectBean();
        subjectBean=new SubjectOperation().getSubjectBean2(SubjectName);

        System.out.println("subjectBean=" +subjectBean.getMarksPerQuestion());
        System.out.println("subjectBean=" +subjectBean.getMarksPerWrong());

        txtPerQueMark.setText(Double.toString(subjectBean.getMarksPerQuestion()));
        txtWrongQueMark.setText(Double.toString(subjectBean.getMarksPerWrong()));
    }//GEN-LAST:event_CmboSubjectItemStateChanged

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        // TODO add your handling code here:
        SubjectBean subjectBean=new SubjectBean();

        //        subjectBean.setSubjectId(CmboSubject.getSelectedIndex()+1);
        subjectBean.setSubjectName(CmboSubject.getSelectedItem().toString());
        subjectBean.setMarksPerQuestion(Double.parseDouble(txtPerQueMark.getText()));
        subjectBean.setMarksPerWrong(Double.parseDouble(txtWrongQueMark.getText()));

        boolean returnValue=new SubjectOperation().UpdateSubjectMarks1(subjectBean);
        if(returnValue)
        {
            JOptionPane.showMessageDialog(null, "SucessfullUpdate");
        }
        else
        {
            JOptionPane.showMessageDialog(null, "NotSucessfullUpdate");
        }
    }//GEN-LAST:event_btnSaveActionPerformed

    private void BtnSettingsSave2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSettingsSave2ActionPerformed
        // TODO add your handling code here:
        if(tempFileLocation != null) {
            if(!tempFileLocation.equalsIgnoreCase(currentFolderDirectory)) {
                currentFolderDirectory = tempFileLocation;
                writeFolderPathInDirectory();
            } else {
                JOptionPane.showMessageDialog(rootPane, "Selected Path Already Saved.");
            }
        }

    }//GEN-LAST:event_BtnSettingsSave2ActionPerformed

    private void BtnSettingsSave1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSettingsSave1ActionPerformed
        // TODO add your handling code here:
        tempFileLocation = new FileChooser().getFolderLocation(this,currentFolderDirectory);
        if(tempFileLocation != null)
        LblDirectoryPath.setText(" Current Directory : "+tempFileLocation);
        else
        tempFileLocation = currentFolderDirectory;
    }//GEN-LAST:event_BtnSettingsSave1ActionPerformed

    private void LblMorePatternInfoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblMorePatternInfoMouseClicked
        // TODO add your handling code here:
        if(chapterViewList == null)
        chapterViewList = new ChapterOperation().getViewChapterList();
        new PatternInfo(subjectList, chapterViewList, this).setVisible(true);
    }//GEN-LAST:event_LblMorePatternInfoMouseClicked

    private void BtnSettingsSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSettingsSaveActionPerformed
        Object[] options = {"YES", "CANCEL"};
        int i = JOptionPane.showOptionDialog(this, "Are you sure to change the pattern...?(will take time as per the system!)", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
        System.out.println("i= " +i);
        System.out.println("currentPatternIndex" +currentPatternIndex);

        if (i == 0) {
            patternIndexValue = 0;
            if (RdoPatternFirst.isSelected())
            patternIndexValue = 0;
            else if (RdoPatternSecond.isSelected())
            patternIndexValue = 1;
            else if (RdoPatternThird.isSelected())
            patternIndexValue = 2;

            System.out.println("patternIndexValue=" +patternIndexValue);

            if(patternIndexValue != currentPatternIndex) {
                currentPatternIndex = patternIndexValue;
                this.setEnabled(true);
                new ProcessingPanel(this).setVisible(true);
                testList = null;
                setTableRows();
                 initweightage();
                writePathInDirectory();
                setPatternRadio();
            } else {
                JOptionPane.showMessageDialog(rootPane, "Already You Have Same Pattern.");
            }

        }
    }//GEN-LAST:event_BtnSettingsSaveActionPerformed

    private void RdoPatternSecondActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RdoPatternSecondActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_RdoPatternSecondActionPerformed

    private void RdoPatternFirstActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RdoPatternFirstActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_RdoPatternFirstActionPerformed

    private void BtnBackSettingsSelectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBackSettingsSelectionActionPerformed
        // TODO add your handling code here:
        cardLayout.show(LeftBodyPanel, "HomeCard");
    }//GEN-LAST:event_BtnBackSettingsSelectionActionPerformed

    private void BtnBackSettingsSelectionMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnBackSettingsSelectionMouseExited
        // TODO add your handling code here:
        BtnBackSettingsSelection.setBackground(new java.awt.Color(11, 45, 55));
        BtnBackSettingsSelection.setForeground(new java.awt.Color(255, 255, 255));
    }//GEN-LAST:event_BtnBackSettingsSelectionMouseExited

    private void BtnBackSettingsSelectionMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnBackSettingsSelectionMouseEntered
        // TODO add your handling code here:
        BtnBackSettingsSelection.setBackground(new java.awt.Color(255, 255, 255));
        BtnBackSettingsSelection.setForeground(new java.awt.Color(11, 45, 55));
    }//GEN-LAST:event_BtnBackSettingsSelectionMouseEntered

    private void btn_saveFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_btn_saveFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_saveFocusGained

    private void btn_saveMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_saveMouseEntered
        // TODO add your handling code here:
        btn_save.setBackground(new java.awt.Color(255, 255, 255));
        btn_save.setForeground(new java.awt.Color(11, 45, 55));
    }//GEN-LAST:event_btn_saveMouseEntered

    private void btn_saveMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_saveMouseExited
        // TODO add your handling code here:
        btn_save.setBackground(new java.awt.Color(11, 45, 55));
        btn_save.setForeground(new java.awt.Color(255, 255, 255));
    }//GEN-LAST:event_btn_saveMouseExited
  private int getTableRowCount(Vector rows){
        int count = 0;
        for(int i=0; i< rows.size()-2; i++) {
            Vector row = (Vector) rows.elementAt(i);
            count+= (Integer) row.elementAt(2);
        }
        return count;
    }
     private int getTableRowCount1(Vector rows){
        int count = 0;
        for(int i=0; i< rows.size(); i++) {
            Vector row = (Vector) rows.elementAt(i);
            count+= (Integer) row.elementAt(2);
        }
        return count;
    }
     
     private void initweightage()
     {
        
         if(submasterchapterList == null)
            submasterchapterList = new SubMasterChapterOperation().getSubMasterChapterList1(currentPatternIndex+1); 
     int index = 0;
        for(SubjectBean subjectBean : subjectList) {
            if(index == 0)
                subjectFirstId = subjectBean.getSubjectId();
            else if(index == 1)
                subjectSecondId = subjectBean.getSubjectId();
            else if(index == 2)
                subjectThirdId = subjectBean.getSubjectId();
          
            index++;
        }
        setTableSettings(phy_table);
        setTableSettings(chem_table);
        setTableSettings(bio_table);
                 
//                 for(SubMasterChapterBean subMasterChapter: submasterchapterList)
//                  {     
//                      if(subMasterChapter.getSubjectBean().getSubjectId() == subjectFirstId) 
//                           {
//                               subFirstCount += subMasterChapter.getWeightage();
//                           }
//                      if(subMasterChapter.getSubjectBean().getSubjectId() == subjectSecondId) 
//                           {
//                               subSecondCount += subMasterChapter.getWeightage();
//                           }
//                      if(subMasterChapter.getSubjectBean().getSubjectId() == subjectThirdId) 
//                           {
//                               subThirdCount += subMasterChapter.getWeightage();
//                           }
//                  }
                groupList1=new SubMasterChapterOperation().getGroupList1(currentPatternIndex+1,subjectFirstId);
                groupList2=new SubMasterChapterOperation().getGroupList1(currentPatternIndex+1,subjectSecondId);   
                groupList3=new SubMasterChapterOperation().getGroupList1(currentPatternIndex+1,subjectThirdId);
                
                
                          loadValues1(groupList1); 
                          loadValues2(groupList2);
                          loadValues3(groupList3);
                
                    
                 
     }
private void setTableSettings(JTable table){
        table.getTableHeader().setDefaultRenderer(centerRenderer);
        table.getTableHeader().setForeground(Color.black);
        table.getTableHeader().setBackground(new Color(57, 97, 152));
        table.getColumnModel().getColumn(0).setPreferredWidth(40);
        table.getColumnModel().getColumn(1).setPreferredWidth(200);
        table.getColumnModel().getColumn(2).setPreferredWidth(80);
        table.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        table.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        table.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
        table.getTableHeader().setFont(new java.awt.Font("Microsoft JhengHei", java.awt.Font.PLAIN, 14));
    }
public void loadValues1(ArrayList<Object[]> groupList1) {
        DefaultTableModel model = (DefaultTableModel) phy_table.getModel();
        
      int subFirstCount=0;
        int count = model.getRowCount();
         
        if (count > 0) 
        {
            for (int i = count - 1; i >= 0; i--) 
            {
                model.removeRow(0);
            }
        }
        
        if (groupList1 != null)
        {
            if (groupList1.size() > 0)
            {  // model.addRow(new Object[]{srNo++},groupList1.get(1),groupList1.get(2));
                Iterator<Object[]> it = groupList1.iterator();
                while (it.hasNext()) 
                {
                model.addRow(it.next());   
                }
            }
        }
        Vector rowsFirstSubject = model.getDataVector();
       subFirstCount = getTableRowCount1(rowsFirstSubject);
        model.addRow(new Object[]{"","",""});
        model.addRow(new Object[]{"", " Total " , subFirstCount });
    }
      public void loadValues2(ArrayList<Object[]> groupList1) {
        DefaultTableModel model = (DefaultTableModel) chem_table.getModel();
        int count = model.getRowCount();
        int subSecondCount;
        if (count > 0) 
        {
            for (int i = count - 1; i >= 0; i--) 
            {
                model.removeRow(0);
            }
        }
        
        if (groupList1 != null)
        {
             if (groupList1.size() > 0)
            {
                Iterator<Object[]> it = groupList1.iterator();
                while (it.hasNext()) 
                {
                    model.addRow(it.next());                   
                }
            }
          Vector rowsSecSubject = model.getDataVector();
       subSecondCount = getTableRowCount1(rowsSecSubject);
         model.addRow(new Object[]{"","",""});
        model.addRow(new Object[]{"", " Total " , subSecondCount });
      }
   }
       public void loadValues3(ArrayList<Object[]> groupList1) {
        DefaultTableModel model = (DefaultTableModel) bio_table.getModel();
        int count = model.getRowCount();
        int subThirdCount;
        if (count > 0) 
        {
            for (int i = count - 1; i >= 0; i--) 
            {
                model.removeRow(0);
            }
        }
        
        if (groupList1 != null) {
            if (groupList1.size() > 0)
            {
                Iterator<Object[]> it = groupList1.iterator();
                while (it.hasNext()) 
                {
                    model.addRow(it.next());                    
            }
        }
             Vector rowsFourthSubject = model.getDataVector();
         subThirdCount = getTableRowCount1(rowsFourthSubject);
         model.addRow(new Object[]{"","",""});
        model.addRow(new Object[]{"", " Total " , subThirdCount });
    }
  }
    private void btn_saveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_saveActionPerformed
        // TODO add your handling code here:
        DefaultTableModel modelFirstSubject = (DefaultTableModel) phy_table.getModel();
        Vector rowsFirstSubject = modelFirstSubject.getDataVector();
        int subFirstweightCount = getTableRowCount(rowsFirstSubject);

        DefaultTableModel modelSecondSubject = (DefaultTableModel) chem_table.getModel();
        Vector rowsSecondSubject = modelSecondSubject.getDataVector();
        int subSecondweightCount = getTableRowCount(rowsSecondSubject);

        DefaultTableModel modelThirdSubject = (DefaultTableModel) bio_table.getModel();
        Vector rowsThirdSubject = modelThirdSubject.getDataVector();
        int subThirdweightCount = getTableRowCount(rowsThirdSubject);
        SubMasterchapterList1=new SubMasterChapterOperation().getSubMasterChapterList1(currentPatternIndex+1, subjectFirstId);
        SubMasterchapterList2=new SubMasterChapterOperation().getSubMasterChapterList1(currentPatternIndex+1, subjectSecondId);
        SubMasterchapterList3=new SubMasterChapterOperation().getSubMasterChapterList1(currentPatternIndex+1, subjectThirdId);
        if(subFirstweightCount == 100 && subSecondweightCount == 100 && subThirdweightCount == 100)
        {
            new SubMasterChapterOperation().updateChapterWeight(subjectFirstId,rowsFirstSubject,SubMasterchapterList1,currentPatternIndex+1);
            new SubMasterChapterOperation().updateChapterWeight(subjectSecondId,rowsSecondSubject,SubMasterchapterList2,currentPatternIndex+1);
            new SubMasterChapterOperation().updateChapterWeight(subjectThirdId,rowsThirdSubject,SubMasterchapterList3,currentPatternIndex+1);
            JOptionPane.showMessageDialog(rootPane, "All Settings are saved...!!!");
        }
        else
        {
            String message = "";
            if (subFirstweightCount != 100 && subSecondweightCount !=100 && subThirdweightCount !=100)
            {
                message = "All";
            }
            else if(subFirstweightCount != 100)
            {
                if(subSecondweightCount == 100 && subThirdweightCount == 100)
                {
                    new SubMasterChapterOperation().updateChapterWeight(subjectSecondId,rowsSecondSubject,SubMasterchapterList2,currentPatternIndex+1);
                    new SubMasterChapterOperation().updateChapterWeight(subjectThirdId,rowsThirdSubject,SubMasterchapterList3,currentPatternIndex+1);
                    message = "Physics = "+subFirstweightCount;
                }
                else if(subSecondweightCount != 100 && subThirdweightCount == 100)
                {
                    message = "Physics = "+subFirstweightCount+" Chem = "+subSecondweightCount;
                    new SubMasterChapterOperation().updateChapterWeight(subjectThirdId,rowsThirdSubject,SubMasterchapterList3,currentPatternIndex+1);
                }else if(subSecondweightCount == 100 && subThirdweightCount != 100)
                {
                    new SubMasterChapterOperation().updateChapterWeight(subjectSecondId,rowsSecondSubject,SubMasterchapterList2,currentPatternIndex+1);
                    message = "Physics = "+subFirstweightCount+" Bio = "+subThirdweightCount;
                }
            } else if ( subSecondweightCount != 100)
            {
                if(subFirstweightCount == 100 && subThirdweightCount == 100)
                {
                    new SubMasterChapterOperation().updateChapterWeight(subjectFirstId,rowsFirstSubject,SubMasterchapterList1,currentPatternIndex+1);
                    new SubMasterChapterOperation().updateChapterWeight(subjectThirdId,rowsThirdSubject,SubMasterchapterList3,currentPatternIndex+1);
                    message = "Chem = "+subSecondweightCount;
                }
                else if(subFirstweightCount != 100 && subThirdweightCount == 100)
                {
                    message = "Physics = "+subFirstweightCount+" Chem = "+subSecondweightCount;
                    new SubMasterChapterOperation().updateChapterWeight(subjectThirdId,rowsThirdSubject,SubMasterchapterList3,currentPatternIndex+1);
                }else if(subFirstweightCount == 100 && subThirdweightCount != 100)
                {
                    new SubMasterChapterOperation().updateChapterWeight(subjectFirstId,rowsFirstSubject,SubMasterchapterList1,currentPatternIndex+1);
                    message = "Chem = "+subSecondweightCount+" Bio = "+subThirdweightCount;
                }
            } else if(subThirdweightCount != 100)
            {
                if(subSecondweightCount == 100 && subFirstweightCount == 100)
                {
                    new SubMasterChapterOperation().updateChapterWeight(subjectSecondId,rowsSecondSubject,SubMasterchapterList2,currentPatternIndex+1);
                    new SubMasterChapterOperation().updateChapterWeight(subjectFirstId,rowsFirstSubject,SubMasterchapterList1,currentPatternIndex+1);
                    message = "Bio = "+subThirdweightCount;
                }
                else if(subSecondweightCount != 100 && subFirstweightCount == 100)
                {
                    message = "Chem = "+subSecondweightCount+" Bio = "+subThirdweightCount;
                    new SubMasterChapterOperation().updateChapterWeight(subjectFirstId,rowsFirstSubject,SubMasterchapterList1,currentPatternIndex+1);
                }else if(subSecondweightCount == 100 && subFirstweightCount != 100)
                {
                    new SubMasterChapterOperation().updateChapterWeight(subjectSecondId,rowsSecondSubject,SubMasterchapterList2,currentPatternIndex+1);
                    message = "Physics = "+subFirstweightCount+" Bio = "+subThirdweightCount;
                }

            }
            JOptionPane.showMessageDialog(rootPane,message + " Subject Weightage is invalid.");
            //this.dispose();
        }
    }//GEN-LAST:event_btn_saveActionPerformed

    private void BtnBackSelection2MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnBackSelection2MouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnBackSelection2MouseEntered

    private void BtnBackSelection2MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnBackSelection2MouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnBackSelection2MouseExited

    private void BtnBackSelection2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBackSelection2ActionPerformed
        // TODO add your handling code here:

        cardLayout.show(LeftBodyPanel, "HomeCard");
        tablesetting1(); //Distribute Test table settting
        resumeTest1();

    }//GEN-LAST:event_BtnBackSelection2ActionPerformed

    private void BtnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPrintActionPerformed
        // TODO add your handling code here:
        //Student Detail Module Print Button
        int koko = 0;
        try {
            String directoryTosave = "";
            String fileName = "";
            try {
                JFileChooser saveFile = new JFileChooser();//new save dialog
                saveFile.resetChoosableFileFilters();
                saveFile.setFileFilter(new FileFilter()//adds new filter into list
                    {
                        String description = "PDF Files(*.pdf)";//the filter you see
                        String extension = "pdf";//the filter passed to program

                        public String getDescription() {
                            return description;
                        }

                        public boolean accept(File f) {
                            if (f == null) {
                                return false;
                            }
                            if (f.isDirectory()) {
                                return true;
                            }
                            return f.getName().toLowerCase().endsWith(extension);
                        }
                    });

                    saveFile.setCurrentDirectory(new File("*.pdf"));
                    int result = saveFile.showSaveDialog(this);
                    // strSaveFileMenuItem is the parent Component which calls this method, ex: a toolbar, button or a menu item.
                    fileName = saveFile.getSelectedFile().getName();
                    directoryTosave = saveFile.getSelectedFile().getPath();
                    directoryTosave = saveFile.getCurrentDirectory().getPath();
                    System.out.println(fileName + "     path : " + directoryTosave);
                    koko = 1;
                    // the file name selected by the user is now in the string 'strFilename'.
                } catch (Exception er) {
                    //      statusBar.setText("Error Saving File:" + er.getMessage()+"\n");
                }
                if (koko == 1) {
                    int count = tblStudentDetail.getRowCount();
                    Document document = new Document();
                    //        String t = JOptionPane.showInputDialog("Enter Name of File.");
                    String fna = directoryTosave + "/" + fileName + ".pdf";
                    PdfWriter.getInstance(document, new FileOutputStream(fna));
                    document.open();
                    //for heading......
                    Font font = FontFactory.getFont("Times-Roman", 14, Font.BOLD);
                    Font font1 = FontFactory.getFont("Arial", 12, Font.BOLD);
                    Font font3 = FontFactory.getFont("Arial", 7, Font.BOLD);
                    Font font4 = FontFactory.getFont("Arial", 7, Font.NORMAL);
                    Font font5 = FontFactory.getFont("Baskerville Old Face", 22, Font.NORMAL);
                    Font font6 = FontFactory.getFont("Arial", 8, Font.BOLD);
                    Font font7 = FontFactory.getFont("Arial", 7, Font.BOLD);
                    Font font8 = FontFactory.getFont("Arial", 8, Font.NORMAL);

                    Paragraph paragraph = new Paragraph("", font);
                    Paragraph paragraph1 = new Paragraph("REGISTERED STUDENT DETAILS", font1);
                    Paragraph Space = new Paragraph("", font);
                    paragraph.setAlignment(Element.ALIGN_CENTER);
                    paragraph1.setAlignment(Element.ALIGN_CENTER);
                    paragraph.setSpacingAfter(3);
                    paragraph1.setSpacingAfter(15);

                    String instituteName = new RegistrationOperation().getLastRegistrationInfoBean().getInstituteName().trim();

                    PdfPTable tabHeadTitle = new PdfPTable(1);
                    float ffht = tabHeadTitle.getWidthPercentage();
                    tabHeadTitle.setWidthPercentage(100);
                    float[] sglTblHdWidthsHeadTitle = new float[1];
                    sglTblHdWidthsHeadTitle[0] = ((ffht * 80) / 80);// 1st column
                    tabHeadTitle.setWidths(sglTblHdWidthsHeadTitle);

                    PdfPCell cellht1 = new PdfPCell(new Phrase(instituteName, font5));
                    cellht1.setHorizontalAlignment(Element.ALIGN_CENTER);

                    cellht1.setUseAscender(true);
                    cellht1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    cellht1.setPadding(5.0f);
                    tabHeadTitle.addCell(cellht1);
                    document.add(tabHeadTitle);

                    Space.setSpacingAfter(4);
                    document.add(Space);
                    document.add(paragraph1);

                    //heading end.....
                    PdfPTable tab = new PdfPTable(8);
                    tab.setWidthPercentage(100);
                    float ff = tab.getWidthPercentage();
                    float[] sglTblHdWidths = new float[8];
                    sglTblHdWidths[0] = ((ff * 8) / 80);//Login id
                    sglTblHdWidths[1] = ((ff * 8) / 80);//User_Id
                    sglTblHdWidths[2] = ((ff * 36) / 80);//name
                    sglTblHdWidths[3] = ((ff * 15) / 80);//mblno
                    sglTblHdWidths[4] = ((ff * 12) / 80);//std
                    sglTblHdWidths[5] = ((ff * 11) / 80);//div
                    sglTblHdWidths[6] = ((ff * 24) / 80);//ay
                    sglTblHdWidths[7] = ((ff * 13) / 80);//pass
                    tab.setWidths(sglTblHdWidths);
                    //"Login ID", "User_ID","Student Name","mbl no", "Standard", "Division", "Academic Year", "Password"

                    PdfPCell cellh1 = new PdfPCell(new Phrase("LOGIN ID", font3));
                    PdfPCell cellh2 = new PdfPCell(new Phrase("USER ID", font3));
                    PdfPCell cellh3 = new PdfPCell(new Phrase("STUDENT NAME", font3));
                    PdfPCell cellh4 = new PdfPCell(new Phrase("MOBILE NO", font3));
                    PdfPCell cellh5 = new PdfPCell(new Phrase("STANDARD", font3));
                    PdfPCell cellh6 = new PdfPCell(new Phrase("DIVISION", font3));
                    PdfPCell cellh7 = new PdfPCell(new Phrase("ACADEMIC YEAR", font3));
                    PdfPCell cellh8 = new PdfPCell(new Phrase("PASSWORD", font3));

                    cellh1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cellh2.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cellh3.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cellh4.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cellh5.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cellh6.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cellh7.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cellh8.setHorizontalAlignment(Element.ALIGN_CENTER);

                    tab.addCell(cellh1);
                    tab.addCell(cellh2);
                    tab.addCell(cellh3);
                    tab.addCell(cellh4);
                    tab.addCell(cellh5);
                    tab.addCell(cellh6);
                    tab.addCell(cellh7);
                    tab.addCell(cellh8);

                    for (int i = 0; i < count; i++) {
                        Object obj1 = GetData(tblStudentDetail, i, 0);
                        Object obj2 = GetData(tblStudentDetail, i, 1);
                        Object obj3 = GetData(tblStudentDetail, i, 2);
                        Object obj4 = GetData(tblStudentDetail, i, 3);
                        Object obj5 = GetData(tblStudentDetail, i, 4);
                        Object obj6 = GetData(tblStudentDetail, i, 5);
                        Object obj7 = GetData(tblStudentDetail, i, 6);
                        Object obj8 = GetData(tblStudentDetail, i, 7);

                        String value1 = obj1.toString();
                        String value2 = obj2.toString();
                        String value3 = obj3.toString();
                        String value4 = obj4.toString();
                        String value5 = obj5.toString();
                        String value6 = obj6.toString();
                        String value7 = obj7.toString();
                        String value8 = obj8.toString();

                        PdfPCell cellData1 = new PdfPCell(new Phrase(value1, font4));
                        PdfPCell cellData2 = new PdfPCell(new Phrase(value2, font4));
                        PdfPCell cellData3 = new PdfPCell(new Phrase(value3, font4));
                        PdfPCell cellData4 = new PdfPCell(new Phrase(value4, font4));
                        PdfPCell cellData5 = new PdfPCell(new Phrase(value5, font4));
                        PdfPCell cellData6 = new PdfPCell(new Phrase(value6, font4));
                        PdfPCell cellData7 = new PdfPCell(new Phrase(value7, font4));
                        PdfPCell cellData8 = new PdfPCell(new Phrase(value8, font4));

                        cellData1.setHorizontalAlignment(Element.ALIGN_CENTER);
                        cellData2.setHorizontalAlignment(Element.ALIGN_CENTER);
                        cellData3.setHorizontalAlignment(Element.ALIGN_CENTER);
                        cellData4.setHorizontalAlignment(Element.ALIGN_CENTER);
                        cellData5.setHorizontalAlignment(Element.ALIGN_CENTER);
                        cellData6.setHorizontalAlignment(Element.ALIGN_CENTER);
                        cellData7.setHorizontalAlignment(Element.ALIGN_CENTER);
                        cellData8.setHorizontalAlignment(Element.ALIGN_CENTER);

                        tab.addCell(cellData1);
                        tab.addCell(cellData2);
                        tab.addCell(cellData3);
                        tab.addCell(cellData4);
                        tab.addCell(cellData5);
                        tab.addCell(cellData6);
                        tab.addCell(cellData7);
                        tab.addCell(cellData8);

                    }
                    document.add(tab);
                    document.close();
                    File f = new File(fna);
                    JLROpener.open(f);
                }
                else{
                    JOptionPane.showMessageDialog(null,"Please enter name for PDF.");
                }
            } catch (Exception e) {
            }
    }//GEN-LAST:event_BtnPrintActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        // TODO add your handling code here:
        studentBeanList=new StudentRegistrationOperation().getStudentInfo();
        int row = tblStudentDetail.getSelectedRow();
        System.out.println("row"+row);
        if(row >= 1) {
            Object[] options = {"YES", "CANCEL"};
            int i = JOptionPane.showOptionDialog(rootPane, "Are You Sure to Delete Student?", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
            if (i == 0) {
                row--;
                if(row >= 0) {

                    System.out.println("row"+row);
                    System.out.println("row");
                    System.out.println(studentBeanList.size());
                    StudentBean studentBean = studentBeanList.get(row);

                    System.out.println("studentBeanList.get(row)"+studentBeanList.get(row));
                    System.out.println("studentBeanList.get(row)"+studentBeanList.get(row));

                    new StudentRegistrationOperation().deleteStudent(studentBean);
                    int id = 0;
                    for(int j=0;j<row;j++) {
                        id = studentBeanList.get(j).getRollno();
                        studentBeanList.get(j).setRollno(id-1);
                    }
                    studentBeanList.remove(studentBean);
                    DefaultTableModel model = (DefaultTableModel) tblStudentDetail.getModel();
                    clearTableRow(model);
                    resumeTest();
                }
            }
        } else {
            JOptionPane.showMessageDialog(rootPane, "Select Valid Row For Deletion.");
        }
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnModifyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModifyActionPerformed
        // TODO add your handling code here:

        int row = tblStudentDetail.getSelectedRow();
        System.out.println("row"+row);
        studentBeanList=new StudentRegistrationOperation().getStudentInfo();
        if(row >= 1) {
            Object[] options = {"YES", "CANCEL"};
            int i = JOptionPane.showOptionDialog(rootPane, "Are You Sure to Modify "+studentBeanList.get(row-1).getStudentName()+" Record.", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
            if (i == 0) {
                row--;
                if(row >= 0) {
                    System.out.println("--------------Poonam--------------------");
                    StudentBean studentBean = studentBeanList.get(row);
                    new StudentModification(studentBean,this).setVisible(true);
                    this.setVisible(false);

                    setTableRows1();
                    resumeTest();
                }
            }
        } else {
            JOptionPane.showMessageDialog(rootPane, "Select Valid Row For Deletion.");
        }

    }//GEN-LAST:event_btnModifyActionPerformed

    private void cmbStandardItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbStandardItemStateChanged
        // TODO add your handling code here:

        ArrayList<StudentBean> studentList = new ArrayList<StudentBean>();
        DefaultTableModel model = (DefaultTableModel) tblStudentDetail.getModel();
        clearTableRow(model);

        Standard = (String) cmbStandard.getSelectedItem();
        Division= (String)cmbDivision.getSelectedItem();

        if(Standard.equals("All"))
        {
            studentBeanList=new StudentRegistrationOperation().getStudentInfo();
            clearTableRow(model);
            model.addRow(new Object[]{"", "", "", "", "", "", "", ""});
            if(studentBeanList != null && !studentBeanList.isEmpty()) {
                for(int i=0 ;i < studentBeanList.size() ;i++)
                model.addRow(new Object[]{studentBeanList.get(i).getRollno(), studentBeanList.get(i).getFullname(),studentBeanList.get(i).getStudentName(),studentBeanList.get(i).getMobileNo(), studentBeanList.get(i).getStandard(), studentBeanList.get(i).getDivision(), studentBeanList.get(i).getAcademicYear(), studentBeanList.get(i).getPass()});
            }
            btnModify.setEnabled(true);
            btnDelete.setEnabled(true);
            btnImportFile.setEnabled(true);
            btnExport.setEnabled(true);
        }
        else
        {
            studentList = new StudentRegistrationOperation().getStudentInfoByStandard(Standard);
            clearTableRow(model);
            model.addRow(new Object[]{"", "", "", "", "", "", "", ""});
            if(studentList != null && !studentList.isEmpty()) {
                for(int i=0 ;i < studentList.size() ;i++)
                //                        model.addRow(new Object[]{studentList.get(i).getRollno(), studentList.get(i).getFullname(),studentList.get(i).getStudentName(),studentList.get(i).getMobileNo(), studentList.get(i).getStandard(), studentList.get(i).getDivision(), studentList.get(i).getAcademicYear(), studentList.get(i).getPass()});
                model.addRow(new Object[]{i+1, studentList.get(i).getFullname(),studentList.get(i).getStudentName(),studentList.get(i).getMobileNo(), studentList.get(i).getStandard(), studentList.get(i).getDivision(), studentList.get(i).getAcademicYear(), studentList.get(i).getPass()});
            }
            btnModify.setEnabled(false);
            btnDelete.setEnabled(false);
            btnImportFile.setEnabled(false);
            btnExport.setEnabled(false);
        }

    }//GEN-LAST:event_cmbStandardItemStateChanged

    private void cmbDivisionItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbDivisionItemStateChanged
        // TODO add your handling code here:
        ArrayList<StudentBean> studentList = new ArrayList<StudentBean>();
        DefaultTableModel model = (DefaultTableModel) tblStudentDetail.getModel();
        clearTableRow(model);

        Standard = (String) cmbStandard.getSelectedItem();
        Division= (String)cmbDivision.getSelectedItem();

        if(Standard.equals("All") && Division.equals("All"))
        {
            studentBeanList=new StudentRegistrationOperation().getStudentInfo();
            clearTableRow(model);
            model.addRow(new Object[]{"", "", "", "", "", "", "", ""});
            if(studentBeanList != null && !studentBeanList.isEmpty()) {
                for(int i=0 ;i < studentBeanList.size() ;i++)
                model.addRow(new Object[]{studentBeanList.get(i).getRollno(), studentBeanList.get(i).getFullname(),studentBeanList.get(i).getStudentName(),studentBeanList.get(i).getMobileNo(), studentBeanList.get(i).getStandard(), studentBeanList.get(i).getDivision(), studentBeanList.get(i).getAcademicYear(), studentBeanList.get(i).getPass()});
            }
            btnModify.setEnabled(true);
            btnDelete.setEnabled(true);
            btnImportFile.setEnabled(true);
            btnExport.setEnabled(true);
        }
        else
        {
            studentList = new StudentRegistrationOperation().getStudentInfoByStandardDivision(Standard, Division);
            clearTableRow(model);
            model.addRow(new Object[]{"", "", "", "", "", "", "", ""});
            if(studentList != null && !studentList.isEmpty()) {
                for(int i=0 ;i < studentList.size() ;i++)
                //                         model.addRow(new Object[]{studentList.get(i).getRollno(), studentList.get(i).getFullname(),studentList.get(i).getStudentName(),studentList.get(i).getMobileNo(), studentList.get(i).getStandard(), studentList.get(i).getDivision(), studentList.get(i).getAcademicYear(), studentList.get(i).getPass()});
                model.addRow(new Object[]{i+1, studentList.get(i).getFullname(),studentList.get(i).getStudentName(),studentList.get(i).getMobileNo(), studentList.get(i).getStandard(), studentList.get(i).getDivision(), studentList.get(i).getAcademicYear(), studentList.get(i).getPass()});
            }
            btnModify.setEnabled(false);
            btnDelete.setEnabled(false);
            btnImportFile.setEnabled(false);
            btnExport.setEnabled(false);
        }

    }//GEN-LAST:event_cmbDivisionItemStateChanged

    private void cmbAcademicYearItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbAcademicYearItemStateChanged
        // TODO add your handling code here:
        ArrayList<StudentBean> studentList = new ArrayList<StudentBean>();
        DefaultTableModel model = (DefaultTableModel) tblStudentDetail.getModel();
        clearTableRow(model);

        Standard = (String) cmbStandard.getSelectedItem();
        Division= (String)cmbDivision.getSelectedItem();
        AcademicYear= (String)cmbAcademicYear.getSelectedItem();

        if( AcademicYear.equals("All"))
        {
            studentBeanList=new StudentRegistrationOperation().getStudentInfo();
            clearTableRow(model);
            model.addRow(new Object[]{"", "", "", "", "", "", "", ""});
            if(studentBeanList != null && !studentBeanList.isEmpty()) {
                for(int i=0 ;i < studentBeanList.size() ;i++)
                model.addRow(new Object[]{studentBeanList.get(i).getRollno(), studentBeanList.get(i).getFullname(),studentBeanList.get(i).getStudentName(),studentBeanList.get(i).getMobileNo(), studentBeanList.get(i).getStandard(), studentBeanList.get(i).getDivision(), studentBeanList.get(i).getAcademicYear(), studentBeanList.get(i).getPass()});
            }
            btnModify.setEnabled(true);
            btnDelete.setEnabled(true);
            btnImportFile.setEnabled(true);
            btnExport.setEnabled(true);
        }
        else
        {
            studentList = new StudentRegistrationOperation().getStudentInfoByStandardDivisionAcdemicYear(Standard, Division,AcademicYear);
            clearTableRow(model);
            model.addRow(new Object[]{"", "", "", "", "", "", "", ""});
            if(studentList != null && !studentList.isEmpty()) {
                for(int i=0 ;i < studentList.size() ;i++)
                //                        model.addRow(new Object[]{studentList.get(i).getRollno(), studentList.get(i).getFullname(),studentList.get(i).getStudentName(),studentList.get(i).getMobileNo(), studentList.get(i).getStandard(), studentList.get(i).getDivision(), studentList.get(i).getAcademicYear(), studentList.get(i).getPass()});
                model.addRow(new Object[]{i+1, studentList.get(i).getFullname(),studentList.get(i).getStudentName(),studentList.get(i).getMobileNo(), studentList.get(i).getStandard(), studentList.get(i).getDivision(), studentList.get(i).getAcademicYear(), studentList.get(i).getPass()});
            }
            btnModify.setEnabled(false);
            btnDelete.setEnabled(false);
            btnImportFile.setEnabled(false);
            btnExport.setEnabled(false);
        }

    }//GEN-LAST:event_cmbAcademicYearItemStateChanged

    private void btnImportFileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImportFileActionPerformed
        //         try {
            //            // TODO add your handling code here:
            //
            //
            //            StudentBean studentBean = new StudentBean();
            ////            String fileLocation = new FileChooser().getExcelFileLocation(this);
            ////            System.out.println("*****fileLocation:="+fileLocation);
            ////            String Directory=fileLocation.split(",")[0];
            ////            String fileName=fileLocation.split(",")[1];
            ////
            //
            ////            FileInputStream input = new FileInputStream(Directory + "\\" + fileName);
                ////            POIFSFileSystem fs = new POIFSFileSystem( input );
                //            File excelFile;
                //            String DefPath="C:\\Users\\Admin\\Desktop";
                //            JFileChooser exlFilechooser = new JFileChooser(DefPath);
                //            FileNameExtensionFilter fltr =new FileNameExtensionFilter("Excel Files","xls","xlsx","xlsm");
                //            exlFilechooser.setFileFilter(fltr);
                //            exlFilechooser.setDialogTitle("Select Excel File");
                //            int exlchooser=exlFilechooser.showOpenDialog(null);
                //            if(exlchooser==JFileChooser.APPROVE_OPTION )
                //            {
                    //               excelFile=exlFilechooser.getSelectedFile();
                    //
                    //                fs = new FileInputStream(excelFile);
                    //                Bfs= new BufferedInputStream(fs);
                    //
                    //                workbook1 = new XSSFWorkbook(Bfs);
                    //                worksheet1 = workbook1.getSheetAt(0);
                    //            fbfg
                    //
                    //
                    //                    XSSFRow row = null;
                    //                    for(int i=1; i<=worksheet1.getLastRowNum(); i++){
                        //                        row = worksheet1.getRow(i);
                        //
                        ////                         XSSFCell cell1=row.getCell(1);
                        ////                         XSSFCell cell2=row.getCell(2);
                        ////                         XSSFCell cell3=row.getCell(3);
                        ////                         XSSFCell cell4=row.getCell(4);
                        ////                         XSSFCell cell5=row.getCell(5);
                        ////                         XSSFCell cell6=row.getCell(6);
                        ////                         XSSFCell cell7=row.getCell(7);
                        ////                        int Rollno=new NewIdOperation().getNewId("STUDENT_INFO");
                        ////                        studentBean.setRollno(Rollno);
                        ////                        studentBean.setFullname(cell1.toString());
                        ////                        studentBean.setStudentName(cell2.toString());
                        ////                        studentBean.setMobileNo(cell3.toString());
                        ////                        studentBean.setStandard(cell4.toString());
                        ////                        studentBean.setDivision(cell5.toString());
                        ////                        studentBean.setAcademicYear(cell6.toString());
                        ////                        studentBean.setPass(cell7.toString());
                        ////                        System.out.println(cell1);
                        //
                        //
                        //                int Rollno=new NewIdOperation().getNewId("STUDENT_INFO");
                        //                studentBean.setRollno(Rollno);
                        //                studentBean.setFullname(Integer.toString((int)row.getCell(1).getNumericCellValue()));
                        //                studentBean.setStudentName(row.getCell(2).getStringCellValue());
                        //                studentBean.setMobileNo(Long.toString((long)row.getCell(3).getNumericCellValue()));
                        //                studentBean.setStandard(row.getCell(4).getStringCellValue());
                        //                studentBean.setDivision(row.getCell(5).getStringCellValue());
                        //                studentBean.setAcademicYear(Integer.toString((int) row.getCell(6).getNumericCellValue()));
                        //                studentBean.setPass(row.getCell(7).getStringCellValue());
                        //
                        //
                        //
                        //                   Status=new StudentRegistrationOperation().insertStudentRegistration(studentBean);
                        //
                        //
                        //            }
                    //
                    //            if(Status)
                    //            {
                        //                JOptionPane.showMessageDialog(this, "Sucessfull Import");
                        //
                        //            }
                    //            else
                    //            {
                        //                JOptionPane.showMessageDialog(this, "Not Sucessfull Import");
                        //
                        //            }
                    //           fs.close();
                    //
                    //            resumeTest();
                    //        }
                //        } catch (Exception ex) {
                //            ex.printStackTrace();
                //        }

            try {
                // TODO add your handling code here:
                ProgressBarClass m=new ProgressBarClass(); 
                        m.setVisible(true); 
                        
                //Boolean Status=false;
                StudentBean studentBean = new StudentBean();
                String fileLocation = new FileChooser().getExcelFileLocation(this);
                System.out.println("*****fileLocation:="+fileLocation);
                String Directory=fileLocation.split(",")[0];
                String fileName=fileLocation.split(",")[1];

                FileInputStream input = new FileInputStream(Directory + "/" + fileName);
                POIFSFileSystem fs = new POIFSFileSystem( input );
                workbook = new HSSFWorkbook(fs);
                worksheet = workbook.getSheetAt(0);
                Row row;
                for(int i=1; i<=worksheet.getLastRowNum(); i++){
                    row = worksheet.getRow(i);
                    
                    m.iterate(i);
                    
                    int Rollno=new NewIdOperation().getNewId("STUDENT_INFO");
                    studentBean.setRollno(Rollno);

//                    //studentBean.setStudentName(row.getCell(1).getStringCellValue());
//                    // studentBean.setfullname(Integer.toString((int)row.getCell(1).getNumericCellValue()));
//                    studentBean.setFullname(row.getCell(1).getStringCellValue());
//                    studentBean.setStudentName(row.getCell(2).getStringCellValue());
//                    // studentBean.setMobileNo(Long.toString((long)row.getCell(3).getNumericCellValue()));
//                    studentBean.setMobileNo(row.getCell(3).getStringCellValue());
//                    studentBean.setStandard(row.getCell(4).getStringCellValue());
//                    studentBean.setDivision(row.getCell(5).getStringCellValue());
//                    studentBean.setAcademicYear(row.getCell(6).getStringCellValue());
//                    //studentBean.setPass(Integer.toString((int)row.getCell(7).getNumericCellValue()));
//                    studentBean.setPass(row.getCell(7).getStringCellValue());

               //-------------Edited By Poonam------------------------
                    DataFormatter formatter = new DataFormatter();
                   
                    studentBean.setFullname(formatter.formatCellValue(row.getCell(1)));
                    studentBean.setStudentName(formatter.formatCellValue(row.getCell(2)));              
                    studentBean.setMobileNo(formatter.formatCellValue(row.getCell(3)));
                    studentBean.setStandard(formatter.formatCellValue(row.getCell(4)));
                    studentBean.setDivision(formatter.formatCellValue(row.getCell(5)));
                    studentBean.setAcademicYear(formatter.formatCellValue(row.getCell(6)));
                    studentBean.setPass(formatter.formatCellValue(row.getCell(7)));
                  

                    new StudentRegistrationOperation().insertStudentRegistration(studentBean);

                }
//                if(Status)
//                {
//                    JOptionPane.showMessageDialog(this, "Sucessfull Import");
//                }
//                else
//                {
//                    JOptionPane.showMessageDialog(this, "Sucessfull Import");
//
//                }
                input.close();

                resumeTest();
            } catch (IOException ex) {
                Logger.getLogger(HomePage.class.getName()).log(Level.SEVERE, null, ex);
            }

    }//GEN-LAST:event_btnImportFileActionPerformed

    private void btnExportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExportActionPerformed
        //        try {
            //            // TODO add your handling code here:
            ////            ArrayList<StudentBean> studentList = new ArrayList<StudentBean>();
            ////            studentList = new StudentRegistrationOperation().getStudentInfo();
            //            String fileLocation = new FileChooser().getExcelFileLocation(this);
            //            System.out.println("*****fileLocation:="+fileLocation);
            //            String Directory=fileLocation.split(",")[0];
            //            String fileName=fileLocation.split(",")[1];
            //
            //
            //            FileOutputStream fileOut;
            //            fileOut = new FileOutputStream(Directory + "/" + fileName+".xlsx");
            //            workbook1 = new XSSFWorkbook();
            //            worksheet1 = workbook1.createSheet("JEE Student List");
            //
            //            row1 = worksheet1.createRow((short)0);
            //            row1.createCell(0).setCellValue("Login Id");
            //            row1.createCell(1).setCellValue("User Id");
            //            row1.createCell(2).setCellValue("Student Name");
            //            row1.createCell(3).setCellValue("Mobile No");
            //            row1.createCell(4).setCellValue("Stanard");
            //            row1.createCell(5).setCellValue("Division");
            //            row1.createCell(6).setCellValue("Academic Year");
            //            row1.createCell(7).setCellValue("Password");
            //           XSSFRow row2 = null;
            //
            //            if(studentBeanList != null && !studentBeanList.isEmpty()) {
                //                  for(int i=0 ;i < studentBeanList.size() ;i++)
                //                  {
                    //                    row2 = worksheet1.createRow((short)i+1);
                    //                    row2.createCell(0).setCellValue(studentBeanList.get(i).getRollno());
                    //                    row2.createCell(1).setCellValue(studentBeanList.get(i).getFullname());
                    //                    row2.createCell(2).setCellValue(studentBeanList.get(i).getStudentName());
                    //                    row2.createCell(3).setCellValue(studentBeanList.get(i).getMobileNo());
                    //                    row2.createCell(4).setCellValue(studentBeanList.get(i).getStandard());
                    //                    row2.createCell(5).setCellValue(studentBeanList.get(i).getDivision());
                    //                    row2.createCell(6).setCellValue(studentBeanList.get(i).getAcademicYear());
                    //                    row2.createCell(7).setCellValue(studentBeanList.get(i).getPass());
                    //                  }
                //
                //            }
            //            workbook1.write(fileOut);
            //            fileOut.flush();
            //            fileOut.close();
            //            JOptionPane.showMessageDialog(this, "Export Success");
            //        } catch (FileNotFoundException ex) {
            //            ex.printStackTrace();
            //            System.out.println(ex);
            //        } catch (IOException ex) {
            //            ex.printStackTrace();
            //            System.out.println(ex);
            //        }

        try {
            // TODO add your handling code here:
            //            ArrayList<StudentBean> studentList = new ArrayList<StudentBean>();
            //            studentList = new StudentRegistrationOperation().getStudentInfo();
            String fileLocation = new FileChooser().getExcelFileLocation(this);
            System.out.println("*****fileLocation:="+fileLocation);
            String Directory=fileLocation.split(",")[0];
            String fileName=fileLocation.split(",")[1];

            FileOutputStream fileOut;
            fileOut = new FileOutputStream(Directory + "/" + fileName+".xls");
            workbook = new HSSFWorkbook();
            worksheet = workbook.createSheet("JEE Student List");

            Row row1 = worksheet.createRow((short)0);
            row1.createCell(0).setCellValue("Login Id");
            row1.createCell(1).setCellValue("User Id");
            row1.createCell(2).setCellValue("Student Name");
            row1.createCell(3).setCellValue("Mobile No");
            row1.createCell(4).setCellValue("Stanard");
            row1.createCell(5).setCellValue("Division");
            row1.createCell(6).setCellValue("Academic Year");
            row1.createCell(7).setCellValue("Password");
            Row row2 ;

            studentBeanList=new StudentRegistrationOperation().getStudentInfo();
            //Collections.sort(studentBeanList);
            if(studentBeanList != null && !studentBeanList.isEmpty()) {
                for(int i=0 ;i < studentBeanList.size() ;i++)
                {

                    System.out.print("not null.............................................");
                    row2 = worksheet.createRow((short)i+1);
                    row2.createCell(0).setCellValue(studentBeanList.get(i).getRollno());
                    row2.createCell(1).setCellValue(studentBeanList.get(i).getFullname());
                    row2.createCell(2).setCellValue(studentBeanList.get(i).getStudentName());
                    row2.createCell(3).setCellValue(studentBeanList.get(i).getMobileNo());
                    row2.createCell(4).setCellValue(studentBeanList.get(i).getStandard());
                    row2.createCell(5).setCellValue(studentBeanList.get(i).getDivision());
                    row2.createCell(6).setCellValue(studentBeanList.get(i).getAcademicYear());
                    row2.createCell(7).setCellValue(studentBeanList.get(i).getPass());
                }

            }else{
                System.out.print("null...............................................");
            }
            workbook.write(fileOut);
            fileOut.flush();
            fileOut.close();
            JOptionPane.showMessageDialog(this, "Export Success...!!!");
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
            System.out.println(ex);
        } catch (IOException ex) {
            ex.printStackTrace();
            System.out.println(ex);
        }

    }//GEN-LAST:event_btnExportActionPerformed

    private void btnDeleteAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteAllActionPerformed
        // TODO add your handling code here:
        studentBeanList=new StudentRegistrationOperation().getStudentInfo();

        Object[] options = {"YES", "CANCEL"};
        int i = JOptionPane.showOptionDialog(rootPane, "Are You Sure to Delete Students Records?", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
        if (i == 0) {
            try {
                new StudentRegistrationOperation().deleteAll();

                JOptionPane.showMessageDialog(rootPane, "Records Deleted Successfully...!!!");
                DefaultTableModel model = (DefaultTableModel) tblStudentDetail.getModel();
                clearTableRow(model);
                model.addRow(new Object[]{"", "", "", "", "", "", "", ""});
            } catch (SQLException ex) {
                Logger.getLogger(HomePage.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_btnDeleteAllActionPerformed

    private void tblStudentDetailMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblStudentDetailMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tblStudentDetailMouseClicked
    
    private void SendSMS( ArrayList<RankBean> rankBeanList1,String instituteName) throws SQLException
    {
        if (rankBeanList1 != null) {
                String textMessage = "";
                String smsResponse = "";
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                
               
                    senderIdList= new SenderIdOperation().getSenderIdbyflagList();
//                    senderIdList= new SenderIdOperation().getSenderIdList();
            if (!senderIdList.isEmpty() ) {
                    for(SenderIdBean senderIdBean : senderIdList) 
                    {
                        this.senderIdBean=senderIdBean;                      
                        System.out.println("senderIdBean.getSenderId()"+senderIdBean.getSenderId()); 
                    }
               
                String UserName=senderIdBean.getUserName().trim();
                String Password=senderIdBean.getPassword().trim();
                String SenderId=senderIdBean.getSenderId().trim();
                System.out.println(UserName+"...."+Password+"...."+SenderId);
                
                
                for(RankBean rankBean : rankBeanList1) {
                    if(rankBean.getSubject_Id()>0){
                        

                                String SubjectName=new SubjectOperation().getSubject(rankBean.getSubject_Id());
                                String testName=new ClassSaveTestOperation().getTestName(rankBean.getUnitTestId());
                                System.out.println("testName="+testName+"rankBean.getUnitTestId()");
                                studentBeanList=new StudentRegistrationOperation().getAllInfoStudent(rankBean.getRollNo());

                                for(StudentBean studentBean : studentBeanList) {

                                textMessage = "";
                                textMessage += instituteName.trim() + "\n";
                                textMessage += "Name : "+studentBean.getStudentName().trim() + "\n";
                                textMessage += "Subject Name : MH-CET-"+SubjectName.trim() + "\n";
                                textMessage += "Test Id : "+rankBean.getUnitTestId()+ "\n";
                                textMessage += "Test Name : " +testName+ "\n";
                                textMessage += "Class/Stand : "+studentBean.getStandard().trim() +"/"+studentBean.getDivision().trim()+ "\n";
                                textMessage += "Correct : "+rankBean.getCorrect_Questions()+"\n";
                                textMessage += "Incorrect : "+rankBean.getIncorrect_Questions()+"\n";
                                textMessage += "Marks : "+rankBean.getObtainMark()+"/"+rankBean.getTotalMark()+"\n";
            //                    textMessage += "Date : "+sdf.format(new Date(rankBean.getTimeinstring().trim()));        

            //                    smsResponse +=  new SmsOperation().sendMessages1(UserName,Password,textMessage,SenderId,studentBean.getMobileNo().trim())+"," ;
            //                    smsResponse +=new SmsOperation().sendSMS(UserName,Password,textMessage,SenderId,studentBean.getMobileNo().trim())+"," ;
                                new SmsOperation().sendSMS(studentBean.getStudentName(),rankBean.getUnitTestId(),testName,rankBean.getRollNo(),UserName,Password,textMessage,SenderId,studentBean.getMobileNo().trim());
            //                    System.out.println(smsResponse);
                     
                    }
                }
                    else
                        {
                            
                            //  String SubjectName=new SubjectOperation().getSubject(rankBean.getSubject_Id());
                                String testName=new ClassSaveTestOperation().getTestName(rankBean.getUnitTestId());

                                studentBeanList=new StudentRegistrationOperation().getAllInfoStudent(rankBean.getRollNo());

                                for(StudentBean studentBean : studentBeanList) {

                                textMessage = "";
                                textMessage += instituteName.trim() + "\n";
                                textMessage += "Name :"+studentBean.getStudentName().trim() + "\n";
                                textMessage += "Subject Name :"+ "MH-CET-PCB" + ".\n";
                                textMessage += "Test Id :"+rankBean.getUnitTestId()+ "\n";
                                textMessage += "Test Name :"+ testName+ "\n";
                                textMessage += "Class/Stand :"+studentBean.getStandard().trim() +"/"+studentBean.getDivision().trim()+ "\n";                   
                                textMessage += "Phy Correct/Incorrect :"+rankBean.getPHY_CORRECT_QUESTIONS()+"/"+rankBean.getPHY_INCORRECT_QUESTIONS()+"\n";
                                textMessage += "Chem Correct/Incorrect :"+rankBean.getCHEM_CORRECT_QUESTIONS()+"/"+rankBean.getCHEM_INCORRECT_QUESTIONS()+"\n";
                                textMessage += "Bio Correct/Incorrect :"+rankBean.getBIO_CORRECT_QUESTIONS()+"/"+rankBean.getBIO_INCORRECT_QUESTIONS()+"\n";                  
                                textMessage += "Total Correct/Incorrect:"+rankBean.getCorrect_Questions()+"/"+rankBean.getIncorrect_Questions()+"\n";
                                textMessage += "Phy Marks :"+rankBean.getTOTAL_PHY_MARK()+"/"+rankBean.getTOTAL_PHY_QUESTIONS()+"\n";
                                textMessage += "Chem Marks :"+rankBean.getTOTAL_CHEM_MARK()+"/"+rankBean.getTOTAL_CHEM_QUESTIONS()+"\n";
                                textMessage += "Bio Marks :"+rankBean.getTOTAL_BIO_MARK()+"/"+rankBean.getTOTAL_BIO_QUESTIONS()+"\n";                  
                                textMessage += "Total Marks :"+rankBean.getObtainMark()+"/"+rankBean.getTotalMark()+"\n";
            //                    textMessage += "Date : "+sdf.format(new Date(rankBean.getTimeinstring().trim()));        

            //                    smsResponse +=  new SmsOperation().sendMessages1(UserName,Password,textMessage,SenderId,studentBean.getMobileNo().trim())+"," ;
            //                    smsResponse +=new SmsOperation().sendSMS(UserName,Password,textMessage,SenderId,studentBean.getMobileNo().trim())+"," ;
                                 new SmsOperation().sendSMS(studentBean.getStudentName(),rankBean.getUnitTestId(),testName,rankBean.getRollNo(),UserName,Password,textMessage,SenderId,studentBean.getMobileNo().trim());
                                 System.out.println(smsResponse);
                     
                                }
                    }
                }
            } else 
            {
                JOptionPane.showMessageDialog(rootPane, "Please Set SenderID from SMS Setting .");
            }     
        } else 
        {
                JOptionPane.showMessageDialog(rootPane, "Select Atleast One Sender.");
        }
    }
    
//    private void SendGroupSMS( ArrayList<RankBean> rankBeanList,String instituteName)
//    {
//      if (rankBeanList != null) {
//               String textMessage = "";
//                String smsResponse = "";
//                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
//                
//                try {
//                    senderIdList= new SenderIdOperation().getSenderIdbyflagList();
////                    senderIdList= new SenderIdOperation().getSenderIdList();
//                    for(SenderIdBean senderIdBean : senderIdList) 
//                    {
//                        this.senderIdBean=senderIdBean;                      
//                        System.out.println("senderIdBean.getSenderId()"+senderIdBean.getSenderId());
//                    }
//                } catch (Exception ex) {
//                    ex.printStackTrace();
//                }
//                String UserName=senderIdBean.getUserName().trim();
//                String Password=senderIdBean.getPassword().trim();
//                String SenderId=senderIdBean.getSenderId().trim();
//                System.out.println(UserName+"...."+Password+"...."+SenderId);
//                
//                
//                for(RankBean rankBean : rankBeanList) {
////                    String SubjectName=new SubjectOperation().getSubject(rankBean.getSubject_Id());
//                    String testName=new ClassSaveTestOperation().getTestName(rankBean.getUnitTestId());
//                    
//                    studentBeanList=new StudentRegistrationOperation().getAllInfoStudent(rankBean.getRollNo());
//                    
//                    for(StudentBean studentBean : studentBeanList) {
//                        
//                    textMessage = "";
//                    textMessage += instituteName.trim() + "\n";
//                    textMessage += "Name :"+studentBean.getStudentName().trim() + "\n";
//                    textMessage += "Subject Name :"+ "MH-CET-PCB" + ".\n";
//                    textMessage += "Test Id :"+rankBean.getUnitTestId()+ "\n";
//                    textMessage += "Test Name :"+ testName+ "\n";
//                    textMessage += "Class/Stand :"+studentBean.getStandard().trim() +"/"+studentBean.getDivision().trim()+ "\n";                   
//                    textMessage += "Phy Correct/Incorrect :"+rankBean.getPHY_CORRECT_QUESTIONS()+"/"+rankBean.getPHY_INCORRECT_QUESTIONS()+"\n";
//                    textMessage += "Chem Correct/Incorrect :"+rankBean.getCHEM_CORRECT_QUESTIONS()+"/"+rankBean.getCHEM_INCORRECT_QUESTIONS()+"\n";
//                    textMessage += "Bio Correct/Incorrect :"+rankBean.getBIO_CORRECT_QUESTIONS()+"/"+rankBean.getBIO_INCORRECT_QUESTIONS()+"\n";                  
//                    textMessage += "Total Correct/Incorrect:"+rankBean.getCorrect_Questions()+"/"+rankBean.getIncorrect_Questions()+"\n";
//                    textMessage += "Phy Marks :"+rankBean.getTOTAL_PHY_MARK()+"/"+rankBean.getTOTAL_PHY_QUESTIONS()+"\n";
//                    textMessage += "Chem Marks :"+rankBean.getTOTAL_CHEM_MARK()+"/"+rankBean.getTOTAL_CHEM_QUESTIONS()+"\n";
//                    textMessage += "Bio Marks :"+rankBean.getTOTAL_BIO_MARK()+"/"+rankBean.getTOTAL_BIO_QUESTIONS()+"\n";                  
//                    textMessage += "Total Marks :"+rankBean.getObtainMark()+"/"+rankBean.getTotalMark()+"\n";
////                    textMessage += "Date : "+sdf.format(new Date(rankBean.getTimeinstring().trim()));        
//
////                    smsResponse +=  new SmsOperation().sendMessages1(UserName,Password,textMessage,SenderId,studentBean.getMobileNo().trim())+"," ;
////                    smsResponse +=new SmsOperation().sendSMS(UserName,Password,textMessage,SenderId,studentBean.getMobileNo().trim())+"," ;
//                   new SmsOperation().sendSMS(studentBean.getStudentName(),rankBean.getUnitTestId(),testName,rankBean.getRollNo(),UserName,Password,textMessage,SenderId,studentBean.getMobileNo().trim());
//                    System.out.println(smsResponse);
//                     
//                    }
//                }
//        } else 
//        {
//                JOptionPane.showMessageDialog(rootPane, "Select Atleast One Sender.");
//        }  
//    }
    
    private void keyTypedAlphabets(KeyEvent evt,JTextField text) {
        char c = evt.getKeyChar();
        if(Character.isLetter(c) || Character.isISOControl(c)) {
            text.setEditable(true);
        } else if(evt.getKeyCode() == 8 || evt.getKeyCode() == 9 || evt.getKeyCode() == 13 || evt.getKeyCode() == 16 || evt.getKeyCode() == 17 || evt.getKeyCode() == 20 || evt.getKeyCode() == 27 || evt.getKeyCode() == 37 || evt.getKeyCode() == 38 || evt.getKeyCode() == 39 || evt.getKeyCode() == 40 || evt.getKeyCode() == 32 || evt.getKeyCode() == 45 || evt.getKeyCode() == 46) {
            text.setEditable(true);
        } else {
            text.setEditable(false);
            evt.consume();
            JOptionPane.showMessageDialog(this,"Only letter allowed here");
            text.setEditable(true);
        }
    }
    private void keyPressedNumbers(KeyEvent evt, JTextField text,int length) {
        if((evt.getKeyCode() == KeyEvent.VK_A || evt.getKeyCode() == KeyEvent.VK_C || evt.getKeyCode() == KeyEvent.VK_V || evt.getKeyCode() == KeyEvent.VK_X) && ((evt.getModifiers() & KeyEvent.CTRL_MASK) != 0)) {
        } else {
            String str = text.getText();
            if(str.length() < length) {
                if ((evt.getKeyChar() >= '0' && evt.getKeyChar() <= '9') || evt.getKeyCode() == 8 || evt.getKeyCode() == 9 || evt.getKeyCode() == 13 || evt.getKeyCode() == 16 || evt.getKeyCode() == 17 || evt.getKeyCode() == 20 || evt.getKeyCode() == 27 || evt.getKeyCode() == 37 || evt.getKeyCode() == 38 || evt.getKeyCode() == 39 || evt.getKeyCode() == 40 || evt.getKeyCode() == 46) {
                    text.setEditable(true);
                } else {
                    text.setEditable(false);
                    JOptionPane.showMessageDialog(this, "Please Enter Numbers Only.");
                    text.setEditable(true);
                }
            } else if(!(evt.getKeyCode() == 8 || evt.getKeyCode() == 9 || evt.getKeyCode() == 13 || evt.getKeyCode() == 16 || evt.getKeyCode() == 17 || evt.getKeyCode() == 20 || evt.getKeyCode() == 27 || evt.getKeyCode() == 37 || evt.getKeyCode() == 38 || evt.getKeyCode() == 39 || evt.getKeyCode() == 40 || evt.getKeyCode() == 46)){
                text.setEditable(false);
                JOptionPane.showMessageDialog(this, "Please Enter Only "+length+" Numbers.");
                text.setEditable(true);
            } else {
                text.setEditable(true);
            }
        }
    }
    
    private void keyReleasedRestrictText(JTextField text,int length) {
        String str = text.getText();
        if(str.length() >= length) {
            str = str.substring(0, length);
            text.setText(str);
        }
    }
    
    public void tablesetting1(){
        //Distribute Server for save Test table
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        tblSaveTest.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tblSaveTest.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        tblSaveTest.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
        tblSaveTest.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
        
    }
    private void clearTableRows1(javax.swing.JTable tblSaveTest) {
        //Distribute Server for save Test table
        DefaultTableModel model = (DefaultTableModel) tblSaveTest.getModel();
        int rowCount = model.getRowCount();
        for (int i = rowCount - 1; i >= 0; i--) {
            model.removeRow(i);
        }
    }

//    private void resumeTest1() {
//        //Distribute Server for save Test table
//       
//        testInfo = new ClassSaveTestOperation().getClassTestInfo();
//        DefaultTableModel model = (DefaultTableModel) tblSaveTest.getModel();
//        clearTableRows(tblSaveTest);
//        for (int i = 0; i < testInfo.size(); i++) {
//            String temp = testInfo.get(i);
//            String[] obj = temp.split("\t");
//            int testId = Integer.parseInt(obj[0]);
////            String subjectName = con.getSubjectName(testInfo.get(1).g);
////            if(subjectName.equals("")){
////                subjectName = "All";
////            }
//            int count = new ClassSaveTestOperation().getQuestionCount(testId);
//            String testName=new ClassSaveTestOperation().getTestName(testId);
//            String date = obj[2];
//            String status = obj[1];
//            model.addRow(new Object[]{testId +"  [" +testName+ "]", count, date, status,false});
//            
//        }
//
//    }
    
    
    private void resumeTest1() {
        //Distribute Server for save Test table
       
        tempclassTestList = new ClassSaveTestOperation().getTempClassTestInfo();
        
        testInfo = new ClassSaveTestOperation().getClassTestInfo();
        classTestList = new ClassSaveTestOperation().getClassTestInfo1(); 
        DefaultTableModel model = (DefaultTableModel) tblSaveTest.getModel();
        clearTableRows(tblSaveTest);
        for (int i = 0; i < classTestList.size(); i++) {
//            String temp = testInfo.get(i);
//            String[] obj = temp.split("\t");
//            int testId = Integer.parseInt(obj[0]);
//            String subjectName = con.getSubjectName(testInfo.get(1).g);
//            if(subjectName.equals("")){
//                subjectName = "All";
//            }
            int count = new ClassSaveTestOperation().getQuestionCount(classTestList.get(i).getTestId());
            QuestionCount=count;
//            String testName=new ClassSaveTestOperation().getTestName(testId);
//            String date = obj[2];
//            String status = obj[1];
//            String testName= obj[1];
//            model.addRow(new Object[]{classTestList.get(i).getTestId() , classTestList.get(i).getTestName(),count, classTestList.get(i).getTestDate(), "Click Here",false});
            
            if(tempclassTestList.isEmpty())
            {
                 model.addRow(new Object[]{classTestList.get(i).getTestId() , classTestList.get(i).getTestName(),count, classTestList.get(i).getTestDate(),"False", "Click Here",false});
            }
            else
            {
                    if(tempclassTestList.get(0).getTestId()==classTestList.get(i).getTestId())   
                {
                     model.addRow(new Object[]{classTestList.get(i).getTestId() , classTestList.get(i).getTestName(),count, classTestList.get(i).getTestDate(),"True", "Click Here",false});
                }
                else
                {
                     model.addRow(new Object[]{classTestList.get(i).getTestId() , classTestList.get(i).getTestName(),count, classTestList.get(i).getTestDate(),"False", "Click Here",false});       
                }
            }
        }

    }
    
    //Start Server....View ClientList
    public void loadClients() {
        try {
           
            ArrayList<ClientInfoBean> clientList;           
            clientList=new ClientOperation().getClientList();
            
           int i = 1;
           int cnt = 0;
           rowRange = 0;
            
            DefaultTableModel model = (DefaultTableModel) tblConnectedClient.getModel();
            clearTableRow(model);
            if (clientList != null) {
                model.addRow(new Object[]{"", "", "", "", ""});
                cnt++;
                
                for (ClientInfoBean clientBean : clientList) {
                    System.out.println(clientBean.getClientId()+"***"+clientBean.getClientName());
                    model.addRow(new Object[]{(i++),clientBean.getClientId(),clientBean.getClientName().trim(),clientBean.getClientIp(),clientBean.getDateTime()});
                    cnt++;
                    rowRange++;
                }
              
            }
           
        } catch (SQLException ex) {
            Logger.getLogger(HomePage.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public Object GetData(JTable table, int row_index, int col_index) {
        return table.getModel().getValueAt(row_index, col_index);
    }
    private void keyPressed(KeyEvent evt, JTextField text) {
        if ((evt.getKeyChar() >= '0' && evt.getKeyChar() <= '9') || evt.getKeyCode() == 8 || evt.getKeyCode() == 9 || evt.getKeyCode() == 13 || evt.getKeyCode() == 16 || evt.getKeyCode() == 17 || evt.getKeyCode() == 20 || evt.getKeyCode() == 27 || evt.getKeyCode() == 37 || evt.getKeyCode() == 38 || evt.getKeyCode() == 39 || evt.getKeyCode() == 40 || evt.getKeyCode() == 46) {
            text.setEditable(true);
        } else {
            text.setEditable(false);
            JOptionPane.showMessageDialog(null, "Please Enter Numbers Only.");
            text.setEditable(true);
        }
    }

    public static void main(String args[]) {
//        JOptionPane.showMessageDialog(null, "This is a DEMO Version. Visit www.Scholars Katta.com for more information.");
        new HomePage().setVisible(true);
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel BodyPanel;
    private javax.swing.JButton BtnAboutUs;
    private javax.swing.JButton BtnAddQuestion;
    private javax.swing.JButton BtnAnalysis;
    private javax.swing.JButton BtnAutoSendSMS;
    private javax.swing.JButton BtnBackOmrSelection;
    private javax.swing.JButton BtnBackSelection;
    private javax.swing.JButton BtnBackSelection1;
    private javax.swing.JButton BtnBackSelection2;
    private javax.swing.JButton BtnBackSelection3;
    private javax.swing.JButton BtnBackSelection4;
    private javax.swing.JButton BtnBackSettingsSelection;
    private javax.swing.JButton BtnBackSubjectSelection;
    private javax.swing.JButton BtnBackSubjectSelection1;
    private javax.swing.JButton BtnChapterWise;
    private javax.swing.JButton BtnCloseMainPanel;
    private javax.swing.JButton BtnCreate;
    private javax.swing.JButton BtnCreateOmr;
    private javax.swing.JButton BtnDistributeTest;
    private javax.swing.JButton BtnFirstSubject;
    private javax.swing.JButton BtnGroupWise;
    private javax.swing.JButton BtnLogoutStudent;
    private javax.swing.JButton BtnMinimizeMainPanel;
    private javax.swing.JButton BtnOMR;
    private javax.swing.JButton BtnOldQuestionPapers;
    private javax.swing.JButton BtnPrint;
    private javax.swing.JButton BtnRefClientList;
    private javax.swing.JButton BtnRegNewStudent;
    private javax.swing.JButton BtnSecondSubject;
    private javax.swing.JButton BtnSettingsSave;
    private javax.swing.JButton BtnSettingsSave1;
    private javax.swing.JButton BtnSettingsSave2;
    private javax.swing.JButton BtnStartServer;
    private javax.swing.JButton BtnStudentDetail;
    private javax.swing.JButton BtnSubjectWise;
    private javax.swing.JButton BtnSummary;
    private javax.swing.JButton BtnSyllabus;
    private javax.swing.JButton BtnThirdSubject;
    private javax.swing.JButton BtnUnitWise;
    private javax.swing.JButton BtnUpdatePrefix;
    private javax.swing.JButton BtnView;
    private javax.swing.JCheckBox ChkSelectAll;
    private javax.swing.JComboBox<String> CmbAcademicYear;
    private javax.swing.JComboBox<String> CmbClass;
    private javax.swing.JComboBox<String> CmbDivision;
    private javax.swing.JComboBox<String> CmboSubject;
    private javax.swing.JButton CreatePapers;
    private javax.swing.JPanel DefaultPathBody;
    private javax.swing.JPanel HeaderPanel;
    private javax.swing.JLabel LblCompanyName;
    private javax.swing.JLabel LblDelete;
    private javax.swing.JLabel LblDirectoryPath;
    private javax.swing.JLabel LblMorePatternInfo;
    private javax.swing.JLabel LblOmrMessage;
    private javax.swing.JLabel LblPrintSeetingsHeader;
    private javax.swing.JLabel LblPrintSeetingsHeader1;
    private javax.swing.JLabel LblProductName;
    private javax.swing.JLabel LblSeetingsHeader;
    private javax.swing.JLabel LblWhtasNew;
    private javax.swing.JPanel LeftBodyPanel;
    private javax.swing.JScrollPane MainPanelScrollPane;
    private javax.swing.JPanel MainRegisterPanel;
    private javax.swing.JPanel PanelAddYourOwn;
    private javax.swing.JPanel PanelDistributeTest;
    private javax.swing.JPanel PanelHome;
    private javax.swing.JPanel PanelOMRSelection;
    private javax.swing.JPanel PanelQuestionSelectionFormat;
    private javax.swing.JPanel PanelRegisterNewStudent;
    private javax.swing.JPanel PanelSettings;
    private javax.swing.JPanel PanelSettingsBody;
    private javax.swing.JPanel PanelStartServer;
    private javax.swing.JPanel PanelStudentDetail;
    private javax.swing.JPanel PanelSubjectSelection;
    private javax.swing.JRadioButton RdoPatternFirst;
    private javax.swing.JRadioButton RdoPatternSecond;
    private javax.swing.JRadioButton RdoPatternThird;
    private javax.swing.JRadioButton RdoStartServer;
    private javax.swing.JRadioButton RdoStopServer;
    private javax.swing.JPanel ResetPanel;
    private javax.swing.JPanel RightBodyPanel;
    private javax.swing.JScrollPane ScrollPanel;
    private javax.swing.JPanel SetOnlineSubjectMark;
    private javax.swing.JButton Settings;
    private javax.swing.ButtonGroup SettingsBtnGroup;
    private javax.swing.JLabel SettingsNote;
    private javax.swing.JTabbedPane SettingsTab;
    private javax.swing.ButtonGroup StartStopBtnGroup;
    private javax.swing.JPanel SubPanelOmrSelection;
    private javax.swing.JScrollPane TableScrollPane;
    private javax.swing.JTable TableTest;
    private javax.swing.JTextField TxtFldOmrRange;
    private javax.swing.JTable bio_table;
    private javax.swing.JScrollPane biology;
    private javax.swing.JButton btnAddAcademicYear;
    private javax.swing.JButton btnAddChapter;
    private javax.swing.JButton btnAddDivision;
    private javax.swing.JButton btnAddQuestions;
    private javax.swing.JButton btnAddStandard;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnDeleteAll;
    private javax.swing.JButton btnExport;
    private javax.swing.JButton btnImportFile;
    private javax.swing.JButton btnModify;
    private javax.swing.JButton btnRecheckMarks;
    private javax.swing.JButton btnRegisterStudent;
    private javax.swing.JButton btnResetUsedQ;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnSendSMSHistory;
    private javax.swing.JButton btn_save;
    private javax.swing.JTable chem_table;
    private javax.swing.JScrollPane chemistry;
    private javax.swing.JComboBox<String> cmbAcademicYear;
    private javax.swing.JComboBox<String> cmbDivision;
    private javax.swing.JComboBox<String> cmbStandard;
    private javax.swing.JTabbedPane coustom_weight;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblAcademicYear;
    private javax.swing.JLabel lblCnfrmPassword;
    private javax.swing.JLabel lblDivision;
    private javax.swing.JLabel lblError;
    private javax.swing.JLabel lblMarkPerQue;
    private javax.swing.JLabel lblMarkPerWrongQuestion;
    private javax.swing.JLabel lblNetworkPrefix;
    private javax.swing.JLabel lblPassword;
    private javax.swing.JLabel lblRollNo;
    private javax.swing.JLabel lblStandard;
    private javax.swing.JLabel lblStudentDetail;
    private javax.swing.JLabel lblStudentName;
    private javax.swing.JLabel lblTitle;
    private javax.swing.JLabel lblUserId;
    private javax.swing.JLabel lblm;
    private javax.swing.JTable phy_table;
    private javax.swing.JScrollPane physics;
    private javax.swing.JTable tblConnectedClient;
    private javax.swing.JTable tblSaveTest;
    private javax.swing.JTable tblStudentDetail;
    private javax.swing.JPasswordField txtConfirmPassword;
    private javax.swing.JTextField txtLoginID;
    private javax.swing.JTextField txtMblNo;
    private javax.swing.JTextField txtNetworkPrefix;
    private javax.swing.JPasswordField txtPassword;
    private javax.swing.JTextField txtPerQueMark;
    private javax.swing.JTextField txtStudentName;
    private javax.swing.JTextField txtUserID;
    private javax.swing.JTextField txtWrongQueMark;
    // End of variables declaration//GEN-END:variables

    private void callSubjectWise() {
        if(printingPaperType.equalsIgnoreCase("ChapterWise")) {
            if(!isNextPageCallStatus()) {
                setNextPageCallStatus(true);
                new SingleChapterSelection(currentPatternIndex,selectedSubjectBean,"ChapterWise").setVisible(true);
                this.dispose();
            }
        } else if(printingPaperType.equalsIgnoreCase("UnitWise")) {
            if(!isNextPageCallStatus()) {
                setNextPageCallStatus(true);
                new MultipleChapterSelection(currentPatternIndex,selectedSubjectBean,"UnitWise").setVisible(true);
                this.dispose();
            }
        } else if(printingPaperType.equalsIgnoreCase("SubjectWise")) {
            CompleteSubject("SubjectWise");
        } else if(printingPaperType.equalsIgnoreCase("ChapterSummery")) {
            if(!isNextPageCallStatus()) {
               setNextPageCallStatus(true);
               new ChapterSummary(selectedSubjectBean,this).setVisible(true);
               this.setVisible(false);
            }
        } else if(printingPaperType.equalsIgnoreCase("AddQuestion")) {
            if(!isNextPageCallStatus()) {
                setNextPageCallStatus(true);
                new AddQuestionChapterSelection(selectedSubjectBean,this).setVisible(true);
                this.setVisible(false);
            }
        }
        else if(printingPaperType.equalsIgnoreCase("AddChapter")) {
            if(!isNextPageCallStatus()) {
                setNextPageCallStatus(true);
               new AddNewChapter(selectedSubjectBean,this).setVisible(true);
                
            }
        }
    }
    
    private void CompleteSubject(String printingPaperType) {
        if(!isNextPageCallStatus()) {
            try {
                questionsList = new QuestionOperation().getQuestuionsSubjectWise(selectedSubjectBean.getSubjectId());
                chapterList = new ChapterOperation().getChapterList(selectedSubjectBean.getSubjectId());
                ArrayList<QuestionBean> sortQuestionsList = new ArrayList<QuestionBean>();
                sortingSubjectList = new ArrayList<SubjectBean>();
                sortingSubjectList.add(selectedSubjectBean);

                String strOption = "All";
                int resultOption = 0;
                Object[] possibilities = {"All", "With Hints", "Without Hints"};
                strOption = (String) JOptionPane.showInputDialog(null, "Select Question Type:\n", "", JOptionPane.PLAIN_MESSAGE, null, possibilities, "ham");
                if(strOption != null) {
                    if (strOption.endsWith("All")) {
                        resultOption = 0;
                    } else if (strOption.endsWith("With Hints"))  {
                        resultOption = 1;
                    } else if (strOption.endsWith("Without Hints")) {
                        resultOption = 2;
                    }

                    if (resultOption == 0) {
                        sortQuestionsList = questionsList;
                    } else {
                        for (QuestionBean questionsBean : questionsList) {
                            if (resultOption == 1) {
                                if (!questionsBean.getHint().equals("") && !questionsBean.getHint().equals(" ") && !questionsBean.getHint().equals(null) && !questionsBean.getHint().equals("\\mbox{}") && !questionsBean.getHint().equals("\\mbox{ }")) {
                                    sortQuestionsList.add(questionsBean);
                                }
                            } else if (resultOption == 2) {
                                if (questionsBean.getHint().equals("") || questionsBean.getHint().equals(" ") || questionsBean.getHint().equals(null) || questionsBean.getHint().equals("\\mbox{}") || questionsBean.getHint().equals("\\mbox{ }")) {
                                    sortQuestionsList.add(questionsBean);
                                }
                            }
                        }
                    }

                    ArrayList<CountChapterBean> countedChapterList = new ArrayList<CountChapterBean>();
                    CountChapterBean ccBean = null;

                    for (ChapterBean bean : chapterList) {
                        int totalQuestions = 0;
                        for (QuestionBean questionsBean : sortQuestionsList) {
                            if (bean.getChapterId() == questionsBean.getChapterId()) {
                                totalQuestions++;
                            }
                        }
                        ccBean = new CountChapterBean();
                        ccBean.setChapterBean(bean);
                        ccBean.setTotalQuestions(totalQuestions);
                        countedChapterList.add(ccBean);
                    }
                    setNextPageCallStatus(true);
                    new MultipleChapterQuestionsSelection(currentPatternIndex,sortQuestionsList, chapterList, sortingSubjectList, countedChapterList,this,printingPaperType).setVisible(true);  
                    this.setVisible(false);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
    
    private void deleteTests1() {
        ArrayList<StudentBean> deleteStudentList = null;
        if(studentBeanList != null) {
            if(studentBeanList.size() > 50) {
                for(int i=50;i<studentBeanList.size();i++) {
                    if(deleteStudentList == null)
                        deleteStudentList = new ArrayList<StudentBean>();
                    deleteStudentList.add(studentBeanList.get(i));
                }
            }
                if(deleteStudentList != null)
                    new StudentRegistrationOperation().deleteStudentList(deleteStudentList);
           
        }
    }
    
    private void deleteTests() {
        ArrayList<PrintedTestBean> deleteTestList = null;
        if(testList != null) {
            if(testList.size() > 50) {
                for(int i=50;i<testList.size();i++) {
                    if(deleteTestList == null)
                        deleteTestList = new ArrayList<PrintedTestBean>();
                    deleteTestList.add(testList.get(i));
                }
                
                if(deleteTestList != null)
                    new PrintedTestOperation().deleteTestList(deleteTestList);
            }
        }
    }
    
    private void setDefalutFileDirectory() {
        currentFolderDirectory = new DefaultFolderLocation().getFolderLocation();
        tempFileLocation = currentFolderDirectory;
        if(currentFolderDirectory != null)
            LblDirectoryPath.setText(" Current Directory : "+currentFolderDirectory);
    }
    
    private void writeFolderPathInDirectory() {
        if(currentFolderDirectory != null) {
            BufferedWriter bw = null;
            try {
                File fl = new File("DefaultPath.txt");
                if(!fl.exists())
                    fl.createNewFile();
                bw = new BufferedWriter(new FileWriter("DefaultPath.txt"));
                bw.write(currentFolderDirectory);
                JOptionPane.showMessageDialog(rootPane, "Default Path Set Successfully.");
            } catch (IOException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (bw != null)
                        bw.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
    
    //Setters And Getters
    public boolean isNextPageCallStatus() {
        return nextPageCallStatus;
    }

    public void setNextPageCallStatus(boolean nextPageCallStatus) {
        this.nextPageCallStatus = nextPageCallStatus;
    }

    public int getPatternIndexValue() {
        return patternIndexValue;
    }
    
    public ArrayList<SubjectBean> getSubjectList() {
        return subjectList;
    }

    public ArrayList<PrintedTestBean> getTestList() {
        return testList;
    }

    public void setTestList(ArrayList<PrintedTestBean> testList) {
        this.testList = testList;
        setTableRows();
    }
}
