/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * NewRegistrationForm.java
 *
 * Created on Aug 1, 2014, 2:50:03 PM
 */
package com.pages;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import com.bean.SubjectBean;
import javax.swing.JTable;
import com.Model.TitleInfo;
import com.bean.ChapterViewBean;
import com.bean.GroupBean;
import com.db.operations.MasterGroupOperation;

/**
 *
 * @author Aniket
 */
public class PatternInfo extends javax.swing.JFrame {

    private HomePage homePage;
    private DefaultTableCellRenderer centerRenderer;
    private int subjectFirstId = 0,subjectSecondId = 0,subjectThirdId = 0;
    private ArrayList<GroupBean> groupList;
    
    public PatternInfo(ArrayList<SubjectBean> subjectList,ArrayList<ChapterViewBean> chapterViewList,HomePage homePage) {
        initComponents();
        groupList = new MasterGroupOperation().getGroupList();
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        MainPanel.setSize(screenSize);
        this.homePage = homePage;
        this.setLocationRelativeTo(null);
        String titl = new TitleInfo().getTitle();
        setTitle(titl);
        String lgo = new TitleInfo().getLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        
        centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        
        setTableSettings(SubFirstTablePattern1);
        setTableSettings(SubSecondTablePattern1);
        setTableSettings(SubThirdTablePattern1);
        setTableSettings(SubFirstTablePattern2);
        setTableSettings(SubSecondTablePattern2);
        setTableSettings(SubThirdTablePattern2);
        setTableSettings(SubFirstTablePattern3);
        setTableSettings(SubSecondTablePattern3);
        setTableSettings(SubThirdTablePattern3);
        
        int index = 0;
        for(SubjectBean subjectBean : subjectList) {
            if(index == 0)
                subjectFirstId = subjectBean.getSubjectId();
            else if(index == 1)
                subjectSecondId = subjectBean.getSubjectId();
            else if(index == 2)
                subjectThirdId = subjectBean.getSubjectId();
            Pattern1.setTitleAt(index, subjectBean.getSubjectName());
            Pattern2.setTitleAt(index, subjectBean.getSubjectName());
            Pattern3.setTitleAt(index, subjectBean.getSubjectName());
            index++;
        }
        
        getContentPane().setBackground(new Color(0, 100, 50));
        ArrayList<Object[]> subjectFirstChapterList = new ArrayList<>();
        ArrayList<Object[]> subjectSecondChapterList = new ArrayList<>();
        ArrayList<Object[]> subjectThirdChapterList = new ArrayList<>();
        
        int srNo1,srNo2,srNo3;
        int subFirstCount,subSecondCount,subThirdCount;
        int groupIndex = 0;
        for(GroupBean groupBean : groupList) {
            srNo1 = 1;srNo2 = 1;srNo3 = 1;subFirstCount = 0;subSecondCount = 0;subThirdCount = 0;
            subjectFirstChapterList.clear();
            subjectSecondChapterList.clear();
            subjectThirdChapterList.clear();
            for(SubjectBean subjectBean : subjectList) {
                for(ChapterViewBean chapterViewBean : chapterViewList) {
                    if(chapterViewBean.getGroupBean().getGroupId() == groupBean.getGroupId() &&
                        chapterViewBean.getSubjectBean().getSubjectId() == subjectBean.getSubjectId()) {
                        if(subjectBean.getSubjectId() == subjectFirstId) {
                            subjectFirstChapterList.add(new Object[]{srNo1++,chapterViewBean.getChapterName(),chapterViewBean.getTotalQuesCount()});
                            subFirstCount += chapterViewBean.getTotalQuesCount();
                        } else if(subjectBean.getSubjectId() == subjectSecondId) {
                            subjectSecondChapterList.add(new Object[]{srNo2++,chapterViewBean.getChapterName(),chapterViewBean.getTotalQuesCount()});
                            subSecondCount += chapterViewBean.getTotalQuesCount();
                        } else if(subjectBean.getSubjectId() == subjectThirdId) {
                            subjectThirdChapterList.add(new Object[]{srNo3++,chapterViewBean.getChapterName(),chapterViewBean.getTotalQuesCount()});
                            subThirdCount += chapterViewBean.getTotalQuesCount();
                        }
                    }
                }
            }
            if(groupIndex == 0) {
                loadsQuesCount(subjectFirstChapterList,SubFirstTablePattern1,subFirstCount);
                loadsQuesCount(subjectSecondChapterList,SubSecondTablePattern1,subSecondCount);
                loadsQuesCount(subjectThirdChapterList,SubThirdTablePattern1,subThirdCount);
            } else if(groupIndex == 1) {
                loadsQuesCount(subjectFirstChapterList,SubFirstTablePattern2,subFirstCount);
                loadsQuesCount(subjectSecondChapterList,SubSecondTablePattern2,subSecondCount);
                loadsQuesCount(subjectThirdChapterList,SubThirdTablePattern2,subThirdCount);
            } else if(groupIndex == 2) {
                loadsQuesCount(subjectFirstChapterList,SubFirstTablePattern3,subFirstCount);
                loadsQuesCount(subjectSecondChapterList,SubSecondTablePattern3,subSecondCount);
                loadsQuesCount(subjectThirdChapterList,SubThirdTablePattern3,subThirdCount);
            }
            groupIndex++;
        }
    }
    
    private void setTableSettings(JTable table){
        table.getTableHeader().setDefaultRenderer(centerRenderer);
        table.getTableHeader().setForeground(Color.black);
        table.getTableHeader().setBackground(new Color(57, 97, 152));
        table.getColumnModel().getColumn(0).setPreferredWidth(40);
        table.getColumnModel().getColumn(1).setPreferredWidth(200);
        table.getColumnModel().getColumn(2).setPreferredWidth(80);
        table.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        table.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        table.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
        table.getTableHeader().setFont(new Font("Microsoft JhengHei", Font.PLAIN, 14));
    }
    
    private void loadsQuesCount(ArrayList<Object[]> chapterInfoList,JTable table, int weightageCount) {
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        int count = model.getRowCount();
        if (count > 0) {
            for (int i = count - 1; i >= 0; i--) {
                model.removeRow(i);
            }
        }
        if (chapterInfoList != null) {
            if (chapterInfoList.size() > 0) {
                Iterator<Object[]> it = chapterInfoList.iterator();
                while (it.hasNext()) {
                    model.addRow(it.next());
                }
            }
        }
        model.addRow(new Object[]{"","",""});
        model.addRow(new Object[]{"", " Total " , weightageCount });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        MainPanel = new javax.swing.JPanel();
        LblPatternHeader = new javax.swing.JLabel();
        BtnBack = new javax.swing.JButton();
        BodyPanelScrollPane = new javax.swing.JScrollPane();
        BodyPanel = new javax.swing.JPanel();
        LblMessage1 = new javax.swing.JLabel();
        PattternTabPane = new javax.swing.JTabbedPane();
        Pattern1 = new javax.swing.JTabbedPane();
        SubFirstScrollPanePatern1 = new javax.swing.JScrollPane();
        SubFirstTablePattern1 = new javax.swing.JTable();
        SubSecondScrollPanePattern1 = new javax.swing.JScrollPane();
        SubSecondTablePattern1 = new javax.swing.JTable();
        SubThirdScrollPanePattern1 = new javax.swing.JScrollPane();
        SubThirdTablePattern1 = new javax.swing.JTable();
        Pattern2 = new javax.swing.JTabbedPane();
        SubFirstScrollPanePatern2 = new javax.swing.JScrollPane();
        SubFirstTablePattern2 = new javax.swing.JTable();
        SubSecondScrollPanePattern2 = new javax.swing.JScrollPane();
        SubSecondTablePattern2 = new javax.swing.JTable();
        SubThirdScrollPanePattern2 = new javax.swing.JScrollPane();
        SubThirdTablePattern2 = new javax.swing.JTable();
        Pattern3 = new javax.swing.JTabbedPane();
        SubFirstScrollPanePatern3 = new javax.swing.JScrollPane();
        SubFirstTablePattern3 = new javax.swing.JTable();
        SubSecondScrollPanePattern3 = new javax.swing.JScrollPane();
        SubSecondTablePattern3 = new javax.swing.JTable();
        SubThirdScrollPanePattern3 = new javax.swing.JScrollPane();
        SubThirdTablePattern3 = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("JEE Preaparation Software 2015");
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setResizable(false);
        setType(java.awt.Window.Type.UTILITY);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        MainPanel.setBackground(new java.awt.Color(0, 102, 102));
        MainPanel.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        MainPanel.setName("MainPanel"); // NOI18N

        LblPatternHeader.setFont(new java.awt.Font("Microsoft JhengHei", 0, 36)); // NOI18N
        LblPatternHeader.setForeground(new java.awt.Color(255, 255, 255));
        LblPatternHeader.setText("Pattern Details");
        LblPatternHeader.setName("LblPatternHeader"); // NOI18N

        BtnBack.setBackground(new java.awt.Color(0, 20, 72));
        BtnBack.setFont(new java.awt.Font("Microsoft JhengHei", 1, 16)); // NOI18N
        BtnBack.setForeground(new java.awt.Color(255, 255, 255));
        BtnBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/Back-1.png"))); // NOI18N
        BtnBack.setBorder(null);
        BtnBack.setContentAreaFilled(false);
        BtnBack.setName("BtnBack"); // NOI18N
        BtnBack.setPreferredSize(new java.awt.Dimension(60, 60));
        BtnBack.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnBackMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnBackMouseExited(evt);
            }
        });
        BtnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBackActionPerformed(evt);
            }
        });

        BodyPanelScrollPane.setName("BodyPanelScrollPane"); // NOI18N

        BodyPanel.setBackground(new java.awt.Color(11, 45, 55));
        BodyPanel.setForeground(new java.awt.Color(255, 255, 255));
        BodyPanel.setName("BodyPanel"); // NOI18N

        LblMessage1.setFont(new java.awt.Font("Microsoft JhengHei", 0, 16)); // NOI18N
        LblMessage1.setForeground(new java.awt.Color(255, 255, 255));
        LblMessage1.setText("Chapter And Total Questions");
        LblMessage1.setName("LblMessage1"); // NOI18N

        PattternTabPane.setName("PattternTabPane"); // NOI18N

        Pattern1.setName("Pattern1"); // NOI18N

        SubFirstScrollPanePatern1.setName("SubFirstScrollPanePatern1"); // NOI18N

        SubFirstTablePattern1.setFont(new java.awt.Font("Times New Roman", 1, 11)); // NOI18N
        SubFirstTablePattern1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                { new Integer(1), "Physics",  new Integer(50)},
                { new Integer(2), "Chemistry",  new Integer(0)},
                { new Integer(3), "Maths",  new Integer(0)},
                { new Integer(4), "PCM",  new Integer(50)}
            },
            new String [] {
                "Chapter Id", "Chapter Name", "Total Questions"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Object.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        SubFirstTablePattern1.setName("SubFirstTablePattern1"); // NOI18N
        SubFirstScrollPanePatern1.setViewportView(SubFirstTablePattern1);

        Pattern1.addTab("Physics", SubFirstScrollPanePatern1);

        SubSecondScrollPanePattern1.setName("SubSecondScrollPanePattern1"); // NOI18N

        SubSecondTablePattern1.setFont(new java.awt.Font("Times New Roman", 1, 11)); // NOI18N
        SubSecondTablePattern1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                { new Integer(1), "Physics",  new Integer(50)},
                { new Integer(2), "Chemistry",  new Integer(0)},
                { new Integer(3), "Maths",  new Integer(0)},
                { new Integer(4), "PCM",  new Integer(50)}
            },
            new String [] {
                "Chapter Id", "Chapter Name", "Total Questions"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Object.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        SubSecondTablePattern1.setName("SubSecondTablePattern1"); // NOI18N
        SubSecondScrollPanePattern1.setViewportView(SubSecondTablePattern1);

        Pattern1.addTab("Chemistry", SubSecondScrollPanePattern1);

        SubThirdScrollPanePattern1.setName("SubThirdScrollPanePattern1"); // NOI18N

        SubThirdTablePattern1.setFont(new java.awt.Font("Times New Roman", 1, 11)); // NOI18N
        SubThirdTablePattern1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                { new Integer(1), "Physics",  new Integer(50)},
                { new Integer(2), "Chemistry",  new Integer(0)},
                { new Integer(3), "Maths",  new Integer(0)},
                { new Integer(4), "PCM",  new Integer(50)}
            },
            new String [] {
                "Chapter Id", "Chapter Name", "Total Questions"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Object.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        SubThirdTablePattern1.setName("SubThirdTablePattern1"); // NOI18N
        SubThirdScrollPanePattern1.setViewportView(SubThirdTablePattern1);

        Pattern1.addTab("Mathematics", SubThirdScrollPanePattern1);

        PattternTabPane.addTab("Pattern-I", Pattern1);

        Pattern2.setName("Pattern2"); // NOI18N

        SubFirstScrollPanePatern2.setName("SubFirstScrollPanePatern2"); // NOI18N

        SubFirstTablePattern2.setFont(new java.awt.Font("Times New Roman", 1, 11)); // NOI18N
        SubFirstTablePattern2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                { new Integer(1), "Physics",  new Integer(50)},
                { new Integer(2), "Chemistry",  new Integer(0)},
                { new Integer(3), "Maths",  new Integer(0)},
                { new Integer(4), "PCM",  new Integer(50)}
            },
            new String [] {
                "Chapter Id", "Chapter Name", "Total Questions"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Object.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        SubFirstTablePattern2.setName("SubFirstTablePattern2"); // NOI18N
        SubFirstScrollPanePatern2.setViewportView(SubFirstTablePattern2);

        Pattern2.addTab("Physics", SubFirstScrollPanePatern2);

        SubSecondScrollPanePattern2.setName("SubSecondScrollPanePattern2"); // NOI18N

        SubSecondTablePattern2.setFont(new java.awt.Font("Times New Roman", 1, 11)); // NOI18N
        SubSecondTablePattern2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                { new Integer(1), "Physics",  new Integer(50)},
                { new Integer(2), "Chemistry",  new Integer(0)},
                { new Integer(3), "Maths",  new Integer(0)},
                { new Integer(4), "PCM",  new Integer(50)}
            },
            new String [] {
                "Chapter Id", "Chapter Name", "Total Questions"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Object.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        SubSecondTablePattern2.setName("SubSecondTablePattern2"); // NOI18N
        SubSecondScrollPanePattern2.setViewportView(SubSecondTablePattern2);

        Pattern2.addTab("Chemistry", SubSecondScrollPanePattern2);

        SubThirdScrollPanePattern2.setName("SubThirdScrollPanePattern2"); // NOI18N

        SubThirdTablePattern2.setFont(new java.awt.Font("Times New Roman", 1, 11)); // NOI18N
        SubThirdTablePattern2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                { new Integer(1), "Physics",  new Integer(50)},
                { new Integer(2), "Chemistry",  new Integer(0)},
                { new Integer(3), "Maths",  new Integer(0)},
                { new Integer(4), "PCM",  new Integer(50)}
            },
            new String [] {
                "Chapter Id", "Chapter Name", "Total Questions"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Object.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        SubThirdTablePattern2.setName("SubThirdTablePattern2"); // NOI18N
        SubThirdScrollPanePattern2.setViewportView(SubThirdTablePattern2);

        Pattern2.addTab("Mathematics", SubThirdScrollPanePattern2);

        PattternTabPane.addTab("Pattern-II", Pattern2);

        Pattern3.setName("Pattern3"); // NOI18N

        SubFirstScrollPanePatern3.setName("SubFirstScrollPanePatern3"); // NOI18N

        SubFirstTablePattern3.setFont(new java.awt.Font("Times New Roman", 1, 11)); // NOI18N
        SubFirstTablePattern3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                { new Integer(1), "Physics",  new Integer(50)},
                { new Integer(2), "Chemistry",  new Integer(0)},
                { new Integer(3), "Maths",  new Integer(0)},
                { new Integer(4), "PCM",  new Integer(50)}
            },
            new String [] {
                "Chapter Id", "Chapter Name", "Total Questions"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Object.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        SubFirstTablePattern3.setName("SubFirstTablePattern3"); // NOI18N
        SubFirstScrollPanePatern3.setViewportView(SubFirstTablePattern3);

        Pattern3.addTab("Physics", SubFirstScrollPanePatern3);

        SubSecondScrollPanePattern3.setName("SubSecondScrollPanePattern3"); // NOI18N

        SubSecondTablePattern3.setFont(new java.awt.Font("Times New Roman", 1, 11)); // NOI18N
        SubSecondTablePattern3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                { new Integer(1), "Physics",  new Integer(50)},
                { new Integer(2), "Chemistry",  new Integer(0)},
                { new Integer(3), "Maths",  new Integer(0)},
                { new Integer(4), "PCM",  new Integer(50)}
            },
            new String [] {
                "Chapter Id", "Chapter Name", "Total Questions"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Object.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        SubSecondTablePattern3.setName("SubSecondTablePattern3"); // NOI18N
        SubSecondScrollPanePattern3.setViewportView(SubSecondTablePattern3);

        Pattern3.addTab("Chemistry", SubSecondScrollPanePattern3);

        SubThirdScrollPanePattern3.setName("SubThirdScrollPanePattern3"); // NOI18N

        SubThirdTablePattern3.setFont(new java.awt.Font("Times New Roman", 1, 11)); // NOI18N
        SubThirdTablePattern3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                { new Integer(1), "Physics",  new Integer(50)},
                { new Integer(2), "Chemistry",  new Integer(0)},
                { new Integer(3), "Maths",  new Integer(0)},
                { new Integer(4), "PCM",  new Integer(50)}
            },
            new String [] {
                "Chapter Id", "Chapter Name", "Total Questions"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Object.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        SubThirdTablePattern3.setName("SubThirdTablePattern3"); // NOI18N
        SubThirdScrollPanePattern3.setViewportView(SubThirdTablePattern3);

        Pattern3.addTab("Mathematics", SubThirdScrollPanePattern3);

        PattternTabPane.addTab("Pattern-III", Pattern3);

        javax.swing.GroupLayout BodyPanelLayout = new javax.swing.GroupLayout(BodyPanel);
        BodyPanel.setLayout(BodyPanelLayout);
        BodyPanelLayout.setHorizontalGroup(
            BodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(BodyPanelLayout.createSequentialGroup()
                .addGroup(BodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(BodyPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(PattternTabPane, javax.swing.GroupLayout.DEFAULT_SIZE, 872, Short.MAX_VALUE))
                    .addGroup(BodyPanelLayout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addComponent(LblMessage1)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        BodyPanelLayout.setVerticalGroup(
            BodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(BodyPanelLayout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(LblMessage1)
                .addGap(18, 18, 18)
                .addComponent(PattternTabPane)
                .addContainerGap())
        );

        BodyPanelScrollPane.setViewportView(BodyPanel);

        javax.swing.GroupLayout MainPanelLayout = new javax.swing.GroupLayout(MainPanel);
        MainPanel.setLayout(MainPanelLayout);
        MainPanelLayout.setHorizontalGroup(
            MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MainPanelLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(MainPanelLayout.createSequentialGroup()
                        .addComponent(BtnBack, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(39, 39, 39)
                        .addComponent(LblPatternHeader)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(MainPanelLayout.createSequentialGroup()
                        .addComponent(BodyPanelScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 911, Short.MAX_VALUE)
                        .addGap(25, 25, 25))))
        );
        MainPanelLayout.setVerticalGroup(
            MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MainPanelLayout.createSequentialGroup()
                .addGroup(MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(MainPanelLayout.createSequentialGroup()
                        .addGap(46, 46, 46)
                        .addComponent(LblPatternHeader))
                    .addGroup(MainPanelLayout.createSequentialGroup()
                        .addGap(23, 23, 23)
                        .addComponent(BtnBack, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(25, 25, 25)
                .addComponent(BodyPanelScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 484, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(MainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(MainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
//    if(homePage != null)
//        homePage.dispose();
//    new HomePage().setVisible(true);
    this.dispose();
}//GEN-LAST:event_formWindowClosing

private void BtnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBackActionPerformed
    this.dispose();
}//GEN-LAST:event_BtnBackActionPerformed

private void BtnBackMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnBackMouseEntered
    BtnBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/Back-2.png"))); // NOI18N
}//GEN-LAST:event_BtnBackMouseEntered

private void BtnBackMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnBackMouseExited
    BtnBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/Back-1.png")));
}//GEN-LAST:event_BtnBackMouseExited

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel BodyPanel;
    private javax.swing.JScrollPane BodyPanelScrollPane;
    private javax.swing.JButton BtnBack;
    private javax.swing.JLabel LblMessage1;
    private javax.swing.JLabel LblPatternHeader;
    private javax.swing.JPanel MainPanel;
    private javax.swing.JTabbedPane Pattern1;
    private javax.swing.JTabbedPane Pattern2;
    private javax.swing.JTabbedPane Pattern3;
    private javax.swing.JTabbedPane PattternTabPane;
    private javax.swing.JScrollPane SubFirstScrollPanePatern1;
    private javax.swing.JScrollPane SubFirstScrollPanePatern2;
    private javax.swing.JScrollPane SubFirstScrollPanePatern3;
    private javax.swing.JTable SubFirstTablePattern1;
    private javax.swing.JTable SubFirstTablePattern2;
    private javax.swing.JTable SubFirstTablePattern3;
    private javax.swing.JScrollPane SubSecondScrollPanePattern1;
    private javax.swing.JScrollPane SubSecondScrollPanePattern2;
    private javax.swing.JScrollPane SubSecondScrollPanePattern3;
    private javax.swing.JTable SubSecondTablePattern1;
    private javax.swing.JTable SubSecondTablePattern2;
    private javax.swing.JTable SubSecondTablePattern3;
    private javax.swing.JScrollPane SubThirdScrollPanePattern1;
    private javax.swing.JScrollPane SubThirdScrollPanePattern2;
    private javax.swing.JScrollPane SubThirdScrollPanePattern3;
    private javax.swing.JTable SubThirdTablePattern1;
    private javax.swing.JTable SubThirdTablePattern2;
    private javax.swing.JTable SubThirdTablePattern3;
    // End of variables declaration//GEN-END:variables
}