/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattern.operations;

import com.bean.SubMasterChapterBean;
import com.bean.SubjectBean;
import com.db.DbConnection;
import com.db.operations.SubMasterChapterOperation;
import com.pages.HomePage;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import javax.swing.JOptionPane;

/**
 *
 * @author Aniket
 */
public class WeightageManager {
    private int totalWt;
    private Connection conn;
    HomePage homepage;
    private SubMasterChapterBean subMasterChapterBean;
    private ArrayList<SubMasterChapterBean> updatedChapterList;

    public void setAllWieghtage(int index,ArrayList<SubjectBean> subjectList) {
        ArrayList<WeightageSortBean> updateList = null;
        if(subjectList != null) {
            conn = new DbConnection().getConnection();
            for(SubjectBean subjectBean : subjectList) {
                ArrayList<WeightageSortBean> returnList = setSubjectWieghtage(index,subjectBean.getSubjectId());
                 updatedChapterList=new SubMasterChapterOperation().getSubMasterChapterList1(index,subjectBean.getSubjectId());
                if(returnList.size() != 0) {
                    if(updateList == null)
                        updateList = new ArrayList<WeightageSortBean>();
                    updateList.addAll(returnList);
                }
            }
        }
        
        if(updatedChapterList != null) {
            setWeightageList(updatedChapterList);
        }
        
    }
    
    private ArrayList<WeightageSortBean> setSubjectWieghtage(int index,int subjectId) {
        updatedChapterList=new SubMasterChapterOperation().getSubMasterChapterList1(index);
        ArrayList<WeightageSortBean> chapterInfoList = getWeightList(subjectId,updatedChapterList);
        subMasterChapterBean=updatedChapterList.get(4);
        Collections.sort(chapterInfoList);
        totalWt = 0;
        setWieghtage(chapterInfoList,subMasterChapterBean);
        while(true) {
            if(totalWt == 100)
                break;
            else
                setWieghtage(chapterInfoList,subMasterChapterBean);
        }
        return chapterInfoList;
    }
    
    private void setWieghtage(ArrayList<WeightageSortBean> chapterInfoList,SubMasterChapterBean subMasterChapterBean) {
        for(int i=0;i<chapterInfoList.size();i++) 
        {
            totalWt++;
            chapterInfoList.get(i).setWeightage((subMasterChapterBean.getWeightage())+1);
                      
            if(totalWt == 100)
                break;
        }
    }
    
    private ArrayList<WeightageSortBean> getWeightList(int subjectId,ArrayList<SubMasterChapterBean> updatedChapterList) {
        ArrayList<WeightageSortBean> chapterInfoList = new ArrayList<WeightageSortBean> ();
        try {
            String query = "SELECT CHAPTER_ID,COUNT(*) AS QUES_COUNT FROM QUESTION_INFO WHERE SUBJECT_ID = ? GROUP BY CHAPTER_ID ";
            PreparedStatement ps = conn.prepareStatement(query);
            ps.setInt(1, subjectId);
            ResultSet rs = ps.executeQuery();
            WeightageSortBean weightageSortBean = null;
            while(rs.next()) {
                weightageSortBean = new WeightageSortBean();
                weightageSortBean.setChapterId(rs.getInt(1));
                weightageSortBean.setQuesCount(rs.getInt(2));
                for(SubMasterChapterBean subMasterChapterBean:updatedChapterList)
                {
                weightageSortBean.setWeightage(subMasterChapterBean.getWeightage());
                }
                chapterInfoList.add(weightageSortBean);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return chapterInfoList;
    }
    
    private void setWeightageList(ArrayList<SubMasterChapterBean> updatedChapterList) {
        try {
            
            PreparedStatement ps = null;
            String query = "UPDATE CHAPTER_INFO SET WEIGHTAGE = ? WHERE CHAPTER_ID =?";
            for(SubMasterChapterBean subMasterChapterBean : updatedChapterList) {
                ps = conn.prepareStatement(query);
                ps.setInt(1, subMasterChapterBean.getWeightage());
                ps.setInt(2, subMasterChapterBean.getChapterId());
                ps.executeUpdate();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}

class WeightageSortBean implements Comparable<WeightageSortBean> {
    private int chapterId;
    private int quesCount;
    private int weightage;

    public int getChapterId() {
        return chapterId;
    }

    public void setChapterId(int chapterId) {
        this.chapterId = chapterId;
    }

    public int getQuesCount() {
        return quesCount;
    }

    public void setQuesCount(int quesCount) {
        this.quesCount = quesCount;
    }

    public int getWeightage() {
        return weightage;
    }

    public void setWeightage(int weightage) {
        this.weightage = weightage;
    }
    
    @Override
    public int compareTo(WeightageSortBean wht) {
        if(quesCount == wht.quesCount)
            return 0;
        else if(quesCount < wht.quesCount)
            return 1;
        else
            return -1;
    }

}
