/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Server;

import com.bean.ClientInfoBean;
import com.db.operations.ClientOperation;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 007
 */
public class ClientMonitorSocket {
    ServerSocket server=null;
    Socket client=null;
    ExecutorService pool=null;
    int clientcount=0;
    int port;
    
    public ClientMonitorSocket(int port) {
        this.port=port;
        pool =  Executors.newFixedThreadPool(5);
    }
    
    public void start() throws SQLException, IOException{
        
            server=new ServerSocket(8088);
            System.out.println("Server Booted");
            System.out.println("Any client can stop the server by sending=1");
            
            while(true){
                client = server.accept();
                clientcount++;
                ServerThread runnable = new ServerThread(client,clientcount,this);               
                pool.execute(runnable);
                
//               InetAddress inetAddress = clientSocket.getInetAddress();
//               String clientIp= inetAddress.getHostAddress();
//               String clientPcName=inetAddress.getHostName();
//               System.out.println("*****clientIp="+clientIp);
//               System.out.println("*****clientPcName"+clientPcName);
//               addClientIP(inetAddress.getHostAddress());
//               System.out.println(inetAddress.getHostAddress());
            }
            
        
    }
    private static class ServerThread implements Runnable
    {
        ClientMonitorSocket clientMoni=null;
        Socket client;
//        BufferedReader cin;
//        PrintStream cout;
//        Scanner sc=new Scanner(System.in);
          int id;
//        String s;
        
        ServerThread(Socket client,int count, ClientMonitorSocket clientMoni) throws IOException, SQLException
        {
            this.client=client;
            this.clientMoni=clientMoni;
            this.id=count;
            System.out.println("Connection" +id+ "established with client..." +client);
            
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");  
            Date date = new Date();            
            System.out.println(formatter.format(date)); 
            String DateTime1=formatter.format(date);
            
            int maxId = new ClientOperation().getNewId("CLIENT_INFO");
            ClientInfoBean clientBean =new ClientInfoBean();         
            String clientIP = client.getInetAddress().getHostAddress();            
            String clientName = client.getInetAddress().getHostName();
            
            System.out.println("clientIP=" +clientIP);               
            System.out.println("clientName=" +clientName);
            
            clientBean.setClientId(maxId);         
            clientBean.setClientName(clientName);
            clientBean.setClientIp(clientIP);
            clientBean.setDateTime(DateTime1);
                    
            Boolean Status=new ClientOperation().SetClientList(clientBean);
            if(Status)
            {
                System.out.println("Sucessfull insert");               
            }
            else
            {
                System.out.println("Not Sucessfull insert");
            }
            
                     
        }

        @Override
        public void run() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
        
    }     
    
    public void  stop(){
        if(server!=null){
            try {
                server.close();
            } catch (IOException ex) {
                Logger.getLogger(ClientMonitorSocket.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public static void main(String[] s){
//       ClientMonitorSocket clientMonitorSocket=new ClientMonitorSocket();
//       clientMonitorSocket.start();
    }

//    private void addClientIP(String hostAddress) {
//        throw new UnsupportedOperationException("Not yet implemented");
//    }
}
