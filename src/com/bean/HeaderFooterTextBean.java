/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean;

/**
 *
 * @author Aniket
 */
public class HeaderFooterTextBean {
    private boolean checkBoxSelected;
    private String textValue;

    public boolean isCheckBoxSelected() {
        return checkBoxSelected;
    }

    public void setCheckBoxSelected(boolean checkBoxSelected) {
        this.checkBoxSelected = checkBoxSelected;
    }

    public String getTextValue() {
        return textValue;
    }

    public void setTextValue(String textValue) {
        this.textValue = textValue;
    }
}
