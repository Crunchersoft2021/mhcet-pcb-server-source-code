/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean;

/**
 *
 * @author Aniket
 */
public class WordPageSetupBean {
    private boolean printYear;
    private int questionStartNo;
    private boolean questionBold;
    private boolean optionBold;
    private boolean solutionBold;

    public boolean isPrintYear() {
        return printYear;
    }

    public void setPrintYear(boolean printYear) {
        this.printYear = printYear;
    }

    public int getQuestionStartNo() {
        return questionStartNo;
    }

    public void setQuestionStartNo(int questionStartNo) {
        this.questionStartNo = questionStartNo;
    }

    public boolean isQuestionBold() {
        return questionBold;
    }

    public void setQuestionBold(boolean questionBold) {
        this.questionBold = questionBold;
    }

    public boolean isOptionBold() {
        return optionBold;
    }

    public void setOptionBold(boolean optionBold) {
        this.optionBold = optionBold;
    }

    public boolean isSolutionBold() {
        return solutionBold;
    }

    public void setSolutionBold(boolean solutionBold) {
        this.solutionBold = solutionBold;
    }
    
}
