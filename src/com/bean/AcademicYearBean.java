/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean;

/**
 *
 * @author Aniket
 */
public class AcademicYearBean {
    private int AcademicYearId;
    private String Name;

    public int getAcademicYearId() {
        return AcademicYearId;
    }

    public void setAcademicYearId(int AcademicYearId) {
        this.AcademicYearId = AcademicYearId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    @Override
    public String toString() {
        return "AcademicYearBean{" + "AcademicYearId=" + AcademicYearId + ", Name=" + Name + '}';
    }
}
