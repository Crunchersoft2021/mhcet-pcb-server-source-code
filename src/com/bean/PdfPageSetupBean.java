/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean;

/**
 *
 * @author Aniket
 */
public class PdfPageSetupBean {
    private int pageFormat;
    private boolean twoColumn;
    private boolean printYear;
    private String paperType;
    private boolean setLogo;
    private String logoPath;
    private boolean setImageActualsize;
    private int logoPosition;
    private int waterMarkInfo;
    private int waterMarkTextGrayScale;
    private int waterMarkScale;
    private int waterMarkAngle;
    private String waterMarkLogoPath;
    private boolean headerFooterOnFirstPage;
    private int questionStartNo;
    private boolean questionBold;
    private boolean optionBold;
    private boolean solutionBold;
    private boolean pageBorder;
    private int borderWidth;
    private HeaderFooterTextBean headerLeftBean;
    private HeaderFooterTextBean headerCenterBean;
    private HeaderFooterTextBean headerRightBean;
    private HeaderFooterTextBean footerLeftBean;
    private HeaderFooterTextBean footerCenterBean;
    private HeaderFooterTextBean footerRightBean;
    private boolean chapterList;
    private boolean optimizeLineSpace;
    private int fontSize;

    public int getFontSize() {
        return fontSize;
    }

    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
    }

    public boolean isOptimizeLineSpace() {
        return optimizeLineSpace;
    }

    public void setOptimizeLineSpace(boolean optimizeLineSpace) {
        this.optimizeLineSpace = optimizeLineSpace;
    }
    

    public int getPageFormat() {
        return pageFormat;
    }

    public void setPageFormat(int pageFormat) {
        this.pageFormat = pageFormat;
    }

    public boolean isTwoColumn() {
        return twoColumn;
    }

    public void setTwoColumn(boolean twoColumn) {
        this.twoColumn = twoColumn;
    }

    public boolean isPrintYear() {
        return printYear;
    }

    public void setPrintYear(boolean printYear) {
        this.printYear = printYear;
    }

    public String getPaperType() {
        return paperType;
    }

    public void setPaperType(String paperType) {
        this.paperType = paperType;
    }

    public boolean isSetLogo() {
        return setLogo;
    }

    public void setSetLogo(boolean setLogo) {
        this.setLogo = setLogo;
    }

    public String getLogoPath() {
        return logoPath;
    }

    public void setLogoPath(String logoPath) {
        this.logoPath = logoPath;
    }

    public boolean isSetImageActualsize() {
        return setImageActualsize;
    }

    public void setSetImageActualsize(boolean setImageActualsize) {
        this.setImageActualsize = setImageActualsize;
    }

    public int getLogoPosition() {
        return logoPosition;
    }

    public void setLogoPosition(int logoPosition) {
        this.logoPosition = logoPosition;
    }

    public int getWaterMarkInfo() {
        return waterMarkInfo;
    }

    public void setWaterMarkInfo(int waterMarkInfo) {
        this.waterMarkInfo = waterMarkInfo;
    }

    public int getWaterMarkTextGrayScale() {
        return waterMarkTextGrayScale;
    }

    public void setWaterMarkTextGrayScale(int waterMarkTextGrayScale) {
        this.waterMarkTextGrayScale = waterMarkTextGrayScale;
    }

    public int getWaterMarkScale() {
        return waterMarkScale;
    }

    public void setWaterMarkScale(int waterMarkScale) {
        this.waterMarkScale = waterMarkScale;
    }

    public int getWaterMarkAngle() {
        return waterMarkAngle;
    }

    public void setWaterMarkAngle(int waterMarkAngle) {
        this.waterMarkAngle = waterMarkAngle;
    }

    public String getWaterMarkLogoPath() {
        return waterMarkLogoPath;
    }

    public void setWaterMarkLogoPath(String waterMarkLogoPath) {
        this.waterMarkLogoPath = waterMarkLogoPath;
    }
  
    public boolean isHeaderFooterOnFirstPage() {
        return headerFooterOnFirstPage;
    }

    public void setHeaderFooterOnFirstPage(boolean headerFooterOnFirstPage) {
        this.headerFooterOnFirstPage = headerFooterOnFirstPage;
    }

    public int getQuestionStartNo() {
        return questionStartNo;
    }

    public void setQuestionStartNo(int questionStartNo) {
        this.questionStartNo = questionStartNo;
    }

    public boolean isQuestionBold() {
        return questionBold;
    }

    public void setQuestionBold(boolean questionBold) {
        this.questionBold = questionBold;
    }

    public boolean isOptionBold() {
        return optionBold;
    }

    public void setOptionBold(boolean optionBold) {
        this.optionBold = optionBold;
    }

    public boolean isSolutionBold() {
        return solutionBold;
    }

    public void setSolutionBold(boolean solutionBold) {
        this.solutionBold = solutionBold;
    }
//
//    public boolean isCenterLine() {
//        return centerLine;
//    }
//
//    public void setCenterLine(boolean centerLine) {
//        this.centerLine = centerLine;
//    }

    public boolean isPageBorder() {
        return pageBorder;
    }

    public void setPageBorder(boolean pageBorder) {
        this.pageBorder = pageBorder;
    }

    public int getBorderWidth() {
        return borderWidth;
    }

    public void setBorderWidth(int borderWidth) {
        this.borderWidth = borderWidth;
    }

    public HeaderFooterTextBean getHeaderLeftBean() {
        return headerLeftBean;
    }

    public void setHeaderLeftBean(HeaderFooterTextBean headerLeftBean) {
        this.headerLeftBean = headerLeftBean;
    }

    public HeaderFooterTextBean getHeaderCenterBean() {
        return headerCenterBean;
    }

    public void setHeaderCenterBean(HeaderFooterTextBean headerCenterBean) {
        this.headerCenterBean = headerCenterBean;
    }

    public HeaderFooterTextBean getHeaderRightBean() {
        return headerRightBean;
    }

    public void setHeaderRightBean(HeaderFooterTextBean headerRightBean) {
        this.headerRightBean = headerRightBean;
    }

    public HeaderFooterTextBean getFooterLeftBean() {
        return footerLeftBean;
    }

    public void setFooterLeftBean(HeaderFooterTextBean footerLeftBean) {
        this.footerLeftBean = footerLeftBean;
    }

    public HeaderFooterTextBean getFooterCenterBean() {
        return footerCenterBean;
    }

    public void setFooterCenterBean(HeaderFooterTextBean footerCenterBean) {
        this.footerCenterBean = footerCenterBean;
    }

    public HeaderFooterTextBean getFooterRightBean() {
        return footerRightBean;
    }

    public void setFooterRightBean(HeaderFooterTextBean footerRightBean) {
        this.footerRightBean = footerRightBean;
    }

    public boolean isChapterList() {
        return chapterList;
    }

    public void setChapterList(boolean chapterList) {
        this.chapterList = chapterList;
    }
}
