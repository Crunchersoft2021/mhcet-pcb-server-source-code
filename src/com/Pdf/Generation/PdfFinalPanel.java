/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Pdf.Generation;

import com.Model.ExportProcess;
import com.Model.FileChooser;
import com.Model.PrintFormatFirstPdf;
import com.Model.PrintFormatSecondPdf;
import com.Model.TestUtility;
import com.bean.ChapterBean;
import com.bean.PdfFirstFormatBean;
import com.bean.PdfPageSetupBean;
import com.bean.PdfSecondFormatBean;
import com.bean.PrintedTestBean;
import com.bean.QuestionBean;
import com.bean.SubjectBean;
import com.bean.ViewTestBean;
import com.db.operations.PrintedTestOperation;
import com.db.operations.QuestionOperation;
import com.db.operations.QuestionPaperOperation;
import com.db.operations.RegistrationOperation;
import com.pages.HomePage;
import com.pages.MultipleChapterQuestionsSelection;
import com.pages.MultipleYearQuestionsSelection;
import com.pages.SingleChapterQuestionsSelection;
import com.Model.TitleInfo;
import java.util.ArrayList;
import java.util.Collections;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Aniket
 */
public class PdfFinalPanel extends javax.swing.JFrame {

    private PdfPageSetupBean pdfPageSetupBean;
    private PdfFirstFormatBean pdfFirstFormatBean;
    private PdfSecondFormatBean pdfSecondFormatBean;
    private PdfFormatFirst pdfFormatFirst;
    private PdfFormatSecond pdfFormatSecond;
    private String printingPaperType;
    private ArrayList<SubjectBean> selectedSubjectList;
    private ArrayList<QuestionBean> selectedQuestionList;
    private ArrayList<QuestionBean> questionList;
    private ArrayList<ChapterBean> chapterList;
    private int totalMarks;
    private String instituteName;
    private PdfPageSetup pdfPageSetup;
    private Object quesPageObject;

    public PdfFinalPanel() {
        initComponents();
        setLocationRelativeTo(null);
        String titl = new TitleInfo().getTitle();
        setTitle(titl);
        String lgo = new TitleInfo().getLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        instituteName = new RegistrationOperation().getLastRegistrationInfoBean().getInstituteName().trim();
    }
    
    // Format First
    public PdfFinalPanel(PdfPageSetupBean pdfPageSetupBean, PdfFirstFormatBean pdfFirstFormatBean,ArrayList<QuestionBean> selectedQuestionList,ArrayList<SubjectBean> selectedSubjectList,ArrayList<ChapterBean> chapterList,String printingPaperType,int totalMarks,ArrayList<QuestionBean> questionList,PdfFormatFirst pdfFormatFirst,Object quesPageObject,PdfPageSetup pdfPageSetup) {
        initComponents();
        setLocationRelativeTo(null);
        String titl = new TitleInfo().getTitle();
        setTitle(titl);
        String lgo = new TitleInfo().getLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        this.pdfPageSetupBean = pdfPageSetupBean;
        this.pdfFirstFormatBean = pdfFirstFormatBean;
        pdfSecondFormatBean = null;
        this.pdfFormatFirst = pdfFormatFirst;
        pdfFormatSecond = null;
        this.printingPaperType = printingPaperType;
        this.selectedQuestionList = selectedQuestionList;
        this.selectedSubjectList = selectedSubjectList;
        this.totalMarks = totalMarks;
        this.questionList = questionList;
        this.pdfPageSetup = pdfPageSetup;
        instituteName = new RegistrationOperation().getLastRegistrationInfoBean().getInstituteName().trim();
        this.quesPageObject = quesPageObject;
        this.chapterList = chapterList;
    }
    
    //Format Second 
    public PdfFinalPanel(PdfPageSetupBean pdfPageSetupBean, PdfSecondFormatBean pdfSecondFormatBean,ArrayList<QuestionBean> selectedQuestionList,ArrayList<SubjectBean> selectedSubjectList,ArrayList<ChapterBean> chapterList,String printingPaperType,int totalMarks,ArrayList<QuestionBean> questionList,PdfFormatSecond pdfFormatSecond,Object quesPageObject,PdfPageSetup pdfPageSetup) {
        initComponents();
        setLocationRelativeTo(null);
        String titl = new TitleInfo().getTitle();
        setTitle(titl);
        String lgo = new TitleInfo().getLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        this.pdfPageSetupBean = pdfPageSetupBean;
        pdfFirstFormatBean = null;
        this.pdfSecondFormatBean = pdfSecondFormatBean;
        pdfFormatFirst = null;
        this.pdfFormatSecond = pdfFormatSecond;
        this.printingPaperType = printingPaperType;
        this.selectedQuestionList = selectedQuestionList;
        this.selectedSubjectList = selectedSubjectList;
        this.totalMarks = totalMarks;
        this.questionList = questionList;
        this.pdfPageSetup = pdfPageSetup;
        instituteName = new RegistrationOperation().getLastRegistrationInfoBean().getInstituteName().trim();
        this.quesPageObject = quesPageObject;
        this.chapterList = chapterList;
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        MainPanel = new javax.swing.JPanel();
        BodyPanel = new javax.swing.JPanel();
        ChkAnsSheet = new javax.swing.JCheckBox();
        ChkOMR = new javax.swing.JCheckBox();
        ChkSolutionSheet = new javax.swing.JCheckBox();
        TwoColumnSolPanel = new javax.swing.JPanel();
        ChkSolutionColumnType = new javax.swing.JCheckBox();
        ChkSaveTest = new javax.swing.JCheckBox();
        ChkTeachersCopy = new javax.swing.JCheckBox();
        FooterPanel = new javax.swing.JPanel();
        BtnBack = new javax.swing.JButton();
        BtnCreate = new javax.swing.JButton();

        jPanel1.setName("jPanel1"); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        jPanel3.setName("jPanel3"); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setAlwaysOnTop(true);
        setBackground(new java.awt.Color(255, 255, 255));
        setFocusCycleRoot(false);
        setForeground(new java.awt.Color(255, 255, 255));
        setUndecorated(true);
        setResizable(false);
        setType(java.awt.Window.Type.UTILITY);

        MainPanel.setBackground(new java.awt.Color(255, 255, 255));
        MainPanel.setName("MainPanel"); // NOI18N

        BodyPanel.setBackground(new java.awt.Color(11, 45, 55));
        BodyPanel.setName("BodyPanel"); // NOI18N

        ChkAnsSheet.setBackground(new java.awt.Color(11, 45, 55));
        ChkAnsSheet.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        ChkAnsSheet.setForeground(new java.awt.Color(255, 255, 255));
        ChkAnsSheet.setSelected(true);
        ChkAnsSheet.setText("Create Correct Answer Sheet");
        ChkAnsSheet.setName("ChkAnsSheet"); // NOI18N
        ChkAnsSheet.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ChkAnsSheetItemStateChanged(evt);
            }
        });

        ChkOMR.setBackground(new java.awt.Color(11, 45, 55));
        ChkOMR.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        ChkOMR.setForeground(new java.awt.Color(255, 255, 255));
        ChkOMR.setText("Create OMR Sheet");
        ChkOMR.setName("ChkOMR"); // NOI18N

        ChkSolutionSheet.setBackground(new java.awt.Color(11, 45, 55));
        ChkSolutionSheet.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        ChkSolutionSheet.setForeground(new java.awt.Color(255, 255, 255));
        ChkSolutionSheet.setSelected(true);
        ChkSolutionSheet.setText("Create Solution Sheet");
        ChkSolutionSheet.setName("ChkSolutionSheet"); // NOI18N
        ChkSolutionSheet.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ChkSolutionSheetItemStateChanged(evt);
            }
        });

        TwoColumnSolPanel.setBackground(new java.awt.Color(11, 45, 55));
        TwoColumnSolPanel.setName("TwoColumnSolPanel"); // NOI18N

        ChkSolutionColumnType.setBackground(new java.awt.Color(11, 45, 55));
        ChkSolutionColumnType.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        ChkSolutionColumnType.setForeground(new java.awt.Color(255, 255, 255));
        ChkSolutionColumnType.setSelected(true);
        ChkSolutionColumnType.setText("Two Column");
        ChkSolutionColumnType.setName("ChkSolutionColumnType"); // NOI18N

        javax.swing.GroupLayout TwoColumnSolPanelLayout = new javax.swing.GroupLayout(TwoColumnSolPanel);
        TwoColumnSolPanel.setLayout(TwoColumnSolPanelLayout);
        TwoColumnSolPanelLayout.setHorizontalGroup(
            TwoColumnSolPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, TwoColumnSolPanelLayout.createSequentialGroup()
                .addGap(0, 27, Short.MAX_VALUE)
                .addComponent(ChkSolutionColumnType)
                .addGap(52, 52, 52))
        );
        TwoColumnSolPanelLayout.setVerticalGroup(
            TwoColumnSolPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(TwoColumnSolPanelLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(ChkSolutionColumnType)
                .addGap(0, 0, 0))
        );

        ChkSaveTest.setBackground(new java.awt.Color(11, 45, 55));
        ChkSaveTest.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        ChkSaveTest.setForeground(new java.awt.Color(255, 255, 255));
        ChkSaveTest.setText("Save Test");
        ChkSaveTest.setName("ChkSaveTest"); // NOI18N

        ChkTeachersCopy.setBackground(new java.awt.Color(11, 45, 55));
        ChkTeachersCopy.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        ChkTeachersCopy.setForeground(new java.awt.Color(255, 255, 255));
        ChkTeachersCopy.setText("Create Teacher Copy");
        ChkTeachersCopy.setName("ChkTeachersCopy"); // NOI18N

        javax.swing.GroupLayout BodyPanelLayout = new javax.swing.GroupLayout(BodyPanel);
        BodyPanel.setLayout(BodyPanelLayout);
        BodyPanelLayout.setHorizontalGroup(
            BodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(BodyPanelLayout.createSequentialGroup()
                .addGroup(BodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(BodyPanelLayout.createSequentialGroup()
                        .addGap(51, 51, 51)
                        .addGroup(BodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(ChkAnsSheet)
                            .addComponent(TwoColumnSolPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(ChkSolutionSheet)
                            .addComponent(ChkSaveTest)
                            .addComponent(ChkTeachersCopy)))
                    .addGroup(BodyPanelLayout.createSequentialGroup()
                        .addGap(50, 50, 50)
                        .addComponent(ChkOMR)))
                .addGap(9, 9, 9))
        );
        BodyPanelLayout.setVerticalGroup(
            BodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(BodyPanelLayout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(ChkOMR)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(ChkAnsSheet)
                .addGap(0, 0, 0)
                .addComponent(ChkSolutionSheet)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(TwoColumnSolPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(ChkSaveTest)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(ChkTeachersCopy)
                .addContainerGap(8, Short.MAX_VALUE))
        );

        FooterPanel.setBackground(new java.awt.Color(255, 255, 255));
        FooterPanel.setName("FooterPanel"); // NOI18N

        BtnBack.setBackground(new java.awt.Color(208, 87, 96));
        BtnBack.setFont(new java.awt.Font("Calibri", 0, 18)); // NOI18N
        BtnBack.setForeground(new java.awt.Color(255, 255, 255));
        BtnBack.setText("Back");
        BtnBack.setBorderPainted(false);
        BtnBack.setName("BtnBack"); // NOI18N
        BtnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBackActionPerformed(evt);
            }
        });

        BtnCreate.setBackground(new java.awt.Color(208, 87, 96));
        BtnCreate.setFont(new java.awt.Font("Calibri", 0, 18)); // NOI18N
        BtnCreate.setForeground(new java.awt.Color(255, 255, 255));
        BtnCreate.setText("Create");
        BtnCreate.setBorderPainted(false);
        BtnCreate.setName("BtnCreate"); // NOI18N
        BtnCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCreateActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout FooterPanelLayout = new javax.swing.GroupLayout(FooterPanel);
        FooterPanel.setLayout(FooterPanelLayout);
        FooterPanelLayout.setHorizontalGroup(
            FooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FooterPanelLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(BtnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(BtnCreate, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        FooterPanelLayout.setVerticalGroup(
            FooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, FooterPanelLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(FooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BtnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BtnCreate, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        javax.swing.GroupLayout MainPanelLayout = new javax.swing.GroupLayout(MainPanel);
        MainPanel.setLayout(MainPanelLayout);
        MainPanelLayout.setHorizontalGroup(
            MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(FooterPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(BodyPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        MainPanelLayout.setVerticalGroup(
            MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(BodyPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(FooterPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(MainPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(MainPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BtnCreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCreateActionPerformed
        // TODO add your handling code here:
        String fileLocation = new FileChooser().getPdfFileLocation(this);
         System.out.println("*****fileLocation:="+fileLocation);
        
        if(fileLocation != null) {
            if(ChkSaveTest.isSelected()) {
                String testName = JOptionPane.showInputDialog(rootPane, "Enter Test Name:","Saved Test", JOptionPane.QUESTION_MESSAGE);
                                                                                                                
                if(testName != null) {
                    if(testName.trim().isEmpty())
                        testName = "Test";
                    SaveTest(testName);
                }
            }
            if (ChkOMR.isSelected()) {
                new ExportProcess().createOmrSheet(selectedQuestionList.size(), pdfPageSetupBean.getQuestionStartNo(), instituteName, fileLocation, this, null);
            } 
            if(pdfPageSetupBean.getPageFormat() == 0) {
                new PrintFormatFirstPdf().setPrintPdf(pdfPageSetupBean, pdfFirstFormatBean, selectedSubjectList, chapterList,selectedQuestionList, printingPaperType,totalMarks, ChkAnsSheet.isSelected(), ChkSolutionSheet.isSelected(),ChkSolutionColumnType.isSelected(),ChkTeachersCopy.isSelected(),instituteName,this,fileLocation);
                pdfFormatFirst.dispose();
            } else {
                new PrintFormatSecondPdf().setPrintPdf(pdfPageSetupBean, pdfSecondFormatBean, selectedSubjectList,  chapterList,selectedQuestionList, printingPaperType, totalMarks, ChkAnsSheet.isSelected(), ChkSolutionSheet.isSelected(),ChkSolutionColumnType.isSelected(),ChkTeachersCopy.isSelected(), instituteName, this, fileLocation);
                pdfFormatSecond.dispose();
            }
            pdfPageSetup.dispose();
            if(SingleChapterQuestionsSelection.class.isInstance(quesPageObject)) {
                SingleChapterQuestionsSelection frm = (SingleChapterQuestionsSelection)quesPageObject;
                frm.setEnabled(true);
            } else if(MultipleChapterQuestionsSelection.class.isInstance(quesPageObject)) {
                MultipleChapterQuestionsSelection frm = (MultipleChapterQuestionsSelection)quesPageObject;
                frm.setEnabled(true);
            } else if(MultipleYearQuestionsSelection.class.isInstance(quesPageObject)) {
                MultipleYearQuestionsSelection frm = (MultipleYearQuestionsSelection)quesPageObject;
                frm.setEnabled(true);
            } else if(HomePage.class.isInstance(quesPageObject)) {
                HomePage frm = (HomePage)quesPageObject;
                frm.setEnabled(true);
            }
            setAttemptedValue();
//            quesFrame.setEnabled(true);
            this.dispose();
        }
    }//GEN-LAST:event_BtnCreateActionPerformed

    private void BtnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBackActionPerformed
        // TODO add your handling code here:
        if (pdfFormatFirst != null) {
            pdfFormatFirst.setVisible(true);
        } else if (pdfFormatSecond != null) {
            pdfFormatSecond.setVisible(true);
        }
        this.dispose();
    }//GEN-LAST:event_BtnBackActionPerformed

    private void ChkAnsSheetItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ChkAnsSheetItemStateChanged
        // TODO add your handling code here:

    }//GEN-LAST:event_ChkAnsSheetItemStateChanged

    private void ChkSolutionSheetItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ChkSolutionSheetItemStateChanged
        // TODO add your handling code here
        if (ChkSolutionSheet.isSelected()) {
            TwoColumnSolPanel.setVisible(true);
        } else {
            TwoColumnSolPanel.setVisible(false);
        }
    }//GEN-LAST:event_ChkSolutionSheetItemStateChanged

    private void SaveTest(String testName) {
        boolean testSaveStatus = false;
        ArrayList<Integer> saveTestSelectedQuesIdList = null;
        PrintedTestBean printedTestBean = null;
        SingleChapterQuestionsSelection singleQuesPanel = null;
        MultipleChapterQuestionsSelection multipleQuesPanel = null;
        MultipleYearQuestionsSelection multiplePaperQuesPanel = null;
        HomePage homePage = null;
        
        if(SingleChapterQuestionsSelection.class.isInstance(quesPageObject)) {
            singleQuesPanel = (SingleChapterQuestionsSelection)quesPageObject;
            testSaveStatus = singleQuesPanel.isTestSaveStatus();
            saveTestSelectedQuesIdList = singleQuesPanel.getSaveTestSelectedQuesIdList();
            printedTestBean = singleQuesPanel.getPrintedTestBean();
        } else if(MultipleChapterQuestionsSelection.class.isInstance(quesPageObject)) {
            multipleQuesPanel = (MultipleChapterQuestionsSelection)quesPageObject;
            testSaveStatus = multipleQuesPanel.isTestSaveStatus();
            saveTestSelectedQuesIdList = multipleQuesPanel.getSaveTestSelectedQuesIdList();
            printedTestBean = multipleQuesPanel.getPrintedTestBean();
        } else if(MultipleYearQuestionsSelection.class.isInstance(quesPageObject)) {
            multiplePaperQuesPanel = (MultipleYearQuestionsSelection)quesPageObject;
            testSaveStatus = multiplePaperQuesPanel.isTestSaveStatus();
            saveTestSelectedQuesIdList = multiplePaperQuesPanel.getSaveTestSelectedQuesIdList();
            printedTestBean = multiplePaperQuesPanel.getPrintedTestBean();
        } else if(HomePage.class.isInstance(quesPageObject)) {
            homePage = (HomePage)quesPageObject;
            testSaveStatus = false;
            saveTestSelectedQuesIdList = null;
            printedTestBean = null;
        }
        
        if(testSaveStatus && saveTestSelectedQuesIdList != null) {
            ArrayList<Integer> selectedQuesIdList = new ArrayList<Integer>();
            for(QuestionBean bean : selectedQuestionList) {
                if(!saveTestSelectedQuesIdList.contains(bean.getQuestionId())) {
                    testSaveStatus = false;
                    break;
                }
                selectedQuesIdList.add(bean.getQuestionId());
            }
            
            if(saveTestSelectedQuesIdList.size() != selectedQuesIdList.size())
                testSaveStatus = false;
            
            if(testSaveStatus) {
                for(int quesId : saveTestSelectedQuesIdList) {
                    if(!selectedQuesIdList.contains(quesId)) {
                        testSaveStatus = false;
                        break;
                    }
                }
            }
            
            if(!testSaveStatus) {
                new PrintedTestOperation().deletePrintedTest(printedTestBean);
            }
        }
        
        if(!testSaveStatus) {
            ViewTestBean viewTestBean = new TestUtility().saveTests(selectedQuestionList,questionList,selectedSubjectList, printingPaperType, testName);
            testSaveStatus = true;
            printedTestBean = viewTestBean.getPrintedTestBean();
            
            if(saveTestSelectedQuesIdList == null)
                saveTestSelectedQuesIdList = new ArrayList<Integer>();
            else
                saveTestSelectedQuesIdList.clear();
            for(QuestionBean bean : selectedQuestionList) 
                saveTestSelectedQuesIdList.add(bean.getQuestionId());
            
            if(singleQuesPanel != null) {
                singleQuesPanel.setTestSaveStatus(testSaveStatus);
                singleQuesPanel.setSaveTestSelectedQuesIdList(saveTestSelectedQuesIdList);
                singleQuesPanel.setPrintedTestBean(printedTestBean);
            } else if(multipleQuesPanel != null) {
                multipleQuesPanel.setTestSaveStatus(testSaveStatus);
                multipleQuesPanel.setSaveTestSelectedQuesIdList(saveTestSelectedQuesIdList);
                multipleQuesPanel.setPrintedTestBean(printedTestBean);
            } else if(multiplePaperQuesPanel != null) {
                multiplePaperQuesPanel.setTestSaveStatus(testSaveStatus);
                multiplePaperQuesPanel.setSaveTestSelectedQuesIdList(saveTestSelectedQuesIdList);
                multiplePaperQuesPanel.setPrintedTestBean(printedTestBean);
            } else if(homePage != null) {
                ArrayList<PrintedTestBean> testList = homePage.getTestList();
                if(testList == null)
                    testList = new ArrayList<PrintedTestBean>();
                testList.add(printedTestBean);
                Collections.sort(testList);
                homePage.setTestList(testList);
            }
        } else {
            JOptionPane.showMessageDialog(rootPane, "Your Test Not Saved.\nYou Have Already Same Saved Test");
        }
    }
    
    private void setAttemptedValue() {
        ArrayList<QuestionBean> updateQuesList = null;
        for(QuestionBean questionBean : selectedQuestionList) {
            if(questionBean.getAttempt() == 0) {
                if(updateQuesList == null)
                    updateQuesList = new ArrayList<QuestionBean>();
                questionBean.setAttempt(1);
                updateQuesList.add(questionBean);
            }
        }

        if(updateQuesList != null && printingPaperType.equalsIgnoreCase("YearWise"))
            new QuestionPaperOperation().updateUsedValue(updateQuesList);
        else if(updateQuesList != null)
            new QuestionOperation().updateUsedValue(updateQuesList);
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(PdfFinalPanel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(PdfFinalPanel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(PdfFinalPanel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(PdfFinalPanel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
        //</editor-fold>
        /* Create and display the form */
        new PdfFinalPanel().setVisible(true);
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel BodyPanel;
    private javax.swing.JButton BtnBack;
    private javax.swing.JButton BtnCreate;
    private javax.swing.JCheckBox ChkAnsSheet;
    private javax.swing.JCheckBox ChkOMR;
    private javax.swing.JCheckBox ChkSaveTest;
    private javax.swing.JCheckBox ChkSolutionColumnType;
    private javax.swing.JCheckBox ChkSolutionSheet;
    private javax.swing.JCheckBox ChkTeachersCopy;
    private javax.swing.JPanel FooterPanel;
    private javax.swing.JPanel MainPanel;
    private javax.swing.JPanel TwoColumnSolPanel;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    // End of variables declaration//GEN-END:variables
}
