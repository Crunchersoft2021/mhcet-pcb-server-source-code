/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Model;

import com.bean.RegistrationBean;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author Aniket
 */
public class ValidatePinKey {
    public boolean checkPinKey(RegistrationBean infoBean) {
        boolean returnValue = false;
        
        String machinePinKey1 = new PinKeyManager().getPinKey(infoBean, getSystemDate(0));
        String machinePinKey2 = new PinKeyManager().getPinKey(infoBean, getSystemDate(1));
        String userPinKey = infoBean.getPinKey().trim().toUpperCase();
        userPinKey = new SwappingKeyRetrive().swapPinKey(userPinKey, infoBean.getCustMobile());
        
//        System.out.println(userPinKey.substring(0, 6).trim().equalsIgnoreCase(machinePinKey1.substring(0, 6)) +" "+
//            userPinKey.substring(14, 20).trim().equalsIgnoreCase(machinePinKey1.substring(7, 13)) +" "+
//            userPinKey.substring(28, 34).trim().equalsIgnoreCase(machinePinKey1.substring(14, 20)) +"\nNextDay"+
//            userPinKey.substring(0, 6).trim().equalsIgnoreCase(machinePinKey2.substring(0, 6)) +" "+
//            userPinKey.substring(14, 20).trim().equalsIgnoreCase(machinePinKey2.substring(7, 13)) +" "+
//            userPinKey.substring(28, 34).trim().equalsIgnoreCase(machinePinKey2.substring(14, 20)));
        
        
        if(userPinKey.substring(0, 6).trim().equalsIgnoreCase(machinePinKey1.substring(0, 6)) &&
            userPinKey.substring(14, 20).trim().equalsIgnoreCase(machinePinKey1.substring(7, 13)) &&
            userPinKey.substring(28, 34).trim().equalsIgnoreCase(machinePinKey1.substring(14, 20))) {
            returnValue = true;
        } else if(userPinKey.substring(0, 6).trim().equalsIgnoreCase(machinePinKey2.substring(0, 6)) &&
            userPinKey.substring(14, 20).trim().equalsIgnoreCase(machinePinKey2.substring(7, 13)) &&
            userPinKey.substring(28, 34).trim().equalsIgnoreCase(machinePinKey2.substring(14, 20))) {
            returnValue = true;
        } else {
            returnValue = false;
        }
        
        if(returnValue) {
            returnValue = new ValidateDate().getValidateDate(infoBean);
        }
        
        return returnValue;
    }
    
    private String getSystemDate(int count) {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, count);
        String returnDate = dateFormat.format(cal.getTimeInMillis());
        return returnDate;
    }
}
