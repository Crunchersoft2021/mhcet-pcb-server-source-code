/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.operations;

import com.bean.QuestionBean;
import com.db.DbConnection;
import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Aniket
 */
public class TotalMarkOperation {
    private Connection conn = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    private Statement stmt = null;
    
    public void clearTotalMark() {
        conn = new DbConnection().getConnection();
        try {
             stmt = conn.createStatement();
            stmt.execute("delete from TotalMark");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TotalMarkOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
   
    public void insertIntoTotalMark(int rollNo, double Ftotal, String ddd,int i) {
        conn = new DbConnection().getConnection();
        try {
            stmt = conn.createStatement();
            stmt.execute("insert into TotalMark values(" + rollNo + "," + Ftotal + ",'" + ddd + "',"+i+")");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TotalMarkOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public ArrayList<Double> getTotalMarks() {
        ArrayList<Double> ret = new ArrayList<Double>();
        conn = new DbConnection().getConnection();
        
        try {
             stmt = conn.createStatement();
            rs = stmt.executeQuery("Select Total  from TotalMark order by Total DESC");//rollNo
            while (rs.next()) {
                ret.add(rs.getDouble(1));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TotalMarkOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return ret;
    }

    public ArrayList<Double> getTotalMarksforPrint() {
        ArrayList<Double> ret = new ArrayList<Double>();
        conn = new DbConnection().getConnection();
        
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("Select Total from TotalMark order by rollno");//rollNo
            while (rs.next()) {
                ret.add(rs.getDouble(1));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TotalMarkOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return ret;
    }

    public ArrayList<Integer> getTotalRolls() {
        ArrayList<Integer> ret = new ArrayList<Integer>();
        conn = new DbConnection().getConnection();
        
        try {
            stmt= conn.createStatement();
            rs = stmt.executeQuery("Select rollNo from TotalMark order by Total DESC");//rollNo
            while (rs.next()) {
                ret.add(rs.getInt(1));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TotalMarkOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return ret;
    }

    public ArrayList<Integer> getTotalRollsForPrint() {
        ArrayList<Integer> ret = new ArrayList<Integer>();
        conn = new DbConnection().getConnection();
        
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("Select rollNo from TotalMark order by rollNo");//rollNo
            while (rs.next()) {
                ret.add(rs.getInt(1));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TotalMarkOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return ret;
    }
    
   public ArrayList<String> getTotalRollsDate() {
        ArrayList<String> ret = new ArrayList<String>();
        conn = new DbConnection().getConnection();
        
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("Select DateOfExam from TotalMark order by Total DESC");//rollNo
            while (rs.next()) {
                ret.add(rs.getString(1));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TotalMarkOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return ret;
    } 
}
