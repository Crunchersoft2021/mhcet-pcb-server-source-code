/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.operations;
import com.bean.NewAndOldBean;
import com.bean.QuestionBean;
import com.db.DbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Aniket
 */
public class NewAndOldQuestionOperation {
    private Connection conn;
    private ResultSet rs;
    private PreparedStatement ps;
    
    public ArrayList<QuestionBean> getNewQuestionsList(ArrayList<QuestionBean> selectedQuestionsList,
            boolean isPreviousPaperQuestions){
        ArrayList<QuestionBean> returnList = new ArrayList<QuestionBean>();
        try{
            conn = new DbConnection().getConnection();
            String query = "";
            if(isPreviousPaperQuestions)
                query = "SELECT * FROM NEWANDOLDQUESTIONFORPAPER WHERE QUE_ID = ?";
            else
                query = "SELECT * FROM NEWANDOLDQUESTION WHERE QUE_ID = ?";
            ps=conn.prepareStatement(query);
            for(QuestionBean questionsBean : selectedQuestionsList){
                int quesId =  questionsBean.getQuestionId();
                ps.setInt(1, quesId);
                rs=ps.executeQuery();
                while(rs.next()) {
                    returnList.add(questionsBean);
                }
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return returnList;
    }
    
    public ArrayList<Integer> getNewQuestionsLists(ArrayList<QuestionBean> selectedQuestionsList,
            boolean isPreviousPaperQuestions){
        ArrayList<Integer> returnList = new ArrayList<Integer>();
        try{
            conn = new DbConnection().getConnection();
            String query = "";
            if(isPreviousPaperQuestions)
                query = "SELECT * FROM NEWANDOLDQUESTIONFORPAPER WHERE QUE_ID = ?";
            else
                query = "SELECT * FROM NEWANDOLDQUESTION WHERE QUE_ID = ?";
            ps=conn.prepareStatement(query);
            for(QuestionBean questionsBean : selectedQuestionsList){
                int quesId =  questionsBean.getQuestionId();
                ps.setInt(1, quesId);
                rs=ps.executeQuery();
                while(rs.next()) {
                    returnList.add(quesId);
                }
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return returnList;
    }
    
    public boolean insertNewAndOldBean(NewAndOldBean newAndOldBean,boolean isPreviousPaperQuestions) {
        boolean returnValue = false;
        try {
            conn = new DbConnection().getConnection();
            String query = "";
            if(isPreviousPaperQuestions) {
                query = "INSERT INTO NEWANDOLDQUESTIONFORPAPER VALUES(?,?)";
            } else {
                query = "INSERT INTO NEWANDOLDQUESTION VALUES(?,?)";
            }
            ps = conn.prepareStatement(query);
            ps.setInt(1, newAndOldBean.getQuestionId());
            ps.setInt(2, newAndOldBean.getQuestionType());
            ps.executeUpdate();
            returnValue = true;
        } catch (SQLException ex) {
            returnValue = false;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return returnValue;
    }
    
    public NewAndOldBean getNewQuestionsBean(QuestionBean questionBean,
            boolean isPreviousPaperQuestions){
        NewAndOldBean returnBean = null;
        try{
            conn = new DbConnection().getConnection();
            String query = "";
            if(isPreviousPaperQuestions)
                query = "SELECT * FROM NEWANDOLDQUESTIONFORPAPER WHERE QUE_ID = ?";
            else
                query = "SELECT * FROM NEWANDOLDQUESTION WHERE QUE_ID = ?";
            ps=conn.prepareStatement(query);
            ps.setInt(1, questionBean.getQuestionId());
            rs=ps.executeQuery();
            
            while(rs.next()) {
                returnBean = new NewAndOldBean();
                returnBean.setQuestionId(rs.getInt(1));
                returnBean.setQuestionType(rs.getInt(2));
            }
        } catch(Exception ex) {
            returnBean = null;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return returnBean;
    }
    
    public boolean deleteNewAndOldBean(NewAndOldBean newAndOldBean,boolean isPreviousPaperQuestions) {
        boolean returnValue = false;
        try {
            conn = new DbConnection().getConnection();
            String query = "";
            if(isPreviousPaperQuestions) {
                query = "DELETE FROM NEWANDOLDQUESTIONFORPAPER WHERE QUE_ID = ?";
            } else {
                query = "DELETE FROM NEWANDOLDQUESTION WHERE QUE_ID = ?";
            }
            ps = conn.prepareStatement(query);
            ps.setInt(1, newAndOldBean.getQuestionId());
            ps.executeUpdate();
            returnValue = true;
        } catch (SQLException ex) {
            returnValue = false;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return returnValue;
    }
    
    private void sqlClose() {
        try {
            if(rs != null)
                rs.close();
            if(ps != null)
                ps.close();
            if(conn != null)
                conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
