/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.operations;

import com.bean.GroupBean;
import com.bean.MasterSubjectBean;
import com.db.DbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Aniket
 */
public class MasterGroupOperation {
    private Connection conn = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    
    
    
    public ArrayList<GroupBean> getGroupList() {
        ArrayList<MasterSubjectBean> subjectList = new MasterSubjectOperation().getSubjectList();
        ArrayList<GroupBean> returnList = null;
        conn = new DbConnection().getConnection();
        try {
            String query = "SELECT * FROM MASTER_GROUP_INFO";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            GroupBean groupBean = null;
            while(rs.next()) {
                groupBean = new GroupBean();
                groupBean.setGroupId(rs.getInt(1));
                groupBean.setGroupName(rs.getString(2));
                String subjectIds = rs.getString(3);
                String [] subjectIdArray = subjectIds.split(",");
                
                int[] subIds = new int[subjectIdArray.length];
                
                for(int i=0;i<subjectIdArray.length;i++) {
                    subIds[i] = Integer.parseInt(subjectIdArray[i]);
                }
                ArrayList<MasterSubjectBean> sortSubjectList = new ArrayList<MasterSubjectBean>();
                for(int subjectId : subIds) {
                    for(MasterSubjectBean subjectBean : subjectList) {
                        if(subjectId == subjectBean.getSubjectId()) {
                            sortSubjectList.add(subjectBean);
                        }
                    }
                }
                groupBean.setSubjectList(sortSubjectList);
                
                if(returnList == null)
                    returnList = new ArrayList<GroupBean>();
                
                returnList.add(groupBean);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            returnList = null;
        } finally {
            sqlClose();
        }    
        return returnList;
    }
    
    public boolean insertGroup(GroupBean groupBean) {
        boolean returnValue = false;
        conn = new DbConnection().getConnection();
        try {
            String query = "INSERT INTO MASTER_GROUP_INFO VALUES(?,?,?)";
            ps = conn.prepareStatement(query);
            ps.setInt(1, groupBean.getGroupId());
            ps.setString(2, groupBean.getGroupName());
            String subjectIds = "";
            for(MasterSubjectBean bean : groupBean.getSubjectList()) {
                subjectIds += bean.getSubjectId()+",";
            }
            ps.setString(3, subjectIds);
            ps.executeUpdate();
            returnValue = true;
        } catch (SQLException ex) {
            ex.printStackTrace();
            returnValue = false;
        } finally {
            sqlClose();
        }    
        return returnValue;
    }
    
    private void sqlClose() {
        try {
            if(rs != null)
                rs.close();
            if(ps != null)
                ps.close();
            if(conn != null)
                conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
