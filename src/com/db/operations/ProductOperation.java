/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.operations;

import com.bean.ProductBean;
import com.bean.ProductTypeBean;
import com.db.DbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Aniket
 */
public class ProductOperation {

    private Connection conn = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;

    public ProductBean getProductBean(int productId) {
        ProductBean productBean = null;
        ArrayList<ProductTypeBean> productTypeList = getProductTypeList();
        if (productTypeList != null) {
            try {
                conn = new DbConnection().getConnection();
                String query = "SELECT * FROM PRODUCT_INFO WHERE PRD_ID = ?";
                ps = conn.prepareStatement(query);
                ps.setInt(1, productId);
                rs = ps.executeQuery();
                while (rs.next()) {
                    productBean = new ProductBean();
                    productBean.setProductId(rs.getInt(1));
                    productBean.setProductName(rs.getString(2));
                    for (ProductTypeBean productTypeBean : productTypeList) {
                        if (productTypeBean.getProductTypeId() == rs.getInt(3)) {
                            productBean.setProductTypeBean(productTypeBean);
                            break;
                        }
                    }
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                sqlClose();
            }
        }
        return productBean;
    }

    public ArrayList<ProductBean> getProductList() {
        ArrayList<ProductBean> returnList = null;
        ProductBean productBean = null;
        ArrayList<ProductTypeBean> productTypeList = getProductTypeList();
        if (productTypeList != null) {
            try {
                conn = new DbConnection().getConnection();
                String query = "SELECT * FROM PRODUCT_INFO";
                ps = conn.prepareStatement(query);
                rs = ps.executeQuery();
                while (rs.next()) {
                    productBean = new ProductBean();
                    productBean.setProductId(rs.getInt(1));
                    productBean.setProductName(rs.getString(2));
                    for (ProductTypeBean productTypeBean : productTypeList) {
                        if (productTypeBean.getProductTypeId() == rs.getInt(3)) {
                            productBean.setProductTypeBean(productTypeBean);
                            break;
                        }
                    }
                    if (returnList == null) {
                        returnList = new ArrayList<ProductBean>();
                    }
                    returnList.add(productBean);
                }
            } catch (SQLException ex) {
                returnList = null;
                ex.printStackTrace();
            } finally {
                sqlClose();
            }
        }
        return returnList;
    }

    public ArrayList<ProductTypeBean> getProductTypeList() {
        ArrayList<ProductTypeBean> returnList = null;
        ProductTypeBean productTypeBean = null;
        try {
            conn = new DbConnection().getConnection();
            String query = "SELECT * FROM PRODUCT_TYPE_INFO";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                productTypeBean = new ProductTypeBean();
                productTypeBean.setProductTypeId(rs.getInt(1));
                productTypeBean.setProductType(rs.getString(2));
                if (returnList == null) {
                    returnList = new ArrayList<ProductTypeBean>();
                }
                returnList.add(productTypeBean);
            }
        } catch (SQLException ex) {
            returnList = null;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return returnList;
    }

    private void sqlClose() {
        try {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
